<?xml version="1.0" encoding="UTF-8"?>
<source>
@foreach ($jobs AS $job)
<job>
    <title><![CDATA[{{$job->title}}]]></title>
    <!--
    date:
    Optional. The date provided for each job should indicate when the job was first published.
    When not present the date and time of import will be used
    The date should be either ISO8601 Format or "%a, %e %b %Y %H:%M:%S %Z" strftime format,
    i.e. 2014-07-31T07:27:14 or Thu, 31 Jul 2014 07:27:14 PDT
    -->
    <date><![CDATA[{{$job->created_at}}]]></date>
    <!--
    referencenumber:
    A unique alphanumeric reference ID for the job entry
    -->
    <referencenumber><![CDATA[{{$job->id}}]]></referencenumber>
    <!--
    company:
    The company actually hiring for the position.
    Avoid terms like "confidential" or "unknown".
    Vague company names are generally blacklisted by search engines
    -->
    <company><![CDATA[{{$job->company->name}}]]></company>
    <!--
    city:
    The city where the position is located.
    <postalcode> will be preferred over this field.
    When provided, <state> must be provided as well
    -->
    <city><![CDATA[{{$job->city_name}}]]></city>
    <!--
    state:
    The state or province where the position is located
    <postalcode> will be preferred over this field.
    When provided, <city> must be provided as well
    -->
    <state><![CDATA[{{$job->province->code}}]]></state>
    <!--
    postalcode:
    Optional, but preferred over <city> and <state>.
    The Zip or Postal Code where the position is located.
    -->
    <postalcode><![CDATA[{{$job->postal}}]]></postalcode>
    <!--
    country:
    The two letter ISO country code where the position is located, i.e. US, CA
    -->
    <country><![CDATA[{{$job->country->code}}]]></country>
    <!--
    description:
    Your full job description goes here.
    HTML markup is allowed, but will probably be ignored by our search engine.
    We may use this field for indexing and snippet generation.
    -->
    <description><![CDATA[{{$job->description}}]]></description>
    <!--
    category:
    See the “Categories” section below for valid values
    -->
    <category><![CDATA[{!!$job->category->title!!}]]></category>
    <!--
    url, email:
    Application Data fields
    For ZipRecruiter Customers:
    url or email may be provided. Both are optional
    By default applications will be collected into our ATS and an email will be sent to
    either the default user account or the specified email address
    If the URL field is specified applicants will be redirected to the specified URL
    -->
    <!-- <url><![CDATA[http://jobs.dev.ziprecruiter.com:4014/job/Auto-Tech/cc68d2f943/?source=emailcandidate-job-alert]]></url> -->
    <!-- <email><![CDATA[where-to-email-applications@example.com]]></email> -->
</job>
@endforeach
</source>