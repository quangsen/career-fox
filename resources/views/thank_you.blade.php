@extends('layouts.master')

@section('title', 'Thank you')

@section('content')

@if (session('message'))
<h3 class='text-alert text-success'>{{session('message')}}</h3>
@endif

@endsection('content')