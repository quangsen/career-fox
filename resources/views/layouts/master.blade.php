<html>
    <head>
        <title>Career Fox - @yield('title')</title>
        {!!HTML::style('css/bootstrap.min.css')!!}
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    </head>
    <body>
        @section('header')
        <div class="container">
            <h1 class="mainTitle text-success text-center">
                <a class='btn btn-success pull-left' href='{{url('demo')}}'>Home</a>
                Career Fox header.
                @if ($user)
                <a class='btn btn-danger pull-right' href='{{url('demo/logout')}}'>Logout</a>
                @endif
            </h1>
            
        </div>
        @show

        <div class="container">
            @yield('content')
        </div>
        
        @section('footer')
        <div class="container">
            <h1 class="mainTitle text-success text-center">Career Fox footer.</h1>
        </div>
        @show
    </body>
</html>