<?php

$serializer = new SuperClosure\Serializer;

return [

    'initialize' => $serializer->serialize(function ($authority) {

                $user = JWTAuth::getToken() ? JWTAuth::parseToken()->authenticate() : new Fox\User;

                //echo $user->role->name; die;
                
                if ($user->hasRole('admin')) {
                    $authority->allow('manage', 'all');
                } else if ($user->hasRole('manager')) {
                    $authority->allow('manage', 'all');
                } else if ($user->hasRole('company')) {
                    $authority->allow('manage', ['Fox\Job', 'Fox\User', 'Fox\Candidate', 'Fox\JobQuestion', 'Fox\VideoInterview', 'Fox\Question', 'Fox\Questionnaire', 'Fox\PhoneScreen', 'Fox\Interview', 'Fox\ReferenceCheck', 'Fox\OfferLetter', 'Fox\ReferenceCheck',]);
                    $authority->allow(['show', 'update'], 'Fox\Company');
                    //$authority->deny('destroy', 'Fox\Company');
                    
                } else if ($user->hasRole('recruiter')) {
                    $authority->allow('manage', ['Fox\Job', 'Fox\User', 'Fox\Candidate', 'Fox\JobQuestion', 'Fox\VideoInterview', 'Fox\Question', 'Fox\Questionnaire', 'Fox\PhoneScreen', 'Fox\Interview', 'Fox\ReferenceCheck', 'Fox\OfferLetter', 'Fox\ReferenceCheck',]);
                } else if ($user->hasRole('employee')) {
                    //$authority->deny('manage', ['Fox\Job', 'Fox\Candidate', 'Fox\JobQuestion', 'Fox\VideoInterview', 'Fox\Question', 'Fox\Questionnaire', 'Fox\PhoneScreen', 'Fox\Interview', 'Fox\ReferenceCheck', 'Fox\OfferLetter', 'Fox\ReferenceCheck', ]);
                    $authority->allow([
                        'read',
                        'getCandidateByJob',
                        'getCandidatesInPhoneScreen',
                        'getCandidatesInVideoInterview',
                        'getCandidatesInQuestionnaire',
                        'getCandidatesInInterview',
                        'getCandidatesInReferenceCheck',
                        'getCandidatesInOfferLetter',
                            ],
                        ['Fox\Job', 'Fox\Candidate', 'Fox\JobQuestion', 'Fox\VideoInterview', 'Fox\Question', 'Fox\Questionnaire', 'Fox\PhoneScreen', 'Fox\Interview', 'Fox\ReferenceCheck', 'Fox\OfferLetter', 'Fox\ReferenceCheck',
                    ]);
                    $authority->allow('manage', 'Fox\User');
                } else if ($user->hasRole('candidate')) {
                    $authority->allow('read', 'all');
                } else {
                    $authority->allow('read', 'all');
                    //$authority->allow('manage', 'Fox/User');
                }

                //
                // The first argument to `allow` is the action you are giving the user
                // permission to do.
                // If you pass 'manage' it will apply to every action. Other common actions
                // here are 'read', 'create', 'update' and 'destroy'.
                //
        // The second argument is the resource the user can perform the action on.
                // If you pass 'all' it will apply to every resource. Otherwise pass a Eloquent
                // class name of the resource.
                //
        // The third argument is an optional anonymous function (Closure) to further filter the
                // objects.
                // For example, here the user can only update available products.
                //
        //  $authority->allow('update', 'Product', function($self, $product) {
                //      return $product->available === true;
                //  });
                //
        // See the wiki of AuthorityController for details:
                // https://github.com/efficiently/authority-controller/wiki/Defining-Authority-rules
                //
        // Loop through each of the users permissions, and create rules:
                //
        // foreach($user->permissions as $perm) {
                //  if ($perm->type == 'allow') {
                //      $authority->allow($perm->action, $perm->resource);
                //  } else {
                //      $authority->deny($perm->action, $perm->resource);
                //  }
                // }
            //

    })
        ];
        