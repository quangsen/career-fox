
(function(app) {
    app.controller('PlacementRecordController', controller);

    controller.$inject = ['$scope', '$state', 'ngDialog', 'API', 'factoryService', '$stateParams'];

    function controller($scope, $state, ngDialog, API, factoryService, $stateParams) {
        var vm = this;
		
		// get benefit list
        vm.benefits = [];
        vm.bePaginator = factoryService.createPaginator();

        vm.getBenefitList = function() {
            var currentPage = 1;
            var limit = 20;
            if (vm.benefitPaginator) {
                currentPage = vm.benefitPaginator.current_page;
                limit = vm.benefitPaginator.per_page;
            }

            var params = {
                job_id: jobId,
                page: currentPage,
                limit: limit
            };

            API.benefit.list(params)
                .then(function(recordSet) {
                    vm.benefits = recordSet.getItems();
                    vm.paginator = recordSet.getPaginator();
                })
                .catch(function(err) {
                    throw err;
                });
        };
		
		function showDlg(benefit) {
            var dlg = ngDialog.openConfirm({
                className: 'ngdialog-theme-default add-benefits',
                showClose: false,
                resolve: {
                    
                },
                controller: ['$scope',
                    function($scope) {
                        $scope.create = function() {
                            if ($scope.benefit.getId()) {
                                var data = {
                                    title: $scope.benefit.title,
									description: $scope.benefit.description,
									start_date: $scope.benefit.start_date
                                };
                                API.benefit.update($scope.benefit.getId(), data)
                                    .then(function(rs) {
                                        $scope.confirm(true);
                                    }, function(err) {
                                        throw err;
                                    });
                            } else {
                                API.benefit.create($scope.benefit.title, $scope.benefit.description, $scope.benefit.start_date )
                                    .then(function(rs) {
                                        $scope.confirm(true);
                                    }, function(err) {
                                        throw err;
                                    });
                            }
                        };
                    }
                ],
                template: 'placement-record/add.tpl.html'
            });
            dlg.then(function(val) {
                vm.benefitPaginator.current_page = 1;
                vm.getBenefitList();
            }, function(reason) {

            });
        }

        vm.addBenifits = function() {
			//var benefit = new Benefit({});
			var benefit = {};
			showDlg(benefit);
        };
		
		vm.addNewRecords = function() {
			//var newRecord = new newRecord({});
			var newRecord = {};
			recordShowDlg(newRecord);
        };
		
		function recordShowDlg(newRecord) {
            var dlg = ngDialog.openConfirm({
                className: 'ngdialog-theme-default add-record',
                showClose: false,
                resolve: {
                    
                },
                controller: ['$scope',
                    function($scope) {
						vm =this;
                        vm.items = [];
						vm.paginator = factoryService.createPaginator();
						vm.keyword = '';
						vm.invited = [];

						vm.getList = function() {
							var currentPage = 1;
							var limit = 20;
							if (vm.paginator) {
								currentPage = vm.paginator.current_page;
								limit = vm.paginator.per_page;
							}

							var params = {
								page: currentPage,
								limit: limit
							};

							if (vm.keyword) {
								params.q = ('(keyword:' + vm.keyword + ')');
							}

							var jobId = $stateParams.id || null;
							
							API.jobs.candidates.getReviewList(jobId, params)
								.then(function(recordSet) {
									vm.items = recordSet.getItems();
									vm.items.forEach(function(value, key) {
										if (vm.invited.indexOf(value.id) > -1) {
											vm.items[key].is_invited = true;
										}
									});
									vm.paginator = recordSet.getPaginator();
								})
								.catch(function(err) {
									throw err;
								});
						};

						// init functions for list candidate of a job
						vm.getList();

						vm.keypress = function(e) {
							if (e.which === 13) {
								vm.paginator.current_page = 1;
								vm.getList();
							}
						};

                    }
                ],
                template: 'jobs/templates/candidate/popup-review.tpl.html'
            });
            dlg.then(function(val) {
                vm.benefitPaginator.current_page = 1;
                vm.getBenefitList();
            }, function(reason) {

            });
        }
		
		vm.uploadShowDlg = function() {
            var dlg = ngDialog.openConfirm({
                className: 'ngdialog-theme-default add-benefits',
                showClose: false,
                resolve: {
                    
                },
                controller: ['$scope',
                    function($scope) {
                        $scope.create = function() {
                            
                        };
                    }
                ],
                template: 'placement-record/add-documents.tpl.html'
            });
            dlg.then(function(val) {
                vm.benefitPaginator.current_page = 1;
                vm.getBenefitList();
            }, function(reason) {

            });
        };

        vm.uploadDocument = function() {
			vm.uploadShowDlg();
        };
		
		

    }
})(angular.module('fox.placement-record.controllers', []));
