require('./controllers/');

(function(app) {
    app.config([
        '$stateProvider',
        function($stateProvider) {
            $stateProvider
                .state('fox.dashboard', {
                    url: '/dashboard/index',
                    templateUrl: 'dashboard/templates/dashboard.tpl.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    resolve: {}
                });
        }
    ]);
})(angular.module('fox.dashboard.routes', [
    'fox.dashboard.controllers'
]));
