Chart = require('chartjs');

(function(app) {
    app.controller('DashboardController', DashboardController);
    app.controller('ChatCtrl', ChatCtrl);
    app.controller('LastCtrl', LastCtrl);
    app.controller('OnotherCtrl', OnotherCtrl);
    app.controller('SalesCtrl', SalesCtrl);
    app.controller('SparklineCtrl', SparklineCtrl);
    app.controller('VisitsCtrl', VisitsCtrl);


    DashboardController.$inject = ['$state', '$scope'];

    function DashboardController($state, $scope) {
        var vm = this;
		
		$scope.$watch('vm.start_date', function(newVal, oldVal) {
			if(newVal !== oldVal) {
			  vm.reloadPayrollGraph();
			}
		  });
		 $scope.$watch('vm.end_date', function(newVal, oldVal) {
			if(newVal !== oldVal) {
			  vm.reloadPayrollGraph();
			}
		  });
		  
		 vm.reloadPayrollGraph = function(){
			 
		 };
    }

    ChatCtrl.$inject = ['$state', '$scope'];

    function ChatCtrl($state, $scope) {
        $scope.selfIdUser = 50223456;
        $scope.otherIdUser = 50223457;
        $scope.setOtherId = function(value) {

            $scope.otherIdUser = value;
        };
        var exampleDate = new Date().setTime(new Date().getTime() - 240000 * 60);

        $scope.chat = [{
            "user": "Peter Clark",
            "avatar": "img/avatar-1.jpg",
            "to": "Nicole Bell",
            "date": exampleDate,
            "content": "Hi, Nicole",
            "idUser": 50223456,
            "idOther": 50223457
        }, {
            "user": "Peter Clark",
            "avatar": "img/avatar-1.jpg",
            "to": "Nicole Bell",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 1000 * 60),
            "content": "How are you?",
            "idUser": 50223456,
            "idOther": 50223457
        }, {
            "user": "Nicole Bell",
            "avatar": "img/avatar-2.jpg",
            "to": "Peter Clark",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 1000 * 60),
            "content": "Hi, i am good",
            "idUser": 50223457,
            "idOther": 50223456
        }, {
            "user": "Peter Clark",
            "avatar": "img/avatar-1.jpg",
            "to": "Nicole Bell",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 1000 * 60),
            "content": "Glad to see you ;)",
            "idUser": 50223456,
            "idOther": 50223457
        }, {
            "user": "Nicole Bell",
            "avatar": "img/avatar-2.jpg",
            "to": "Peter Clark",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 65000 * 60),
            "content": "What do you think about my new Dashboard?",
            "idUser": 50223457,
            "idOther": 50223456
        }, {
            "user": "Nicole Bell",
            "avatar": "img/avatar-2.jpg",
            "to": "Peter Clark",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 128000 * 60),
            "content": "Alo...",
            "idUser": 50223457,
            "idOther": 50223456
        }, {
            "user": "Nicole Bell",
            "avatar": "img/avatar-2.jpg",
            "to": "Peter Clark",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 128000 * 60),
            "content": "Are you there?",
            "idUser": 50223457,
            "idOther": 50223456
        }, {
            "user": "Peter Clark",
            "avatar": "img/avatar-1.jpg",
            "to": "Nicole Bell",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 1000 * 60),
            "content": "Hi, i am here",
            "idUser": 50223456,
            "idOther": 50223457
        }, {
            "user": "Peter Clark",
            "avatar": "img/avatar-1.jpg",
            "to": "Nicole Bell",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 1000 * 60),
            "content": "Your Dashboard is great",
            "idUser": 50223456,
            "idOther": 50223457
        }, {
            "user": "Nicole Bell",
            "avatar": "img/avatar-2.jpg",
            "to": "Peter Clark",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 230000 * 60),
            "content": "How does the binding and digesting work in AngularJS?, Peter? ",
            "idUser": 50223457,
            "idOther": 50223456
        }, {
            "user": "Peter Clark",
            "avatar": "img/avatar-1.jpg",
            "to": "Nicole Bell",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 238000 * 60),
            "content": "oh that's your question?",
            "idUser": 50223456,
            "idOther": 50223457
        }, {
            "user": "Peter Clark",
            "avatar": "img/avatar-1.jpg",
            "to": "Nicole Bell",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 238000 * 60),
            "content": "little reduntant, no?",
            "idUser": 50223456,
            "idOther": 50223457
        }, {
            "user": "Peter Clark",
            "avatar": "img/avatar-1.jpg",
            "to": "Nicole Bell",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 238000 * 60),
            "content": "literally we get the question daily",
            "idUser": 50223456,
            "idOther": 50223457
        }, {
            "user": "Nicole Bell",
            "avatar": "img/avatar-2.jpg",
            "to": "Peter Clark",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 238000 * 60),
            "content": "I know. I, however, am not a nerd, and want to know",
            "idUser": 50223457,
            "idOther": 50223456
        }, {
            "user": "Peter Clark",
            "avatar": "img/avatar-1.jpg",
            "to": "Nicole Bell",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 238000 * 60),
            "content": "for this type of question, wouldn't it be better to try Google?",
            "idUser": 50223456,
            "idOther": 50223457
        }, {
            "user": "Nicole Bell",
            "avatar": "img/avatar-2.jpg",
            "to": "Peter Clark",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 238000 * 60),
            "content": "Lucky for us :)",
            "idUser": 50223457,
            "idOther": 50223456
        }, {
            "user": "Steven Thompson",
            "avatar": "img/avatar-3.jpg",
            "to": "Peter Clark",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 1000 * 60),
            "content": "Hi, Peter. I'd like to start using AngularJS.",
            "idUser": 50223458,
            "idOther": 50223456
        }, {
            "user": "Steven Thompson",
            "avatar": "img/avatar-3.jpg",
            "to": "Peter Clark",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 1000 * 60),
            "content": "There are many differences from jquery?",
            "idUser": 50223458,
            "idOther": 50223456
        }, {
            "user": "Peter Clark",
            "avatar": "img/avatar-1.jpg",
            "to": "Steven Thompson",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 5000 * 60),
            "content": "Enough!",
            "idUser": 50223456,
            "idOther": 50223458
        }, {
            "user": "Peter Clark",
            "avatar": "img/avatar-1.jpg",
            "to": "Steven Thompson",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 5000 * 60),
            "content": "In jQuery, you design a page, and then you make it dynamic...",
            "idUser": 50223456,
            "idOther": 50223458
        }, {
            "user": "Peter Clark",
            "avatar": "img/avatar-1.jpg",
            "to": "Steven Thompson",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 5000 * 60),
            "content": "but in AngularJS, you must start from the ground up with your architecture in mind",
            "idUser": 50223456,
            "idOther": 50223458
        }, {
            "user": "Steven Thompson",
            "avatar": "img/avatar-3.jpg",
            "to": "Peter Clark",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 7000 * 60),
            "content": "ok!",
            "idUser": 50223458,
            "idOther": 50223456
        }, {
            "user": "Steven Thompson",
            "avatar": "img/avatar-3.jpg",
            "to": "Peter Clark",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 7000 * 60),
            "content": "could you give me some lessons?",
            "idUser": 50223458,
            "idOther": 50223456
        }, {
            "user": "Peter Clark",
            "avatar": "img/avatar-1.jpg",
            "to": "Steven Thompson",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 7000 * 60),
            "content": "sure!",
            "idUser": 50223456,
            "idOther": 50223458
        }, {
            "user": "Steven Thompson",
            "avatar": "img/avatar-3.jpg",
            "to": "Peter Clark",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 7000 * 60),
            "content": "Thanks a lot!",
            "idUser": 50223458,
            "idOther": 50223456
        }, {
            "user": "Ella Patterson",
            "avatar": "img/avatar-4.jpg",
            "to": "Peter Clark",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 16700 * 60),
            "content": "Peter what can you tell me about the new marketing project?",
            "idUser": 50223459,
            "idOther": 50223456
        }, {
            "user": "Peter Clark",
            "avatar": "img/avatar-1.jpg",
            "to": "Steven Thompson",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 18000 * 60),
            "content": "Well, there is a lot to say. Are you free tomorrow?",
            "idUser": 50223456,
            "idOther": 50223459
        }, {
            "user": "Ella Patterson",
            "avatar": "img/avatar-4.jpg",
            "to": "Peter Clark",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 19700 * 60),
            "content": "Yes",
            "idUser": 50223459,
            "idOther": 50223456
        }, {
            "user": "Peter Clark",
            "avatar": "img/avatar-1.jpg",
            "to": "Steven Thompson",
            "date": new Date(exampleDate).setTime(new Date(exampleDate).getTime() + 19700 * 60),
            "content": "OK, we will have a meeting tomorrow afternoon",
            "idUser": 50223456,
            "idOther": 50223459
        }, {
            "user": "Kenneth Ross",
            "avatar": "img/avatar-5.jpg",
            "to": "Peter Clark",
            "date": new Date(exampleDate).setTime(new Date(exampleDate)),
            "content": "Mr. Clark, congratulations for your new project",
            "idUser": 50223460,
            "idOther": 50223456
        }, {
            "user": "Peter Clark",
            "avatar": "img/avatar-1.jpg",
            "to": "Kenneth Ross",
            "date": new Date(exampleDate).setTime(new Date(exampleDate)),
            "content": "Thank You very much Mr. Ross",
            "idUser": 50223456,
            "idOther": 50223460
        }];

        $scope.sendMessage = function() {
            var newMessage = {
                "user": "Peter Clark",
                "avatar": "img/avatar-1.jpg",
                "date": new Date(),
                "content": $scope.chatMessage,
                "idUser": $scope.selfIdUser,
                "idOther": $scope.otherIdUser
            };
            $scope.chat.push(newMessage);
            $scope.chatMessage = '';
        };
    }


    LastCtrl.$inject = ['$state', '$scope'];

    function LastCtrl($state, $scope) {
        // Chart.js Data
        $scope.data = {
            labels: ['Eating', 'Drinking', 'Sleeping', 'Designing', 'Coding', 'Cycling', 'Running'],
            datasets: [{
                label: 'My First dataset',
                fillColor: 'rgba(220,220,220,0.2)',
                strokeColor: 'rgba(220,220,220,1)',
                pointColor: 'rgba(220,220,220,1)',
                pointStrokeColor: '#fff',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(220,220,220,1)',
                data: [65, 59, 90, 81, 56, 55, 40]
            }, {
                label: 'My Second dataset',
                fillColor: 'rgba(151,187,205,0.2)',
                strokeColor: 'rgba(151,187,205,1)',
                pointColor: 'rgba(151,187,205,1)',
                pointStrokeColor: '#fff',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(151,187,205,1)',
                data: [28, 48, 40, 19, 96, 27, 100]
            }]
        };

        // Chart.js Options
        $scope.options = {

            // Sets the chart to be responsive
            responsive: true,

            //Boolean - Whether to show lines for each scale point
            scaleShowLine: true,

            //Boolean - Whether we show the angle lines out of the radar
            angleShowLineOut: true,

            //Boolean - Whether to show labels on the scale
            scaleShowLabels: false,

            // Boolean - Whether the scale should begin at zero
            scaleBeginAtZero: true,

            //String - Colour of the angle line
            angleLineColor: 'rgba(0,0,0,.1)',

            //Number - Pixel width of the angle line
            angleLineWidth: 1,

            //String - Point label font declaration
            pointLabelFontFamily: '"Arial"',

            //String - Point label font weight
            pointLabelFontStyle: 'normal',

            //Number - Point label font size in pixels
            pointLabelFontSize: 10,

            //String - Point label font colour
            pointLabelFontColor: '#666',

            //Boolean - Whether to show a dot for each point
            pointDot: true,

            //Number - Radius of each point dot in pixels
            pointDotRadius: 3,

            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,

            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,

            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,

            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,

            //Boolean - Whether to fill the dataset with a colour
            datasetFill: true,

            //String - A legend template
            legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].strokeColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'
        };
    }

    OnotherCtrl.$inject = ['$state', '$scope'];

    function OnotherCtrl($state, $scope) {
        // Chart.js Data
        $scope.data = [{
            value: 300,
            color: '#F7464A',
            highlight: '#FF5A5E',
            label: 'Red'
        }, {
            value: 50,
            color: '#46BFBD',
            highlight: '#5AD3D1',
            label: 'Green'
        }, {
            value: 100,
            color: '#FDB45C',
            highlight: '#FFC870',
            label: 'Yellow'
        }];
        $scope.total = 450;
        // Chart.js Options
        $scope.options = {

            // Sets the chart to be responsive
            responsive: false,

            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke: true,

            //String - The colour of each segment stroke
            segmentStrokeColor: '#fff',

            //Number - The width of each segment stroke
            segmentStrokeWidth: 2,

            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 50, // This is 0 for Pie charts

            //Number - Amount of animation steps
            animationSteps: 100,

            //String - Animation easing effect
            animationEasing: 'easeOutBounce',

            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate: true,

            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale: false,

            //String - A legend template
            legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'

        };
    }

    SalesCtrl.$inject = ['$state', '$scope'];

    function SalesCtrl($state, $scope) {
        // Chart.js Data
        $scope.data = {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [{
                label: 'My First dataset',
                fillColor: 'rgba(220,220,220,0.5)',
                strokeColor: 'rgba(220,220,220,0.8)',
                highlightFill: 'rgba(220,220,220,0.75)',
                highlightStroke: 'rgba(220,220,220,1)',
                data: [65, 59, 80, 81, 56, 55, 40]
            }, {
                label: 'My Second dataset',
                fillColor: 'rgba(151,187,205,0.5)',
                strokeColor: 'rgba(151,187,205,0.8)',
                highlightFill: 'rgba(151,187,205,0.75)',
                highlightStroke: 'rgba(151,187,205,1)',
                data: [28, 48, 40, 19, 86, 27, 90]
            }]
        };

        // Chart.js Options
        $scope.options = {
            maintainAspectRatio: false,

            // Sets the chart to be responsive
            responsive: true,

            //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
            scaleBeginAtZero: true,

            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,

            //String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",

            //Number - Width of the grid lines
            scaleGridLineWidth: 1,

            //Boolean - If there is a stroke on each bar
            barShowStroke: true,

            //Number - Pixel width of the bar stroke
            barStrokeWidth: 2,

            //Number - Spacing between each of the X value sets
            barValueSpacing: 5,

            //Number - Spacing between data sets within X values
            barDatasetSpacing: 1,

            //String - A legend template
            legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'
        };
    }

    SparklineCtrl.$inject = ['$state', '$scope'];

    function SparklineCtrl($state, $scope) {
        $scope.sales = [600, 923, 482, 1211, 490, 1125, 1487];
        $scope.earnings = [400, 650, 886, 443, 502, 412, 353];
        $scope.referrals = [4879, 6567, 5022, 5890, 9234, 7128, 4811];
    }

    VisitsCtrl.$inject = ['$state', '$scope'];

    function VisitsCtrl($state, $scope) {
        $scope.data = {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [{
                label: 'Hires',
                fillColor: 'rgba(220,220,220,0.2)',
                strokeColor: 'rgba(220,220,220,1)',
                pointColor: 'rgba(220,220,220,1)',
                pointStrokeColor: '#fff',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(220,220,220,1)',
                data: [65, 59, 80, 81, 56, 55, 40, 84, 64, 120, 132, 87]
            }, {
                label: '$',
                fillColor: 'rgba(151,187,205,0.2)',
                strokeColor: 'rgba(151,187,205,1)',
                pointColor: 'rgba(151,187,205,1)',
                pointStrokeColor: '#fff',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(151,187,205,1)',
                data: [28, 48, 40, 19, 86, 27, 90, 102, 123, 145, 60, 161]
            }]
        };

        $scope.options = {

            maintainAspectRatio: false,

            // Sets the chart to be responsive
            responsive: true,

            ///Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,

            //String - Colour of the grid lines
            scaleGridLineColor: 'rgba(0,0,0,.05)',

            //Number - Width of the grid lines
            scaleGridLineWidth: 1,

            scaleLabel: function(value){
                return value + '$';
            },

            //Boolean - Whether the line is curved between points
            bezierCurve: false,

            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.4,

            //Boolean - Whether to show a dot for each point
            pointDot: true,

            //Number - Radius of each point dot in pixels
            pointDotRadius: 4,

            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,

            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,

            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,

            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,

            //Boolean - Whether to fill the dataset with a colour
            datasetFill: true,

            // Function - on animation progress
            onAnimationProgress: function() {},

            // Function - on animation complete
            onAnimationComplete: function() {},

            //String - A legend template
            legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].strokeColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'
        };
    }

})(angular.module('fox.dashboard.controllers', []));
