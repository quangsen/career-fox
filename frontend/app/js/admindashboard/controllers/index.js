require('./sparkline');
require('./visit');
require('./sale');
require('./onother');
require('./last');
require('./chat');

(function(app) {
	app.controller('AdminDashboardController', controller); 
	controller.$inject = ['$state', '$scope'];
	function controller($state, $scope) {
		var vm = this;
		$scope.$watch('vm.start_date', function(newVal, oldVal) {
			if(newVal !== oldVal) {
			  vm.reloadPayrollGraph();
			}
		  });
		 $scope.$watch('vm.end_date', function(newVal, oldVal) {
			if(newVal !== oldVal) {
			  vm.reloadPayrollGraph();
			}
		  });
		  
		 vm.reloadPayrollGraph = function(){
			 
		 };
	}
})(angular.module('fox.admindashboard.controllers', [
    'fox.admindashboard.controllers.sparkline',
    'fox.admindashboard.controllers.sale',
    'fox.admindashboard.controllers.onother',
    'fox.admindashboard.controllers.last',
    'fox.admindashboard.controllers.chat',
    'fox.admindashboard.controllers.visit'
]));
