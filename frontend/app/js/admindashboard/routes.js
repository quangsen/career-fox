require('./controllers/');

(function(app) {
    app.config([
        '$stateProvider',
        function($stateProvider) {
            $stateProvider
                .state('fox.admindashboard', {
                    url: '/admindashboard',
                    templateUrl: 'admindashboard/templates/admin_dashboard.tpl.html',
                    controller: 'AdminDashboardController',
                    controllerAs: 'vm',
                    resolve: {}
                });
        }
    ]);
})(angular.module('fox.admindashboard.routes', [
    'fox.admindashboard.controllers'
]));
