(function(app) {
    app.directive('choiceMultiAnswerHorizontal', choiceMultiAnswerHorizontal);

    function choiceMultiAnswerHorizontal() {
        var directive = {
            restrict: 'EA',
            scope: {
                control: '='
            },
            templateUrl: 'questionnaire/types/ChoiceMultiAnswerHorizontal/view.tpl.html',
            link: function(scope) {
                scope.select = function(item) {
                    var selectedItems = [];
                    scope.control.getItems().forEach(function(v, k) {
                        if (v.value) {
                            selectedItems.push(v);
                        }
                    });
                    scope.control.setValue(selectedItems);
                };
            },
        };
        return directive;
    }

})(angular.module('fox.questionnaire.types.choiceMultiAnswerHorizontal', []));
