(function(app) {
    app.directive('fileUpload', fileUpload);

    function fileUpload() {
        var directive = {
            restrict: 'EA',
            scope: {
                control: '='
            },
            templateUrl: 'questionnaire/types/fileUpload/view.tpl.html',
            link: function(scope) {
                
            },
        };
        return directive;
    }

})(angular.module('fox.questionnaire.types.fileUpload', []));
