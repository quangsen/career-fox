(function(app) {
    app.directive('choiceMultiAnswerSelectBox', choiceMultiAnswerSelectBox);

    function choiceMultiAnswerSelectBox() {
        var directive = {
            restrict: 'EA',
            scope: {
                control: '='
            },
            templateUrl: 'questionnaire/types/ChoiceMultiAnswerSelectBox/view.tpl.html',
            link: function(scope) {
                scope.select = function(item) {
                    item.value = !item.value;
                    var selectedItems = [];
                    scope.control.getItems().forEach(function(v, k) {
                        if (v.value) {
                            selectedItems.push(v);
                        }
                    });
                    scope.control.setValue(selectedItems);
                };
            },
        };
        return directive;
    }

})(angular.module('fox.questionnaire.types.choiceMultiAnswerSelectBox', []));
