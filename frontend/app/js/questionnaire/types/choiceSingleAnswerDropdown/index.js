(function(app) {
    app.directive('choiceSingleAnswerDropdown', choiceSingleAnswerDropdown);

    function choiceSingleAnswerDropdown() {
        var directive = {
            restrict: 'EA',
            scope: {
                control: '='
            },
            templateUrl: 'questionnaire/types/choiceSingleAnswerDropdown/view.tpl.html',
            link: function(scope) {
                scope.select = function(item) {
                    scope.control.setValue(item);
                };
            },
        };
        return directive;
    }

})(angular.module('fox.questionnaire.types.choiceSingleAnswerDropdown', []));
