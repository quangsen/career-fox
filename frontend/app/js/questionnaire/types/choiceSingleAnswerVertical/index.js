
(function(app) {
    app.directive('choiceSingleAnswerVertical', choiceSingleAnswerVertical);

    function choiceSingleAnswerVertical() {
        var directive = {
            restrict: 'EA',
            scope: {
                control: '='
            },
            templateUrl: 'questionnaire/types/choiceSingleAnswerVertical/view.tpl.html',
            link: function(scope) {
                scope.select = function(item) {
                    scope.control.setValue(item);
                };
            },
        };
        return directive;
    }

})(angular.module('fox.questionnaire.types.choiceSingleAnswerVertical', []));
