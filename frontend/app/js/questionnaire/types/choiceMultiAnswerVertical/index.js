(function(app) {
    app.directive('choiceMultiAnswerVertical', choiceMultiAnswerVertical);

    function choiceMultiAnswerVertical() {
        var directive = {
            restrict: 'EA',
            scope: {
                control: '='
            },
            templateUrl: 'questionnaire/types/choiceMultiAnswerVertical/view.tpl.html',
            link: function(scope) {
                scope.select = function(item) {
                    var selectedItems = [];
                    scope.control.getItems().forEach(function(v, k) {
                        if (v.value) {
                            selectedItems.push(v);
                        }
                    });
                    scope.control.setValue(selectedItems);
                };
            },
        };
        return directive;
    }

})(angular.module('fox.questionnaire.types.choiceMultiAnswerVertical', []));
