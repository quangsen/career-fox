/**
 * Created by quynh on 10/24/2015.
 */

(function (app) {
    app.directive('questionTypeBoolean', theDirective);
    theDirective.$inject = [];

    function theDirective() {
        var directive = {
            restrict: 'EA',
            scope: {
                control: '='
            },
            templateUrl: 'questionnaire/types/boolean/view.tpl.html'
        };

        return directive;
    }

})(angular.module('fox.questionnaire.types.boolean', []));