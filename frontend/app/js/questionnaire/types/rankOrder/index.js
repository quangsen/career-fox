(function(app) {
    app.directive('rankOrder', rankOrder);

    function rankOrder() {
        var directive = {
            restrict: 'EA',
            scope: {
                control: '='
            },
            templateUrl: 'questionnaire/types/rankOrder/view.tpl.html',
            link: function(scope) {
                
            },
        };
        return directive;
    }

})(angular.module('fox.questionnaire.types.rankOrder', ['ui.mask']));
