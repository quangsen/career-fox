(function(app) {
    app.directive('choiceSingleAnswerHorizontal', choiceSingleAnswerHorizontal);

    function choiceSingleAnswerHorizontal() {
        var directive = {
            restrict: 'EA',
            scope: {
                control: '='
            },
            templateUrl: 'questionnaire/types/choiceSingleAnswerHorizontal/view.tpl.html',
            link: function(scope) {
                scope.select = function(item) {
                    scope.control.setValue(item);
                };
            },
        };
        return directive;
    }

})(angular.module('fox.questionnaire.types.choiceSingleAnswerHorizontal', []));
