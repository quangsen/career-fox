/**
 * Created by quynh on 10/28/2015.
 */

(function (app) {
    app.directive('survey', theDirective);
    theDirective.$inject = ['API'];

    function theDirective(API) {
        var directive = {
            restrict: 'EA',
            scope: {
                job: '='
            },
            templateUrl: 'jobs/templates/interview/survey.tpl.html',
            controller: function ($scope) {
                // For demo
                var questions = [
                    new API.models.questionTypes.Boolean(),
                    new API.models.questionTypes.SingleChoice(),
                    new API.models.questionTypes.MultipleChoice(),
                    new API.models.questionTypes.DropDown()
                ];
                $scope.type = 'boolean';
                $scope.questions = questions;
                $scope.surveys = [];
                // $scope.survey = null;

                // Demo
                $scope.survey = new API.models.Survey({id:1, title: 'Survey 1'});

                var jobId = $scope.job.id;
                API.surveys.list(jobId)
                    .then(function (surveys) {
                        console.log('surveys:', surveys);
                        $scope.surveys = surveys.getItems();
                    })
                    .catch(function (err) {
                        throw err;
                    });

                $scope.add = function () {
                    $scope.questions.push(API.models.questionTypes.createQuestionType($scope.type));
                };

                $scope.isEditing = function () {
                    return ($scope.survey !== null);
                };

                $scope.hasSurvey = function () {
                    return ($scope.surveys.length > 0);
                };

                $scope.edit = function (survey) {
                    $scope.survey = survey;
                };

            }
        };

        return directive;
    }
})(angular.module('fox.questionnaire.survey', []));