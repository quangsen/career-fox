require('./routes');
require('./exception');

(function(app) {})(angular.module('fox.error', [
    'fox.error.routes',
    'fox.error.exception'
]));