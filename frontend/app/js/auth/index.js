require('./routes');
require('./controller');
require('./provider');
require('./service');

(function(app) {
    app.config([
        '$httpProvider',
        function($httpProvider) {
            $httpProvider.interceptors.push('authenticationInterceptor');
        }
    ]);

    app.run([
        'auth',
        function(auth) {
            auth.validateUser();
        }
    ]);
})(angular.module('fox.auth', [
    'fox.auth.routes',
    'fox.auth.provider',
    'fox.auth.service'
]));