(function(app) {
    app.controller('LoginController', controller);
    controller.$inject = [
        '$state',
        'auth',
        'authService',
        '$rootScope',
        'dialog'
    ];

    function controller(
        $state,
        auth,
        authService,
        $rootScope,
        dialog
    ) {

        var vm = this;

        $rootScope.app.isLogin = true;

        vm.form = {
            email: '',
            password: ''
        };

        vm.login = function() {
            authService.login(vm.form.email, vm.form.password)
                .then(function(token) {
                    auth.setToken(token);
                    // Go to dashboard
                    $state.go('fox.dashboard');
                }, function(err) {
                    dialog.alert('', 'Email or Password is invalid.');
                });
        };

        vm.gotoHome = function() {
            $state.go('auth');
        };

        vm.forgotPassword = function() {};
    }
})(angular.module('fox.controllers.login', []));