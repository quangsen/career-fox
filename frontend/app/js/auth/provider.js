var User = require('../api/models/user');
var Base64 = require('base64codec');
(function(app) {
    app.factory('authenticationInterceptor', [
        '$q',
        'auth',
        function($q, auth) {
            function request(config) {
                if (auth.isAuthenticated()) {
                    var token = auth.getToken();
                    config.headers = config.headers || {};
                    config.headers.Authorization = 'Bearer ' + token;
                }
                return config;
            }

            function responseSuccess(response) {
                var token = response.headers('Authorization');
                if (token && token.indexOf('Bearer') === 0) {
                    token = token.replace('Bearer ', '');
                    auth.setToken(token);
                }
                return response;
            }

            function responseError(response) {
                var status = response.status;

                function openLogin() {
                    window.location = '#/login';
                }

                switch (status) {
                    case 401:
                    case 403:
                        openLogin();
                        break;
                    case 400:
                        if (response.data &&
                            response.data.error &&
                            ((response.data.error === 'token_expired') ||
                                (response.data.error === 'token_invalid') ||
                                (response.data.error === 'token_not_provided')
                            )) {
                            return openLogin();
                        }
                        return $q.reject(response);
                    default:
                        return $q.reject(response);
                }

                return $q.reject(response);
            }

            return {
                request: request,
                responseError: responseError
            };
        }
    ]);

    app.provider('auth', [

        function() {
            this.$get = [
                '$rootScope',
                'storageService',
                function($rootScope, storage) {
                    return {
                        TOKEN_KEY: 'token',
                        user: null,

                        logout: function() {
                            storage.clearAll();
                        },

                        isAuthenticated: function() {
                            var token = storage.get(this.TOKEN_KEY);
                            if (token) {
                                return true;
                            }
                            return false;
                        },

                        isAdmin: function() {
                            if (!this.isAuthenticated()) {
                                return false;
                            }
                            return this.user.isAdmin();
                        },

                        setToken: function(token) {
                            this.getUserFromToken(token);
                            storage.set(this.TOKEN_KEY, token);
                        },

                        getToken: function() {
                            return storage.get(this.TOKEN_KEY);
                        },

                        getUser: function() {
                            return this.user;
                        },

                        setUser: function(user) {
                            this.user = user;
                        },

                        getUserFromToken: function(token) {
                            var profile = token.split('.')[1];
                            var jsonProfile = Base64.decode(profile);
                            var obj = angular.fromJson(jsonProfile);

                            var user = new User(obj);
                            this.setUser(user);
                        },

                        /**
                         * Verify and restore User object from
                         * the token
                         */
                        validateUser: function() {
                            if (!this.isAuthenticated()) {
                                $rootScope.$broadcast('auth:invalid');
                                return false;
                            } else {
                                var token = storage.get(this.TOKEN_KEY);
                                this.getUserFromToken(token);
                            }
                            return true;
                        }
                    };
                }
            ];
        }
    ]);
})(angular.module('fox.auth.provider', []));