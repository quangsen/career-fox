var BaseModel = require('./base');
var inherits = require('inherits');

function Province(options) {
    this.id = null;
    this.name = '';
    this.code = '';
    this.country_id = null;
    BaseModel.call(this, options);
}
inherits(Province, BaseModel);

module.exports = Province;