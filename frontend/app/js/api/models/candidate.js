var BaseModel = require('./base');
var inherits = require('inherits');

function Candidate(options) {
    this.id = null;
    this.company_id = null;
    this.date = null;
    this.job_id = null;
    this.job_title = '';
    this.photo = null;
    this.views = 0;

    this.name = '';
    this.phone = '';
    this.email = '';

    // model address
    this.address = null;
    // model company
    this.company = null;
    // model job
    this.job = null;
    // model user
    this.user = null;

    this.resume = '';

    this.cover_letter = '';

    this.is_invited = false;
    this.invited_questionnaire = false;
    this.video_interview_id = '';

    this.invited_interview = false;
    this.questionnaire_id = null;

    this.invited_offer_letter = false;
    this.reference_check_id = null;

    this.interview_invitation_id = '';

    BaseModel.call(this, options);
}
inherits(Candidate, BaseModel);

Candidate.prototype.bind = function (options) {
    BaseModel.prototype.bind.call(this, options);

    if (options.address) {
        var Address = require('./address');
        this.address = new Address(options.address.data);
    }
    if (options.company) {
        var Company = require('./company');
        this.company = new Company(options.company.data);
    }
    if (options.job) {
        var Job = require('./job');
        this.job = new Job(options.job.data);
    }
    if (options.user) {
        var User = require('./user');
        this.user = new User(options.user.data);
    }
};

Candidate.prototype.hasInvitedToPhone = function () {
    return (this.is_invited === true);
};
Candidate.prototype.setInvitedPhone = function (val) {
    this.is_invited = val;
};
Candidate.prototype.getPdf = function () {
    return this.resume;
};
Candidate.prototype.getCoverLetter = function () {
    return this.cover_letter;
};
Candidate.prototype.getShortResume = function () {
    return 'Candidate review<br />Short resume';
};
Candidate.prototype.getShortLetter = function () {
    return 'Candidate review<br />Short cover letter';
};
Candidate.prototype.getName = function () {
    return this.name;
};
Candidate.prototype.isInvitedQuestionnaire = function () {
    return (this.invited_questionnaire === true);
};
Candidate.prototype.setInvitedQuestionnaire = function (val) {
    this.invited_questionnaire = val;
};

Candidate.prototype.setInvitedInterview = function (val) {
    this.invited_interview = val;
};

Candidate.prototype.isInvitedInterview = function () {
    return (this.invited_interview === true);
};

Candidate.prototype.setInvitedOffer = function (val) {
    this.invited_offer_letter = val;
};

Candidate.prototype.isInvitedOffer = function () {
    return (this.invited_offer_letter === true);
};

Candidate.prototype.getInterviewInvitationId = function() {
    return this.interview_invitation_id;
};

module.exports = Candidate;
