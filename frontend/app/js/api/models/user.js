var BaseModel = require('./base');
var inherits = require('inherits');

function User(options) {
    this.id = null;
    this.name = null;
    this.role_name = '';

    this.email = '';
    this.first_name = '';
    this.last_name = '';
    this.username = '';
    this.phone = '';
    this.company = '';
    this.company_id = null;

    this.checked = false;

    BaseModel.call(this, options);
}
inherits(User, BaseModel);

User.prototype.getId = function() {
    return this.id;
};

User.prototype.getCompanyId = function() {
    return this.company_id;
};

User.prototype.getCompanyName = function() {
   return this.company;
};

User.prototype.getName = function() {
    return this.name;
};

User.prototype.getUserName = function() {
    return this.username;
};

User.prototype.getRole = function() {
    return this.role;
};

User.prototype.getFullName = function() {
    var name = this.first_name;

    if (this.last_name) {
        name = name + ' ' + this.last_name;
    }

    return name;
};

module.exports = User;