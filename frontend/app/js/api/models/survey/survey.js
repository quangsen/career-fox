var BaseModel = require('../base');
var inherits = require('inherits');

function Survey(options) {
    this.id = null;
    this.title = '';
    this.description = '';
    
    BaseModel.call(this, options);
}
inherits(Survey, BaseModel);

module.exports = Survey;