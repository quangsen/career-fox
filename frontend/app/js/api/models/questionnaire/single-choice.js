/**
 * Created by quynh on 10/24/2015.
 */
var BaseModel = require('../base');
var inherits = require('inherits');
var Element = require('./element');

function SingleChoice(options) {
    this.id = null;
    this.type = 'single-choice';
    this.name = 'single_choice_' + Math.random();
    this.title = 'Question ?';
    this.value = null;
    this.items = [new Element({title: 'Option 1'})];

    BaseModel.call(this, options);
}
inherits(SingleChoice, BaseModel);

SingleChoice.prototype.add = function (item) {
    if (!(item instanceof Element)) {
        throw new Error('an Element object is expected');
    }
    this.items.push(item);
    return this;
};

SingleChoice.prototype.remove = function (item) {
    var index = this.items.indexOf(item);
    if (index === -1) {
        return null;
    }
    this.items.splice(index, 1);
    return item;
};

SingleChoice.prototype.getValue = function () {
    return this.value;
};

SingleChoice.prototype.getItems = function () {
    return this.items;
};

SingleChoice.prototype.setValue = function (val) {
    this.value = val;
    return this;
};

module.exports = SingleChoice;