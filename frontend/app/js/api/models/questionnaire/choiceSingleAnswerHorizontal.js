// hieupv.est@gmail.com

var BaseModel = require('../base');
var inherits = require('inherits');
var Element = require('./element');

function choiceSingleAnswerHorizontal(options) {
    this.id = null;
    this.type = 'choiceSingleAnswerHorizontal';
    this.name = 'choiceSingleAnswerHorizontal_' + Math.random();
    this.title = '';
    this.value = null;
    this.items = [];

    BaseModel.call(this, options);
}
inherits(choiceSingleAnswerHorizontal, BaseModel);


choiceSingleAnswerHorizontal.prototype.add = function(item) {
    if (!(item instanceof Element)) {
        throw new Error('an Element object is expected');
    }
    this.items.push(item);
    return this;
};

choiceSingleAnswerHorizontal.prototype.getValue = function() {
    return this.value;
};


choiceSingleAnswerHorizontal.prototype.getItems = function() {
    return this.items;
};

choiceSingleAnswerHorizontal.prototype.setValue = function(val) {
    this.value = val;
    return this;
};




module.exports = choiceSingleAnswerHorizontal;
