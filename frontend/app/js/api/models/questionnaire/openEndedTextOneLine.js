// hieupv.est@gmail.com
var BaseModel = require('../base');
var inherits = require('inherits');
var Element = require('./element');

function openEndedTextOneLine(options) {
    this.id = null;
    this.type = 'openEndedTextOneLine';
    this.name = 'openEndedTextOneLine_' + Math.random();
    this.title = '';
    this.value = '';
    
    BaseModel.call(this, options);
}
inherits(openEndedTextOneLine, BaseModel);


openEndedTextOneLine.prototype.getValue = function() {
    return this.value;
};

openEndedTextOneLine.prototype.setValue = function(val) {
    this.value = val;
};


module.exports = openEndedTextOneLine;
