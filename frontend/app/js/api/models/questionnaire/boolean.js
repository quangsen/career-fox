/**
 * Created by quynh on 10/24/2015.
 */
var BaseModel = require('../base');
var inherits = require('inherits');
var Element = require('./element');

function Boolean(options) {
    this.id = null;
    this.type = 'boolean';
    this.name = 'boolean_'+ Math.random();
    this.title = '';
    this.value = null;
    this.items = [
        new Element({title: 'Yes', value: 'Yes'}),
        new Element({title: 'No', value: 'No'})
    ];

    BaseModel.call(this, options);
}
inherits(Boolean, BaseModel);


Boolean.prototype.getValue = function () {
    return this.values;
};


Boolean.prototype.getItems = function () {
    return this.items;
};

Boolean.prototype.setValue = function (val) {
    this.value = val;
    return this;
};

module.exports = Boolean;