// hieupv.est@gmail.com
var BaseModel = require('../base');
var inherits = require('inherits');
var Element = require('./element');

function choiceMultiAnswerVertical(options) {
    this.id = null;
    this.type = 'choiceMultiAnswerVertical';
    this.name = 'choiceMultiAnswerVertical_' + Math.random();
    this.title = '';
    this.value = null;
    this.items = [];

    BaseModel.call(this, options);
}
inherits(choiceMultiAnswerVertical, BaseModel);


choiceMultiAnswerVertical.prototype.add = function(item) {
    if (!(item instanceof Element)) {
        throw new Error('an Element object is expected');
    }
    this.items.push(item);
    return this;
};

choiceMultiAnswerVertical.prototype.getValue = function() {
    return this.values;
};


choiceMultiAnswerVertical.prototype.getItems = function() {
    return this.items;
};

choiceMultiAnswerVertical.prototype.setValue = function(val) {
    if (!Array.isArray(val)) {
        throw new Error('an array is expected');
    }
    this.values = val;
    return this;
};

module.exports = choiceMultiAnswerVertical;
