// hieupv.est@gmail.com
var BaseModel = require('../base');
var inherits = require('inherits');
var Element = require('./element');

function fileUpload(options) {
    this.id = null;
    this.type = 'fileUpload';
    this.name = 'fileUpload_' + Math.random();
    this.title = '';
    this.value = null;
    
    BaseModel.call(this, options);
}
inherits(fileUpload, BaseModel);


fileUpload.prototype.getValue = function() {
    return this.value;
};

fileUpload.prototype.setValue = function(val) {
    this.value = val;
    return this;
};


module.exports = fileUpload;
