// hieupv.est@gmail.com
var BaseModel = require('../base');
var inherits = require('inherits');
var Element = require('./element');
var choiceSingleAnswerHorizontal = require('./choiceSingleAnswerHorizontal');

function matrixSingleAnswerPerRow(options) {
    this.id = null;
    this.type = 'matrixSingleAnswerPerRow';
    this.name = 'matrixSingleAnswerPerRow_' + Math.random();
    this.title = '';
    this.values = [];
    this.items = [];

    BaseModel.call(this, options);
}
inherits(matrixSingleAnswerPerRow, BaseModel);


matrixSingleAnswerPerRow.prototype.add = function(item) {
    if (!(item instanceof Element)) {
        throw new Error('an Element object is expected');
    }
    this.items.push(item);
    return this;
};

matrixSingleAnswerPerRow.prototype.getValue = function() {
    return this.values;
};


matrixSingleAnswerPerRow.prototype.getItems = function() {
    return this.items;
};

matrixSingleAnswerPerRow.prototype.addValue = function (val) {
    var el = new choiceSingleAnswerHorizontal(val);
    var items = this.getItems();
    items.forEach(function (item) {
        var element = new Element(item);
        el.add(element);
    });
    this.values.push(el);
    return this;
};

matrixSingleAnswerPerRow.prototype.setValue = function(val) {
    if (!Array.isArray(val)) {
        throw new Error('an array is expected');
    }
    var items = this.getItems();
    val.forEach(function(v, k) {
        val[k] = new choiceSingleAnswerHorizontal(v);
        items.forEach(function(item) {
            var element = new Element(item);
            val[k].add(element);
        });
    });
    this.values = val;
    return this;
};

module.exports = matrixSingleAnswerPerRow;
