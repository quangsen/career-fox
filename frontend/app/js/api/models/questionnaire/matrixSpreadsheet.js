// hieupv.est@gmail.com
var BaseModel = require('../base');
var inherits = require('inherits');
var Element = require('./element');
var choiceMultiAnswerHorizontal = require('./choiceMultiAnswerHorizontal');

function matrixSpreadsheet(options) {
    this.id = null;
    this.type = 'matrixSpreadsheet';
    this.name = 'matrixSpreadsheet_' + Math.random();
    this.title = '';
    this.values = [];
    this.items = [];

    BaseModel.call(this, options);
}
inherits(matrixSpreadsheet, BaseModel);


matrixSpreadsheet.prototype.add = function(item) {
    if (!(item instanceof Element)) {
        throw new Error('an Element object is expected');
    }
    this.items.push(item);
    return this;
};

matrixSpreadsheet.prototype.getValue = function() {
    return this.values;
};

matrixSpreadsheet.prototype.addValue = function (val) {
    var el = new choiceMultiAnswerHorizontal(val);
    var items = this.getItems();
    items.forEach(function (item) {
        var element = new Element(item);
        el.add(element);
    });
    this.values.push(el);
    return this;
};


matrixSpreadsheet.prototype.getItems = function() {
    return this.items;
};

matrixSpreadsheet.prototype.setValue = function(val) {
    if (!Array.isArray(val)) {
        throw new Error('an array is expected');
    }
    var items = this.getItems();
    val.forEach(function(v, k) {
        val[k] = new choiceMultiAnswerHorizontal(v);
        items.forEach(function(item) {
            var element = new Element(item);
            val[k].add(element);
        });
    });
    this.values = val;
    return this;
};

module.exports = matrixSpreadsheet;
