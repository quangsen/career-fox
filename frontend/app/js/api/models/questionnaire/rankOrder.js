// hieupv.est@gmail.com
var BaseModel = require('../base');
var inherits = require('inherits');
var Element = require('./element');

function rankOrder(options) {
    this.id = null;
    this.type = 'rankOrder';
    this.name = 'rankOrder_' + Math.random();
    this.title = '';
    this.description = '';
    this.items = [];
    
    BaseModel.call(this, options);
}
inherits(rankOrder, BaseModel);


rankOrder.prototype.add = function(item) {
    if (!(item instanceof Element)) {
        throw new Error('an Element object is expected');
    }
    this.items.push(item);
    return this;
};

rankOrder.prototype.getValue = function() {
    return this.values;
};


rankOrder.prototype.getItems = function() {
    return this.items;
};

rankOrder.prototype.setValue = function(val) {
    if (!Array.isArray(val)) {
        throw new Error('an array is expected');
    }
    this.values = val;
    return this;
};


module.exports = rankOrder;
