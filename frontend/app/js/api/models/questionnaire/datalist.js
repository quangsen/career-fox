// hieupv.est@gmail.com
var BaseModel = require('../base');
var inherits = require('inherits');
var choiceSingleAnswerDropdown = require('./choiceSingleAnswerDropdown.js');
var openEndedTextOneLine = require('./openEndedTextOneLine');

function dataList(options) {
    this.id = null;
    this.type = 'dataList';
    this.name = 'dataList_' + Math.random();
    this.title = '';
    this.values = [];

    BaseModel.call(this, options);
}
inherits(dataList, BaseModel);

dataList.prototype.add = function(item) {
    if ((item instanceof choiceSingleAnswerDropdown) || (item instanceof openEndedTextOneLine)) {
        this.values.push(item);
        return this;
    } else {
        throw new Error('choiceSingleAnswerDropdown or openEndedTextOneLine object are expected');
    }
};

dataList.prototype.getValue = function() {
    return this.values;
};


module.exports = dataList;
