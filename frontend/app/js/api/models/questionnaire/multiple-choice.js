/**
 * Created by quynh on 10/24/2015.
 */
var BaseModel = require('../base');
var inherits = require('inherits');
var Element = require('./element');

function MultipleChoice(options) {
    this.id = null;
    this.type = 'multiple-choice';
    this.name = 'multiple_choice_'+ Math.random();
    this.title = 'Question ?';
    this.values = [];
    this.items = [new Element({title:'Option 1'})];

    BaseModel.call(this, options);
}
inherits(MultipleChoice, BaseModel);

MultipleChoice.prototype.add = function (item) {
    if (!(item instanceof Element)) {
        throw new Error('an Element object is expected');
    }
    this.items.push(item);
    return this;
};

MultipleChoice.prototype.getValue = function () {
    return this.values;
};


MultipleChoice.prototype.getItems=function(){
    return this.items;
};

MultipleChoice.prototype.setValue = function (val) {
    if (!Array.isArray(val)) {
        throw new Error('an array is expected');
    }
    this.values = val;
    return this;
};

module.exports = MultipleChoice;