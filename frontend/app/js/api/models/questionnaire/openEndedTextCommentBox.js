// hieupv.est@gmail.com
var BaseModel = require('../base');
var inherits = require('inherits');
var Element = require('./element');

function openEndedTextCommentBox(options) {
    this.id = null;
    this.type = 'openEndedTextCommentBox';
    this.name = 'openEndedTextCommentBox_' + Math.random();
    this.title = '';
    this.value = '';
    
    BaseModel.call(this, options);
}
inherits(openEndedTextCommentBox, BaseModel);


openEndedTextCommentBox.prototype.getValue = function() {
    return this.value;
};

openEndedTextCommentBox.prototype.setValue = function(val) {
    this.value = val;
};


module.exports = openEndedTextCommentBox;
