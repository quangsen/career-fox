var BaseModel = require('./base');
var inherits = require('inherits');
var Province = require('./province');

function City(options) {
    this.id = null;
    this.name = '';
    this.code = '';
    this.province_id = null;
    this.province = new Province();
    BaseModel.call(this, options);
}

inherits(City, BaseModel);
//
City.prototype.bind = function(options) {
    options = options || {};
    BaseModel.prototype.bind.call(this, options);
    if (options.province && options.province.data) {
        this.province = new Province(options.province.data);
    }
};

City.prototype.toJson = function() {
    return {
        id: this.id,
        name: this.name,
        code: this.code
    };
};
City.prototype.setProvince = function(province) {
    this.province = province;
};

module.exports = City;