var BaseModel = require('./base');
var inherits = require('inherits');

function Job(options) {
    this.id = null;
    this.category_id = null;
    this.company_id = null;
    this.description = '';
    this.postal = '';
    this.title = '';
    this.user_id = null;
    this.status = '';
    this.age = '';
    this.candidates = '';

    this.city = null;
    this.province = null;
    this.job_category = null;
    this.checked = false;
    this.city_name = null;
    this.priority = 'standard';

    BaseModel.call(this, options);
}
inherits(Job, BaseModel);

Job.prototype.bind = function(options) {
    options = options || {};
    BaseModel.prototype.bind.call(this, options);

    var City = require('./city');
    var Province = require('./province');
    var Category = require('./category');

    if (options.city) {
        this.city = new City(options.city.data);
    }

    if (options.province) {
        this.province = new Province(options.province.data);
    }

    if (options.category) {
        this.category = new Category(options.category.data);
    }
};

Job.prototype.toJson = function() {
    return {
        title: this.title,
        postal: this.postal,
        description: this.description,
        priority: this.priority,

        province_id: this.province.getId(),
        city_id: this.city.getId(),
        category_id: this.category.getId()
    };
};

Job.prototype.getLocation = function() {
    return this.city_name;
};

Job.prototype.isActive = function() {
    return (this.status === 'active');
};

module.exports = Job;