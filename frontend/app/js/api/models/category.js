var BaseModel = require('./base');
var inherits = require('inherits');

function Category(options) {
    this.id = null;
    this.title = '';
    BaseModel.call(this, options);
}
inherits(Category, BaseModel);
Category.prototype.toJson = function() {
    return {
        id: this.id,
        title: this.title
    };
};
module.exports = Category;