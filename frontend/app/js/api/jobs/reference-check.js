var RecordSet = require('../recordset');
var Candidate = require('../models/candidate');

(function(app) {
    app.factory('jobReferenceCheckServices', theFactory);

    theFactory.$inject = [
        '$q',
        '$resource',
        'apiUrl'
    ];

    function theFactory(
        $q,
        $resource,
        apiUrl
    ) {
        var service = {
            list: getList,
            invite: invite
        };

        var url = apiUrl.get('jobs/:job_id/reference-checks');
        var resource = $resource(url, {}, {});

        function getList(jobId, params) {
            var deferred = $q.defer();
            params = params || {};

            params.job_id = jobId;

            resource.get(params, function(result) {
                var data = result.data || [];
                var items = [];
                for (var i = 0, n = data.length; i < n; i++) {
                    var item = data[i];
                    var model = new Candidate(item);
                    items.push(model);
                }
                var recordSet = new RecordSet(items, result.meta.pagination);
                deferred.resolve(recordSet);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function invite(id) {
            var url = apiUrl.get('reference-checks/:id/offer-invitation');
            var resource = $resource(url, {}, {
                invite: {
                    method: 'POST'
                }
            });

            var params = {
                id: id
            };

            var data = {};

            var deferred = $q.defer();

            resource.invite(params, data, function(result) {
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        return service;
    }
})(angular.module('fox.api.jobs.reference-check', []));