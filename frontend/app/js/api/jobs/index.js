var RecordSet = require('../recordset');
var Job = require('../models/job');
require('./candidate');
require('./video-interview');
require('./questionnaire');
require('./interview-note');
require('./reference-check');
require('./offer');

(function(app) {
    app.factory('jobsServices', theFactory);

    theFactory.$inject = [
        '$q',
        '$resource',
        'apiUrl',
        'jobCandidatesServices',
        'jobVideoInterviewServices',
        'jobQuestionnaireServices',
        'jobInterviewNoteServices',
        'jobReferenceCheckServices',
        'jobOfferServices'
    ];

    function theFactory(
        $q,
        $resource,
        apiUrl,
        jobCandidatesServices,
        jobVideoInterviewServices,
        jobQuestionnaireServices,
        jobInterviewNoteServices,
        jobReferenceCheckServices,
        jobOfferServices
    ) {
        var service = {
            list: getList,
            del: del,
            get: get,
            create: create,
            update: update,
            listCandidates: listCandidates,
            candidates: jobCandidatesServices,
            postJob: postJob,
            videoInterview: jobVideoInterviewServices,
            questionnaire: jobQuestionnaireServices,
            interviewNote: jobInterviewNoteServices,
            reference: jobReferenceCheckServices,
            offer: jobOfferServices,
            getMonthlyJob: getMonthlyJob
        };

        function listCandidates(jobId, params) {
            return jobCandidatesServices.list(jobId, params);
        }

        var url = apiUrl.get('jobs/:id');
        var resource = $resource(url, {}, {
            del: {
                method: 'DELETE'
            },

            create: {
                method: 'POST'
            },

            update: {
                method: 'PUT'
            }
        });

        function postJob(jobId) {
            var url = apiUrl.get('post_job');
            var resource = $resource(url, {}, {
                update: {
                    method: 'POST'
                }
            });
            var deferred = $q.defer();
            var data = {
                job_id: jobId
            };
            resource.update(data, function(result) {
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            });

            return deferred.promise;
        }

        function update(job) {
            var deferred = $q.defer();
            var data = job.toJson();
            var params = {
                id: job.getId()
            };
            resource.update(params, data, function(result) {
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            });

            return deferred.promise;
        }

        function create(job) {
            var deferred = $q.defer();
            var data = job.toJson();
            resource.create(data, function(result) {
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            });

            return deferred.promise;
        }

        function get(id, params) {
            params = params || {};
            params.id = id;

            var deferred = $q.defer();
            resource.get(params, function(result) {
                var model = new Job(result.data);
                deferred.resolve(model);
            }, function(err) {
                deferred.reject(err);
            });

            return deferred.promise;
        }

        function getList(params) {
            var deferred = $q.defer();
            params = params || {};

            resource.get(params, function(result) {
                var data = result.data || [];
                var items = [];
                for (var i = 0, n = data.length; i < n; i++) {
                    var item = data[i];
                    var model = new Job(item);

                    if (null === model) {
                        continue;
                    }

                    items.push(model);
                }

                var recordSet = new RecordSet(items, result.meta.pagination);
                deferred.resolve(recordSet);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function del(id) {
            var deferred = $q.defer();
            var params = {
                id: id
            };

            resource.del(params, function(result) {
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }
		
		var mUrl = apiUrl.get('monthly-job');
        var monthResource = $resource(mUrl, {}, {
            get: {
                method: 'GET',
				isArray: true
            }
        });
		
		function getMonthlyJob() {
            var deferred = $q.defer();
            
            monthResource.get(function(result) {
				
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        return service;
    }
})(angular.module('fox.api.jobs', [
    'fox.api.jobs.candidates',
    'fox.api.jobs.video-interview',
    'fox.api.jobs.questionnaire',
    'fox.api.jobs.interview-note',
    'fox.api.jobs.reference-check',
    'fox.api.jobs.offer'
]));