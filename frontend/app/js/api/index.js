require('./jobs/');
require('./jobs/question');
require('./candidates/');
require('./employees/');
require('./surveys/survey');
require('./invoices');
require('./company');
require('./users');
var models = require('./models/');

(function(app) {
    app.factory('API', theFactory);

    theFactory.$inject = [
        'jobsServices',
        'candidatesServices',
        'questionServices',
        'employeeServices',
        'surveyServices',
        'invoicesServices',
        'companyService',
        'userServices'
    ];

    function theFactory(
        jobsServices,
        candidatesServices,
        questionServices,
        employeeServices,
        surveyServices,
        invoicesServices,
        companyService,
        userServices
    ) {
        return {
            jobs: jobsServices,
            candidates: candidatesServices,
            question: questionServices,
            employees: employeeServices,
            surveys: surveyServices,
            models: models,
            invoices: invoicesServices,
            company: companyService,
            user: userServices
        };
    }
})(angular.module('fox.api', [
    'fox.api.jobs',
    'fox.api.candidates',
    'fox.api.employees',
    'fox.api.questions',
    'fox.api.surveys',
    'fox.api.invoices',
    'fox.api.company',
    'fox.api.users'
]));
