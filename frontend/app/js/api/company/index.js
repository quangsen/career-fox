var Company = require('../models/company');

(function(app) {
    app.factory('companyService', theFactory);

    theFactory.$inject = [
        '$q',
        '$resource',
        'apiUrl'
    ];

    function theFactory(
        $q,
        $resource,
        apiUrl
    ) {
        var service = {
            getCompanyInfo: getCompanyInfo,
            update: update,
			allCompanies: allCompanies
        };

        var url = apiUrl.get('companies/:company_id');
        var companyResource = $resource(url, {}, {
            getInfo: {
                method: 'GET'
            },
            update: {
                method: 'PUT'
            }
        });
		
		var allCompaniesResource = $resource(apiUrl.get('companies'), {}, {
            get: {
                method: 'GET'
            }
        });

		function allCompanies() {
            var defferred = $q.defer();
            allCompaniesResource.get(function(result) {
                defferred.resolve(new Company(result.data));
            }, function(err) {
                defferred.reject(err);
            });
            return defferred.promise;
        }
		
        function getCompanyInfo(company_id) {
            var defferred = $q.defer();
            var params = {
                company_id: company_id
            };
            companyResource.getInfo(params, function(result) {
                defferred.resolve(new Company(result.data));
            }, function(err) {
                defferred.reject(err);
            });
            return defferred.promise;
        }

        function update(data) {
            var defferred = $q.defer();
            data = data || {};
            companyResource.update({
                company_id: data.id
            }, data, function(result) {
                defferred.resolve(result);
            }, function(err) {
                defferred.reject(err);
            });
            return defferred.promise;
        }
        return service;
    }
})(angular.module('fox.api.company', []));
