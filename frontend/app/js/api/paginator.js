function Paginator(options) {
    this.total = 0;
    this.count = 0;
    this.current_page = 1;
    this.total_pages = 0;
    this.per_page = 20;

    this.bind(options);
}

Paginator.prototype.bind = function(options) {
    options = options || {};
    for (var k in options) {
        var v = options[k];
        if (typeof v === 'function') {
            continue;
        }

        if (!this.hasOwnProperty(k)) {
            continue;
        }
        this[k] = v;
    }
};

Paginator.prototype.getPage = function() {
    return Number(this.current_page);
};

Paginator.prototype.getTotalPages = function() {
    return Number(this.total_pages);
};

Paginator.prototype.getTotal = function() {
    return Number(this.total);
};

Paginator.prototype.getCount = function() {
    return Number(this.count);
};

Paginator.prototype.getLimit = function() {
    return Number(this.per_page);
};

Paginator.prototype.hasPrev = function() {
    return (this.getPage() > 1);
};

Paginator.prototype.hasNext = function() {
    return (this.getPage() < this.getTotalPages());
};

Paginator.prototype.next = function() {
    if (!this.hasNext()) {
        return false;
    }
    this.current_page++;
    return true;
};

Paginator.prototype.prev = function() {
    if (!this.hasPrev()) {
        return false;
    }
    this.current_page--;
    return true;
};

module.exports = Paginator;