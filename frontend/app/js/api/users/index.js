var User = require('../models/user');

(function(app) {
    app.factory('userServices', theFactory);

    theFactory.$inject = [
        '$q',
        '$resource',
        'apiUrl'
    ];

    function theFactory(
        $q,
        $resource,
        apiUrl
    ) {
        var service = {
            get: get,
            create: create
        };

        var url = apiUrl.get('users/:user_id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            }
        });

        function get(user_id) {
            var defferred = $q.defer();
            var params = {
                user_id: user_id,
            };
            resource.get(params, function(result) {
                var user = new User(result.data);
                defferred.resolve(user);
            }, function(err) {
                throw err;
            });
            return defferred.promise;
        }

        function create(model) {
            var defferred = $q.defer();
            model = model || {};
            resource.create({}, model, function(result) {
                var user = new User(result.data);
                defferred.resolve(user);
            }, function(err) {
                throw err;
            });
            return defferred.promise;
        }

        return service;
    }
})(angular.module('fox.api.users', []));
