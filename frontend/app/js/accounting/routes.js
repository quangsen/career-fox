(function(app) {
    app.config([
        '$stateProvider',
        function($stateProvider) {
            $stateProvider
                .state('fox.accounting', {
                    abstract: true,
                    template: '<div ui-view class="fade-in-up"></div>',
                    resolve: {}
                })
                .state('fox.accounting.subscription', {
                    url: '/accounting/subscription',
                    templateUrl: 'accounting/subscription.tpl.html',
                    controller: 'AccountingSubscriptionController',
                    controllerAs: 'vm',
                    resolve: {}
                })
                .state('fox.accounting.accounting', {
                    url: '/accounting',
                    templateUrl: 'accounting/index.tpl.html',
                    controller: 'AccountingController',
                    controllerAs: 'vm',
                    resolve: {}
                })
                .state('fox.accounting.revenue', {
                    url: '/accounting/revenue',
                    templateUrl: 'accounting/revenue.tpl.html',
                    controller: 'AccountingRevenueController',
                    controllerAs: 'vm',
                    resolve: {}
                })
                .state('fox.accounting.invoices', {
                    url: '/accounting/invoices',
                    templateUrl: 'accounting/invoices.tpl.html',
                    controller: 'AccountingInvoicesController',
                    controllerAs: 'vm',
                    resolve: {}
                })
                .state('fox.accounting.payment-details', {
                    url: '/accounting/payment-details',
                    templateUrl: 'accounting/payment-details.tpl.html',
                    controller: 'AccountingPaymentDetailController',
                    controllerAs: 'vm',
                    resolve: {}
                })
                .state('fox.accounting.create-invoices', {
                    url: '/accounting/create-invoices',
                    templateUrl: 'accounting/create-invoices.tpl.html',
                    controller: 'CreateInvoicesController',
                    controllerAs: 'vm',
                    resolve: {
                        company: function($stateParams, API) {
                            return API.company.allCompanies();
                        }
                    }
                });
        }
    ]);
})(angular.module('fox.accounting.routes', []));
