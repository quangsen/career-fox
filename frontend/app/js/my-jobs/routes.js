(function(app) {
    app.config([
        '$stateProvider',
        function($stateProvider) {
            $stateProvider
                .state('fox.my-jobs', {
                    abstract: true,
                    template: '<div ui-view class="fade-in-up"></div>',
                    resolve: {}
                })
                .state('fox.my-jobs.list', {
                    url: '/my-jobs?status',
                    templateUrl: 'my-jobs/list.tpl.html',
                    controller: 'MyJobsController',
                    controllerAs: 'vm',
                    resolve: {}
                });
        }
    ]);
})(angular.module('fox.my-jobs.routes', []));