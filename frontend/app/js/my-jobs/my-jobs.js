(function(app) {
    app.run(run);
    app.controller('MyJobsController', controller);


    run.$inject = ['API', '$state', '$rootScope'];

    function run(API, $state, $rootScope) {
        var vm = this;
        $rootScope.jobsCount = [];
        var status = [0, 1];
        $rootScope.GoToMyJobsPage = function() {
            $state.go('fox.my-jobs.list', {
                status: null
            });
        };

        getJobCount(status);

        $rootScope.$on('UserLogin', function(evt, user) {
            getJobCount(status);
        });

        function getJobCount(status) {
            status.forEach(function(value) {
                var params = {
                    status: value,
                };
                API.jobs.list(params)
                    .then(function(result) {
                        $rootScope.jobsCount['status-' + value] = result.paginator.total;
                    });
            });
        }
    }

    controller.$inject = [
        '$state',
        '$rootScope',
        '$stateParams',
        'API',
        'factoryService',
        '$location',
        'dialog',
		'$scope'
    ];

    function controller(
        $state,
        $rootScope,
        $stateParams,
        API,
        factoryService,
        $location,
        dialog,
        $scope
    ) {

        var vm = this;

        vm.items = [];
        vm.paginator = factoryService.createPaginator();
        vm.status = '';
        vm.keyword = '';
        $rootScope.MyJobsListStatus = $location.$$search.status;

        if ($stateParams.status) {
            vm.status = $stateParams.status;
        }

        vm.getList = function() {
            var currentPage = 1;
            var limit = 20;
            if (vm.paginator) {
                currentPage = vm.paginator.current_page;
                limit = vm.paginator.per_page;
            }

            var params = {
                page: currentPage,
                limit: limit
            };

            var arrQ = [];
            if (vm.keyword) {
                arrQ.push('keyword:' + vm.keyword);
            }

            if (vm.status !== '') {
                arrQ.push('status:' + vm.status);
            }

            if (arrQ.length) {
                params.q = '(' + arrQ.toString() + ')';
            }

            API.jobs.list(params)
                .then(function(recordSet) {
                    vm.items = recordSet.getItems();
                    vm.paginator = recordSet.getPaginator();
                })
                .catch(function(err) {
                    throw err;
                });
        };

        vm.getList();

        vm.postAJob = function() {
            $state.go('fox.jobs-create.create');
        };

        vm.data2 = {
            labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
            datasets: [{
                label: '',
                fillColor: 'rgba(151,187,205,0.2)',
                strokeColor: 'rgba(151,187,205,1)',
                pointColor: 'rgba(151,187,205,1)',
                pointStrokeColor: '#fff',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(151,187,205,1)',
                data: [1, 9, 25, 15, 16, 21, 15]
            }]
        };

        vm.options8 = {

            maintainAspectRatio: false,

            // Sets the chart to be responsive
            responsive: true,

            ///Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,

            //String - Colour of the grid lines
            scaleGridLineColor: 'rgba(0,0,0,.05)',

            //Number - Width of the grid lines
            scaleGridLineWidth: 1,

            //Boolean - Whether the line is curved between points
            bezierCurve: false,

            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.4,

            //Boolean - Whether to show a dot for each point
            pointDot: false,

            //Number - Radius of each point dot in pixels
            pointDotRadius: 4,

            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,

            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,

            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,

            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,

            //Boolean - Whether to fill the dataset with a colour
            datasetFill: false
        };

        vm.delete = function(id) {
            dialog.confirm(null, 'Are you sure to delete?')
                .then(function(value) {
                    API.jobs.del(id)
                        .then(function(rs) {
                            vm.getList();
                        })
                        .catch(function(err) {
                            throw err;
                        });
                }, function(reason) {});
        };

        vm.search = function() {
            console.log('vm.status: ', vm.status);
            // get list by status
            vm.paginator.current_page = 1;
            vm.getList();
        };
		
		$scope.sort = {
			column: 'title',
			descending: false
		};

		$scope.selectedCls = function(column) {
			return column == $scope.sort.column && 'sort-' + $scope.sort.descending;
		};
		
		$scope.changeSorting = function(column) {
			var sort = $scope.sort;
			if (sort.column == column) {
				sort.descending = !sort.descending;
			} else {
				sort.column = column;
				sort.descending = false;
			}
		};
    }
})(angular.module('fox.my-jobs.controllers.list', []));
