require('angular-local-storage');

(function(app) {
    app.config([
        'localStorageServiceProvider',
        function(localStorageServiceProvider) {
            localStorageServiceProvider
                .setPrefix('fox')
                .setStorageType('localStorage')
                .setNotify(true, true);
        }
    ]);

    app.factory('storageService', [
        'localStorageService',
        function(localStorageService) {
            return {
                get: function(key) {
                    return localStorageService.get(key);
                },
                set: function(key, value) {
                    return localStorageService.set(key, value);
                },
                remove: function(key) {
                    localStorageService.remove(key);
                },
                clearAll: function() {
                    localStorageService.clearAll();
                }
            };
        }
    ]);
})(angular.module('fox.storage', ['LocalStorageModule']));