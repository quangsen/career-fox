require('./create');
require('./edit');

(function(app) {})(angular.module('fox.jobs.controllers.job', [
    'fox.jobs.controllers.job.create',
    'fox.jobs.controllers.job.edit'
]));