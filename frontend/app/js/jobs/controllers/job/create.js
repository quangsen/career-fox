var Job = require('../../../api/models/job');
var City = require('../../../api/models/city');
var Province = require('../../../api/models/province');
var Category = require('../../../api/models/category');

(function(app) {
    app.controller('JobsJobCreate', controller);
    controller.$inject = [
        '$rootScope',
        '$state',
        'API',
        'apiUrl',
        'dialog'
    ];

    function controller(
        $rootScope,
        $state,
        API,
        apiUrl,
        dialog
    ) {

        var vm = this;
        vm.testObj = null;

        $rootScope.app.layout.isSidebarClosed = false;

        vm.job = new Job();
        vm.city = new City();
        vm.province = new Province();
        vm.category = new Category();
        vm.initCity = {};
        vm.initCategory = {};

        vm.ckeditorOptions = {
            toolbar: 'basic',
            height: '200px'
        };

        vm.filterUrls = {
            city: apiUrl.get('cities'),
            province: apiUrl.get('provinces'),
            category: apiUrl.get('categories')
        };

        /**
         * Format Query String before sending
         * for auto complete
         *
         * @param str
         * @returns {{q: string, limit: number, fields: string}}
         */
        vm.cityRequestFormat = function(str) {
            return {
                q: '(keyword:' + str + ')',
                limit: 20,
                include: 'province'
            };
        };

        /**
         * Callback function for auto complete
         * @param object
         */
        vm.citySelectCallback = function(object) {
            if (object && object.originalObject) {
                vm.city = new City(object.originalObject);
                vm.province = vm.city.province;
            } else {
                vm.city.bind({});
                vm.province.bind({});
            }
        };

        vm.categorySelectCallback = function(object) {
            if (object && object.originalObject) {
                vm.category.bind(object.originalObject);
            } else {
                vm.category.bind({});
            }
        };


        /**
         * Format Query String before sending
         * for auto complete
         *
         * @param str
         * @returns {{q: string, limit: number, fields: string}}
         */
        vm.categoryRequestFormat = function(str) {
            return {
                q: '(keyword:' + str + ')',
                limit: 20
            };
        };

        function create() {
            // set data for job
            vm.job.city = vm.city;
            vm.job.province = vm.province;
            vm.job.category = vm.category;
            return API.jobs.create(vm.job);
        }

        function update() {
            vm.job.city = vm.city;
            vm.job.province = vm.province;
            vm.job.category = vm.category;
            return API.jobs.update(vm.job);
        }

        vm.submitForm = function() {

            // show confirm dialog
            dialog.confirm(null, 'Are you sure you would like to post a job?')
                .then(function(result) {
                    // show posting
                    var posting = dialog.showPostingJob();

                    setTimeout(function() {
                        var promise = create();
                        promise
                            .then(function(rs) {
                                posting.close(null);

                                var dlgComplete = dialog.showCompletedJob();
                                dlgComplete.closePromise.then(function() {
                                    $state.go('fox.jobs.create-interview', {
                                        id: rs.data.id
                                    });
                                });
                            }, function(err) {
                                throw err;
                            });
                        }, 5000);
                }, function(reason) {});
        };

        vm.saveDraft = function() {
            var promise = null;

            // updaye
            if (vm.job.getId()) {
                promise = update();
            } else {
                promise = create();
            }
            promise
                .then(function(rs) {
                    $state.go('fox.jobs.edit', {
                        id: rs.data.id
                    });
                }, function(err) {
                    throw err;
                });
        };

        vm.cancel = function() {
            $state.go('fox.my-jobs.list');
        };
    }
})(angular.module('fox.jobs.controllers.job.create', [
    'ckeditor',
    'angucomplete-alt'
]));