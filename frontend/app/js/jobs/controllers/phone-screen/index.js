(function(app) {
    app.controller('JobsPhoneScreen', controller);
    controller.$inject = [
        '$state',
        'dialog',
        'ngDialog',
        'job',
        'factoryService',
        'API'
    ];

    function controller(
        $state,
        dialog,
        ngDialog,
        job,
        factoryService,
        API
    ) {

        var vm = this;

        vm.items = [];
        vm.paginator = factoryService.createPaginator();
        vm.candidateSelected = null;
        vm.selectedPhoneScreen = null;

        vm.getList = function() {
            var currentPage = 1;
            var limit = 20;
            if (vm.paginator) {
                currentPage = vm.paginator.current_page;
                limit = vm.paginator.per_page;
            }

            var params = {
                page: currentPage,
                limit: limit
            };

            if (vm.keyword) {
                params.q = ('(keyword:' + vm.keyword + ')');
            }

            API.jobs.candidates.getPhoneScreenCandidates(job.getId(), params)
                .then(function(recordSet) {
                    vm.items = recordSet.getItems();
                    vm.paginator = recordSet.getPaginator();
                })
                .catch(function(err) {
                    throw err;
                });
        };

        // init functions for list candidate of a job
        vm.getList();

        vm.select = function(item) {
            vm.selectedPhoneScreen = item;
            vm.candidateSelected = item.candidate;
        };

        // validate selected candidate or not
        vm.isSelected = function() {
            if (vm.selectedPhoneScreen) {
                return true;
            }
            return false;
        };

        vm.isInvited = function() {
            if (!vm.selectedPhoneScreen) {
                return false;
            }
            return vm.selectedPhoneScreen.isInvited();
        };

        vm.showInvite = function() {
            if (!vm.isSelected()) {
                return true;
            }
			
            return !vm.selectedPhoneScreen.isInvited();
        };

        vm.isActive = function(item) {
            if (!vm.selectedPhoneScreen) {
                return false;
            }

            return (vm.selectedPhoneScreen.getId() === item.getId());
        };

        vm.gotoVideo = function() {
            $state.go('fox.jobs.review-video-interview');
        };

        vm.gotoCandidates = function() {
            $state.go('fox.jobs.candidate-review');
        };

        vm.share = function() {
            if (vm.candidateSelected) {

                var dlg = ngDialog.open({
                    className: 'ngdialog-theme-default share-candidate',
                    showClose: false,
                    resolve: {
                        phoneScreen: function() {
                            return vm.selectedPhoneScreen;
                        }
                    },
                    controller: [
                        '$scope',
                        'phoneScreen',
                        'factoryService',
                        'API',
                        function(
                            $scope,
                            phoneScreen,
                            factoryService,
                            API
                        ) {
                            $scope.phoneScreen = phoneScreen;

                            $scope.employees = [];
                            $scope.paginator = factoryService.createPaginator();

                            $scope.getList = function() {
                                var currentPage = 1;
                                var limit = 20;
                                if ($scope.paginator) {
                                    currentPage = $scope.paginator.current_page;
                                    limit = $scope.paginator.per_page;
                                }

                                var params = {
                                    page: currentPage,
                                    limit: limit
                                };

                                API.employees.list(params)
                                    .then(function(recordSet) {
                                        $scope.employees = recordSet.getItems();
                                        $scope.paginator = recordSet.getPaginator();
                                    })
                                    .catch(function(err) {
                                        throw err;
                                    });
                            };

                            $scope.share = function() {
                                // call api share

                                var ids = [];
                                angular.forEach($scope.employees, function(item) {
                                    if (item.checked) {
                                        ids.push(item.getId());
                                    }
                                });

                                if (ids.length === 0) {
                                    dialog.alert(null, 'Please select at least one employee!');
                                    return false;
                                }

                                API.jobs.candidates.share(phoneScreen.getId(), ids)
                                    .then(function(rs) {
                                        var alert = dialog.alert('', 'The candidate is shared successfully');
                                        alert.closePromise
                                            .then(function() {
                                                $scope.closeThisDialog('shared');
                                            });
                                    }, function(err) {
                                        throw err;
                                    });
                            };

                            $scope.getList();
                        }
                    ],
                    template: 'jobs/templates/phone-screen/share.tpl.html'
                });

            } else {
                dialog.alert(null, 'Please select a candidate.');
            }
        };

        vm.saveQuickNote = function() {
            if (vm.isSelected()) {
                API.jobs.candidates.saveQuickNote(job.getId(), vm.candidateSelected.getId(), vm.selectedPhoneScreen.quick_note)
                    .then(function(result) {
                        dialog.alert(null, 'Add note for candidate successfully!');
                    })
                    .catch(function(err) {
                        throw err;
                    });
            } else {
                dialog.alert(null, 'Please select a candidate.');
            }
        };

        vm.invite = function() {
            if (!vm.isSelected()) {
                return false;
            }
            
			var phonescreenId = vm.selectedPhoneScreen.getId();
			
			showDlg(phonescreenId);
        };
		
		function showDlg(phonescreenId) {
            var dlg = ngDialog.openConfirm({
                className: 'ngdialog-theme-default add-user',
                showClose: false,
                controller: ['$scope', 'API',
                    function($scope, API) {
                        $scope.phonescreenId = phonescreenId;
						$scope.minDate = new Date();
						$scope.date = {};
						$scope.date.hours = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'];
						$scope.date.min = ['00', '15', '30', '45'];

                        $scope.create = function() {
                            API.jobs.candidates.inviteToVideo(phonescreenId)
							.then(function(result) {
								vm.selectedPhoneScreen.setInvited(true);
								dialog.alert(null, 'The candidate has been invited successfully!');
							})
							.catch(function(err) {
								throw err;
							});
                        };
                    }
                ],
                template: 'jobs/templates/phone-screen/add-schedule.tpl.html'
            });
            dlg.then(function(val) {
                vm.questionPaginator.current_page = 1;
                vm.getQuestionList();
            }, function(reason) {

            });
        }
    }
})(angular.module('fox.jobs.controllers.phone-screen', []));