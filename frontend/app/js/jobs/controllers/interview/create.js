var Question = require('../../../api/models/question');

(function(app) {
    app.controller('JobsCreateInterview', controller);
    controller.$inject = [
        '$state',
        '$stateParams',
        'ngDialog',
        'API',
        'job',
        'factoryService',
        'dialog'
    ];

    function controller(
        $state,
        $stateParams,
        ngDialog,
        API,
        job,
        factoryService,
        dialog
    ) {

        var vm = this;

        var JobsList = new Array("253",200,"Aerospace Engineering and Operations Technicians","214",200,"Aerospace Engineers","215",200,"Agricultural Engineers","209",200,"Architects, Except Landscape and Naval","200",200,"Architectural Drafter","216",200,"Biomedical Engineers","211",200,"Cartographers and Photogrammetrists","201",200,"Chemical Engineer","202",200,"Civil Drafter","203",200,"Civil Engineer","254",200,"Civil Engineering Technicians","219",200,"Computer Hardware Engineers","251",200,"Electrical Drafters","257",200,"Electrical Engineering Technicians","220",200,"Electrical Engineers","258",200,"Electro-Mechanical Technicians","207",200,"Electronic Drafter","256",200,"Electronics Engineering Technicians","268",200,"Electronics Engineering Technologists","221",200,"Electronics Engineers, Except Computer","240",200,"Energy Engineers","260",200,"Environmental Engineering Technicians","223",200,"Environmental Engineers","226",200,"Fire-Prevention and Protection Engineers","213",200,"Geodetic Surveyors","204",200,"Industrial Engineer","261",200,"Industrial Engineering Technicians","225",200,"Industrial Safety and Health Engineers","210",200,"Landscape Architects","270",200,"Manufacturing Engineering Technologists","241",200,"Manufacturing Engineers","273",200,"Manufacturing Production Technicians","279",200,"Mapping Technicians","231",200,"Marine Architects","230",200,"Marine Engineers","205",200,"Materials Engineer","206",200,"Mechanical Drafter","208",200,"Mechanical Engineer","262",200,"Mechanical Engineering Technicians","234",200,"Mining and Geological Engineers, Including Mining Safety Engineers","235",200,"Nuclear Engineers","236",200,"Petroleum Engineers","244",200,"Photonics Engineers","227",200,"Product Safety Engineers","245",200,"Robotics Engineers","278",200,"Surveying Technicians","212",200,"Surveyors","239",200,"Validation Engineers","300",300,"Accountant","315",300,"Agents and Business Managers of Artists, Performers, and Athletes","301",300,"Appraiser, Real Estate","302",300,"Assessors","303",300,"Auditors","347",300,"Budget Analysts","304",300,"Claims Examiners, Property and Casualty Insurance","305",300,"Compensation, Benefits, and Job Analysis Specialists","327",300,"Coroners","329",300,"Cost Estimators","306",300,"Credit Analysts","330",300,"Emergency Management Specialists","332",300,"Employment Interviewers","340",300,"Energy Auditors","323",300,"Environmental Compliance Inspectors","325",300,"Equal Opportunity Representatives and Officers","307",300,"Financial Analysts","348",300,"Financial Examiners","356",300,"Fraud Examiners, Investigators and Analysts","326",300,"Government Property Inspectors and Investigators","308",300,"Insurance Adjusters, Examiners and Investigators","321",300,"Insurance Appraisers, Auto Damage","309",300,"Insurance Underwriters","324",300,"Licensing Examiners and Inspectors","349",300,"Loan Counselors","310",300,"Loan Officer","334",300,"Logisticians","313",300,"Management Analysts","338",300,"Meeting and Convention Planners","311",300,"Personal Financial Advisor","312",300,"Personnel Recruiters","316",300,"Purchasing Agents and Buyers, Farm Products","318",300,"Purchasing Agents, Except Wholesale, Retail, and Farm Products","328",300,"Regulatory Affairs Specialists","350",300,"Tax Examiners, Collectors, and Revenue Agents","351",300,"Tax Preparers","314",300,"Training and Development Specialists","317",300,"Wholesale and Retail Buyers, Except Farm Products","400",400,"Actuaries","431",400,"Biostatisticians","432",400,"Clinical Data Managers","411",400,"Computer and Information Scientists, Research","401",400,"Computer Programmers","402",400,"Computer Security Specialists","403",400,"Computer Software Engineers, Applications","412",400,"Computer Software Engineers, Systems Software","404",400,"Computer Support Specialist","405",400,"Computer Systems Analyst","417",400,"Computer Systems Engineers/Architects","406",400,"Database Administrator","419",400,"Geospatial Information Scientists and Technologists","413",400,"Informatics Nurse Specialists","433",400,"Mathematical Technicians","428",400,"Mathematicians","407",400,"Network and Computer System Administrators","418",400,"Network Designers","414",400,"Network Systems and Data Communications Analysts","429",400,"Operations Research Analysts","408",400,"Software Quality Assurance Engineers and Testers","430",400,"Statisticians","409",400,"Web Administrator","410",400,"Web Developer","500",500,"Adult Literacy, Remedial Education, and GED Teachers and Instructors","513",500,"Agricultural Sciences Teachers, Postsecondary","520",500,"Anthropology and Archeology Teachers, Postsecondary","511",500,"Architecture Teachers, Postsecondary","555",500,"Archivists","521",500,"Area, Ethnic, and Cultural Studies Teachers, Postsecondary","535",500,"Art, Drama, and Music Teachers, Postsecondary","516",500,"Atmospheric, Earth, Marine, and Space Sciences Teachers, Postsecondary","560",500,"Audio-Visual Collections Specialists","514",500,"Biological Science Teachers, Postsecondary","508",500,"Business Teachers, Postsecondary","517",500,"Chemistry Teachers, Postsecondary","536",500,"Communications Teachers, Postsecondary","509",500,"Computer Science Teachers, Postsecondary","532",500,"Criminal Justice and Law Enforcement Teachers, Postsecondary","556",500,"Curators","522",500,"Economics Teachers, Postsecondary","530",500,"Education Teachers, Postsecondary","501",500,"Elementary School Teachers, Except Special Education","512",500,"Engineering Teachers, Postsecondary","537",500,"English Language and Literature Teachers, Postsecondary","518",500,"Environmental Science Teachers, Postsecondary","561",500,"Farm and Home Management Advisors","538",500,"Foreign Language and Literature Teachers, Postsecondary","515",500,"Forestry and Conservation Science Teachers, Postsecondary","523",500,"Geography Teachers, Postsecondary","502",500,"Graduate Teaching Assistants","528",500,"Health Specialties Teachers, Postsecondary","539",500,"History Teachers, Postsecondary","541",500,"Home Economics Teachers, Postsecondary","503",500,"Instructional Coordinators","504",500,"Kindergarten Teachers, Except Special Education","533",500,"Law Teachers, Postsecondary","558",500,"Librarians","531",500,"Library Science Teachers, Postsecondary","559",500,"Library Technicians","510",500,"Mathematical Science Teachers, Postsecondary","545",500,"Middle School Teachers, Except Special and Vocational Education","557",500,"Museum Technicians and Conservators","529",500,"Nursing Instructors and Teachers, Postsecondary","540",500,"Philosophy and Religion Teachers, Postsecondary","519",500,"Physics Teachers, Postsecondary","524",500,"Political Science Teachers, Postsecondary","505",500,"Preschool Teachers, Except Special Education","525",500,"Psychology Teachers, Postsecondary","542",500,"Recreation and Fitness Studies Teachers, Postsecondary","506",500,"Secondary School Teachers, Except Special and Vocational Education","551",500,"Self-Enrichment Education Teachers","534",500,"Social Work Teachers, Postsecondary","526",500,"Sociology Teachers, Postsecondary","549",500,"Special Education Teachers, Middle School","548",500,"Special Education Teachers, Preschool, Kindergarten, and Elementary School","550",500,"Special Education Teachers, Secondary School","507",500,"Teacher Assistants","546",500,"Vocational Education Teachers, Middle School","543",500,"Vocational Education Teachers, Postsecondary","547",500,"Vocational Education Teachers, Secondary School","600",600,"Acute Care Nurses","648",600,"Advanced Practice Psychiatric Nurses","625",600,"Anesthesiologists","688",600,"Athletic Trainers","649",600,"Audiologists","667",600,"Cardiovascular Technologists and Technicians","616",600,"Chiropractors","849",600,"Clinical Nurse Specialists","601",600,"Critical Care Nurses","663",600,"Cytogenetic Technologists","664",600,"Cytotechnologists","696",600,"Dental Assistants","666",600,"Dental Hygienists","617",600,"Dentists, General","668",600,"Diagnostic Medical Sonographers","672",600,"Dietetic Technicians","622",600,"Dietitians and Nutritionists","682",600,"Electroneurodiagnostic Technologists","671",600,"Emergency Medical Technicians and Paramedics","698",600,"Endoscopy Technicians","626",600,"Family and General Practitioners","691",600,"Genetic Counselors","665",600,"Histotechnologists and Histologic Technicians","611",600,"Home Health Aides","635",600,"Hospitalists","627",600,"Internists, General","602",600,"Licensed Practical and Licensed Vocational Nurses","650",600,"Low Vision Therapists, Orientation and Mobility Specialists, and Vision Rehabilitation Therapists","695",600,"Massage Therapists","603",600,"Medical and Clinical Laboratory Technicians","604",600,"Medical and Clinical Laboratory Technologists","612",600,"Medical Assistants","697",600,"Medical Equipment Preparers","678",600,"Medical Records and Health Information Technicians","699",600,"Medical Transcriptionists","660",600,"Naturopathic Physicians","637",600,"Nuclear Medicine Physicians","669",600,"Nuclear Medicine Technologists","659",600,"Nurse Anesthetists","685",600,"Nurse Midwives","605",600,"Nurse Practitioners","613",600,"Nursing Aides, Orderlies, and Attendants","628",600,"Obstetricians and Gynecologists","686",600,"Occupational Health and Safety Specialists","687",600,"Occupational Health and Safety Technicians","693",600,"Occupational Therapist Aides","614",600,"Occupational Therapist Assistants","606",600,"Occupational Therapists","684",600,"Ophthalmic Medical Technologists and Technicians","679",600,"Opticians, Dispensing","623",600,"Optometrists","618",600,"Oral and Maxillofacial Surgeons","619",600,"Orthodontists","661",600,"Orthoptists","680",600,"Orthotists and Prosthetists","629",600,"Pediatricians, General","624",600,"Pharmacists","7000",600,"Pharmacy Aides","673",600,"Pharmacy Technicians","694",600,"Physical Therapist Aides","615",600,"Physical Therapist Assistants","651",600,"Physical Therapists","645",600,"Physician Assistants","647",600,"Podiatrists","641",600,"Preventive Medicine Physicians","620",600,"Prosthodontists","692",600,"Psychiatric Aides","674",600,"Psychiatric Technicians","630",600,"Psychiatrists","652",600,"Radiation Therapists","607",600,"Radiologic Technicians","608",600,"Radiologic Technologists","653",600,"Recreational Therapists","609",600,"Registered Nurses","610",600,"Respiratory Therapists","675",600,"Respiratory Therapy Technicians","654",600,"Speech-Language Pathologists","631",600,"Surgeons","676",600,"Surgical Technologists","656",600,"Veterinarians","7001",600,"Veterinary Assistants and Laboratory Animal Caretakers","677",600,"Veterinary Technologists and Technicians","706",700,"Administrative Law Judges, Adjudicators, and Hearing Officers","700",700,"Arbitrators, Mediators, and Conciliators","701",700,"Court Reporters","707",700,"Judges, Magistrate Judges, and Magistrates","708",700,"Law Clerks","705",700,"Lawyers","702",700,"Paralegals and Legal Assistants","704",700,"Production Coordinator (Legal Scanning &amp; Coding)","703",700,"Title Examiners, Abstractors, and Searchers","800",800,"Administrative Services Manager","817",800,"Advertising and Promotions Managers","834",800,"Aquacultural Managers","813",800,"Chief Executive","877",800,"Chief Information Officer","801",800,"Compensation and Benefits Manager","802",800,"Computer and Information Systems Manager","836",800,"Construction Managers","833",800,"Crop and Livestock Managers","882",800,"Director of Finance","880",800,"Director of Sales","883",800,"Director Platform Engineering","878",800,"Director Strategic Sourcing","838",800,"Education Administrators, Elementary and Secondary School","839",800,"Education Administrators, Postsecondary","837",800,"Education Administrators, Preschool and Child Care Center/Program","803",800,"Engineering Manager","835",800,"Farmers and Ranchers","804",800,"Financial Managers, Branch or Department","844",800,"Food Service Managers","845",800,"Funeral Directors","846",800,"Gaming Managers","806",800,"General and Operations Manager","881",800,"Global Product Director","807",800,"Human Resource Manager","822",800,"Industrial Production Managers","847",800,"Lodging Managers","808",800,"Marketing Manager","848",800,"Medical and Health Services Managers","850",800,"Natural Sciences Managers","832",800,"Nursery and Greenhouse Managers","868",800,"Personnel Staffing Manager","853",800,"Postmasters and Mail Superintendents","854",800,"Property, Real Estate, and Community Association Managers","814",800,"Public Relations Managers","809",800,"Purchasing Manager","857",800,"Regulatory Affairs Managers","810",800,"Sales Manager","855",800,"Social and Community Service Managers","830",800,"Storage and Distribution Managers","860",800,"Supply Chain Managers","812",800,"Training and Development Manager","811",800,"Transportation Manager","820",800,"Treasurers and Controllers","874",800,"Vice President Human Resources","875",800,"Vice President Marketing","876",800,"Vice President Operations","879",800,"Vice President Sales","900",900,"Bill and Account Collectors","916",900,"Billing, Cost, and Rate Clerks","917",900,"Billing, Posting, and Calculating Machine Operators","918",900,"Bookkeeping, Accounting, and Auditing Clerks","921",900,"Brokerage Clerks","940",900,"Cargo and Freight Agents","958",900,"Computer Operators","922",900,"Correspondence Clerks","942",900,"Couriers and Messengers","924",900,"Court Clerks","928",900,"Credit Authorizers","929",900,"Credit Checkers","901",900,"Customer Service Representatives","959",900,"Data Entry Keyers","961",900,"Desktop Publishers","944",900,"Dispatchers, Except Police, Fire, and Ambulance","931",900,"Eligibility Interviewers, Government Programs","902",900,"Executive Secretaries and Administrative Assistants","932",900,"File Clerks","919",900,"Gaming Cage Workers","904",900,"Hotel, Motel, and Resort Desk Clerks","905",900,"Human Resource (Non-Payroll) Assistants","963",900,"Insurance Claims Clerks","964",900,"Insurance Policy Processing Clerks","933",900,"Interviewers, Except Eligibility and Loan","906",900,"Legal Secretaries","934",900,"Library Assistants, Clerical","926",900,"License Clerks","935",900,"Loan Interviewers and Clerks","965",900,"Mail Clerks and Mail Machine Operators, Except Postal Service","953",900,"Marking Clerks","907",900,"Medical Secretaries","945",900,"Meter Readers, Utilities","925",900,"Municipal Clerks","936",900,"New Accounts Clerks","966",900,"Office Clerks, General","967",900,"Office Machine Operators, Except Computer","903",900,"Office Supervisors / Managers","937",900,"Order Clerks","955",900,"Order Fillers, Wholesale and Retail Sales","908",900,"Payroll and Timekeeping Clerks","943",900,"Police, Fire, and Ambulance Dispatchers","946",900,"Postal Service Clerks","947",900,"Postal Service Mail Carriers","948",900,"Postal Service Mail Sorters, Processors, and Processing Machine Operators","920",900,"Procurement Clerks","949",900,"Production, Planning, and Expediting Clerks","968",900,"Proofreaders and Copy Markers","909",900,"Receptionists and Information Clerks","938",900,"Reservation and Transportation Ticket Agents and Travel Clerks","957",900,"Secretaries, Except Legal, Medical, and Executive","950",900,"Shipping, Receiving, and Traffic Clerks","915",900,"Statement Clerks","969",900,"Statistical Assistants","952",900,"Stock Clerks, Sales Floor","954",900,"Stock Clerks- Stockroom, Warehouse, or Storage Yard","911",900,"Switchboard Operators, Including Answering Service","912",900,"Telephone Operators","910",900,"Tellers","956",900,"Weighers, Measurers, Checkers, and Samplers, Recordkeeping","960",900,"Word Processors and Typists","1018",1000,"Advertising Sales Agents","1014",1000,"Cashiers","1016",1000,"Counter and Rental Clerks","1025",1000,"Demonstrators and Product Promoters","1028",1000,"Door-To-Door Sales Workers, News and Street Vendors, and Related Workers","1008",1000,"Driver, Sales Workers","1013",1000,"First-Line Supervisors/Managers of Non-Retail Sales Workers","1010",1000,"First-Line Supervisors/Managers of Retail Sales Workers","1015",1000,"Gaming Change Persons and Booth Cashiers","1029",1000,"Inside Sales Rep","1000",1000,"Insurance Sales Agents","1026",1000,"Models","1017",1000,"Parts Salespersons","1001",1000,"Real Estate Brokers","1002",1000,"Real Estate Sales Agents","1003",1000,"Retail Salesperson","1004",1000,"Sales Agent, Financial Sales","1012",1000,"Sales Agents Automobiles and Trucks","1011",1000,"Sales Agents Securities and Commodities","1005",1000,"Sales Engineers","1009",1000,"Sales Managers","1007",1000,"Sales Rep, Technical &amp; Scientific Products","1006",1000,"Sales Rep, Wholesale &amp; Manufacturing","1027",1000,"Telemarketers","1021",1000,"Travel Agents","1114",1100,"Air Traffic Controllers","1100",1100,"Aircraft Cargo Handling Supervisors","1101",1100,"Airfield Operations Specialists","1102",1100,"Airline Pilots, Copilots, and Flight Engineers","1103",1100,"Ambulance Drivers and Attendants, Except Emergency Medical Technicians","1136",1100,"Aviation Inspectors","1132",1100,"Bridge and Lock Tenders","1104",1100,"Bus Drivers, School","1105",1100,"Bus Drivers, Transit and Intercity","1146",1100,"Cleaners of Vehicles and Equipment","1113",1100,"Commercial Pilots","1140",1100,"Conveyor Operators and Tenders","1141",1100,"Crane and Tower Operators","1142",1100,"Dredge Operators","1115",1100,"Driver/Sales Workers","1143",1100,"Excavating and Loading Machine and Dragline Operators","1110",1100,"First-Line Supervisors/Managers of Helpers, Laborers, and Material Movers, Hand","1112",1100,"First-Line Supervisors/Managers of Transportation and Material-Moving Machine and Vehicle Operators","1138",1100,"Freight and Cargo Inspectors","1150",1100,"Gas Compressor and Gas Pumping Station Operators","1145",1100,"Hoist and Winch Operators","1106",1100,"Industrial Truck and Tractor Operators","1147",1100,"Laborers and Freight, Stock, and Material Movers, Hand","1144",1100,"Loading Machine Operators, Underground Mining","1118",1100,"Locomotive Engineers","1119",1100,"Locomotive Firers","1148",1100,"Machine Feeders and Offbearers","1128",1100,"Mates- Ship, Boat, and Barge","1130",1100,"Motorboat Operators","1149",1100,"Packers and Packagers, Hand","1133",1100,"Parking Lot Attendants","1129",1100,"Pilots, Ship","1151",1100,"Pump Operators, Except Wellhead Pumpers","1120",1100,"Rail Yard Engineers, Dinkey Operators, and Hostlers","1121",1100,"Railroad Brake, Signal, and Switch Operators","1122",1100,"Railroad Conductors and Yardmasters","1153",1100,"Refuse and Recyclable Material Collectors","1125",1100,"Sailors and Marine Oilers","1107",1100,"Service Station Attendants","1127",1100,"Ship and Boat Captains","1131",1100,"Ship Engineers","1154",1100,"Shuttle Car Operators","1123",1100,"Subway and Streetcar Operators","1155",1100,"Tank Car, Truck, and Ship Loaders","1116",1100,"Taxi Drivers and Chauffeurs","1134",1100,"Traffic Technicians","1137",1100,"Transportation Vehicle, Equipment and Systems Inspectors, Except Aviation","1108",1100,"Truck Drivers, Heavy and Tractor-Trailer","1109",1100,"Truck Drivers, Light or Delivery Services","1152",1100,"Wellhead Pumpers","1200",1200,"Bartenders","1201",1200,"Chefs and Head Cooks","1215",1200,"Combined Food Preparation and Serving Workers, Including Fast Food","1202",1200,"Cooks, Fast Food","1212",1200,"Cooks, Institution and Cafeteria","1213",1200,"Cooks, Private Household","1203",1200,"Cooks, Restaurant","1204",1200,"Cooks, Short Order","1205",1200,"Counter Attendants, Cafeteria, Food Concession, and Coffee Shop","1217",1200,"Dining Room and Cafeteria Attendants and Bartender Helpers","1206",1200,"Dishwashers","1207",1200,"First-Line Supervisors/Managers of Food Preparation and Serving Workers","1208",1200,"Food Preparation Workers","1209",1200,"Food Servers, Nonrestaurant","1210",1200,"Hosts and Hostesses, Restaurant, Lounge, and Coffee Shop","1211",1200,"Waiters and Waitresses","1300",1300,"Cabling Network Engineer","1301",1300,"Data Center Engineer/Manager","1303",1300,"Data Network Engineer","1302",1300,"Security Network Engineer","1304",1300,"Voice Network Engineer","1305",1300,"Wireless Network Engineer","1402",1400,"Call Center Manager","1400",1400,"Customer Service Representative (Inbound)","1401",1400,"Telemarketer (Outbound)","1531",1500,"Animal Control Workers","1519",1500,"Bailiffs","1520",1500,"Correctional Officers and Jailers","1523",1500,"Criminal Investigators and Special Agents","1533",1500,"Crossing Guards","1516",1500,"Fire Inspectors","1517",1500,"Fire Investigators","1507",1500,"First-Line Supervisors/Managers of Correctional Officers","1500",1500,"First-Line Supervisors/Managers of Police and Detectives","1526",1500,"Fish and Game Wardens","1514",1500,"Forest Fire Fighters","1510",1500,"Forest Fire Fighting and Prevention Supervisors","1518",1500,"Forest Fire Inspectors and Prevention Specialists","1532",1500,"Gaming Surveillance Officers and Gaming Investigators","1524",1500,"Immigration and Customs Inspectors","1501",1500,"Lifeguards, Ski Patrol, and Other Recreational Protective Service Workers","1513",1500,"Municipal Fire Fighters","1509",1500,"Municipal Fire Fighting and Prevention Supervisors","1527",1500,"Parking Enforcement Workers","1502",1500,"Police Detectives","1522",1500,"Police Identification and Records Officers","1503",1500,"Police Patrol Officers","1504",1500,"Private Detectives and Investigators","1505",1500,"Security Guards","1529",1500,"Sheriffs and Deputy Sheriffs","1530",1500,"Transit and Railroad Police","1506",1500,"Transportation Security Officers","1612",1600,"Amusement and Recreation Attendants","1605",1600,"Animal Trainers","1624",1600,"Baggage Porters and Bellhops","1618",1600,"Barbers","1630",1600,"Child Care Workers","1625",1600,"Concierges","1613",1600,"Costume Attendants","1616",1600,"Embalmers","1603",1600,"First-Line Supervisors/Managers of Personal Service Workers","1633",1600,"Fitness Trainers and Aerobics Instructors","1628",1600,"Flight Attendants","1617",1600,"Funeral Attendants","1608",1600,"Gaming and Sports Book Writers and Runners","1607",1600,"Gaming Dealers","1619",1600,"Hairdressers, Hairstylists, and Cosmetologists","1614",1600,"Locker Room, Coatroom, and Dressing Room Attendants","1620",1600,"Makeup Artists, Theatrical and Performance","1621",1600,"Manicurists and Pedicurists","1610",1600,"Motion Picture Projectionists","1631",1600,"Nannies","1606",1600,"Nonfarm Animal Caretakers","1632",1600,"Personal and Home Care Aides","1600",1600,"Recreation Workers","1634",1600,"Residential Advisors","1622",1600,"Shampooers","1623",1600,"Skin Care Specialists","1602",1600,"Slot Key Persons","1626",1600,"Tour Guides and Escorts","1629",1600,"Transportation Attendants, Except Flight Attendants and Baggage Porters","1627",1600,"Travel Guides","1611",1600,"Ushers, Lobby Attendants, and Ticket Takers","1954",1900,"Agricultural Technicians","1900",1900,"Animal Scientists","1946",1900,"Anthropologists","1947",1900,"Archeologists","1919",1900,"Astronomers","1921",1900,"Atmospheric and Space Scientists","1904",1900,"Biochemists and Biophysicists","1956",1900,"Biological Technicians","1903",1900,"Biologists","1957",1900,"Chemical Technicians","1922",1900,"Chemists","1965",1900,"City and Regional Planning Aides","1938",1900,"Clinical Psychologists","1939",1900,"Counseling Psychologists","1932",1900,"Economists","1966",1900,"Environmental Science and Protection Technicians, Including Health","1924",1900,"Environmental Scientists and Specialists, Including Health","1916",1900,"Epidemiologists","1955",1900,"Food Science Technicians","1901",1900,"Food Scientists and Technologists","1967",1900,"Forensic Science Technicians","1968",1900,"Forest and Conservation Technicians","1915",1900,"Foresters","1910",1900,"Geneticists","1948",1900,"Geographers","1960",1900,"Geological Sample Test Technicians","1959",1900,"Geophysical Data Technicians","1928",1900,"Geoscientists, Except Hydrologists and Geographers","1949",1900,"Historians","1929",1900,"Hydrologists","1940",1900,"Industrial-Organizational Psychologists","1934",1900,"Market Research Analysts","1923",1900,"Materials Scientists","1917",1900,"Medical Scientists, Except Epidemiologists","1905",1900,"Microbiologists","1909",1900,"Molecular and Cellular Biologists","1942",1900,"Neuropsychologists and Clinical Neuropsychologists","1962",1900,"Nuclear Equipment Operation Technicians","1963",1900,"Nuclear Monitoring Technicians","1914",1900,"Park Naturalists","1920",1900,"Physicists","1950",1900,"Political Scientists","1971",1900,"Precision Agriculture Technicians","1913",1900,"Range Managers","1931",1900,"Remote Sensing Scientists and Technologists","1937",1900,"School Psychologists","1964",1900,"Social Science Research Assistants","1943",1900,"Sociologists","1902",1900,"Soil and Plant Scientists","1912",1900,"Soil and Water Conservationists","1935",1900,"Survey Researchers","1944",1900,"Urban and Regional Planners","1906",1900,"Zoologists and Wildlife Biologists","2106",2100,"Child, Family, and School Social Workers","2114",2100,"Clergy","2115",2100,"Directors, Religious Activities and Education","2101",2100,"Educational, Vocational, and School Counselors","2110",2100,"Health Educators","2102",2100,"Marriage and Family Therapists","2107",2100,"Medical and Public Health Social Workers","2108",2100,"Mental Health and Substance Abuse Social Workers","2103",2100,"Mental Health Counselors","2111",2100,"Probation Officers and Correctional Treatment Specialists","2104",2100,"Rehabilitation Counselors","2112",2100,"Social and Human Service Assistants","2100",2100,"Substance Abuse and Behavioral Disorder Counselors","2713",2700,"Actors","2700",2700,"Art Directors","2720",2700,"Athletes and Sports Competitors","2744",2700,"Audio and Video Equipment Technicians","2734",2700,"Broadcast News Analysts","2745",2700,"Broadcast Technicians","2749",2700,"Camera Operators, Television, Video, and Motion Picture","2724",2700,"Choreographers","2721",2700,"Coaches and Scouts","2705",2700,"Commercial and Industrial Designers","2740",2700,"Copy Writers","2701",2700,"Craft Artists","2723",2700,"Dancers","2716",2700,"Directors- Stage, Motion Pictures, Television, and Radio","2737",2700,"Editors","2706",2700,"Fashion Designers","2750",2700,"Film and Video Editors","2702",2700,"Fine Artists, Including Painters, Sculptors, and Illustrators","2707",2700,"Floral Designers","2708",2700,"Graphic Designers","2709",2700,"Interior Designers","2742",2700,"Interpreters and Translators","2710",2700,"Merchandise Displayers and Window Trimmers","2703",2700,"Multi-Media Artists and Animators","2727",2700,"Music Composers and Arrangers","2726",2700,"Music Directors","2730",2700,"Musicians, Instrumental","2748",2700,"Photographers","2741",2700,"Poets, Lyricists and Creative Writers","2715",2700,"Producers","2717",2700,"Program Directors","2733",2700,"Public Address System and Other Announcers","2736",2700,"Public Relations Specialists","2732",2700,"Radio and Television Announcers","2746",2700,"Radio Operators","2735",2700,"Reporters and Correspondents","2711",2700,"Set and Exhibit Designers","2729",2700,"Singers","2747",2700,"Sound Engineering Technicians","2718",2700,"Talent Directors","2719",2700,"Technical Directors/Managers","2738",2700,"Technical Writers","2722",2700,"Umpires, Referees, and Other Sports Officials","3700",3700,"First-Line Supervisors/Managers of Housekeeping and Janitorial Workers","3701",3700,"First-Line Supervisors/Managers of Landscaping, Lawn Service, and Groundskeeping Workers","3702",3700,"Janitors and Cleaners, Except Maids and Housekeeping Cleaners","3706",3700,"Landscaping and Groundskeeping Workers","3703",3700,"Maids and Housekeeping Cleaners","3705",3700,"Pest Control Workers","3707",3700,"Pesticide Handlers, Sprayers, and Applicators, Vegetation","3708",3700,"Tree Trimmers and Pruners","4509",4500,"Agricultural Equipment Operators","4506",4500,"Agricultural Inspectors","4507",4500,"Animal Breeders","4518",4500,"Fallers","4505",4500,"Farm Labor Contractors","4512",4500,"Farmworkers and Laborers, Crop","4513",4500,"Farmworkers, Farm and Ranch Animals","4503",4500,"First-Line Supervisors/Managers of Agricultural Crop and Horticultural Workers","4504",4500,"First-Line Supervisors/Managers of Animal Husbandry and Animal Care Workers","4502",4500,"First-Line Supervisors/Managers of Aquacultural Workers","4501",4500,"First-Line Supervisors/Managers of Logging Workers","4515",4500,"Fishers and Related Fishing Workers","4517",4500,"Forest and Conservation Workers","4508",4500,"Graders and Sorters, Agricultural Products","4516",4500,"Hunters and Trappers","4520",4500,"Log Graders and Scalers","4519",4500,"Logging Equipment Operators","4511",4500,"Nursery Workers","4702",4700,"Boilermakers","4703",4700,"Brickmasons and Blockmasons","4708",4700,"Carpet Installers","4712",4700,"Cement Masons and Concrete Finishers","4742",4700,"Construction and Building Inspectors","4706",4700,"Construction Carpenters","4714",4700,"Construction Laborers","4759",4700,"Continuous Mining Machine Operators","4754",4700,"Derrick Operators, Oil and Gas","4718",4700,"Drywall and Ceiling Tile Installers","4757",4700,"Earth Drillers, Except Oil and Gas","4720",4700,"Electricians","4743",4700,"Elevator Installers and Repairers","4758",4700,"Explosives Workers, Ordnance Handling Experts, and Blasters","4744",4700,"Fence Erectors","4700",4700,"First-Line Supervisors/Managers of Construction Trades and Extraction Workers","4709",4700,"Floor Layers, Except Carpet, Wood, and Hard Tiles","4710",4700,"Floor Sanders and Finishers","4721",4700,"Glaziers","4745",4700,"Hazardous Materials Removal Workers","4735",4700,"Helpers--Brickmasons, Blockmasons, Stonemasons, and Tile and Marble Setters","4736",4700,"Helpers--Carpenters","4737",4700,"Helpers--Electricians","4765",4700,"Helpers--Extraction Workers","4738",4700,"Helpers--Painters, Paperhangers, Plasterers, and Stucco Masons","4739",4700,"Helpers--Pipelayers, Plumbers, Pipefitters, and Steamfitters","4740",4700,"Helpers--Roofers","4746",4700,"Highway Maintenance Workers","4722",4700,"Insulation Workers, Floor, Ceiling, and Wall","4723",4700,"Insulation Workers, Mechanical","4760",4700,"Mine Cutting and Channeling Machine Operators","4717",4700,"Operating Engineers and Other Construction Equipment Operators","4724",4700,"Painters, Construction and Maintenance","4725",4700,"Paperhangers","4715",4700,"Paving, Surfacing, and Tamping Equipment Operators","4716",4700,"Pile-Driver Operators","4728",4700,"Pipe Fitters and Steamfitters","4726",4700,"Pipelayers","4730",4700,"Plasterers and Stucco Masons","4729",4700,"Plumbers","4747",4700,"Rail-Track Laying and Maintenance Equipment Operators","4731",4700,"Reinforcing Iron and Rebar Workers","4762",4700,"Rock Splitters, Quarry","4763",4700,"Roof Bolters, Mining","4732",4700,"Roofers","4755",4700,"Rotary Drill Operators, Oil and Gas","4707",4700,"Rough Carpenters","4764",4700,"Roustabouts, Oil and Gas","4749",4700,"Segmental Pavers","4748",4700,"Septic Tank Servicers and Sewer Pipe Cleaners","4756",4700,"Service Unit Operators, Oil, Gas, and Mining","4733",4700,"Sheet Metal Workers","4704",4700,"Stonemasons","4734",4700,"Structural Iron and Steel Workers","4719",4700,"Tapers","4713",4700,"Terrazzo Workers and Finishers","4711",4700,"Tile and Marble Setters","4912",4900,"Aircraft Mechanics and Service Technicians","4913",4900,"Automotive Body and Related Repairers","4914",4900,"Automotive Glass Installers and Repairers","4916",4900,"Automotive Master Mechanics","4917",4900,"Automotive Specialty Technicians","4904",4900,"Avionics Technicians","4925",4900,"Bicycle Repairers","4918",4900,"Bus and Truck Mechanics and Diesel Engine Specialists","4941",4900,"Camera and Photographic Equipment Repairers","4946",4900,"Coin, Vending, and Amusement Machine Servicers and Repairers","4947",4900,"Commercial Divers","4901",4900,"Computer, Automated Teller, and Office Machine Repairers","4929",4900,"Control and Valve Installers and Repairers, Except Mechanical Door","4905",4900,"Electric Motor, Power Tool, and Related Repairers","4906",4900,"Electrical and Electronics Installers and Repairers, Transportation Equipment","4907",4900,"Electrical and Electronics Repairers, Commercial and Industrial Equipment","4908",4900,"Electrical and Electronics Repairers, Powerhouse, Substation, and Relay","4939",4900,"Electrical Power-Line Installers and Repairers","4909",4900,"Electronic Equipment Installers and Repairers, Motor Vehicles","4910",4900,"Electronic Home Entertainment Equipment Installers and Repairers","4948",4900,"Fabric Menders, Except Garment","4919",4900,"Farm Equipment Mechanics","4900",4900,"First-Line Supervisors/Managers of Mechanics, Installers, and Repairers","4931",4900,"Heating and Air Conditioning Mechanics and Installers","4953",4900,"Helpers--Installation, Maintenance, and Repair Workers","4933",4900,"Home Appliance Repairers","4934",4900,"Industrial Machinery Mechanics","4949",4900,"Locksmiths and Safe Repairers","4935",4900,"Maintenance and Repair Workers, General","4936",4900,"Maintenance Workers, Machinery","4950",4900,"Manufactured Building and Mobile Home Installers","4928",4900,"Mechanical Door Repairers","4942",4900,"Medical Equipment Repairers","4937",4900,"Millwrights","4920",4900,"Mobile Heavy Equipment Mechanics, Except Engines","4922",4900,"Motorboat Mechanics","4923",4900,"Motorcycle Mechanics","4943",4900,"Musical Instrument Repairers and Tuners","4924",4900,"Outdoor Power Equipment and Other Small Engine Mechanics","4902",4900,"Radio Mechanics","4921",4900,"Rail Car Repairers","4926",4900,"Recreational Vehicle Service Technicians","4938",4900,"Refractory Materials Repairers, Except Brickmasons","4932",4900,"Refrigeration Mechanics and Installers","4951",4900,"Riggers","4911",4900,"Security and Fire Alarm Systems Installers","4952",4900,"Signal and Track Switch Repairers","4903",4900,"Telecommunications Equipment Installers and Repairers, Except Line Installers","4940",4900,"Telecommunications Line Installers and Repairers","4927",4900,"Tire Repairers and Changers","4944",4900,"Watch Repairers","5101",5100,"Aircraft Structure, Surfaces, Rigging, and Systems Assemblers","5111",5100,"Bakers","5146",5100,"Bindery Workers","5147",5100,"Bookbinders","5112",5100,"Butchers and Meat Cutters","5166",5100,"Cabinetmakers and Bench Carpenters","5210",5100,"Cementing and Gluing Machine Operators and Tenders","5186",5100,"Chemical Equipment Operators and Tenders","5178",5100,"Chemical Plant and System Operators","5211",5100,"Cleaning, Washing, and Metal Pickling Equipment Operators and Tenders","5204",5100,"Coating, Painting, and Spraying Machine Setters, Operators, and Tenders","5102",5100,"Coil Winders, Tapers, and Finishers","5118",5100,"Computer-Controlled Machine Tool Operators, Metal and Plastic","5212",5100,"Cooling and Freezing Equipment Operators and Tenders","5188",5100,"Crushing, Grinding, and Polishing Machine Setters, Operators, and Tenders","5191",5100,"Cutters and Trimmers, Hand","5192",5100,"Cutting and Slicing Machine Setters, Operators, and Tenders","5123",5100,"Cutting, Punching, and Press Machine Setters, Operators, and Tenders, Metal and Plastic","5200",5100,"Dental Laboratory Technicians","5124",5100,"Drilling and Boring Machine Tool Setters, Operators, and Tenders, Metal and Plastic","5103",5100,"Electrical and Electronic Equipment Assemblers","5104",5100,"Electromechanical Equipment Assemblers","5105",5100,"Engine and Other Machine Assemblers","5213",5100,"Etchers and Engravers","5120",5100,"Extruding and Drawing Machine Setters, Operators, and Tenders, Metal and Plastic","5162",5100,"Extruding and Forming Machine Setters, Operators, and Tenders, Synthetic and Glass Fibers","5193",5100,"Extruding, Forming, Pressing, and Compacting Machine Setters, Operators, and Tenders","5163",5100,"Fabric and Apparel Patternmakers","5107",5100,"Fiberglass Laminators and Fabricators","5100",5100,"First-Line Supervisors/Managers of Production and Operating Workers","5115",5100,"Food and Tobacco Roasting, Baking, and Drying Machine Operators and Tenders","5116",5100,"Food Batchmakers","5117",5100,"Food Cooking Machine Operators and Tenders","5121",5100,"Forging Machine Setters, Operators, and Tenders, Metal and Plastic","5133",5100,"Foundry Mold and Coremakers","5194",5100,"Furnace, Kiln, Oven, Drier, and Kettle Operators and Tenders","5167",5100,"Furniture Finishers","5179",5100,"Gas Plant Operators","5198",5100,"Gem and Diamond Workers","5216",5100,"Glass Blowers, Molders, Benders, and Finishers","5189",5100,"Grinding and Polishing Workers, Hand","5125",5100,"Grinding, Lapping, Polishing, and Buffing Machine Tool Setters, Operators, and Tenders, Metal and Plastic","5141",5100,"Heat Treating Equipment Setters, Operators, and Tenders, Metal and Plastic","5221",5100,"Helpers--Production Workers","5195",5100,"Inspectors, Testers, Sorters, Samplers, and Weighers","5197",5100,"Jewelers","5148",5100,"Job Printers","5126",5100,"Lathe and Turning Machine Tool Setters, Operators, and Tenders, Metal and Plastic","5151",5100,"Laundry and Dry-Cleaning Workers","5142",5100,"Lay-Out Workers, Metal and Plastic","5128",5100,"Machinists","5113",5100,"Meat, Poultry, and Fish Cutters and Trimmers","5201",5100,"Medical Appliance Technicians","5129",5100,"Metal-Refining Furnace Operators and Tenders","5127",5100,"Milling and Planing Machine Setters, Operators, and Tenders, Metal and Plastic","5190",5100,"Mixing and Blending Machine Setters, Operators, and Tenders","5131",5100,"Model Makers, Metal and Plastic","5168",5100,"Model Makers, Wood","5218",5100,"Molding and Casting Workers","5134",5100,"Molding, Coremaking, and Casting Machine Setters, Operators, and Tenders, Metal and Plastic","5135",5100,"Multiple Machine Tool Setters, Operators, and Tenders, Metal and Plastic","5173",5100,"Nuclear Power Reactor Operators","5119",5100,"Numerical Tool and Process Control Programmers","5202",5100,"Ophthalmic Laboratory Technicians","5203",5100,"Packaging and Filling Machine Operators and Tenders","5205",5100,"Painters, Transportation Equipment","5206",5100,"Painting, Coating, and Decorating Workers","5219",5100,"Paper Goods Machine Setters, Operators, and Tenders","5132",5100,"Patternmakers, Metal and Plastic","5169",5100,"Patternmakers, Wood","5180",5100,"Petroleum Pump System Operators, Refinery Operators, and Gaugers","5207",5100,"Photographic Process Workers","5208",5100,"Photographic Processing Machine Operators","5143",5100,"Plating and Coating Machine Setters, Operators, and Tenders, Metal and Plastic","5217",5100,"Potters, Manufacturing","5130",5100,"Pourers and Casters, Metal","5174",5100,"Power Distributors and Dispatchers","5175",5100,"Power Plant Operators","5199",5100,"Precious Metal Workers","5149",5100,"Prepress Technicians and Workers","5152",5100,"Pressers, Textile, Garment, and Related Materials","5150",5100,"Printing Machine Operators","5122",5100,"Rolling Machine Setters, Operators, and Tenders, Metal and Plastic","5170",5100,"Sawing Machine Setters, Operators, and Tenders, Wood","5209",5100,"Semiconductor Processors","5187",5100,"Separating, Filtering, Clarifying, Precipitating, and Still Machine Setters, Operators, and Tenders","5156",5100,"Sewers, Hand","5153",5100,"Sewing Machine Operators","5154",5100,"Shoe and Leather Workers and Repairers","5155",5100,"Shoe Machine Operators and Tenders","5114",5100,"Slaughterers and Meat Packers","5139",5100,"Solderers and Brazers","5176",5100,"Stationary Engineers and Boiler Operators","5215",5100,"Stone Cutters and Carvers, Manufacturing","5106",5100,"Structural Metal Fabricators and Fitters","5157",5100,"Tailors, Dressmakers, and Custom Sewers","5108",5100,"Team Assemblers","5158",5100,"Textile Bleaching and Dyeing Machine Operators and Tenders","5159",5100,"Textile Cutting Machine Setters, Operators, and Tenders","5160",5100,"Textile Knitting and Weaving Machine Setters, Operators, and Tenders","5161",5100,"Textile Winding, Twisting, and Drawing Out Machine Setters, Operators, and Tenders","5109",5100,"Timing Device Assemblers, Adjusters, and Calibrators","5220",5100,"Tire Builders","5136",5100,"Tool and Die Makers","5144",5100,"Tool Grinders, Filers, and Sharpeners","5164",5100,"Upholsterers","5177",5100,"Water and Liquid Waste Treatment Plant and System Operators","5138",5100,"Welders, Cutters, and Welder Fitters","5140",5100,"Welding, Soldering, and Brazing Machine Setters, Operators, and Tenders","5171",5100,"Woodworking Machine Setters, Operators, and Tenders, Except Sawing","5300",5300,"Advertising and Promotions Managers","5301",5300,"Advertising Sales Agents","5302",5300,"Art Directors","5303",5300,"Copy Writers","5304",5300,"Demonstrators and Product Promoters","5305",5300,"Editors","5306",5300,"Graphic Designers","5307",5300,"Market Research Analysts","5308",5300,"Marketing Manager","5309",5300,"Meeting and Convention Planners","5310",5300,"Merchandise Displayers and Window Trimmers","5311",5300,"Public Relations Managers","5312",5300,"Public Relations Specialists","6009",6000,"02N - Keyboard Player","6003",6000,"11B1A - Bomber Pilot - B1","6004",6000,"21E - Construction Equipment Operator","6015",6000,"21K - Plumber","6007",6000,"21M - Firefighter","6017",6000,"25C - Radio Operator-Maintainer","6011",6000,"31B - Military Police","6008",6000,"31E - Internment/Resettlement Specialist","6013",6000,"42A - Personnel Specialist","6016",6000,"46Q - Public Affairs Specialist","6010",6000,"71D - Legal Specialist","6001",6000,"71L - Administrative Specialist","6006",6000,"73C - Finance Specialist","6005",6000,"73Z - Finance Senior Sergeant","6018",6000,"79R - Recruiter NCO","6012",6000,"88M - Motor Transport Operator","6014",6000,"91Q - Pharmacy Specialist","6002",6000,"91T - Animal Care Specialist","8001",8000,"Armored Car Driver / Operative","8002",8000,"ATM Engineer","8003",8000,"Border / Passport Guard","8005",8000,"Cash Handler","8006",8000,"Event Security Steward / Guard","8004",8000,"Meter Reader (Data Collection Agent)","8007",8000,"Prison Officer / Guard");

        function setOptionText(thisSelect,thisType, arrayObject, IsBlank, SelectedValue)
        {
            var ItemCnt = 0;
            thisSelect.length=916;
            thisSelect.options[ItemCnt].value = 0;
            if (IsBlank === 0)
            {thisSelect.options[ItemCnt].text = '          ';}
            if (IsBlank === 1)
            {thisSelect.options[ItemCnt].text = 'All Jobs';}
            if (IsBlank === 2)
            { ItemCnt = -1;}
            for (var loop=0; loop < arrayObject.length / 3; loop++)
            {
                if  (arrayObject[loop * 3 + 1] == thisType )
                {
                    ItemCnt++;
                    thisSelect.options[ItemCnt].value = arrayObject[loop * 3];
                    thisSelect.options[ItemCnt].text = arrayObject[loop * 3 + 2];
                    if (thisSelect.options[ItemCnt].value == SelectedValue){
                        thisSelect.selectedIndex = ItemCnt;
                    }
                }
            }
            thisSelect.length=ItemCnt+1;
        }

        vm.swapOptions = function(thisCbo, thisSwapCbo, IsBlank, SelectedValue)
        {
            var arrayObject = JobsList;
            var JobID = thisCbo.options[thisCbo.selectedIndex].value;
            setOptionText(thisSwapCbo, JobID, arrayObject, IsBlank, SelectedValue);
        };



        var jobId = $stateParams.id || null;

        vm.job = job;

        // get question list
        vm.questions = [];
        vm.questionPaginator = factoryService.createPaginator();

        vm.getQuestionList = function() {
            var currentPage = 1;
            var limit = 20;
            if (vm.questionPaginator) {
                currentPage = vm.questionPaginator.current_page;
                limit = vm.questionPaginator.per_page;
            }

            var params = {
                job_id: jobId,
                page: currentPage,
                limit: limit
            };

            API.question.list(params)
                .then(function(recordSet) {
                    vm.questions = recordSet.getItems();
                    vm.paginator = recordSet.getPaginator();
                })
                .catch(function(err) {
                    throw err;
                });
        };

        function init() {
            vm.getQuestionList();
        }

        // call init function
        init();

        vm.postJob = function() {
            API.jobs.postJob(job.getId())
                .then(function(rs) {
                    $state.go('fox.jobs.candidate-review');
                }, function(err) {
                    throw err;
                });
        };

        vm.gotoJob = function() {
            $state.go('fox.jobs.edit', {
                id: jobId
            });
        };

        vm.getQuestionIndex = function(index) {
            index = index + 1;
            var currentPage = 1;
            var limit = 20;
            if (vm.questionPaginator) {
                currentPage = vm.questionPaginator.current_page;
                limit = vm.questionPaginator.per_page;
            }
            currentPage = currentPage - 1;
            return (currentPage * limit) + index;
        };

        function showDlg(question) {
            var dlg = ngDialog.openConfirm({
                className: 'ngdialog-theme-default add-user',
                showClose: false,
                resolve: {
                    jobId: function() {
                        return jobId;
                    },
                    question: function() {
                        return question;
                    }
                },
                controller: ['$scope', 'API', 'jobId', 'question',
                    function($scope, API, jobId, question) {
                        $scope.question = question;

                        $scope.create = function() {
                            if ($scope.question.getId()) {
                                var data = {
                                    title: $scope.question.title,
										default_answer: $scope.question.default_answer
                                };
                                API.question.update(jobId, $scope.question.getId(), data)
                                    .then(function(rs) {
                                        $scope.confirm(true);
                                    }, function(err) {
                                        throw err;
                                    });
                            } else {
                                API.question.create(jobId, $scope.question.title, $scope.question.default_answer)
                                    .then(function(rs) {
                                        $scope.confirm(true);
                                    }, function(err) {
                                        throw err;
                                    });
                            }
                        };
                    }
                ],
                template: 'jobs/templates/question/add.tpl.html'
            });
            dlg.then(function(val) {
                vm.questionPaginator.current_page = 1;
                vm.getQuestionList();
            }, function(reason) {

            });
        }

        function showAssesmentDialog(question) {
            var dlg = ngDialog.openConfirm({
                className: 'ngdialog-theme-default add-user',
                showClose: false,
                resolve: {
                    jobId: function () {
                        return jobId;
                    },
                    question: function () {
                        return question;
                    }
                },
                controller: ['$scope', 'API', 'jobId', 'question',
                    function ($scope, API, jobId, question) {
                        $scope.question = question;
                        var QuestionType = [{
                            type: 'ChoiceMultiAnswerHorizontal',
                            name: 'Choice - Multi Answers (Horizontal)',
                        }, {
                            type: 'ChoiceMultiAnswerSelectBox',
                            name: 'Choice - Multi Answers (Multi - Select box)',
                        }, {
                            type: 'choiceMultiAnswerVertical',
                            name: 'Choice - Multi Answers (Vertical)',
                        }, {
                            type: 'choiceSingleAnswerDropdown',
                            name: 'Choice - Single Answer (Dropdown)',
                        }, {
                            type: 'choiceSingleAnswerHorizontal',
                            name: 'Choice - Single Answer (Horizotal)',
                        }, {
                            type: 'choiceSingleAnswerVertical',
                            name: 'Choice - Single Answer (Vertical)',
                        }, {
                            type: 'datalist',
                            name: 'Data List',
                        }, {
                            type: 'fileUpload',
                            name: 'File Upload',
                        }, {
                            type: 'InteractiveSlidingScale',
                            name: 'Interactive Sliding Scale',
                        }, {
                            type: 'matrixMultiAnswerPerRow',
                            name: 'Matrix - Multi Answers Per Row',
                        }, {
                            type: 'MatrixSideBySide',
                            name: 'Matrix - Side By Side',
                        }, {
                            type: 'matrixSingleAnswerPerRow',
                            name: 'Matrix - Single Answer Per Row',
                        }, {
                            type: 'matrixSpreadsheet',
                            name: 'Matrix - Spreadsheet',
                        }, {
                            type: 'NetPromoter',
                            name: 'Net Promoter',
                        }, {
                            type: 'numberAllowcationConstantSum',
                            name: 'Number Allowcation - Constant Sum',
                        }, {
                            type: 'openEndedTextCommentBox',
                            name: 'Open Ended Text - Comment Box',
                        }, {
                            type: 'openEndedTextDateTime',
                            name: 'Open Ended Text - Date Time',
                        }, {
                            type: 'openEndedTextOneLine',
                            name: 'Open Ended Text - One Line',
                        }, {
                            type: 'rankOrder',
                            name: 'Rank Order',
                        }];
                        $scope.QuestionType = QuestionType;
                        var colors = [];
                        var attributes = [];
                        var products = [];
                        var items = [];
                        $scope.type = QuestionType[0];




                        // Choice Multi Answer Horizontal
                        var ChoiceMultiAnswerHorizontal = new API.models.questionTypes.ChoiceMultiAnswerHorizontal({
                            type: 'ChoiceMultiAnswerHorizontal',
                            title: 'Select all color you like',
                        });
                        colors = [{
                            title: 'Red',
                            value: false,
                        }, {
                            title: 'Yellow',
                            value: false,
                        }, {
                            title: 'Green',
                            value: false,
                        }, {
                            title: 'White',
                            value: false,
                        }];

                        colors.forEach(function (value) {
                            ChoiceMultiAnswerHorizontal.add(new API.models.questionTypes.Element(value));
                        });

                        $scope.ChoiceMultiAnswerHorizontal = ChoiceMultiAnswerHorizontal;

                        // Choice Multi Answer SelectBox
                        var ChoiceMultiAnswerSelectBox = new API.models.questionTypes.ChoiceMultiAnswerSelectBox({
                            type: 'ChoiceMultiAnswerSelectBox',
                            title: 'Select all color you like',
                        });
                        colors.forEach(function (value) {
                            ChoiceMultiAnswerSelectBox.add(new API.models.questionTypes.Element(value));
                        });
                        $scope.ChoiceMultiAnswerSelectBox = ChoiceMultiAnswerSelectBox;

                        // Choice Multi Answer Vertical
                        var choiceMultiAnswerVertical = new API.models.questionTypes.choiceMultiAnswerVertical({
                            type: 'choiceMultiAnswerVertical',
                            title: 'Select all color you like',
                        });
                        colors.forEach(function (value) {
                            choiceMultiAnswerVertical.add(new API.models.questionTypes.Element(value));
                        });
                        $scope.choiceMultiAnswerVertical = choiceMultiAnswerVertical;



                        // Choice Multi Answer Vertical
                        var choiceSingleAnswerDropdown = new API.models.questionTypes.choiceSingleAnswerDropdown({
                            type: 'choiceSingleAnswerDropdown',
                            title: 'Select color you like',
                        });
                        colors.forEach(function (value) {
                            choiceSingleAnswerDropdown.add(new API.models.questionTypes.Element(value));
                        });
                        $scope.choiceSingleAnswerDropdown = choiceSingleAnswerDropdown;


                        // choiceSingleAnswerHorizontal
                        var choiceSingleAnswerHorizontal = new API.models.questionTypes.choiceSingleAnswerHorizontal({
                            type: 'choiceSingleAnswerHorizontal',
                            title: 'Select color you like',
                        });
                        colors.forEach(function (value) {
                            choiceSingleAnswerHorizontal.add(new API.models.questionTypes.Element(value));
                        });
                        $scope.choiceSingleAnswerHorizontal = choiceSingleAnswerHorizontal;


                        // choiceSingleAnswerVertical
                        var choiceSingleAnswerVertical = new API.models.questionTypes.choiceSingleAnswerVertical({
                            type: 'choiceSingleAnswerVertical',
                            title: 'Select color you like',
                        });
                        colors.forEach(function (value) {
                            choiceSingleAnswerVertical.add(new API.models.questionTypes.Element(value));
                        });
                        $scope.choiceSingleAnswerVertical = choiceSingleAnswerVertical;

                        //numberAllowcationConstantSum
                        var numberAllowcationConstantSum = new API.models.questionTypes.numberAllowcationConstantSum({
                            type: 'numberAllowcationConstantSum',
                            title: 'What percent of your day is spent',
                            total: 100,
                        });
                        attributes = [{
                            title: 'Conferencing',
                            value: 10,
                        }, {
                            title: 'Commuting',
                            value: 30,
                        }, {
                            title: 'Emailing',
                            value: 20,
                        }, {
                            title: 'Writing reports',
                            value: 40,
                        }];
                        attributes.forEach(function (value) {
                            numberAllowcationConstantSum.add(new API.models.questionTypes.Element(value));
                        });
                        $scope.numberAllowcationConstantSum = numberAllowcationConstantSum;

                        //openEndedTextCommentBox
                        var openEndedTextCommentBox = new API.models.questionTypes.openEndedTextCommentBox({
                            title: 'Why is red your favorite color?',
                        });
                        $scope.openEndedTextCommentBox = openEndedTextCommentBox;

                        // openEndedTextDateTime
                        var openEndedTextDateTime = new API.models.questionTypes.openEndedTextDateTime({
                            title: 'What day were you born?',
                        });
                        $scope.openEndedTextDateTime = openEndedTextDateTime;

                        //openEndedTextOneLine
                        var openEndedTextOneLine = new API.models.questionTypes.openEndedTextOneLine({
                            title: 'What is your favorite color?',
                        });
                        $scope.openEndedTextOneLine = openEndedTextOneLine;

                        //rankOrder
                        var rankOrder = new API.models.questionTypes.rankOrder({
                            title: 'Rank the following products',
                            description: '1 = Best, 4 = Worst',
                        });
                        products = [{
                            title: 'Product A',
                            value: 1,
                        }, {
                            title: 'Product B',
                            value: 2,
                        }, {
                            title: 'Product C',
                            value: 3,
                        }, {
                            title: 'Product D',
                            value: 4,
                        }];
                        products.forEach(function (value) {
                            rankOrder.add(new API.models.questionTypes.Element(value));
                        });
                        $scope.rankOrder = rankOrder;

                        //datalist

                        var datalist = new API.models.questionTypes.datalist({
                            title: 'Enter your product infomation',
                        });
                        var manufacturer = new API.models.questionTypes.choiceSingleAnswerDropdown({
                            title: 'Manufacturer',
                        });
                        var manufacturerItems = [{
                            title: 'Data 1',
                            value: 'Data 1',
                        }, {
                            title: 'Data 2',
                            value: 'Data 2',
                        }];
                        manufacturerItems.forEach(function (value) {
                            manufacturer.add(new API.models.questionTypes.Element(value));
                        });
                        datalist.add(manufacturer);
                        var datalistModel = new API.models.questionTypes.openEndedTextOneLine({
                            title: 'Model',
                            value: 'Titan',
                        });
                        datalist.add(datalistModel);
                        var datalistColor = new API.models.questionTypes.openEndedTextOneLine({
                            title: 'Color',
                            value: 'Red',
                        });
                        datalist.add(datalistColor);
                        $scope.datalist = datalist;

                        //fileUpload 
                        var fileUpload = new API.models.questionTypes.fileUpload({
                            title: 'Select your file',
                        });
                        $scope.fileUpload = fileUpload;

                        //matrixMultiAnswerPerRow
                        var matrixMultiAnswerPerRow = new API.models.questionTypes.matrixMultiAnswerPerRow({
                            title: 'Select where you have the following items',
                        });
                        items = [{
                            title: 'Home',
                        }, {
                            title: 'Office',
                        }, {
                            title: 'Car',
                        }];
                        items.forEach(function (value) {
                            matrixMultiAnswerPerRow.add(new API.models.questionTypes.Element(value));
                        });
                        var values = [{
                            title: 'Product A'
                        }, {
                            title: 'Product B'
                        }, {
                            title: 'Product C'
                        }, {
                            title: 'Product D'
                        }, ];
                        matrixMultiAnswerPerRow.setValue(values);

                        $scope.matrixMultiAnswerPerRow = matrixMultiAnswerPerRow;

                        //matrixSingleAnswerPerRow
                        var matrixSingleAnswerPerRow = new API.models.questionTypes.matrixSingleAnswerPerRow({
                            title: 'Do you own any following product',
                        });
                        items = [{
                            title: 'Yes',
                        }, {
                            title: 'No',
                        }, {
                            title: 'Unsure',
                        }];
                        items.forEach(function (value) {
                            matrixSingleAnswerPerRow.add(new API.models.questionTypes.Element(value));
                        });
                        values = [{
                            title: 'Product A'
                        }, {
                            title: 'Product B'
                        }, {
                            title: 'Product C'
                        }, {
                            title: 'Product D'
                        }, ];
                        matrixSingleAnswerPerRow.setValue(values);
                        $scope.matrixSingleAnswerPerRow = matrixSingleAnswerPerRow;

                        //matrixSpreadsheet
                        var matrixSpreadsheet = matrixSpreadsheet;
                        matrixSpreadsheet = new API.models.questionTypes.matrixSpreadsheet({
                            title: 'How much money do you spend on the following gifts on following people?'
                        });
                        items = [{
                            title: 'Children',
                        }, {
                            title: 'Spouse',
                        }, {
                            title: 'Friends',
                        }];
                        items.forEach(function (value) {
                            matrixSpreadsheet.add(new API.models.questionTypes.Element(value));
                        });
                        values = [{
                            title: 'Birthday'
                        }, {
                            title: 'Christmas'
                        }, {
                            title: 'Other'
                        }];
                        matrixSpreadsheet.setValue(values);
                        $scope.matrixSpreadsheet = matrixSpreadsheet;
                        var choiceObject = {};
                        $scope.items = [];
                        $scope.questionText = '';
                        $scope.description = '';
                        $scope.rows = [];
                        $scope.columns = [];

                        $scope.addColumn = function (type) {
                            $scope.columns.push(new API.models.questionTypes.Element(''));
                            switch (type) {
                                case 'matrixMultiAnswerPerRow':
                                    $scope.MatrixMultiAnswerPerRowPreview.add($scope.columns[$scope.columns.length - 1]);
                                    break;
                                case 'matrixSingleAnswerPerRow':
                                    $scope.MatrixSingleAnswerPerRowPreview.add($scope.columns[$scope.columns.length - 1]);
                                    break;
                                case 'matrixSpreadsheet':
                                    $scope.MatrixSpreadsheetPreview.add($scope.columns[$scope.columns.length - 1]);
                                    break;
                            }
                        };

                        $scope.addit = function (row, type,index) {
                            switch (type) {
                                case 'matrixMultiAnswerPerRow':
                                    if (($scope.MatrixMultiAnswerPerRowPreview.getValue()).length < $scope.rows.length) { //it has changed
                                        $scope.MatrixMultiAnswerPerRowPreview.addValue($scope.rows[$scope.rows.length - 1]);
                                        return;
                                    }
                                    $scope.MatrixMultiAnswerPerRowPreview.getValue()[index].title = row.title;
                                    break;
                                case 'matrixSingleAnswerPerRow':
                                    if (($scope.MatrixSingleAnswerPerRowPreview.getValue()).length < $scope.rows.length) { //it has changed
                                        $scope.MatrixSingleAnswerPerRowPreview.addValue($scope.rows[$scope.rows.length - 1]);
                                        return;
                                    }
                                    $scope.MatrixSingleAnswerPerRowPreview.getValue()[index].title = row.title;
                                    break;
                                case 'matrixSpreadsheet':
                                    if (($scope.MatrixSpreadsheetPreview.getValue()).length < $scope.rows.length) { //it has changed
                                        $scope.MatrixSpreadsheetPreview.addValue($scope.rows[$scope.rows.length - 1]);
                                        return;
                                    }
                                    $scope.MatrixSpreadsheetPreview.getValue()[index].title = row.title;
                                    break;
                            }
                            
                        };

                        $scope.addRow = function (type) {
                            switch (type) {
                                case 'matrixMultiAnswerPerRow':
                                    $scope.rows.push({ title: '' });
                                    break;
                                case 'matrixSingleAnswerPerRow':
                                    $scope.rows.push({ title: '' });
                                    break;
                                case 'matrixSpreadsheet':
                                    $scope.rows.push({ title: '' });
                                    break;
                            }
                        };

                        $scope.deleteField = function (value, type, isMatrixColumn) {
                            var index = $scope.items.indexOf(value);
                            $scope.items.splice(index, 1);
                            switch (type) {
                                case 'boolean':
                                case 'single-choice':
                                case 'multiple-choice':
                                case 'drop-down':
                                case 'ChoiceMultiAnswerHorizontal':
                                    $scope.ChoiceMultiAnswerHorizontalPreview.getItems().splice(index, 1);
                                    break;
                                case 'ChoiceMultiAnswerSelectBox':
                                    $scope.ChoiceMultiAnswerSelectBoxPreview.getItems().splice(index, 1);
                                    break;
                                case 'choiceMultiAnswerVertical':
                                    $scope.ChoiceMultiAnswerVerticalPreview.getItems().splice(index, 1);
                                    break;
                                case 'choiceSingleAnswerDropdown':
                                    $scope.ChoiceSingleAnswerDropdownPreview.getItems().splice(index, 1);
                                    break;
                                case 'choiceSingleAnswerHorizontal':
                                    $scope.ChoiceSingleAnswerHorizontalPreview.getItems().splice(index, 1);
                                    break;
                                case 'choiceSingleAnswerVertical':
                                    $scope.ChoiceSingleAnswerVerticalPreview.getItems().splice(index, 1);
                                    break;
                                case 'numberAllowcationConstantSum':
                                    $scope.NumberAllowcationConstantSumPreview.getItems().splice(index, 1);
                                    break;
                                case 'openEndedTextCommentBox':
                                case 'openEndedTextDateTime':
                                case 'openEndedTextOneLine':
                                case 'rankOrder':
                                    $scope.RankOrderPreview.getItems().splice(index, 1);
                                    break;
                                case 'fileUpload':
                                case 'datalist':
                                case 'matrixMultiAnswerPerRow':
                                    if (isMatrixColumn) {
                                        var indexC1 = $scope.columns.indexOf(value);
                                        $scope.columns.splice(indexC1, 1);
                                        $scope.MatrixMultiAnswerPerRowPreview.getItems().splice(indexC1, 1);
                                    } else {
                                        var indexR1 = $scope.rows.indexOf(value);
                                        $scope.MatrixMultiAnswerPerRowPreview.getValue().splice(indexR1, 1);
                                        $scope.rows.splice(indexR, 1);
                                    }
                                    break;
                                case 'matrixSingleAnswerPerRow':
                                    if (isMatrixColumn) {
                                        var indexC = $scope.columns.indexOf(value);
                                        $scope.columns.splice(indexC, 1);
                                        $scope.MatrixSingleAnswerPerRowPreview.getItems().splice(indexC, 1);
                                    } else {
                                        var indexR = $scope.rows.indexOf(value);
                                        $scope.MatrixSingleAnswerPerRowPreview.getValue().splice(indexR, 1);
                                        $scope.rows.splice(indexR, 1);
                                    }
                                    break;
                                case 'matrixSpreadsheet':
                                    if (isMatrixColumn) {
                                        var indexC2 = $scope.columns.indexOf(value);
                                        $scope.columns.splice(indexC2, 1);
                                        $scope.MatrixSpreadsheetPreview.getItems().splice(indexC2, 1);
                                    } else {
                                        var indexR2 = $scope.rows.indexOf(value);
                                        $scope.MatrixSpreadsheetPreview.getValue().splice(indexR2, 1);
                                        $scope.rows.splice(indexR2, 1);
                                    }
                                    break;
                            }
                        };

                        $scope.addField = function (type) {
                            switch (type) {
                                case 'boolean':
                                case 'single-choice':
                                case 'multiple-choice':
                                case 'drop-down':
                                case 'ChoiceMultiAnswerHorizontal':
                                    $scope.items.push(new API.models.questionTypes.Element(''));
                                    $scope.ChoiceMultiAnswerHorizontalPreview.add($scope.items[$scope.items.length - 1]);
                                    break;
                                case 'ChoiceMultiAnswerSelectBox':
                                    $scope.items.push(new API.models.questionTypes.Element(''));
                                    $scope.ChoiceMultiAnswerSelectBoxPreview.add($scope.items[$scope.items.length - 1]);
                                    break;
                                case 'choiceMultiAnswerVertical':
                                    $scope.items.push(new API.models.questionTypes.Element(''));
                                    $scope.ChoiceMultiAnswerVerticalPreview.add($scope.items[$scope.items.length - 1]);
                                    break;
                                case 'choiceSingleAnswerDropdown':
                                    $scope.items.push(new API.models.questionTypes.Element(''));
                                    $scope.ChoiceSingleAnswerDropdownPreview.add($scope.items[$scope.items.length - 1]);
                                    break;
                                case 'choiceSingleAnswerHorizontal':
                                    $scope.items.push(new API.models.questionTypes.Element(''));
                                    $scope.ChoiceSingleAnswerHorizontalPreview.add($scope.items[$scope.items.length - 1]);
                                    break;
                                case 'choiceSingleAnswerVertical':
                                    $scope.items.push(new API.models.questionTypes.Element(''));
                                    $scope.ChoiceSingleAnswerVerticalPreview.add($scope.items[$scope.items.length - 1]);
                                    break;
                                case 'numberAllowcationConstantSum':
                                    $scope.items.push(new API.models.questionTypes.Element(''));
                                    $scope.NumberAllowcationConstantSumPreview.add($scope.items[$scope.items.length - 1]);
                                    break;
                                case 'openEndedTextCommentBox':
                                case 'openEndedTextDateTime':
                                case 'openEndedTextOneLine':
                                case 'rankOrder':
                                    $scope.items.push(new API.models.questionTypes.Element(''));
                                    $scope.RankOrderPreview.add($scope.items[$scope.items.length - 1]);
                                    break;
                                case 'fileUpload':
                                case 'datalist':
                                case 'matrixMultiAnswerPerRow':
                                case 'matrixSingleAnswerPerRow':
                                case 'matrixSpreadsheet':
                            }

                        };
                        $scope.showing = true;
                        $scope.isMatrix = false;
                        $scope.showOptionField = true;
                        $scope.next = function (selected) {
                            $scope.isMatrix = false;
                            if ($scope.showing) {
                                $scope.items = [];
                                $scope.rows = [];
                                $scope.columns = [];
                                switch (selected) {
                                    case 'boolean':
                                    case 'single-choice':
                                    case 'multiple-choice':
                                    case 'drop-down':
                                    case 'ChoiceMultiAnswerHorizontal':
                                        $scope.ChoiceMultiAnswerHorizontalPreview = new API.models.questionTypes.ChoiceMultiAnswerHorizontal({
                                            type: 'ChoiceMultiAnswerHorizontal',
                                            title: $scope.questionText,
                                        });
                                        break;
                                    case 'ChoiceMultiAnswerSelectBox':
                                        $scope.ChoiceMultiAnswerSelectBoxPreview = new API.models.questionTypes.ChoiceMultiAnswerSelectBox({
                                            type: 'ChoiceMultiAnswerHorizontal',
                                            title: $scope.questionText,
                                        });
                                        break;
                                    case 'choiceMultiAnswerVertical':
                                        $scope.ChoiceMultiAnswerVerticalPreview = new API.models.questionTypes.choiceMultiAnswerVertical({
                                            type: 'choiceMultiAnswerVertical',
                                            title: $scope.questionText,
                                        });
                                        break;
                                    case 'choiceSingleAnswerDropdown':
                                        $scope.ChoiceSingleAnswerDropdownPreview = new API.models.questionTypes.choiceSingleAnswerDropdown({
                                            type: 'choiceSingleAnswerDropdown',
                                            title: $scope.questionText,
                                        });
                                        break;
                                    case 'choiceSingleAnswerHorizontal':
                                        $scope.ChoiceSingleAnswerHorizontalPreview = new API.models.questionTypes.choiceSingleAnswerHorizontal({
                                            type: 'choiceSingleAnswerHorizontal',
                                            title: $scope.questionText,
                                        });
                                        break;
                                    case 'choiceSingleAnswerVertical':
                                        $scope.ChoiceSingleAnswerVerticalPreview = new API.models.questionTypes.choiceSingleAnswerVertical({
                                            type: 'choiceSingleAnswerVertical',
                                            title: $scope.questionText,
                                        });
                                        break;
                                    case 'numberAllowcationConstantSum':
                                        $scope.NumberAllowcationConstantSumPreview = new API.models.questionTypes.numberAllowcationConstantSum({
                                            type: 'numberAllowcationConstantSum',
                                            title: $scope.questionText,
                                            total: 100,
                                        });
                                        break;
                                    case 'openEndedTextCommentBox':
                                        $scope.OpenEndedTextCommentBoxPreview = new API.models.questionTypes.openEndedTextCommentBox({
                                            title: $scope.questionText
                                        });
                                        console.log($scope.OpenEndedTextCommentBoxPreview);
                                        $scope.showOptionField = false;
                                        break;
                                    case 'openEndedTextDateTime':
                                        $scope.OpenEndedTextDateTimePreview = new API.models.questionTypes.openEndedTextDateTime({
                                            title: $scope.questionText
                                        });
                                        $scope.showOptionField = false;
                                        break;
                                    case 'openEndedTextOneLine':
                                        $scope.OpenEndedTextOneLinePreview = new API.models.questionTypes.openEndedTextOneLine({
                                            title: $scope.questionText
                                        });
                                        $scope.showOptionField = false;
                                        break;
                                    case 'rankOrder':
                                        $scope.RankOrderPreview = new API.models.questionTypes.rankOrder({
                                            title: $scope.questionText,
                                            description: $scope.description,
                                        });
                                        break;
                                    case 'fileUpload':
                                        $scope.FileUploadPreview = new API.models.questionTypes.fileUpload({
                                            title: $scope.questionText
                                        });
                                        $scope.showOptionField = false;
                                        break;
                                    case 'datalist':
                                    case 'matrixMultiAnswerPerRow':
                                        $scope.MatrixMultiAnswerPerRowPreview = new API.models.questionTypes.matrixMultiAnswerPerRow({
                                            title: $scope.questionText,
                                        });
                                        $scope.isMatrix = true;
                                        break;
                                    case 'matrixSingleAnswerPerRow':
                                        $scope.MatrixSingleAnswerPerRowPreview = new API.models.questionTypes.matrixSingleAnswerPerRow({
                                            title: $scope.questionText,
                                        });
                                        $scope.isMatrix = true;
                                        break;
                                    case 'matrixSpreadsheet':
                                        $scope.MatrixSpreadsheetPreview = new API.models.questionTypes.matrixSpreadsheet({
                                            title: $scope.questionText
                                        });
                                        $scope.isMatrix = true;
                                        break;
                                }
                                $scope.showing = false;
                            } else {
                                $scope.isMatrix = false;
                                $scope.showOptionField = true;
                                $scope.showing = true;
                            }
                        };



                    }
                ],
                template: 'jobs/templates/question/assessment-question.tpl.html'
            });
            dlg.then(function (val) {
                vm.questionPaginator.current_page = 1;
                vm.getQuestionList();
            }, function (reason) {

            });
        }

        vm.addQuestion = function() {
            var question = new Question({});
            showDlg(question);
        };

        vm.addAssessment = function () {
            showAssesmentDialog();
        };

        vm.edit = function(item) {
            var question = angular.copy(item);
            showDlg(question);
        };

        vm.delete = function(item) {
            dialog.confirm(null, 'Are you sure you want to delete?')
                .then(function(result) {
                    API.question.del(jobId, item.getId())
                        .then(function(rs) {
                            vm.getQuestionList();
                        }, function(err) {
                            throw err;
                        });
                }, function(reason) {});
        };
    }
})(angular.module('fox.jobs.controllers.create.interview', []));