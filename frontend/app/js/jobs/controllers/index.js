require('./job/');
require('./interview/');
require('./candidate');
require('./phone-screen');
require('./review-video-interview/');
require('./review-question-result/');
require('./interview-note/');
require('./reference-check/');
require('./offer-letter/');
require('./placement-record');
require('./main');

(function(app) {

})(angular.module('fox.jobs.controllers', [
    'fox.jobs.controllers.job',
    'fox.jobs.controllers.interview',
    'fox.jobs.controllers.candidate',
    'fox.jobs.controllers.phone-screen',
    'fox.jobs.controllers.review-video-interview',
    'fox.jobs.controllers.review-question-result',
    'fox.jobs.controllers.interview-note',
    'fox.jobs.controllers.reference-check',
    'fox.jobs.controllers.offer-letter',
    'fox.jobs.controllers.placement-record',
    'fox.jobs.controllers.main'
]));