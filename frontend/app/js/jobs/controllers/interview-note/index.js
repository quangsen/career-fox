(function(app) {
    app.controller('JobsInterviewNote', controller);
    controller.$inject = [
        '$state',
        'job',
        'dialog',
        'API'
    ];

    function controller(
        $state,
        job,
        dialog,
        API) {

        var vm = this;
        vm.ckeditorOptions = {
            toolbar: 'basic',
            height: '300px'
        };

        var params = {};

        vm.candidates = [];
        vm.keyword = '';
        // get candidates invited
        function getCandidates() {
            var params = {};
            if (vm.keyword) {
                params.q = ('(keyword:' + vm.keyword + ')');
            }
            API.jobs.interviewNote.list(job.getId(), params)
                .then(function(recordSet) {
                    vm.candidates = recordSet.getItems();
                    // set default selected
                    if (vm.candidates.length) {
                        vm.selectedCandidate = vm.candidates[0];
                        getNote(vm.selectedCandidate);
                    }
                })
                .catch(function(err) {
                    throw err;
                });
        }

        function init() {
            // get candidates invited
            getCandidates();
        }

        init();

        vm.selectedCandidate = null;

        vm.select = function(item) {
            vm.selectedCandidate = item;
            getNote(item);
        };

        function getNote(item) {
            API.jobs.interviewNote.getNote(item.getInterviewInvitationId())
                .then(function(result) {
                    vm.body = result.data.quick_note;
                })
                .catch(function(err) {
                    throw err;
                });
        }

        vm.isActive = function(item) {
            if (!vm.selectedCandidate) {
                return false;
            }
            return (vm.selectedCandidate.getId() === item.getId());
        };


        vm.gotoQuestionResult = function() {
            $state.go('fox.jobs.review-question-result');
        };

        vm.gotoRefenCheck = function() {
            $state.go('fox.jobs.reference-check');
        };

        vm.saveInterviewNote = function(note) {
            API.jobs.candidates.saveInterviewNote(vm.selectedCandidate.getInterviewInvitationId(), note)
                .then(function(result) {
                    dialog.alert('','Save note successfully');
                })
                .catch(function(err) {
                    throw err;
                });
        };
    }
})(angular.module('fox.jobs.controllers.interview-note', ['ckeditor']));
