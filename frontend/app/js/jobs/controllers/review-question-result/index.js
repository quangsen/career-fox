(function(app) {
    app.controller('JobsReviewQuestionResult', controller);
    controller.$inject = [
        '$state',
        '$scope',
        '$http',
        'job',
        'API',
        'dialog'
    ];

    function controller(
        $state,
        $scope,
        $http,
        job,
        API,
        dialog
    ) {
        var vm = this;

        var params = {};

        vm.candidates = [];
        vm.keyword = '';
        // get candidates invited
        function getCandidates() {
            var params = {};
            if (vm.keyword) {
                params.q = ('(keyword:' + vm.keyword + ')');
            }
            API.jobs.questionnaire.list(job.getId(), params)
                .then(function(recordSet) {
                    vm.candidates = recordSet.getItems();
                    // set default selected
                    if (vm.candidates.length) {
                        vm.selectedCandidate = vm.candidates[0];
                    }
                })
                .catch(function(err) {
                    throw err;
                });
        }

        function init() {
            // get candidates invited
            getCandidates();
        }

        init();

        vm.selectedCandidate = null;
        // Assessment table
        vm.activeButton = 'question';
        vm.isButtonsActive = false;
        vm.setButtonActive = function (button) {
            if (button === 'question') {
                vm.isButtonsActive = false;
            } else {
                vm.isButtonsActive = true;
            }
            vm.activeButton = button;
        };

        vm.checkActive = function (active) {
            return (vm.activeButton === active);
        };

        // assessment end
        vm.select = function(item) {
            vm.selectedCandidate = item;
        };

        vm.isActive = function(item) {
            if (!vm.selectedCandidate) {
                return false;
            }
            return (vm.selectedCandidate.getId() === item.getId());
        };

        vm.isCandidateSelected = function() {
            if (!vm.selectedCandidate) {
                return false;
            }
            return true;
        };

        vm.invite = function() {
            if (!vm.isCandidateSelected()) {
                return false;
            }
            API.jobs.questionnaire.invite(vm.selectedCandidate.questionnaire_id)
                .then(function(rs) {
                    vm.selectedCandidate.setInvitedInterview(true);
                    dialog.alert(null, 'The candidate has been invited successfully!');
                }, function(err) {
                    throw err;
                });
        };

        vm.showInvite = function() {
            if (!vm.isCandidateSelected()) {
                return true;
            }
            return !vm.selectedCandidate.isInvitedInterview();
        };

        vm.search = function() {
            getCandidates();
        };

        vm.gotoVideo = function() {
            $state.go('fox.jobs.review-video-interview');
        };

        vm.gotoInterviewNote = function() {
            $state.go('fox.jobs.interview-note');
        };

        vm.filter = 'pass';
        vm.changeFilter = function(filter) {
            vm.filter = filter; 
        };
        vm.checkFilter = function(filter) {
            return (vm.filter === filter);
        };

        vm.parseHtml = function(){
             $http({
                url: 'http://localhost/work/career-fox/public/ascentii/candidates',
                method: "GET",
            })
            .then(function(response) {
                var parser = new DOMParser();
                var doc = parser.parseFromString(response.data, 'text/html');
                var ps = $(doc.body.innerHTML);
                $('.table-user').html(ps).children().last('table.colorTableGrid');
                console.log("Body HTML: ", ps);
            }, 
            function(response) {
                console.log("Error: ", response);
            });
        };
    }
})(angular.module('fox.jobs.controllers.review-question-result', []));