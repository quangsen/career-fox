(function(app) {
    app.config([
        '$stateProvider',
        function($stateProvider) {
            $stateProvider
                .state('fox.candidates', {
                    url: '/candidates',
                    templateUrl: 'candidates/index.tpl.html',
                    controller: 'DashboardCandidatesController',
                    controllerAs: 'vm',
                    resolve: {}
                });
        }
    ]);
})(angular.module('fox.candidates.routes', []));