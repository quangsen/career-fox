(function(app) {
    app.controller('DashboardCandidatesController', controller);
    controller.$inject = [
        '$state',
        'factoryService',
        'dialog',
        'API'
    ];

    function controller(
        $state,
        factoryService,
        dialog,
        API
    ) {

        var vm = this;

        vm.currentFilter = 7;
        vm.keyword = '';

        vm.items = [];
        vm.paginator = factoryService.createPaginator();

        vm.getList = function() {
            var currentPage = 1;
            var limit = 20;
            if (vm.paginator) {
                currentPage = vm.paginator.current_page;
                limit = vm.paginator.per_page;
            }

            var params = {
                page: currentPage,
                limit: limit,
                includes: 'job'
            };

            var arrSearch = ['days:' + vm.currentFilter];
            if (vm.keyword) {
                arrSearch.push('keyword:' + vm.keyword);
            }
            params.q = '(' + arrSearch.toString() + ')';

            API.candidates.list(params)
                .then(function(recordSet) {
                    vm.items = recordSet.getItems();
                    vm.paginator = recordSet.getPaginator();
                })
                .catch(function(err) {
                    throw err;
                });
        };

        vm.getList();

        vm.isFilterActive = function(val) {
            if (val === vm.currentFilter) {
                return 'active';
            }
            return '';
        };

        vm.changeFilter = function(val) {
            vm.paginator.current_page = 1;
            vm.currentFilter = val;
            vm.getList();
        };

        vm.search = function() {
            vm.paginator.current_page = 1;
            vm.getList();
        };

        vm.keypress = function(e) {
            if (e.which === 13) {
                vm.search();
            }
        };

        vm.viewDetail = function(item) {
            dialog.openCandidateDetail(item);
        };
    }
})(angular.module('fox.candidates.controller', []));