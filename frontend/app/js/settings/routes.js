(function(app) {
    app.config([
        '$stateProvider',
        function($stateProvider) {
            $stateProvider
                .state('fox.settings', {
                    abstract: true,
                    template: '<div ui-view></div>',
                    resolve: {}
                })
                .state('fox.settings.company', {
                    url: '/settings/company',
                    templateUrl: 'settings/company.tpl.html',
                    controller: 'SettingsCompanyController',
                    controllerAs: 'vm',
                    resolve: {}
                })
                .state('fox.settings.invoices', {
                    url: '/settings/invoices',
                    templateUrl: 'settings/invoices.tpl.html',
                    controller: 'SettingsInvoicesController',
                    controllerAs: 'vm',
                    resolve: {}
                });
        }
    ]);
})(angular.module('fox.settings.routes', []));