(function(app) {
    app.controller('SettingsCompanyController', controller);
    controller.$inject = [
        '$state',
        '$rootScope',
        'dialog',
        'factoryService',
        'ngDialog',
        'API'
    ];

    function controller(
        $state,
        $rootScope,
        dialog,
        factoryService,
        ngDialog,
        API
    ) {

        var vm = this;

        vm.items = [];
        vm.paginator = factoryService.createPaginator();

        var company_id = $rootScope.loggedInUser.company_id;
        vm.getCompanyInfo = function(company_id) {
            API.company.getCompanyInfo(company_id)
                .then(function(result) {
                    vm.company = result;
                });
        };

        vm.getCompanyInfo(company_id);

        vm.getList = function() {
            API.employees.list({
                page: vm.paginator.current_page
            }).then(function(result) {
                console.log(result);
                vm.employees = result.items;
                vm.paginator = result.paginator;
            });
        };
        console.log(vm.paginator);
        vm.getList();

        vm.save = function(company) {
            API.company.update(company)
                .then(function(result) {
                    console.log(result);
                });
        };

        function showDlg(user) {
            var dlg = ngDialog.openConfirm({
                className: 'ngdialog-theme-default add-users',
                showClose: false,
                resolve: {

                },
                controller: ['$scope', '$rootScope',
                    function($scope, $rootScope) {
                        var roles = ['employee', 'recruiter'];
                        $scope.roles = roles;

                        $scope.model = {
                            email: '',
                            first_name: '',
                            last_name: '',
                            phone: '',
                            role: $scope.roles[0],
                            password: '',
                            password_confirmation: ''
                        };
                        if ($rootScope.loggedInUser.role_name == 'company') {
                            $scope.model.company_id = $rootScope.loggedInUser.company_id;
                        }
                        $scope.confirm = function(model) {
                            if (model.password != model.password_confirmation) {
                                dialog.alert("", "Password confirmation does not match.");
                                return;
                            }
                            API.user.create(model)
                                .then(function(result) {
                                    vm.employees.push(result);
                                    $scope.closeThisDialog('cancel');
                                });
                        };
                    }
                ],
                template: 'settings/add-user.tpl.html'
            });
            dlg.then(function(val) {
                vm.benefitPaginator.current_page = 1;
                vm.getBenefitList();
            }, function(reason) {

            });
        }

        vm.addUser = function() {
            /*dialog.addUser()
                .then(function(infos) {
                    console.log('infos: ', infos);
                }, function() {

                });
                */
            //var user = new User({});
            var user = {};
            showDlg();
        };


    }
})(angular.module('fox.settings.controllers.company', []));
