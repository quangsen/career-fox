(function(app) {
    app.controller('SettingsInvoicesController', controller);
    app.filter('SubTotalFilter', SubTotalFilter);
    app.filter('TotalFilter', TotalFilter);
    controller.$inject = [
        '$scope',
        '$state',
        'dialog',
        'factoryService',
        'API',
    ];

    function controller(
        $scope,
        $state,
        dialog,
        factoryService,
        API
    ) {
        var vm = this;
        var validInvoiceStatus = ['all', 'paid', 'pending', 'sent', 'cancelled'];
        var page = 1;
        $scope.page = page;
        $scope.mainTitle = 'invoices';
        $scope.validInvoiceStatus = validInvoiceStatus;
        $scope.status = 'all';
        $scope.discount = 5; //percent
        $scope.vat = 10; //percent 

        vm.getListInvoices = function(page, status) {
            var params;
            if (validInvoiceStatus.indexOf(status) < -1 || status === undefined || status == 'all') {
                params = {
                    page: page,
                };
            } else {
                params = {
                    page: page,
                    q: 'keyword:' + status,
                };
            }
            API.invoices.getListInvoices(params).then(function(result) {
                $scope.invoices = result.data;
                if (result.meta.pagination.total_pages > 1) {
                    var pages = [];
                    for (var i = 1; i <= result.meta.pagination.total_pages; i++) {
                        pages.push(i);
                    }
                    $scope.pages = pages;
                    $scope.showpagination = true;
                } else {
                    $scope.showpagination = false;
                }
            });
        };
        vm.getListInvoices(page);

        $scope.changeStatusFilter = function(status) {
            $scope.status = status;
            vm.getListInvoices($scope.page, status);
        };
        $scope.setPage = function(p) {
            $scope.page = p;
            vm.getListInvoices(p);
        };
    }

    function SubTotalFilter() {
        return function(array, status) {
            return CalcSubTotal(array, status);
        };
    }

    function TotalFilter() {
        return function(array, status, discount, vat) {
            var SubTotal = CalcSubTotal(array, status);
            var total = SubTotal * (1 - discount / 100) * (1 + vat / 100);
            return Math.round(total * 100) / 100;
        };
    }

    function CalcSubTotal(array, status) {
        if (array === undefined) {
            return '';
        } else {
            var SubTotal = 0;
            array.forEach(function(value, key) {
                if (status === undefined || status.toLowerCase() == 'all') {
                    SubTotal += value.amount;
                } else {
                    if (value.status.toLowerCase() == status.toLowerCase()) {
                        SubTotal += value.amount;
                    }
                }
            });
            return Math.round(SubTotal * 100) / 100;
        }
    }
})(angular.module('fox.settings.controllers.invoices', []));
