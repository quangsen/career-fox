(function() {
    'use strict';
    angular.module('fox.common.dialog', [])
        .factory('dialog', Dialog);

    Dialog.$inject = ['ngDialog'];

    function Dialog(ngDialog) {
        return {
            confirm: function(title, message) {
                return ngDialog.openConfirm({
                    showClose: false,
                    resolve: {
                        title: function() {
                            return title;
                        },
                        message: function() {
                            return message;
                        }
                    },
                    controller: ['$scope', 'title', 'message',
                        function($scope, title, message) {
                            $scope.title = title;
                            $scope.message = message;
                        }
                    ],
                    template: 'common/confirm-dialog.tpl.html'
                });
            },

            alert: function(title, message) {
                return ngDialog.open({
                    showClose: false,
                    resolve: {
                        title: function() {
                            return title;
                        },
                        message: function() {
                            return message;
                        }
                    },
                    controller: ['$scope', 'title', 'message',
                        function($scope, title, message) {
                            $scope.title = title;
                            $scope.message = message;
                        }
                    ],
                    template: 'common/alert-dialog.tpl.html'
                });
            },

            showPostingJob: function() {
                return ngDialog.open({
                    id: 'posting-job',
                    showClose: false,
                    closeByDocument: false,
                    closeByEscape: false,
                    resolve: {},
                    controller: ['$scope',
                        function($scope) {}
                    ],
                    template: 'common/posting-job.tpl.html'
                });
            },

            showCompletedJob: function() {
                return ngDialog.open({
                    id: 'posting-job',
                    showClose: false,
                    closeByDocument: false,
                    closeByEscape: false,
                    resolve: {},
                    controller: ['$scope',
                        function($scope) {}
                    ],
                    template: 'common/completed-job.tpl.html'
                });
            },

            addUser: function() {
                return ngDialog.openConfirm({
                    showClose: false,
                    resolve: {

                    },
                    className: 'ngdialog-theme-default add-user',
                    controller: ['$scope',
                        function($scope) {
                            $scope.model = {
                                email: '',
                                first_name: '',
                                last_name: '',
                                position: '',
                                user_level: '',
                                password: ''
                            };
                        }
                    ],
                    template: 'settings/user/add-user.tpl.html'
                });
            },

            openCandidateDetail: function(model) {
                return ngDialog.open({
                    showClose: false,
                    resolve: {
                        data: function() {
                            return model;
                        }
                    },
                    className: 'ngdialog-theme-default pdf-viewer',
                    controller: ['$scope', 'data',
                        function($scope, data) {
                            $scope.data = data;

                            $scope.pdfUrl = data.getPdf();
                            $scope.scroll = 0;
                            $scope.loading = 'loading';

                            $scope.getNavStyle = function(scroll) {
                                if (scroll > 100) return 'pdf-controls fixed';
                                else return 'pdf-controls';
                            };

                            $scope.onError = function(error) {
                                console.log(error);
                            };

                            $scope.onLoad = function() {
                                $scope.loading = '';
                            };

                            $scope.onProgress = function(progress) {};
                        }
                    ],
                    template: 'jobs/templates/candidate/popup.tpl.html'
                });
            },

            openCandidateCoverLetter: function(model) {
                return ngDialog.open({
                    showClose: false,
                    resolve: {
                        data: function() {
                            return model;
                        }
                    },
                    className: 'ngdialog-theme-default pdf-viewer',
                    controller: ['$scope', 'data',
                        function($scope, data) {
                            $scope.data = data;

                            $scope.pdfUrl = data.getCoverLetter();
                            $scope.scroll = 0;
                            $scope.loading = 'loading';

                            $scope.getNavStyle = function(scroll) {
                                if (scroll > 100) return 'pdf-controls fixed';
                                else return 'pdf-controls';
                            };

                            $scope.onError = function(error) {
                                console.log(error);
                            };

                            $scope.onLoad = function() {
                                $scope.loading = '';
                            };

                            $scope.onProgress = function(progress) {};
                        }
                    ],
                    template: 'jobs/templates/candidate/popup.tpl.html'
                });
            }
        };
    }
})();
