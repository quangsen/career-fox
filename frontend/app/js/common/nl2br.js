(function(app) {
    app.filter('nl2br', [

        function() {
            return function(str) {
                if (!str) {
                    return '';
                }
                return str.replace(/\n/g, '<br>');
            };
        }
    ]);
})(angular.module('fox.common.nl2br', []));