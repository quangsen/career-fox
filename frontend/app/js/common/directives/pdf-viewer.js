(function(app) {
    app.directive('pdfViewer', theDirective);

    theDirective.$inject = [];

    function theDirective() {
        return {
            templateUrl: 'common/pdf.tpl.html',
            replace: true,
            restrict: 'A',
            scope: {
                model: '=',
                pdfUrl: '='
            },
            controller: function($scope, $element) {
                $scope.scroll = 0;
                $scope.loading = 'loading';

                $scope.getNavStyle = function(scroll) {
                    if (scroll > 100) return 'pdf-controls fixed';
                    else return 'pdf-controls';
                };

                $scope.onError = function(error) {
                    console.log(error);
                };

                $scope.onLoad = function() {
                    $scope.loading = '';
                };

                $scope.isAvailable = function() {
                    if ($scope.pdfUrl) {
                        return true;
                    }
                    return false;
                };

                $scope.onProgress = function(progress) {};
            }
        };
    }

})(angular.module('fox.common.directives.pdf-viewer', []));