module.exports = {
    /**
     * Build template for frontend
     */
    frontend: {
        src: ['<%= src_dir.js %>/main.js'],
        dest: '<%= build_dir.js %>/main.js'
    }
};