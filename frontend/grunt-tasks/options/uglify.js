module.exports = {
    options: {
        //Do not change variables name
        mangle: false
        /**
         * except: ['jQuery', 'angular']
         */
    },
    frontend: {
        options: {
            sourceMap: false,
            banner: '<%= meta.banner %>'
        },
        src: ['<%= browserify.frontend.dest %>'],
        dest: '<%= build_dir.js %>/frontend.min.js'
    }
};