module.exports = {
    options: {
        livereload: 35731
    },

    frontend_template: {
        files: [
            '<%= src_dir.js %>/**/*.tpl.html',
            'common/**/*.tpl.html'
        ],
        tasks: [
            'html2js:common',
            'html2js:frontend'
        ],
        options: {
            spawn: false
        }
    },

    frontend_js: {
        files: ['<%= src_dir.js %>/**/*.js'],
        tasks: [
            'jshint:frontend',
            'browserify'
        ],
        options: {
            spawn: false
        }
    },

    frontend_less: {
        files: ['<%= src_dir.less %>/**/*.less'],
        tasks: ['less:frontend']
    },

    config: {
        files: ['Gruntfile.js'],
        tasks: [
            'build'
        ],
        options: {
            spawn: false,
            reload: true
        }
    }
};