module.exports = {
    /**
     * Build common templates
     */
    common: {
        options: {
            module: 'fox.common.templates', // no bundle module for all the html2js templates
            base: ''
        },
        src: ['common/**/*.tpl.html'],
        dest: '<%= build_dir.js%>/common.tpl.js'
    },

    /**
     * Build template for frontend
     */
    frontend: {
        options: {
            module: 'fox.templates', // no bundle module for all the html2js templates
            base: '<%= src_dir.js %>'
        },
        src: ['<%= src_dir.js %>/**/*.tpl.html'],
        dest: '<%= build_dir.js %>/main.tpl.js'
    }
};