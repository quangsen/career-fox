module.exports = {
    root: 'app',
    src_dir: {
        js: '<%= root %>/js',
        less: '<%= root %>/less'
    },
    base_build_dir: '../public',
    build_dir: {
        js: '<%= base_build_dir %>/js',
        css: '<%= base_build_dir %>/css'
    }
};