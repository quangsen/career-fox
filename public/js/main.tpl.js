angular.module('fox.templates', ['accounting/create-invoices.tpl.html', 'accounting/index.tpl.html', 'accounting/invoices.tpl.html', 'accounting/payment-details.tpl.html', 'accounting/revenue.tpl.html', 'accounting/subscription.tpl.html', 'admindashboard/templates/admin_dashboard.tpl.html', 'auth/login.tpl.html', 'candidates/index.tpl.html', 'dashboard/templates/dashboard.tpl.html', 'error/404.tpl.html', 'error/500.tpl.html', 'job-creation/templates/job/create.tpl.html', 'jobs/templates/candidate/popup-review.tpl.html', 'jobs/templates/candidate/popup.tpl.html', 'jobs/templates/candidate/review.tpl.html', 'jobs/templates/create-menu.tpl.html', 'jobs/templates/index.tpl.html', 'jobs/templates/interview-note/index.tpl.html', 'jobs/templates/interview/create-questionnaire.tpl.html', 'jobs/templates/interview/create.tpl.html', 'jobs/templates/interview/survey.tpl.html', 'jobs/templates/job/create.tpl.html', 'jobs/templates/offer-letter/index.tpl.html', 'jobs/templates/phone-screen/add-schedule.tpl.html', 'jobs/templates/phone-screen/index.tpl.html', 'jobs/templates/phone-screen/share.tpl.html', 'jobs/templates/placement-record/index.tpl.html', 'jobs/templates/question/add.tpl.html', 'jobs/templates/question/assessment-question.tpl.html', 'jobs/templates/reference-check/add-reference.tpl.html', 'jobs/templates/reference-check/index.tpl.html', 'jobs/templates/review-question-result/index.tpl.html', 'jobs/templates/review-video-interview/index.tpl.html', 'my-jobs/list.tpl.html', 'myaccount/templates/calendar.tpl.html', 'myaccount/templates/messages.tpl.html', 'myaccount/templates/view-message.tpl.html', 'placement-record/add-documents.tpl.html', 'placement-record/add.tpl.html', 'placement-record/placement-record.tpl.html', 'questionnaire/types/ChoiceMultiAnswerHorizontal/view.tpl.html', 'questionnaire/types/ChoiceMultiAnswerSelectBox/view.tpl.html', 'questionnaire/types/boolean/view.tpl.html', 'questionnaire/types/choiceMultiAnswerVertical/view.tpl.html', 'questionnaire/types/choiceSingleAnswerDropdown/view.tpl.html', 'questionnaire/types/choiceSingleAnswerHorizontal/view.tpl.html', 'questionnaire/types/choiceSingleAnswerVertical/view.tpl.html', 'questionnaire/types/datalist/view.tpl.html', 'questionnaire/types/drop-down/edit.tpl.html', 'questionnaire/types/drop-down/index.tpl.html', 'questionnaire/types/drop-down/view.tpl.html', 'questionnaire/types/fileUpload/view.tpl.html', 'questionnaire/types/matrixMultiAnswerPerRow/view.tpl.html', 'questionnaire/types/matrixSingleAnswerPerRow/view.tpl.html', 'questionnaire/types/matrixSpreadsheet/view.tpl.html', 'questionnaire/types/multiple-choice/edit.tpl.html', 'questionnaire/types/multiple-choice/index.tpl.html', 'questionnaire/types/multiple-choice/view.tpl.html', 'questionnaire/types/numberAllowcationConstantSum/view.tpl.html', 'questionnaire/types/openEndedTextCommentBox/view.tpl.html', 'questionnaire/types/openEndedTextDateTime/view.tpl.html', 'questionnaire/types/openEndedTextOneLine/view.tpl.html', 'questionnaire/types/rankOrder/view.tpl.html', 'questionnaire/types/single-choice/edit.tpl.html', 'questionnaire/types/single-choice/index.tpl.html', 'questionnaire/types/single-choice/view.tpl.html', 'settings/add-user.tpl.html', 'settings/company.tpl.html', 'settings/invoices.tpl.html', 'users/add/add.tpl.html', 'users/list.tpl.html']);

angular.module("accounting/create-invoices.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("accounting/create-invoices.tpl.html",
    "<div class=\"container-fluid container-fullw\">\n" +
    "	<div class=\"row\">\n" +
    "		<div class=\"col-md-12\">\n" +
    "			<h1 class=\"mainTitle text-primary\">Administraction</h1>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "	\n" +
    "    <form class=\"form-horizontal\" ng-submit=\"CreateInvoice(email, name, amount, status)\" role=\"form\">\n" +
    "		<div class=\"form-group\">\n" +
    "            <label class=\"control-label col-sm-2\" for=\"date-to-charge\">Company</label>\n" +
    "            <div class=\"col-sm-10\">\n" +
    "               <select class=\"form-control\" ng-options=\"company as company.name for company in companies track by company.id\" ng-model=\"status\"></select>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label class=\"control-label col-sm-2\">Name:</label>\n" +
    "            <div class=\"col-sm-10\">\n" +
    "                <input type=\"text\" class=\"form-control\" ng-model=\"name\" placeholder=\"Name\" required>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label class=\"control-label col-sm-2\">Amount:</label>\n" +
    "            <div class=\"col-sm-10\">\n" +
    "                <input type=\"text\" name=\"amount\" ng-model=\"amount\" class=\"form-control\" placeholder=\"Amount\" required>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label class=\"control-label col-sm-2\" for=\"date-to-charge\">Status</label>\n" +
    "            <div class=\"col-sm-10\">\n" +
    "                <select class=\"form-control\" ng-options=\"status for status in validStatus\" ng-model=\"status\"></select>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <div class=\"col-sm-offset-2 col-sm-10\">\n" +
    "                <button type=\"submit\" class=\"btn btn-default\">Submit</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "</div>\n" +
    "");
}]);

angular.module("accounting/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("accounting/index.tpl.html",
    "<div class=\"container-fluid container-fullw\">\n" +
    "	<div class=\"row\">\n" +
    "		<div class=\"col-md-12\">\n" +
    "			<h1 class=\"mainTitle text-primary\">Accounting</h1>\n" +
    "			<div class=\"form-group text-right\">\n" +
    "				<button ng-click=\"vm.gotoRevenue()\" class=\"btn btn-primary btn-o\" type=\"button\">Revenue</button>\n" +
    "				<button ng-click=\"vm.gotoInvoices()\" class=\"btn btn-primary btn-o\" type=\"button\">Invoices</button>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "	<div class=\"clearfix md-pull\">\n" +
    "		<form class=\"form-inline pull-left\">\n" +
    "			<div class=\"form-group\">\n" +
    "				<input class=\"form-control\" type=\"text\" placeholder=\"From\" bs-datepicker ng-model=\"vm.from\" data-autoclose=\"1\" data-date-type=\"string\" data-date-format=\"{{app.format_date}}\" data-container=\"body\" />\n" +
    "			</div>\n" +
    "			<div class=\"form-group\">\n" +
    "				<input class=\"form-control\" type=\"text\" placeholder=\"To\" bs-datepicker ng-model=\"vm.to\" data-autoclose=\"1\" data-date-type=\"string\" data-date-format=\"{{app.format_date}}\" data-container=\"body\"/>\n" +
    "			</div>\n" +
    "		</form>\n" +
    "		<div class=\"btn-group pull-right\">\n" +
    "			<a class=\"btn btn-primary btn-o\" href=\"#\" ng-class=\"vm.isFilterActive(0)\" ng-click=\"vm.changeFilter(0)\">\n" +
    "				Today\n" +
    "			</a>\n" +
    "			<a class=\"btn btn-primary btn-o\" href=\"#\" ng-class=\"vm.isFilterActive(-1)\" ng-click=\"vm.changeFilter(-1)\">\n" +
    "				Yesterday\n" +
    "			</a>\n" +
    "			<a class=\"btn btn-primary btn-o active\" href=\"#\" ng-class=\"vm.isFilterActive(-7)\" ng-click=\"vm.changeFilter(-7)\">\n" +
    "				Last 7 Days\n" +
    "			</a>\n" +
    "			<a class=\"btn btn-primary btn-o\" href=\"#\" ng-class=\"vm.isFilterActive(-30)\" ng-click=\"vm.changeFilter(-30)\">\n" +
    "				Last 30 Days\n" +
    "			</a>\n" +
    "			<a class=\"btn btn-primary btn-o\" href=\"#\" ng-class=\"vm.isFilterActive(-90)\" ng-click=\"vm.changeFilter(-90)\">\n" +
    "				Last 90 Days\n" +
    "			</a>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "	<div class=\"row\">\n" +
    "		<div class=\"tabbable margin-top-20\">\n" +
    "		  <ul class=\"nav nav-tabs vertical nav-justified\">\n" +
    "				<li ng-class=\"{active: vm.isTab(1)}\" ng-click=\"vm.selectTab(1)\">\n" +
    "		  		<a href=\"#\">\n" +
    "			  		<div class=\"panel panel-tab\">\n" +
    "							<div class=\"panel-heading border-light text-center\">\n" +
    "								<h4 class=\"panel-title\">REVENUE</h4>\n" +
    "							</div>\n" +
    "							<div class=\"panel-body\">\n" +
    "								<div class=\"static-block\">\n" +
    "									<div class=\"chart-statistics normal-font-family\">\n" +
    "										<h1>${{vm.totalRevenue}}</h1>\n" +
    "										<div class=\"rate\">\n" +
    "											<i class=\"fa fa-caret-up text-green\"></i>\n" +
    "											<span class=\"value\">+2000</span><span class=\"percentage\">%</span>\n" +
    "										</div>\n" +
    "									</div>\n" +
    "								</div>\n" +
    "							</div>\n" +
    "						</div>\n" +
    "					</a>\n" +
    "				</li>\n" +
    "				<li  ng-class=\"{active: vm.isTab(2)}\" ng-click=\"vm.selectTab(2)\">\n" +
    "		  		<a href=\"#\">\n" +
    "			  		<div class=\"panel panel-tab\">\n" +
    "							<div class=\"panel-heading border-light text-center\">\n" +
    "								<h4 class=\"panel-title\">JOBS THIS MONTH</h4>\n" +
    "							</div>\n" +
    "							<div class=\"panel-body\">\n" +
    "								<div class=\"static-block\">\n" +
    "									<div class=\"chart-statistics normal-font-family\">\n" +
    "										<h1>{{vm.totalJobs}}</h1>\n" +
    "										<div class=\"rate\">\n" +
    "											<i class=\"fa fa-caret-up text-green\"></i>\n" +
    "											<span class=\"value\">+20</span><span class=\"percentage\">%</span>\n" +
    "										</div>\n" +
    "									</div>\n" +
    "								</div>\n" +
    "							</div>\n" +
    "						</div>\n" +
    "					</a>\n" +
    "				</li>\n" +
    "				<li  ng-class=\"{active: vm.isTab(3)}\" ng-click=\"vm.selectTab(3)\">\n" +
    "					<a href=\"#\">\n" +
    "			  		<div class=\"panel panel-tab\">\n" +
    "							<div class=\"panel-heading border-light text-center\">\n" +
    "								<h4 class=\"panel-title\">PLACEMENT RATE</h4>\n" +
    "							</div>\n" +
    "							<div class=\"panel-body\">\n" +
    "								<div class=\"static-block\">\n" +
    "									<div class=\"chart-statistics normal-font-family\">\n" +
    "										<h1>10,5</h1>\n" +
    "										<div class=\"rate\">\n" +
    "											<i class=\"fa fa-caret-down text-red\"></i>\n" +
    "											<span class=\"value\">-2,3</span><span class=\"percentage\">%</span>\n" +
    "										</div>\n" +
    "									</div>\n" +
    "								</div>\n" +
    "							</div>\n" +
    "						</div>\n" +
    "					</a>\n" +
    "				</li>\n" +
    "			</ul>\n" +
    "			<div class=\"tab-content\" ng-if=\"vm.tab==1\">\n" +
    "				<div class=\"tab-pane active\">\n" +
    "					<div class=\"panel panel-white no-border no-radius\">\n" +
    "						<div class=\"panel-heading text-center\">\n" +
    "							<h4 class=\"panel-title\">Revenue</h4>\n" +
    "						</div>\n" +
    "						<div class=\"panel-body\">\n" +
    "							<div class=\"height-200\">\n" +
    "								<canvas class=\"tc-chart\" tc-chartjs-line chart-options=\"revenueGraphoptions\" chart-data=\"revenueGraphData\" chart-legend=\"chart1\" width=\"100%\"></canvas>\n" +
    "								<div class=\"margin-top-20\">\n" +
    "									<div tc-chartjs-legend chart-legend=\"chart1\" class=\"inline pull-right\"></div>\n" +
    "								</div>\n" +
    "							</div>\n" +
    "						</div>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "			<div class=\"tab-content\" ng-if=\"vm.tab==2\">\n" +
    "				<div class=\"tab-pane active\">\n" +
    "					<div class=\"panel panel-white no-border no-radius\">\n" +
    "						<div class=\"panel-heading text-center\">\n" +
    "							<h4 class=\"panel-title\">Jobs this month</h4>\n" +
    "						</div>\n" +
    "						<div class=\"panel-body\">\n" +
    "							<div class=\"height-200\">\n" +
    "								<canvas class=\"tc-chart\" tc-chartjs-line chart-options=\"jobsGraphoptions\" chart-data=\"jobsGraphData\" chart-legend=\"chart1\" width=\"100%\"></canvas>\n" +
    "								<div class=\"margin-top-20\">\n" +
    "									<div tc-chartjs-legend chart-legend=\"chart1\" class=\"inline pull-right\"></div>\n" +
    "								</div>\n" +
    "							</div>\n" +
    "						</div>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "			<div class=\"tab-content\" ng-if=\"vm.tab==3\">\n" +
    "				<div class=\"tab-pane active\">\n" +
    "					<div class=\"panel panel-white no-border no-radius\">\n" +
    "						<div class=\"panel-heading text-center\">\n" +
    "							<h4 class=\"panel-title\">Placement rate</h4>\n" +
    "						</div>\n" +
    "						<div class=\"panel-body\">\n" +
    "							<div class=\"height-200\">\n" +
    "								<canvas class=\"tc-chart\" tc-chartjs-line chart-options=\"jobsGraphoptions\" chart-data=\"jobsGraphData\" chart-legend=\"chart1\" width=\"100%\"></canvas>\n" +
    "								<div class=\"margin-top-20\">\n" +
    "									<div tc-chartjs-legend chart-legend=\"chart1\" class=\"inline pull-right\"></div>\n" +
    "								</div>\n" +
    "							</div>\n" +
    "						</div>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("accounting/invoices.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("accounting/invoices.tpl.html",
    "Under Construction");
}]);

angular.module("accounting/payment-details.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("accounting/payment-details.tpl.html",
    "Under Construction");
}]);

angular.module("accounting/revenue.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("accounting/revenue.tpl.html",
    "<section id=\"page-title\" class=\"padding-top-15 padding-bottom-15\"  ng-controller=\"RevenueCtrl\">\n" +
    "<div class=\"row\">\n" +
    "        <div class=\"col-sm-12\">\n" +
    "            <div class=\"btn-group pull-left\"><h1 class=\"mainTitle text-primary\">Revenue</h1></div>\n" +
    "			<div class=\"clearfix md-pull\">\n" +
    "				<form class=\"form-inline pull-right\">\n" +
    "				<div class=\"form-group\">\n" +
    "					<input class=\"form-control\" type=\"text\" placeholder=\"From\" bs-datepicker ng-model=\"start_date\" data-autoclose=\"1\" data-date-type=\"string\" data-date-format=\"{{app.format_date}}\" data-container=\"body\" />\n" +
    "				</div>\n" +
    "				<div class=\"form-group\">\n" +
    "					<input class=\"form-control\" type=\"text\" placeholder=\"To\" bs-datepicker ng-model=\"end_date\" data-autoclose=\"1\" data-date-type=\"string\" data-date-format=\"{{app.format_date}}\" data-container=\"body\" data-min-date=\"{{start_date}}\"/>\n" +
    "				</div>\n" +
    "				<div class=\"form-group\">\n" +
    "					<button ng-click=\"search()\" type=\"button\" class=\"btn btn-primary btn-o\">Get</button>\n" +
    "				</div>\n" +
    "			</form>\n" +
    "			</div>\n" +
    "        </div>\n" +
    "</div>\n" +
    "<div class=\"container-fluid container-fullw padding-bottom-10\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-sm-12\">\n" +
    "			<div class=\"panel panel-white no-radius\" id=\"visits\">\n" +
    "				<div class=\"panel-heading border-light\">\n" +
    "					<h4 class=\"panel-title\"> Total Revenue </h4>\n" +
    "				</div>\n" +
    "				<div class=\"panel-wrapper\">\n" +
    "					<div class=\"panel-body\">\n" +
    "						<div class=\"height-350\">\n" +
    "							<canvas class=\"tc-chart\" tc-chartjs-line chart-options=\"options\" chart-data=\"data\" chart-legend=\"chart1\" width=\"100%\"></canvas>\n" +
    "							<div class=\"margin-top-20\">\n" +
    "								<div tc-chartjs-legend chart-legend=\"chart1\" class=\"inline pull-right\"></div>\n" +
    "							</div>\n" +
    "						</div>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "	{{vm.data}}\n" +
    "	 <div class=\"row\">\n" +
    "        <div class=\"col-sm-12\">\n" +
    "			<div class=\"panel panel-white no-radius\" id=\"visits\">\n" +
    "				<div class=\"panel-heading border-light\">\n" +
    "					<h4 class=\"panel-title\"> Revenue </h4>\n" +
    "				</div>\n" +
    "				<div class=\"panel-wrapper\">\n" +
    "					<div class=\"panel-body\">\n" +
    "						<table class=\"table table-striped table-hover margin-top-20\">\n" +
    "							<thead>\n" +
    "							<tr>\n" +
    "								<th>\n" +
    "									Month\n" +
    "								</th>\n" +
    "								<th>\n" +
    "									Amount\n" +
    "								</th>\n" +
    "							</tr>\n" +
    "						</thead>\n" +
    "						<tbody>\n" +
    "							<tr ng-repeat=\"item in revenueData\">\n" +
    "								<td>{{item.mon}}</td>\n" +
    "								<td>{{item.revenue}}</td>\n" +
    "							</tr>\n" +
    "							<tr ng-if=\"revenueData.length === 0\">\n" +
    "								<td colspan=\"2\">\n" +
    "									No record found!\n" +
    "								</td>\n" +
    "							</tr>\n" +
    "						</tbody>\n" +
    "					</table>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "	\n" +
    "</section>");
}]);

angular.module("accounting/subscription.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("accounting/subscription.tpl.html",
    "<section id=\"page-title\" style=\"border:0\" class=\"padding-top-15 padding-bottom-15\" ng-controller=\"RevenueCtrl\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-sm-12\">\n" +
    "            <div class=\"btn-group margin-top-15\">\n" +
    "                <h1 class=\"mainTitle text-primary\">Subscriptions</h1>\n" +
    "            </div>\n" +
    "            <div class=\"divider\"><hr /></div>\n" +
    "            <div class=\"clearfix margin-top-15\">\n" +
    "                <form class=\"form-inline\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <select>\n" +
    "                            <option>Select Subscriptions</option>\n" +
    "                            <option>Per User - $100</option>\n" +
    "                            <option>Monthly - $4000.00</option>\n" +
    "                        </select>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <button type=\"button\" class=\"btn btn-primary btn-o\">Save</button>\n" +
    "                    </div>\n" +
    "                </form>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>");
}]);

angular.module("admindashboard/templates/admin_dashboard.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("admindashboard/templates/admin_dashboard.tpl.html",
    "<!-- start: DASHBOARD TITLE -->\n" +
    "<section id=\"page-title\" class=\"padding-top-15 padding-bottom-15\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-sm-7\">\n" +
    "            <h1 class=\"mainTitle\" translate=\"dashboard.WELCOME\" translate-values=\"{ appName: app.name }\">{{ mainTitle }}</h1>\n" +
    "            <span class=\"mainDescription\">overview &amp; stats </span>\n" +
    "        </div>\n" +
    "        <div class=\"col-sm-5\">\n" +
    "            <!-- start: MINI STATS WITH SPARKLINE -->\n" +
    "            <!-- /// controller:  'SparklineCtrl' -  localtion: assets/js/controllers/dashboardCtrl.js /// -->\n" +
    "            <ul class=\"mini-stats pull-left\" ng-controller=\"SparklineCtrl\">\n" +
    "                <li>\n" +
    "                    <div class=\"sparkline\">\n" +
    "                        <span jq-sparkline ng-model=\"sales\" type=\"bar\" height=\"20px\" bar-color=\"#D43F3A\"></span>\n" +
    "                    </div>\n" +
    "                    <div class=\"values\">\n" +
    "                        <strong class=\"text-dark\">18304</strong>\n" +
    "                        <p class=\"text-small no-margin\">\n" +
    "                            Sales\n" +
    "                        </p>\n" +
    "                    </div>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <div class=\"sparkline\">\n" +
    "                        <span jq-sparkline ng-model=\"earnings\" type=\"bar\" height=\"20px\" bar-color=\"#5CB85C\"></span>\n" +
    "                    </div>\n" +
    "                    <div class=\"values\">\n" +
    "                        <strong class=\"text-dark\">&#36;3,833</strong>\n" +
    "                        <p class=\"text-small no-margin\">\n" +
    "                            Earnings\n" +
    "                        </p>\n" +
    "                    </div>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <div class=\"sparkline\">\n" +
    "                        <span jq-sparkline ng-model=\"referrals\" type=\"bar\" height=\"20px\" bar-color=\"#46B8DA\"></span>\n" +
    "                    </div>\n" +
    "                    <div class=\"values\">\n" +
    "                        <strong class=\"text-dark\">&#36;848</strong>\n" +
    "                        <p class=\"text-small no-margin\">\n" +
    "                            Referrals\n" +
    "                        </p>\n" +
    "                    </div>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "            <!-- end: MINI STATS WITH SPARKLINE -->\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "<!-- end: DASHBOARD TITLE -->\n" +
    "<!-- start: FEATURED BOX LINKS -->\n" +
    "<div class=\"container-fluid container-fullw bg-white\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-sm-4\">\n" +
    "            <div class=\"panel panel-white no-radius text-center\">\n" +
    "                <div class=\"panel-body\">\n" +
    "                    <span class=\"fa-stack fa-2x\"> <i class=\"fa fa-square fa-stack-2x text-primary\"></i> <i class=\"fa fa-smile-o fa-stack-1x fa-inverse\"></i> </span>\n" +
    "                    <h2 class=\"StepTitle\">Manage Users</h2>\n" +
    "                    <p class=\"text-small\">\n" +
    "                        To add users, you need to be signed in as the super user.\n" +
    "                    </p>\n" +
    "                    <p class=\"links cl-effect-1\">\n" +
    "                        <a href>\n" +
    "							view more\n" +
    "						</a>\n" +
    "                    </p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-sm-4\">\n" +
    "            <div class=\"panel panel-white no-radius text-center\">\n" +
    "                <div class=\"panel-body\">\n" +
    "                    <span class=\"fa-stack fa-2x\"> <i class=\"fa fa-square fa-stack-2x text-primary\"></i> <i class=\"fa fa-paperclip fa-stack-1x fa-inverse\"></i> </span>\n" +
    "                    <h2 class=\"StepTitle\">Manage Invoices</h2>\n" +
    "                    <p class=\"text-small\">\n" +
    "                        The Manage Invoices tool provides a view of all your invoices.\n" +
    "                    </p>\n" +
    "                    <p class=\"cl-effect-1\">\n" +
    "                        <a href>\n" +
    "							view more\n" +
    "						</a>\n" +
    "                    </p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-sm-4\">\n" +
    "            <div class=\"panel panel-white no-radius text-center\">\n" +
    "                <div class=\"panel-body\">\n" +
    "                    <span class=\"fa-stack fa-2x\"> <i class=\"fa fa-square fa-stack-2x text-primary\"></i> <i class=\"fa fa-terminal fa-stack-1x fa-inverse\"></i> </span>\n" +
    "                    <h2 class=\"StepTitle\">Manage Jobs</h2>\n" +
    "                    <p class=\"text-small\">\n" +
    "                        The Manage Jobs tool provides a view of all your jobs.\n" +
    "                    </p>\n" +
    "                    <p class=\"links cl-effect-1\">\n" +
    "                        <a href>\n" +
    "							view more\n" +
    "						</a>\n" +
    "                    </p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<!-- end: FEATURED BOX LINKS -->\n" +
    "<!-- start: FIRST SECTION -->\n" +
    "<div class=\"container-fluid container-fullw padding-bottom-10\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-sm-12\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-7 col-lg-8\">\n" +
    "                    <div class=\"panel panel-white no-radius\" id=\"visits\">\n" +
    "                        <div class=\"panel-heading border-light\">\n" +
    "                            <h4 class=\"panel-title\"> Average Revenue Per Client </h4>\n" +
    "                            <ul class=\"panel-heading-tabs border-light\">\n" +
    "                                <li>\n" +
    "                                    <div class=\"pull-left\">\n" +
    "                                        <div class=\"btn-group\" dropdown is-open=\"status.isopen\">\n" +
    "                                            <a dropdown-toggle ng-disabled=\"disabled\" class=\"padding-10\">\n" +
    "                                                <i class=\"ti-more-alt \"></i>\n" +
    "                                            </a>\n" +
    "                                            <ul class=\"dropdown-menu dropdown-light\" role=\"menu\">\n" +
    "                                                <li>\n" +
    "                                                    <a href=\"#\">\n" +
    "														Action\n" +
    "													</a>\n" +
    "                                                </li>\n" +
    "                                                <li>\n" +
    "                                                    <a href=\"#\">\n" +
    "														Another action\n" +
    "													</a>\n" +
    "                                                </li>\n" +
    "                                                <li>\n" +
    "                                                    <a href=\"#\">\n" +
    "														Something else here\n" +
    "													</a>\n" +
    "                                                </li>\n" +
    "                                            </ul>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                </li>\n" +
    "                                <li>\n" +
    "                                    <div class=\"rate\">\n" +
    "                                        <i class=\"fa fa-caret-up text-primary\"></i><span class=\"value\">15</span><span class=\"percentage\">%</span>\n" +
    "                                    </div>\n" +
    "                                </li>\n" +
    "                                <li class=\"panel-tools\">\n" +
    "                                    <ct-paneltool tool-refresh=\"load1\"></ct-paneltool>\n" +
    "                                </li>\n" +
    "                            </ul>\n" +
    "                        </div>\n" +
    "                        <div collapse=\"visits\" ng-init=\"visits=false\" class=\"panel-wrapper\">\n" +
    "                            <div class=\"panel-body\">\n" +
    "                                <!-- /// controller:  'VisitsCtrl' -  localtion: assets/js/controllers/dashboardCtrl.js /// -->\n" +
    "                                <div ng-controller=\"VisitsCtrl\" class=\"height-350\">\n" +
    "                                    <canvas class=\"tc-chart\" tc-chartjs-line chart-options=\"options\" chart-data=\"data\" chart-legend=\"chart1\" width=\"100%\"></canvas>\n" +
    "                                    <div class=\"margin-top-20 row\">\n" +
    "										<div class=\"col-md-3\">\n" +
    "											<input class=\"form-control col-md-2\" type=\"text\" placeholder=\"From\" bs-datepicker ng-model=\"vm.start_date\" data-autoclose=\"1\" data-date-type=\"string\" data-date-format=\"{{app.format_date}}\" data-container=\"body\" />\n" +
    "										</div>\n" +
    "										<div class=\"col-md-3\">\n" +
    "											<input class=\"form-control  col-md-2\" type=\"text\" placeholder=\"To\" bs-datepicker ng-model=\"vm.end_date\" data-autoclose=\"1\" data-date-type=\"string\" data-date-format=\"{{app.format_date}}\" data-container=\"body\" data-min-date=\"{{vm.start_date}}\" />\n" +
    "										</div>\n" +
    "										<div class=\"col-md-6\">\n" +
    "											 <div tc-chartjs-legend chart-legend=\"chart1\" class=\"inline pull-right\"></div>\n" +
    "										</div>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-sm-4\">\n" +
    "                    <div class=\"panel panel-white no-radius\" style=\"margin-bottom: 8px;\">\n" +
    "                        <div class=\"panel-heading border-bottom\">\n" +
    "                            <h4 class=\"panel-title\">Placement Rate</h4>\n" +
    "                        </div>\n" +
    "                        <div class=\"panel-body\">\n" +
    "                            <!-- /// controller:  'OnotherCtrl' -  localtion: assets/js/controllers/dashboardCtrl.js /// -->\n" +
    "                            <div ng-controller=\"OnotherCtrl\">\n" +
    "                                <div class=\"text-center\">\n" +
    "                                    <span class=\"mini-pie\"> <canvas class=\"tc-chart\" tc-chartjs-doughnut chart-options=\"options\" chart-data=\"data\" chart-legend=\"chart3\" width=\"100\"></canvas> <span>{{total}}</span> </span>\n" +
    "                                    <span class=\"inline text-large no-wrap\">Placements</span>\n" +
    "                                </div>\n" +
    "                                <div class=\"margin-top-20 text-center legend-xs\">\n" +
    "                                    <div tc-chartjs-legend chart-legend=\"chart3\" class=\"inline\"></div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"panel-footer\">\n" +
    "                            <div class=\"clearfix padding-5 space5\">\n" +
    "                                <div class=\"col-xs-4 text-center no-padding\">\n" +
    "                                    <div class=\"border-left border-dark\">\n" +
    "                                        <span class=\"text-bold block text-extra-large\">90%</span>\n" +
    "                                        <span class=\"text-light\">Successful</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                                <div class=\"col-xs-4 text-center no-padding\">\n" +
    "                                    <div class=\"border-left border-dark\">\n" +
    "                                        <span class=\"text-bold block text-extra-large\">2%</span>\n" +
    "                                        <span class=\"text-light\">Failed</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                                <div class=\"col-xs-4 text-center no-padding\">\n" +
    "                                    <span class=\"text-bold block text-extra-large\">8%</span>\n" +
    "                                    <span class=\"text-light\">Unknown</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-md-12\" style=\"padding:0px\">\n" +
    "                        <div class=\"panel panel-white no-radius\">\n" +
    "                            <div class=\"panel-body padding-20 text-center\">\n" +
    "                                <div class=\"\">\n" +
    "                                    <h5 class=\"text-dark no-margin\" style=\"font-size: 16px;\">Average Lifeline Of Subscription</h5>\n" +
    "                                    <h2 class=\"no-margin\">1,450</h2>\n" +
    "                                    <span class=\"badge badge-danger margin-top-10\">lifelines</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<!-- end: FIRST SECTION -->\n" +
    "\n" +
    "\n" +
    "\n" +
    "<!-- start: FOURTH SECTION -->\n" +
    "<div class=\"container-fluid container-fullw bg-white\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-xs-12 col-sm-8\">\n" +
    "            <div class=\"row\">\n" +
    "                <!-- /// controller:  'SparklineCtrl' -  localtion: assets/js/controllers/dashboardCtrl.js /// -->\n" +
    "                <div ng-controller=\"SparklineCtrl\">\n" +
    "                    <div class=\"col-md-6\">\n" +
    "                        <div class=\"panel panel-white no-radius\">\n" +
    "                            <div class=\"panel-body padding-20 text-center\">\n" +
    "                                <div class=\"space10\">\n" +
    "                                    <h5 class=\"text-dark admin-box no-margin\">Active Clients</h5>\n" +
    "                                    <h2 class=\"no-margin\">1,450</h2>\n" +
    "                                    <span class=\"badge badge-danger margin-top-10\">active</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-md-6\">\n" +
    "                        <div class=\"panel panel-white no-radius\">\n" +
    "                            <div class=\"panel-body padding-20 text-center\">\n" +
    "                                <div class=\"space10\">\n" +
    "                                    <h5 class=\"text-dark admin-box no-margin\">Average Users Per Client</h5>\n" +
    "                                    <h2 class=\"no-margin\">1,450</h2>\n" +
    "                                    <span class=\"badge badge-danger margin-top-10\">users</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"col-md-6\">\n" +
    "                    <div class=\"panel panel-white no-radius\">\n" +
    "                        <div class=\"panel-body padding-20 text-center\">\n" +
    "                            <div class=\"space10\">\n" +
    "                                <h5 class=\"text-dark admin-box no-margin\">Average Postings Per Client</h5>\n" +
    "                                <h2 class=\"no-margin\">1,450</h2>\n" +
    "                                <span class=\"badge badge-danger margin-top-10\">postings</span>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                    <div class=\"col-md-6\">\n" +
    "                        <div class=\"panel panel-white no-radius\">\n" +
    "                            <div class=\"panel-body padding-20 text-center\">\n" +
    "                                <div class=\"space10\">\n" +
    "                                    <h5 class=\"text-dark admin-box no-margin\">Average Salary Per Posting</h5>\n" +
    "                                    <h2 class=\"no-margin\">1,450</h2>\n" +
    "                                    <span class=\"badge badge-danger margin-top-10\">postings</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-12 col-sm-4\">\n" +
    "            <div class=\"panel panel-white no-radius\">\n" +
    "                <div class=\"panel-heading border-bottom\">\n" +
    "                    <h4 class=\"panel-title\">Recent Activities</h4>\n" +
    "                </div>\n" +
    "                <div class=\"panel-body recent-activities\" style=\"padding:0px 15px;\">\n" +
    "                    <ul class=\"timeline-xs margin-bottom-15\">\n" +
    "                        <li class=\"timeline-item success\">\n" +
    "                            <div class=\"margin-left-15 margin-top-10 \">\n" +
    "                                <div class=\"text-muted text-small\">\n" +
    "                                    2 minutes ago\n" +
    "                                </div>\n" +
    "                                <p>\n" +
    "                                    <a class=\"text-info\" href>\n" +
    "										Steven\n" +
    "									</a> has completed his account.\n" +
    "                                </p>\n" +
    "                            </div>\n" +
    "                        </li>\n" +
    "                        <li class=\"timeline-item\">\n" +
    "                            <div class=\"margin-left-15 margin-top-10\">\n" +
    "                                <div class=\"text-muted text-small\">\n" +
    "                                    12:30\n" +
    "                                </div>\n" +
    "                                <p>\n" +
    "                                    Staff Meeting\n" +
    "                                </p>\n" +
    "                            </div>\n" +
    "                        </li>\n" +
    "                        <li class=\"timeline-item danger\">\n" +
    "                            <div class=\"margin-left-15 margin-top-10\">\n" +
    "                                <div class=\"text-muted text-small\">\n" +
    "                                    11:11\n" +
    "                                </div>\n" +
    "                                <p>\n" +
    "                                    Completed new layout.\n" +
    "                                </p>\n" +
    "                            </div>\n" +
    "                        </li>\n" +
    "                        <li class=\"timeline-item info\">\n" +
    "                            <div class=\"margin-left-15 margin-top-10\">\n" +
    "                                <div class=\"text-muted text-small\">\n" +
    "                                    Thu, 12 Jun\n" +
    "                                </div>\n" +
    "                                <p>\n" +
    "                                    Contacted\n" +
    "                                    <a class=\"text-info\" href>\n" +
    "										Microsoft\n" +
    "									</a> for license upgrades.\n" +
    "                                </p>\n" +
    "                            </div>\n" +
    "                        </li>\n" +
    "\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<!-- end: FOURTH SECTION -->\n" +
    "");
}]);

angular.module("auth/login.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("auth/login.tpl.html",
    "<div class=\"row\">\n" +
    "	<div class=\"main-login col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4\">\n" +
    "		<div class=\"logo\">\n" +
    "			<img src=\"img/logo-home.png\">\n" +
    "		</div>\n" +
    "		<!-- start: LOGIN BOX -->\n" +
    "		<div class=\"box-login\">\n" +
    "			<form class=\"form-login\" ng-submit=\"vm.login()\">\n" +
    "				<fieldset>\n" +
    "					<p>\n" +
    "						Please enter your email and password to log in.\n" +
    "					</p>\n" +
    "					<div class=\"form-group\">\n" +
    "						<span class=\"input-icon\">\n" +
    "							<input type=\"text\" class=\"form-control\" ng-model=\"vm.form.email\" placeholder=\"Email\">\n" +
    "							<i class=\"fa fa-user\"></i> </span>\n" +
    "					</div>\n" +
    "					<div class=\"form-group form-actions\">\n" +
    "						<span class=\"input-icon\">\n" +
    "							<input type=\"password\" class=\"form-control\" ng-model=\"vm.form.password\" placeholder=\"Password\">\n" +
    "							<i class=\"fa fa-lock\"></i>\n" +
    "							</span>\n" +
    "					</div>\n" +
    "					<div class=\"form-actions\">\n" +
    "						<button type=\"submit\" class=\"btn btn-primary btn-o pull-right\">\n" +
    "							Login <i class=\"fa fa-arrow-circle-right\"></i>\n" +
    "						</button>\n" +
    "					</div>\n" +
    "				</fieldset>\n" +
    "			</form>\n" +
    "		</div>\n" +
    "		<!-- end: LOGIN BOX -->\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("candidates/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("candidates/index.tpl.html",
    "<div class=\"container-fluid container-fullw bg-white\">\n" +
    "	<div class=\"row\">\n" +
    "		<div class=\"col-md-12\">\n" +
    "			<h1 class=\"mainTitle text-primary\">Candidates</h1>\n" +
    "			<div class=\"clearfix md-pull\">\n" +
    "				<div class=\"btn-group pull-left\">\n" +
    "					<a class=\"btn btn-primary btn-o\" href=\"#\" ng-class=\"vm.isFilterActive(7)\" ng-click=\"vm.changeFilter(7)\">\n" +
    "						Last 7 Days\n" +
    "					</a>\n" +
    "					<a class=\"btn btn-primary btn-o\" href=\"#\" ng-class=\"vm.isFilterActive(30)\" ng-click=\"vm.changeFilter(30)\">\n" +
    "						Last 30 Days\n" +
    "					</a>\n" +
    "					<a class=\"btn btn-primary btn-o\" href=\"#\" ng-class=\"vm.isFilterActive(90)\" ng-click=\"vm.changeFilter(90)\">\n" +
    "						Last 90 Days\n" +
    "					</a>\n" +
    "				</div>\n" +
    "				<form class=\"form-inline pull-right\">\n" +
    "					<div class=\"form-group\">\n" +
    "						<input ng-model=\"vm.keyword\" ng-keypress=\"vm.keypress($event)\" class=\"form-control\" type=\"text\" placeholder=\"Search\"/>\n" +
    "					</div>\n" +
    "					<div class=\"form-group\">\n" +
    "						<button ng-click=\"vm.search()\" type=\"button\" class=\"btn btn-primary btn-o\">Search</button>\n" +
    "					</div>\n" +
    "				</form>\n" +
    "			</div>\n" +
    "			<table class=\"table table-striped table-hover margin-top-20\">\n" +
    "				<thead>\n" +
    "					<tr>\n" +
    "						<th>\n" +
    "							Name\n" +
    "						</th>\n" +
    "						<th>\n" +
    "							Job Field\n" +
    "						</th>\n" +
    "						<th>\n" +
    "							Employment Status\n" +
    "						</th>\n" +
    "						<th>\n" +
    "							Working\n" +
    "						</th>\n" +
    "						<th>\n" +
    "							Registered On\n" +
    "						</th>\n" +
    "						<th>\n" +
    "							Location\n" +
    "						</th>\n" +
    "						<th>\n" +
    "							Resume\n" +
    "						</th>\n" +
    "					</tr>\n" +
    "				</thead>\n" +
    "				<tbody>\n" +
    "					<tr ng-repeat=\"item in vm.items\">\n" +
    "						<td class=\"cursor-pointer\" ng-click=\"vm.viewDetail(item)\">{{item.name}}</td>\n" +
    "						<td>{{item.job_title}}</td>\n" +
    "						<td></td>\n" +
    "						<td></td>\n" +
    "						<td>{{item.date}}</td>\n" +
    "						<td>{{item.address.province}}</td>\n" +
    "						<td>\n" +
    "							<i ng-click=\"vm.viewDetail(item)\" class=\"ti-eye text-primary pointer\"></i>\n" +
    "						</td>\n" +
    "					</tr>\n" +
    "					<tr ng-if=\"vm.items.length === 0\">\n" +
    "						<td colspan=\"7\">\n" +
    "							No record found!\n" +
    "						</td>\n" +
    "					</tr>\n" +
    "				</tbody>\n" +
    "			</table>\n" +
    "			<div class=\"text-right\">\n" +
    "				<pagination rotate=\"false\" boundary-links=\"true\" max-size=\"5\" items-per-page=\"vm.paginator.per_page\" total-items=\"vm.paginator.total\" ng-model=\"vm.paginator.current_page\" ng-change=\"vm.getList()\"></pagination>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("dashboard/templates/dashboard.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("dashboard/templates/dashboard.tpl.html",
    "<!-- start: DASHBOARD TITLE -->\n" +
    "<section id=\"page-title\" class=\"padding-top-15 padding-bottom-15\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-sm-7\">\n" +
    "            <h1 class=\"mainTitle\" translate=\"dashboard.WELCOME\" translate-values=\"{ appName: app.name }\">{{ mainTitle }}</h1>\n" +
    "            <span class=\"mainDescription\">overview &amp; stats </span>\n" +
    "        </div>\n" +
    "        <div class=\"col-sm-5\">\n" +
    "            <!-- start: MINI STATS WITH SPARKLINE -->\n" +
    "            <!-- /// controller:  'SparklineCtrl' -  localtion: assets/js/controllers/dashboardCtrl.js /// -->\n" +
    "            \n" +
    "            <!-- end: MINI STATS WITH SPARKLINE -->\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "<!-- end: DASHBOARD TITLE -->\n" +
    "<!-- start: FEATURED BOX LINKS -->\n" +
    "<div class=\"container-fluid container-fullw bg-white\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-sm-4\">\n" +
    "            <div class=\"panel panel-white no-radius text-center\">\n" +
    "                <div class=\"panel-body\">\n" +
    "                    <span class=\"fa-stack fa-2x\"> <i class=\"fa fa-square fa-stack-2x text-primary\"></i> <i class=\"fa fa-smile-o fa-stack-1x fa-inverse\"></i> </span>\n" +
    "                    <h2 class=\"StepTitle\">Manage Users</h2>\n" +
    "                    <p class=\"text-small\">\n" +
    "                        To add users, you need to be signed in as the super user.\n" +
    "                    </p>\n" +
    "                    <p class=\"links cl-effect-1\">\n" +
    "                        <a href>\n" +
    "							view more\n" +
    "						</a>\n" +
    "                    </p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-sm-4\">\n" +
    "            <div class=\"panel panel-white no-radius text-center\">\n" +
    "                <div class=\"panel-body\">\n" +
    "                    <span class=\"fa-stack fa-2x\"> <i class=\"fa fa-square fa-stack-2x text-primary\"></i> <i class=\"fa fa-paperclip fa-stack-1x fa-inverse\"></i> </span>\n" +
    "                    <h2 class=\"StepTitle\">Manage Invoices</h2>\n" +
    "                    <p class=\"text-small\">\n" +
    "                        The Manage Invoices tool provides a view of all your invoices.\n" +
    "                    </p>\n" +
    "                    <p class=\"cl-effect-1\">\n" +
    "                        <a href>\n" +
    "							view more\n" +
    "						</a>\n" +
    "                    </p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-sm-4\">\n" +
    "            <div class=\"panel panel-white no-radius text-center\">\n" +
    "                <div class=\"panel-body\">\n" +
    "                    <span class=\"fa-stack fa-2x\"> <i class=\"fa fa-square fa-stack-2x text-primary\"></i> <i class=\"fa fa-terminal fa-stack-1x fa-inverse\"></i> </span>\n" +
    "                    <h2 class=\"StepTitle\">Manage Jobs</h2>\n" +
    "                    <p class=\"text-small\">\n" +
    "                        The Manage Jobs tool provides a view of all your jobs.\n" +
    "                    </p>\n" +
    "                    <p class=\"links cl-effect-1\">\n" +
    "                        <a href>\n" +
    "							view more\n" +
    "						</a>\n" +
    "                    </p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<!-- end: FEATURED BOX LINKS -->\n" +
    "<!-- start: FIRST SECTION -->\n" +
    "<div class=\"container-fluid container-fullw padding-bottom-10\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-sm-12\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-7 col-lg-8\">\n" +
    "                    <div class=\"panel panel-white no-radius\" id=\"visits\">\n" +
    "                        <div class=\"panel-heading border-light\">\n" +
    "                            <h4 class=\"panel-title\"> Gross Payroll of New Hires </h4>\n" +
    "                            <ul class=\"panel-heading-tabs border-light\">\n" +
    "                                <li>\n" +
    "                                    <div class=\"pull-left\">\n" +
    "                                        <div class=\"btn-group\" dropdown is-open=\"status.isopen\">\n" +
    "                                            <a dropdown-toggle ng-disabled=\"disabled\" class=\"padding-10\">\n" +
    "                                                <i class=\"ti-more-alt \"></i>\n" +
    "                                            </a>\n" +
    "                                            <ul class=\"dropdown-menu dropdown-light\" role=\"menu\">\n" +
    "                                                <li>\n" +
    "                                                    <a href=\"#\">\n" +
    "														Action\n" +
    "													</a>\n" +
    "                                                </li>\n" +
    "                                                <li>\n" +
    "                                                    <a href=\"#\">\n" +
    "														Another action\n" +
    "													</a>\n" +
    "                                                </li>\n" +
    "                                                <li>\n" +
    "                                                    <a href=\"#\">\n" +
    "														Something else here\n" +
    "													</a>\n" +
    "                                                </li>\n" +
    "                                            </ul>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                </li>\n" +
    "                                <li>\n" +
    "                                    <div class=\"rate\">\n" +
    "                                        <i class=\"fa fa-caret-up text-primary\"></i><span class=\"value\">15</span><span class=\"percentage\">%</span>\n" +
    "                                    </div>\n" +
    "                                </li>\n" +
    "                                <li class=\"panel-tools\">\n" +
    "                                    <ct-paneltool tool-refresh=\"load1\"></ct-paneltool>\n" +
    "                                </li>\n" +
    "                            </ul>\n" +
    "                        </div>\n" +
    "                        <div collapse=\"visits\" ng-init=\"visits=false\" class=\"panel-wrapper\">\n" +
    "                            <div class=\"panel-body\">\n" +
    "                                <!-- /// controller:  'VisitsCtrl' -  localtion: assets/js/controllers/dashboardCtrl.js /// -->\n" +
    "                                <div ng-controller=\"VisitsCtrl\" class=\"height-350\">\n" +
    "                                    <canvas class=\"tc-chart\" tc-chartjs-line chart-options=\"options\" chart-data=\"data\" chart-legend=\"chart1\" width=\"100%\"></canvas>\n" +
    "                                    <div class=\"margin-top-20 row\">\n" +
    "										<div class=\"col-md-3\">\n" +
    "											<input class=\"form-control col-md-2\" type=\"text\" placeholder=\"From\" bs-datepicker ng-model=\"vm.start_date\" data-autoclose=\"1\" data-date-type=\"string\" data-date-format=\"{{app.format_date}}\" data-container=\"body\" />\n" +
    "										</div>\n" +
    "										<div class=\"col-md-3\">\n" +
    "											<input class=\"form-control  col-md-2\" type=\"text\" placeholder=\"To\" bs-datepicker ng-model=\"vm.end_date\" data-autoclose=\"1\" data-date-type=\"string\" data-date-format=\"{{app.format_date}}\" data-container=\"body\" data-min-date=\"{{vm.start_date}}\" />\n" +
    "										</div>\n" +
    "										<div class=\"col-md-6\">\n" +
    "											 <div tc-chartjs-legend chart-legend=\"chart1\" class=\"inline pull-right\"></div>\n" +
    "										</div>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-sm-4\">\n" +
    "                    <div class=\"panel panel-white no-radius\">\n" +
    "                        <div class=\"panel-heading border-bottom\">\n" +
    "                            <h4 class=\"panel-title\">Successful Placements</h4>\n" +
    "                        </div>\n" +
    "                        <div class=\"panel-body\">\n" +
    "                            <!-- /// controller:  'OnotherCtrl' -  localtion: assets/js/controllers/dashboardCtrl.js /// -->\n" +
    "                            <div ng-controller=\"OnotherCtrl\">\n" +
    "                                <div class=\"text-center\">\n" +
    "                                    <span class=\"mini-pie\"> <canvas class=\"tc-chart\" tc-chartjs-doughnut chart-options=\"options\" chart-data=\"data\" chart-legend=\"chart3\" width=\"100\"></canvas> <span>{{total}}</span> </span>\n" +
    "                                    <span class=\"inline text-large no-wrap\">Placements</span>\n" +
    "                                </div>\n" +
    "                                <div class=\"margin-top-20 text-center legend-xs\">\n" +
    "                                    <div tc-chartjs-legend chart-legend=\"chart3\" class=\"inline\"></div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"panel-footer\">\n" +
    "                            <div class=\"clearfix padding-5 space5\">\n" +
    "                                <div class=\"col-xs-4 text-center no-padding\">\n" +
    "                                    <div class=\"border-left border-dark\">\n" +
    "                                        <span class=\"text-bold block text-extra-large\">90%</span>\n" +
    "                                        <span class=\"text-light\">Successful</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                                <div class=\"col-xs-4 text-center no-padding\">\n" +
    "                                    <div class=\"border-left border-dark\">\n" +
    "                                        <span class=\"text-bold block text-extra-large\">2%</span>\n" +
    "                                        <span class=\"text-light\">Failed</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                                <div class=\"col-xs-4 text-center no-padding\">\n" +
    "                                    <span class=\"text-bold block text-extra-large\">8%</span>\n" +
    "                                    <span class=\"text-light\">Unknown</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<!-- end: FIRST SECTION -->\n" +
    "\n" +
    "<!-- start: FOURTH SECTION -->\n" +
    "<div class=\"container-fluid container-fullw bg-white\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-xs-12 col-sm-4\">\n" +
    "            <div class=\"row\">\n" +
    "                <!-- /// controller:  'SparklineCtrl' -  localtion: assets/js/controllers/dashboardCtrl.js /// -->\n" +
    "                <div ng-controller=\"SparklineCtrl\">\n" +
    "                    <div class=\"col-md-12\">\n" +
    "                        <div class=\"panel panel-white no-radius\">\n" +
    "                            <div class=\"panel-body padding-20 text-center\">\n" +
    "                                <div class=\"space10\">\n" +
    "                                    <h5 class=\"text-dark admin-box no-margin\">Active Jobs</h5>\n" +
    "                                    <h2 class=\"no-margin\">1,50</h2>\n" +
    "                                    <span class=\"badge badge-success margin-top-10\">active</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-md-12\">\n" +
    "                        <div class=\"panel panel-white no-radius\">\n" +
    "                            <div class=\"panel-body padding-20 text-center\">\n" +
    "                                <div class=\"space10\">\n" +
    "                                    <h5 class=\"text-dark admin-box no-margin\">From Posting to Placement</h5>\n" +
    "                                    <h2 class=\"no-margin\">1,450</h2>\n" +
    "                                    <span class=\"badge badge-danger margin-top-10\">days</span>\n" +
    "                                </div>\n" +
    "\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-md-12\">\n" +
    "                        <div class=\"panel panel-white no-radius\">\n" +
    "                            <div class=\"panel-body padding-20 text-center\">\n" +
    "                                <div class=\"space10\">\n" +
    "                                    <h5 class=\"text-dark admin-box no-margin\">Priority Jobs</h5>\n" +
    "                                    <h2 class=\"no-margin\">1,450</h2>\n" +
    "                                    <span class=\"badge badge-danger margin-top-10\">jobs</span>\n" +
    "                                </div>\n" +
    "\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-12 col-sm-4\">\n" +
    "            <div class=\"panel panel-white no-radius\">\n" +
    "                <div class=\"panel-heading border-bottom\">\n" +
    "                    <h4 class=\"panel-title\">Activities</h4>\n" +
    "                </div>\n" +
    "                <div class=\"panel-body\" style=\"padding:0px 15px;\">\n" +
    "                    <ul class=\"timeline-xs margin-top-15 margin-bottom-10\">\n" +
    "                        <li class=\"timeline-item success\">\n" +
    "                            <div class=\"margin-left-15\">\n" +
    "                                <div class=\"text-muted text-small\">\n" +
    "                                    2 minutes ago\n" +
    "                                </div>\n" +
    "                                <p>\n" +
    "                                    <a class=\"text-info\" href>\n" +
    "										Steven\n" +
    "									</a> has completed his account.\n" +
    "                                </p>\n" +
    "                            </div>\n" +
    "                        </li>\n" +
    "                        <li class=\"timeline-item\">\n" +
    "                            <div class=\"margin-left-15\">\n" +
    "                                <div class=\"text-muted text-small\">\n" +
    "                                    12:30\n" +
    "                                </div>\n" +
    "                                <p>\n" +
    "                                    Staff Meeting\n" +
    "                                </p>\n" +
    "                            </div>\n" +
    "                        </li>\n" +
    "                        <li class=\"timeline-item danger\">\n" +
    "                            <div class=\"margin-left-15\">\n" +
    "                                <div class=\"text-muted text-small\">\n" +
    "                                    11:11\n" +
    "                                </div>\n" +
    "                                <p>\n" +
    "                                    Completed new layout.\n" +
    "                                </p>\n" +
    "                            </div>\n" +
    "                        </li>\n" +
    "                        <li class=\"timeline-item info\">\n" +
    "                            <div class=\"margin-left-15\">\n" +
    "                                <div class=\"text-muted text-small\">\n" +
    "                                    Thu, 12 Jun\n" +
    "                                </div>\n" +
    "                                <p>\n" +
    "                                    Contacted\n" +
    "                                    <a class=\"text-info\" href>\n" +
    "										Microsoft\n" +
    "									</a> for license upgrades.\n" +
    "                                </p>\n" +
    "                            </div>\n" +
    "                        </li>\n" +
    "                        <li class=\"timeline-item info\">\n" +
    "                            <div class=\"margin-left-15\">\n" +
    "                                <div class=\"text-muted text-small\">\n" +
    "                                    Thu, 12 Jun\n" +
    "                                </div>\n" +
    "                                <p>\n" +
    "                                    Contacted\n" +
    "                                    <a class=\"text-info\" href>\n" +
    "                                        Microsoft\n" +
    "                                    </a> for license upgrades.\n" +
    "                                </p>\n" +
    "                            </div>\n" +
    "                        </li>\n" +
    "                        <li class=\"timeline-item\">\n" +
    "                            <div class=\"margin-left-15\">\n" +
    "                                <div class=\"text-muted text-small\">\n" +
    "                                    Tue, 10 Jun\n" +
    "                                </div>\n" +
    "                                <p>\n" +
    "                                    Started development new site\n" +
    "                                </p>\n" +
    "                            </div>\n" +
    "                        </li>\n" +
    "                        <li class=\"timeline-item\">\n" +
    "                            <div class=\"margin-left-15\">\n" +
    "                                <div class=\"text-muted text-small\">\n" +
    "                                    Sun, 11 Apr\n" +
    "                                </div>\n" +
    "                                <p>\n" +
    "                                    Lunch with\n" +
    "                                    <a class=\"text-info\" href>\n" +
    "										Nicole\n" +
    "									</a> .\n" +
    "                                </p>\n" +
    "                            </div>\n" +
    "                        </li>\n" +
    "                        <li class=\"timeline-item warning\">\n" +
    "                            <div class=\"margin-left-15\">\n" +
    "                                <div class=\"text-muted text-small\">\n" +
    "                                    Wed, 25 Mar\n" +
    "                                </div>\n" +
    "                                <p>\n" +
    "                                    server Maintenance.\n" +
    "                                </p>\n" +
    "                            </div>\n" +
    "                        </li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-12 col-sm-4\">\n" +
    "            <div class=\"panel panel-white no-radius\">\n" +
    "                <div class=\"panel-heading border-light\">\n" +
    "                    <h4 class=\"panel-title\"> Upcoming Interviews  </h4>\n" +
    "                </div>\n" +
    "                <div class=\"panel-body\" style=\"padding:0px;\">\n" +
    "                    <table class=\"table no-margin margin-bottom-10 table-striped table-user ng-scope\">\n" +
    "                        <thead>\n" +
    "                        <tr>\n" +
    "                            <th>Name</th>\n" +
    "                            <th>Phone</th>\n" +
    "                            <th>Date</th>\n" +
    "                            <th>Time</th>\n" +
    "                        </tr>\n" +
    "                        </thead>\n" +
    "                        <tbody>\n" +
    "                        <tr>\n" +
    "                            <td>Peter Clark</td>\n" +
    "                            <td>+924893489</td>\n" +
    "                            <td>09-02-2015</td>\n" +
    "                            <td>09:15am</td>\n" +
    "                        </tr>\n" +
    "                        <tr>\n" +
    "                            <td>Peter Clark</td>\n" +
    "                            <td>+924893489</td>\n" +
    "                            <td>09-02-2015</td>\n" +
    "                            <td>09:15am</td>\n" +
    "                        </tr>\n" +
    "                        <tr>\n" +
    "                            <td>Peter Clark</td>\n" +
    "                            <td>+924893489</td>\n" +
    "                            <td>09-02-2015</td>\n" +
    "                            <td>09:15am</td>\n" +
    "                        </tr>\n" +
    "                        <tr>\n" +
    "                            <td>Peter Clark</td>\n" +
    "                            <td>+924893489</td>\n" +
    "                            <td>09-02-2015</td>\n" +
    "                            <td>09:15am</td>\n" +
    "                        </tr>\n" +
    "                        </tbody>\n" +
    "                    </table>\n" +
    "                    <div class=\"margin-bottom-10 margin-right-10\" style=\"text-align: right;\">\n" +
    "                        <button class=\"btn btn-primary btn-o\"><i class=\"fa fa-plus\"></i> Add</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-12\" style=\"padding:0px;\">\n" +
    "                <div class=\"panel panel-white no-radius\">\n" +
    "                    <div class=\"panel-body padding-20 text-center\">\n" +
    "                        <div class=\"space10\">\n" +
    "                            <h5 class=\"text-dark admin-box no-margin\">Average Salary Per Job</h5>\n" +
    "                            <h2 class=\"no-margin\"><small>$</small>1,450</h2>\n" +
    "                            <span class=\"badge badge-danger margin-top-10\">salary</span>\n" +
    "                        </div>\n" +
    "\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "			\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<!-- end: FOURTH SECTION -->\n" +
    "");
}]);

angular.module("error/404.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("error/404.tpl.html",
    "<div class=\"container w-xxl w-auto-xs\">\n" +
    "    <div class=\"text-center m-b-lg\">\n" +
    "        <h1 class=\"text-shadow text-white\">404</h1>\n" +
    "    </div>\n" +
    "    <div class=\"list-group bg-info auto m-b-sm m-b-lg\">\n" +
    "         <a ui-sref=\"fox.dashboard\" class=\"list-group-item\">\n" +
    "            <i class=\"fa fa-chevron-right text-muted\"></i>\n" +
    "            <i class=\"fa fa-fw fa-mail-forward m-r-xs\"></i> Goto application\n" +
    "        </a>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("error/500.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("error/500.tpl.html",
    "<div class=\"container w-xxl w-auto-xs\">\n" +
    "    <div class=\"text-center m-b-lg\">\n" +
    "        <h1 class=\"text-shadow text-white\">404</h1>\n" +
    "    </div>\n" +
    "    <div class=\"list-group bg-info auto m-b-sm m-b-lg\">\n" +
    "        <a ui-sref=\"fox.dashboard\" class=\"list-group-item\">\n" +
    "            <i class=\"fa fa-chevron-right text-muted\"></i>\n" +
    "            <i class=\"fa fa-fw fa-mail-forward m-r-xs\"></i> Goto application\n" +
    "        </a>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("job-creation/templates/job/create.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("job-creation/templates/job/create.tpl.html",
    "<div class=\"container-fluid container-fullw bg-white\">\n" +
    "	<div class=\"row\">\n" +
    "		<div class=\"col-md-12\">\n" +
    "			<h1 class=\"mainTitle text-primary\">Create a Job Posting</h1>\n" +
    "			<form ng-submit=\"vm.submitForm()\" name=\"form\" novalidate class=\"form-validation\">\n" +
    "				<div class=\"share-job-content\">\n" +
    "					<button class=\"btn btn-wide btn-o btn-primary btn-share-job\"><i class=\"ti ti-plus\"></i> Share Job </button>\n" +
    "				</div>\n" +
    "				\n" +
    "				<div class=\"form-group\">\n" +
    "					<input type=\"text\" placeholder=\"Enter Job Title\" class=\"form-control\" ng-model=\"vm.job.title\" required />\n" +
    "				</div>\n" +
    "				<div class=\"row\">\n" +
    "					<div class=\"col-md-4\">\n" +
    "						<div class=\"form-group\">\n" +
    "							<angucomplete-alt id=\"city\"\n" +
    "											  placeholder=\"City\"\n" +
    "											  pause=\"400\"\n" +
    "											  initial-value=\"vm.city\"\n" +
    "											  remote-url=\"{{vm.filterUrls.city}}\"\n" +
    "											  remote-url-data-field=\"data\"\n" +
    "											  title-field=\"name\"\n" +
    "											  description-field=\"\"\n" +
    "											  input-class=\"form-control\"\n" +
    "											  minlength=\"2\"\n" +
    "											  remote-url-request-formatter=\"vm.cityRequestFormat\"\n" +
    "											  selected-object=\"vm.citySelectCallback\"\n" +
    "											  match-class=\"highlight\"\n" +
    "											  field-required=\"true\"\n" +
    "							/>\n" +
    "						</div>\n" +
    "					</div>\n" +
    "					<div class=\"col-md-4\">\n" +
    "						<div class=\"form-group\">\n" +
    "							<input value=\"{{vm.province.name}}\" class=\"form-control\" type=\"text\" placeholder=\"Province\" readonly>\n" +
    "						</div>\n" +
    "					</div>\n" +
    "					<div class=\"col-md-4\">\n" +
    "						<div class=\"form-group\">\n" +
    "							<input type=\"text\" placeholder=\"Postal\" class=\"form-control\" ng-model=\"vm.job.postal\" required />\n" +
    "						</div>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "				<div class=\"form-group\">\n" +
    "					<angucomplete-alt id=\"category\"\n" +
    "									  placeholder=\"Category\"\n" +
    "									  pause=\"400\"\n" +
    "									  initial-value=\"vm.category\"\n" +
    "									  remote-url=\"{{vm.filterUrls.category}}\"\n" +
    "									  remote-url-data-field=\"data\"\n" +
    "									  title-field=\"title\"\n" +
    "									  description-field=\"\"\n" +
    "									  input-class=\"form-control\"\n" +
    "									  minlength=\"2\"\n" +
    "									  remote-url-request-formatter=\"vm.categoryRequestFormat\"\n" +
    "									  selected-object=\"vm.category\"\n" +
    "									  match-class=\"highlight\"\n" +
    "									  field-required=\"true\"\n" +
    "							/>\n" +
    "				</div>\n" +
    "				<div class=\"form-group\">\n" +
    "					<textarea ckeditor=\"vm.ckeditorOptions\" ng-model=\"vm.job.description\" rows=\"5\" placeholder=\"\" class=\"form-control\"></textarea>\n" +
    "				</div>\n" +
    "				<div class=\"form-group text-center\">\n" +
    "					<button ng-if=\"!vm.isEdit\" ng-click=\"vm.saveDraft()\" ng-disabled=\"!form.$valid\" class=\"btn btn-o btn-success\" type=\"button\"><i class=\"ti-save\"></i> Save Draft</button>\n" +
    "					<button ng-disabled=\"!form.$valid\" class=\"btn btn-o btn-primary\" type=\"submit\"><i class=\"ti-save-alt\"></i> Questionnaire, Assessment & Video Interview</button>\n" +
    "					<button class=\"btn btn-o btn-wide btn-default\" ng-click=\"vm.cancel()\" type=\"button\">Cancel</button>\n" +
    "				</div>\n" +
    "			</form>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("jobs/templates/candidate/popup-review.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/candidate/popup-review.tpl.html",
    "<div class=\"container-fluid container-fullw bg-white\">\n" +
    "	<div class=\"row\">\n" +
    "		<h1 class=\"mainTitle text-primary\">Candidates</h1>\n" +
    "		<form>\n" +
    "			<div class=\"form-group\">\n" +
    "				<div class=\"input-group\">\n" +
    "					<input type=\"text\" class=\"form-control\" placeholder=\"Search\" ng-keypress=\"vm.keypress($event)\" ng-model=\"vm.keyword\" />\n" +
    "					<span ng-click=\"vm.getList()\" class=\"input-group-addon pointer\"> <i class=\"ti-search\"></i> </span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</form>\n" +
    "		<div class=\"table-responsive\">\n" +
    "			<table class=\"table table-hover\">\n" +
    "				<thead>\n" +
    "					<tr class=\"info\">\n" +
    "						<th>Name</th>\n" +
    "						<th>Phone</th>\n" +
    "						<th>Email</th>\n" +
    "					</tr>\n" +
    "				</thead>\n" +
    "				<tbody>\n" +
    "					<tr ng-repeat=\"item in vm.items\">\n" +
    "						<td>{{item.name}}</td>\n" +
    "						<td>{{item.phone}}</td>\n" +
    "						<td>{{item.email}}</td>\n" +
    "						<td class=\"text-center invite-phone\">\n" +
    "							<div class=\"checkbox clip-check check-primary\">\n" +
    "								<input ng-model=\"item.is_invited\" ng-change=\"vm.updateInvited(item)\" type=\"checkbox\" id=\"checkbox_{{item.getId()}}\" />\n" +
    "								<label for=\"checkbox_{{item.getId()}}\"></label>\n" +
    "							</div>\n" +
    "						</td>\n" +
    "					</tr>\n" +
    "					<tr ng-if=\"vm.items.length == 0\">\n" +
    "						<td colspan=\"7\">\n" +
    "							No record found!\n" +
    "						</td>\n" +
    "					</tr>\n" +
    "				</tbody>\n" +
    "			</table>\n" +
    "			<div class=\"text-right\">\n" +
    "				<pagination rotate=\"false\" boundary-links=\"true\" max-size=\"5\" items-per-page=\"vm.paginator.per_page\" total-items=\"vm.paginator.total\" ng-model=\"vm.paginator.current_page\" ng-change=\"vm.getList()\"></pagination>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"ngdialog-buttons text-right\">\n" +
    "			<button type=\"button\" class=\"btn\" ng-click=\"closeThisDialog('cancel')\">Cancel</button>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("jobs/templates/candidate/popup.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/candidate/popup.tpl.html",
    "<div class=\"ngdialog-message\">\n" +
    "	<div pdf-viewer data-model=\"data\" pdf-url=\"pdfUrl\"></div>\n" +
    "</div>\n" +
    "<div class=\"ngdialog-buttons\">\n" +
    "    <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"closeThisDialog('no')\">Close</button>\n" +
    "</div>");
}]);

angular.module("jobs/templates/candidate/review.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/candidate/review.tpl.html",
    "<div class=\"container-fluid container-fullw bg-white\">\n" +
    "	<div class=\"row\">\n" +
    "		<h1 class=\"mainTitle text-primary\">Review Candidates</h1>\n" +
    "		<form>\n" +
    "			<div class=\"form-group\">\n" +
    "				<div class=\"input-group\">\n" +
    "					<input type=\"text\" class=\"form-control\" placeholder=\"Search\" ng-keypress=\"vm.keypress($event)\" ng-model=\"vm.keyword\" />\n" +
    "					<span ng-click=\"vm.getList()\" class=\"input-group-addon pointer\"> <i class=\"ti-search\"></i> </span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</form>\n" +
    "		<div class=\"table-responsive\">\n" +
    "			<table class=\"table table-hover\">\n" +
    "				<thead>\n" +
    "					<tr class=\"info\">\n" +
    "						<th>Name</th>\n" +
    "						<th>Phone</th>\n" +
    "						<th>Email</th>\n" +
    "						<th>City</th>\n" +
    "						<th width=\"70\">Resume</th>\n" +
    "						<th width=\"100\">Cover Letter</th>\n" +
    "						<th width=\"160\">Invite to Phone Screen</th>\n" +
    "					</tr>\n" +
    "				</thead>\n" +
    "				<tbody>\n" +
    "					<tr ng-repeat=\"item in vm.items\">\n" +
    "						<td>{{item.name}}</td>\n" +
    "						<td>{{item.phone}}</td>\n" +
    "						<td>{{item.email}}</td>\n" +
    "						<td>{{item.address.city}}</td>\n" +
    "						<td ng-click=\"vm.openDetail(item)\" class=\"text-center\">\n" +
    "							<a href=\"#\">\n" +
    "								<i class=\"ti-eye\"></i>\n" +
    "							</a>\n" +
    "						</td>\n" +
    "						<td ng-click=\"vm.openCoverLetter(item)\" class=\"text-center\">\n" +
    "							<a href=\"#\">\n" +
    "								<i class=\"ti-email\"></i>\n" +
    "							</a>\n" +
    "						</td>\n" +
    "						<td class=\"text-center invite-phone\">\n" +
    "							<div class=\"checkbox clip-check check-primary\">\n" +
    "								<input ng-model=\"item.is_invited\" ng-change=\"vm.updateInvited(item)\" type=\"checkbox\" id=\"checkbox_{{item.getId()}}\" />\n" +
    "								<label for=\"checkbox_{{item.getId()}}\"></label>\n" +
    "							</div>\n" +
    "						</td>\n" +
    "					</tr>\n" +
    "					<tr ng-if=\"vm.items.length == 0\">\n" +
    "						<td colspan=\"7\">\n" +
    "							No record found!\n" +
    "						</td>\n" +
    "					</tr>\n" +
    "				</tbody>\n" +
    "			</table>\n" +
    "			<div class=\"text-right\">\n" +
    "				<pagination rotate=\"false\" boundary-links=\"true\" max-size=\"5\" items-per-page=\"vm.paginator.per_page\" total-items=\"vm.paginator.total\" ng-model=\"vm.paginator.current_page\" ng-change=\"vm.getList()\"></pagination>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group text-center\">\n" +
    "			<button ng-click=\"vm.gotoQuestion()\" class=\"btn btn-o btn-success\" type=\"button\"><i class=\"fa fa-arrow-left\"></i> Create Questions</button>\n" +
    "			<button ng-click=\"vm.gotoPhoneScreen()\" class=\"btn btn-o btn-primary\" type=\"button\">Phone Screen <i class=\"fa fa-arrow-right\"></i></button>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("jobs/templates/create-menu.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/create-menu.tpl.html",
    "<div class=\"fade-in-up\">\n" +
    "	<div class=\"row\">\n" +
    "		<div class=\"breadcrumb-nav col-lg-2 col-md-3\">\n" +
    "			<ul>\n" +
    "				<li ui-sref-active=\"active\">\n" +
    "					<a ui-sref=\"fox.jobs-create.create\">\n" +
    "						<i class=\"fa fa-dribbble\"></i> Create Job\n" +
    "					</a>\n" +
    "				</li>\n" +
    "				<li class=\"link-disabled\">\n" +
    "					<a href=\"#\"><i class=\"fa fa-question-circle\"></i> Create interview, assessment, and Questionnaire</a>\n" +
    "				</li>\n" +
    "				<li class=\"link-disabled\">\n" +
    "					<a href=\"#\"><i class=\"fa fa-group\"></i> Review Candidates</a>\n" +
    "				</li>\n" +
    "				<li class=\"link-disabled\">\n" +
    "					<a href=\"#\"><i class=\"ti-microphone\"></i> Phone Screen</a>\n" +
    "				</li>\n" +
    "				<li class=\"link-disabled\">\n" +
    "					<a href=\"#\"><i class=\"ti-video-clapper\"></i> Review Video Interview</a>\n" +
    "				</li>\n" +
    "				<li class=\"link-disabled\">\n" +
    "					<a href=\"#\"><i class=\"fa fa-thumbs-up\"></i> Review Questionnaire and Results</a>\n" +
    "				</li>\n" +
    "				<li class=\"link-disabled\">\n" +
    "					<a href=\"#\"><i class=\"ti-book\"></i> Interview Notes</a>\n" +
    "				</li>\n" +
    "				<li class=\"link-disabled\">\n" +
    "					<a href=\"#\"><i class=\"ti-check\"></i> Reference Checks</a>\n" +
    "				</li>\n" +
    "				<li class=\"link-disabled\">\n" +
    "					<a href=\"#\"><i class=\"ti-bookmark-alt\"></i> Offer Letter</a>\n" +
    "				</li>\n" +
    "				<li class=\"link-disabled\">\n" +
    "					<a href=\"#\"><i class=\"ti-settings\"></i> Placement Record</a>\n" +
    "				</li>\n" +
    "			</ul>\n" +
    "		</div>\n" +
    "		<div class=\"container-breadcrumb col-lg-10 col-md-9 fade-in-up\" ui-view>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("jobs/templates/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/index.tpl.html",
    "<div class=\"fade-in-up\">\n" +
    "	<div class=\"row\">\n" +
    "		<div class=\"breadcrumb-nav col-lg-2 col-md-3\">\n" +
    "			<ul>\n" +
    "				<li ui-sref-active=\"active\">\n" +
    "					<a ui-sref=\"fox.jobs-create.create\">\n" +
    "						<i class=\"fa fa-dribbble\"></i> Create Job\n" +
    "					</a>\n" +
    "				</li>\n" +
    "				<li ui-sref-active=\"active\">\n" +
    "					<a ui-sref=\"fox.jobs.create-interview\"><i class=\"fa fa-question-circle\"></i> Create interview, assessment, and Questionnaire</a>\n" +
    "				</li>\n" +
    "				<li ui-sref-active=\"active\">\n" +
    "					<a ui-sref=\"fox.jobs.candidate-review\"><i class=\"fa fa-group\"></i> Review Candidates</a>\n" +
    "				</li>\n" +
    "				<li ui-sref-active=\"active\">\n" +
    "					<a ui-sref=\"fox.jobs.phone-screen\"><i class=\"ti-microphone\"></i> Phone Screen</a>\n" +
    "				</li>\n" +
    "				<li ui-sref-active=\"active\">\n" +
    "					<a ui-sref=\"fox.jobs.review-video-interview\"><i class=\"ti-video-clapper\"></i> Review Video Interview</a>\n" +
    "				</li>\n" +
    "				<li ui-sref-active=\"active\">\n" +
    "					<a ui-sref=\"fox.jobs.review-question-result\"><i class=\"fa fa-thumbs-up\"></i> Review Questionnaire and Results</a>\n" +
    "				</li>\n" +
    "				<li ui-sref-active=\"active\">\n" +
    "					<a ui-sref=\"fox.jobs.interview-note\"><i class=\"ti-book\"></i> Interview Notes</a>\n" +
    "				</li>\n" +
    "				<li ui-sref-active=\"active\">\n" +
    "					<a ui-sref=\"fox.jobs.reference-check\"><i class=\"ti-check\"></i> Reference Checks</a>\n" +
    "				</li>\n" +
    "				<li ui-sref-active=\"active\">\n" +
    "					<a ui-sref=\"fox.jobs.offer-letter\"><i class=\"ti-bookmark-alt\"></i> Offer Letter</a>\n" +
    "				</li>\n" +
    "				<li ui-sref-active=\"active\">\n" +
    "					<a ui-sref=\"fox.jobs.placement-record\"><i class=\"ti-settings\"></i> Placement Record</a>\n" +
    "				</li>\n" +
    "			</ul>\n" +
    "		</div>\n" +
    "		<div class=\"container-breadcrumb col-lg-10 col-md-9 fade-in-up\" ui-view>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("jobs/templates/interview-note/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/interview-note/index.tpl.html",
    "<div class=\"container-fluid container-fullw bg-white interview-note\">\n" +
    "	<div class=\"row\">\n" +
    "		<div class=\"col-md-12\">\n" +
    "			<div class=\"clearfix\">\n" +
    "				<h1 class=\"mainTitle text-primary pull-left\">Interview Notes</h1>\n" +
    "				<div class=\"pull-right title-btn\">\n" +
    "					<button class=\"btn btn-primary btn-o upload-btn\" type=\"button\"><i class=\"fa fa-upload\"></i> Upload notes</button>\n" +
    "					<button class=\"btn btn-primary btn-o upload-btn\" type=\"button\" ng-click=\"vm.saveInterviewNote(vm.body)\">Save</button>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "			<div class=\"row\">\n" +
    "				<div class=\"col-md-3\">\n" +
    "					<div class=\"panel panel-white no-padding\">\n" +
    "						<div class=\"panel-heading\">\n" +
    "							<h4 class=\"panel-title text-primary\">Candidates</h4>\n" +
    "							<ct-paneltool class=\"panel-tools\" tool-refresh=\"load1\"></ct-paneltool>\n" +
    "						</div>\n" +
    "						<div class=\"panel-body\">\n" +
    "							<div class=\"panel-scroll height-350\" perfect-scrollbar wheel-propagation=\"false\" suppress-scroll-x=\"true\">\n" +
    "								<div class=\"table-responsive\">\n" +
    "									<table class=\"table table-hover\">\n" +
    "										<tbody>\n" +
    "											<tr ng-class=\"{active: vm.isActive(item)}\" ng-click=\"vm.select(item)\" ng-repeat=\"item in vm.candidates\">\n" +
    "												<td class=\"pointer\">\n" +
    "													{{item.name}}\n" +
    "												</td>\n" +
    "											</tr>\n" +
    "										</tbody>\n" +
    "									</table>\n" +
    "								</div>\n" +
    "							</div>\n" +
    "						</div>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "				<div class=\"col-md-9\">\n" +
    "					<form>\n" +
    "						<div class=\"form-group\">\n" +
    "							<textarea ckeditor=\"vm.ckeditorOptions\" ng-model=\"vm.body\" rows=\"5\" placeholder=\"\" class=\"form-control\"></textarea>\n" +
    "						</div>\n" +
    "					</form>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "			<div class=\"form-group floating-nav-jobs text-center\">\n" +
    "				<div>\n" +
    "					<button ng-click=\"vm.gotoQuestionResult()\" class=\"btn btn-o btn-success\" type=\"button\"><i class=\"fa fa-arrow-left\"></i> Questionnaire and Assessment Results</button>\n" +
    "					<button ng-click=\"vm.gotoRefenCheck()\" class=\"btn btn-o btn-primary\" type=\"button\">Reference Checks <i class=\"fa fa-arrow-right\"></i></button>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("jobs/templates/interview/create-questionnaire.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/interview/create-questionnaire.tpl.html",
    "<div id=\"create-questionnaire\">\n" +
    "	<div class=\"col-xs-12 col-md-6\">\n" +
    "		<div class=\"form-group\">\n" +
    "			<label>Select question type</label>\n" +
    "			<select class=\"form-control\" ng-model=\"type\" ng-options=\"type as type.name for type in QuestionType\"></select>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "	<div class=\"col-xs-12 col-md-6\">\n" +
    "		<p>Preview</p>\n" +
    "		<div choice-multi-answer-horizontal data-control=\"ChoiceMultiAnswerHorizontal\" ng-if=\"type.type == 'ChoiceMultiAnswerHorizontal'\"></div>\n" +
    "		<div choice-multi-answer-select-box data-control=\"ChoiceMultiAnswerSelectBox\" ng-if=\"type.type == 'ChoiceMultiAnswerSelectBox'\"></div>\n" +
    "		<div choice-multi-answer-vertical data-control=\"choiceMultiAnswerVertical\" ng-if=\"type.type == 'choiceMultiAnswerVertical'\"></div>\n" +
    "		<div choice-single-answer-dropdown data-control=\"choiceSingleAnswerDropdown\" ng-if=\"type.type == 'choiceSingleAnswerDropdown'\"></div>\n" +
    "		<div choice-single-answer-horizontal data-control=\"choiceSingleAnswerHorizontal\" ng-if=\"type.type == 'choiceSingleAnswerHorizontal'\"></div>\n" +
    "		<div choice-single-answer-vertical data-control=\"choiceSingleAnswerVertical\" ng-if=\"type.type == 'choiceSingleAnswerVertical'\"></div>\n" +
    "		<div number-allowcation-constant-sum data-control=\"numberAllowcationConstantSum\" ng-if=\"type.type == 'numberAllowcationConstantSum'\"></div>\n" +
    "		<div open-ended-text-comment-box data-control=\"openEndedTextCommentBox\" ng-if=\"type.type == 'openEndedTextCommentBox'\"></div>\n" +
    "		<div open-ended-text-date-time data-control=\"openEndedTextDateTime\" ng-if=\"type.type == 'openEndedTextDateTime'\"></div>\n" +
    "		<div open-ended-text-one-line data-control=\"openEndedTextOneLine\" ng-if=\"type.type == 'openEndedTextOneLine'\"></div>\n" +
    "		<div rank-order data-control=\"rankOrder\" ng-if=\"type.type == 'rankOrder'\"></div>\n" +
    "		<div file-upload data-control=\"fileUpload\" ng-if=\"type.type == 'fileUpload'\"></div>\n" +
    "		<div datalist data-control=\"datalist\" ng-if=\"type.type == 'datalist'\"></div>\n" +
    "		<div matrix-multi-answer-per-row data-control=\"matrixMultiAnswerPerRow\" ng-if=\"type.type == 'matrixMultiAnswerPerRow'\"></div>\n" +
    "		<div matrix-single-answer-per-row data-control=\"matrixSingleAnswerPerRow\" ng-if=\"type.type == 'matrixSingleAnswerPerRow'\"></div>\n" +
    "		<div matrix-spreadsheet data-control=\"matrixSpreadsheet\" ng-if=\"type.type == 'matrixSpreadsheet'\"></div>\n" +
    "	</div>\n" +
    "	<div class=\"col-xs-12\">\n" +
    "		<button class=\"btn btn-default\">\n" +
    "			<span>Next</span>\n" +
    "			<i class=\"fa fa-arrow-right\"></i>\n" +
    "		</button>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("jobs/templates/interview/create.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/interview/create.tpl.html",
    "<div class=\"container-fluid container-fullw interview bg-white\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-12\">\n" +
    "            <h1 class=\"mainTitle text-primary\">\n" +
    "                Create Video Interview, Assessment & Questionnaire\n" +
    "            </h1>\n" +
    "            <div class=\"tabbable\">\n" +
    "                <tabset justified=\"true\" class=\"tabbable\">\n" +
    "                    <tab heading=\"Video Interview\">\n" +
    "                        <div class=\"title-heading\">\n" +
    "                            <h4 class=\"title text-primary\">Create or Edit Video Interview Questions</h4>\n" +
    "                            <button ng-click=\"vm.addQuestion()\" class=\"btn btn-primary btn-o\" type=\"button\"><i class=\"fa fa-plus\"></i> Question</button>\n" +
    "                        </div>\n" +
    "                        <div class=\"table-responsive\">\n" +
    "                            <table class=\"table table-hover\">\n" +
    "                                <thead>\n" +
    "                                    <tr class=\"info\">\n" +
    "                                        <th width=\"55\">#</th>\n" +
    "                                        <th>Question</th>\n" +
    "                                        <th width=\"100\">Edit / Delete</th>\n" +
    "                                    </tr>\n" +
    "                                </thead>\n" +
    "                                <tbody>\n" +
    "                                    <tr ng-repeat=\"item in vm.questions\">\n" +
    "                                        <td>{{vm.getQuestionIndex($index)}}</td>\n" +
    "                                        <td ng-bind-html=\"item.title | nl2br\"></td>\n" +
    "                                        <td>\n" +
    "                                            <a href=\"#\" ng-click=\"vm.edit(item)\" class=\"btn btn-transparent\"><i class=\"fa fa-edit\"></i></a>\n" +
    "                                            <a href=\"#\" ng-click=\"vm.delete(item)\" class=\"btn btn-transparent btn-xs\"><i class=\"fa fa-trash-o\"></i></a>\n" +
    "                                        </td>\n" +
    "                                    </tr>\n" +
    "                                    <tr ng-if=\"vm.questions.length == 0\">\n" +
    "                                        <td colspan=\"3\">No record found!</td>\n" +
    "                                    </tr>\n" +
    "                                </tbody>\n" +
    "                            </table>\n" +
    "                            <div class=\"text-right\">\n" +
    "                                <pagination rotate=\"false\" boundary-links=\"true\" max-size=\"5\" items-per-page=\"vm.questionPaginator.per_page\" total-items=\"vm.questionPaginator.total\" ng-model=\"vm.questionPaginator.current_page\" ng-change=\"vm.getQuestionList()\"></pagination>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </tab>\n" +
    "                    <tab heading=\"Choose Assessment\">\n" +
    "                        <form action=\"http://www.ascentiiassessments.com/RF/Scripts/default.asp\" method=\"POST\" name=\"frmProcessMenu\">\n" +
    "                            <input type=\"hidden\" id=\"hidScreenName\" name=\"hidScreenName\" />\n" +
    "                            <input type=\"hidden\" id=\"hidScreenAction\" name=\"hidScreenAction\" />\n" +
    "                        </form>\n" +
    "\n" +
    "                        <div class=\"row\">\n" +
    "                            <div class=\"col-sm-12\">\n" +
    "                                <form name=\"frmProcess\">\n" +
    "                                    <input TYPE=\"hidden\" ng-model=\"vm.hidScreenName\" ng-init=\"vm.hidScreenName=COMPANYURL\" ID=\"hidScreenName\" NAME=\"hidScreenName\" VALUE=\"COMPANYURL\" />\n" +
    "                                    <input TYPE=\"hidden\" ng-model=\"vm.hidScreenCompanyID\" ID=\"hidScreenCompanyID\" NAME=\"hidScreenCompanyID\" VALUE=\"136\" />\n" +
    "                                    <input TYPE=\"hidden\" ng-model=\"vm.hidScreenAction\" ID=\"hidScreenAction\" NAME=\"hidScreenAction\" />\n" +
    "                                    <input TYPE=\"hidden\" ng-model=\"vm.hidScreenCandidateID\" ID=\"hidScreenCandidateID\" NAME=\"hidScreenCandidateID\" VALUE=\"4505246717531652256C3F727A2C653A4C184719092B0B473D122B0469401A75462054405F3C472466017E164F214C2B7A0C6F10572C\" />\n" +
    "                                    <input TYPE=\"hidden\" ng-model=\"vm.hidScreenPositionDescID\" ID=\"hidScreenPositionDescID\" NAME=\"hidScreenPositionDescID\" VALUE=\"0\" />\n" +
    "                                    <input TYPE=\"hidden\" ng-model=\"vm.hidScreenEmailID\" ID=\"hidScreenEmailID\" NAME=\"hidScreenEmailID\" VALUE=\"0\" />\n" +
    "\n" +
    "                                    <div class=\"form-group\">\n" +
    "                                        <label>Job Family</label>\n" +
    "                                        <select class=\"form-control\"\n" +
    "                                                name=\"cboTraitFit\"\n" +
    "                                                size=\"0\"\n" +
    "                                                ng-init=\"s = 'hey fill it'\" ;\n" +
    "                                                ng-model=\"vm.select_job_primary\"\n" +
    "                                                ng-change=\"vm.swapOptions();\"\n" +
    "                                                ng-options=\"job as job.label for job in vm.job_family\"></select><br />\n" +
    "\n" +
    "                                        <select class=\"bodytext\"\n" +
    "                                                name=\"cboJob\"\n" +
    "                                                size=\"0\"\n" +
    "                                                ng-disabled=\"!vm.select_job_primary\"\n" +
    "                                                ng-model=\"vm.select_job_secondary\"\n" +
    "                                                ng-init=\"vm.select_job_secondary = options[0]\"\n" +
    "                                                ng-options=\"job as job.label for job in vm.sub_jobs\">\n" +
    "                                            <option style=\"display:none\" value=\"\">Select a Job</option>\n" +
    "                                        </select>\n" +
    "                                    </div>\n" +
    "                                    <div class=\"form-group\">\n" +
    "                                        <button type=\"submit\" ng-click=\"vm.processForm()\" class=\"btn btn-default\">Send</button>\n" +
    "                                    </div>\n" +
    "                                </form>\n" +
    "                            </div>\n" +
    "\n" +
    "                        </div>\n" +
    "                    </tab>\n" +
    "                    <tab heading=\"Build Questionnaire\">\n" +
    "                        <!-- <div survey job=\"vm.job\"></div> -->\n" +
    "                        <div create-questionnaire></div>\n" +
    "                        <div>\n" +
    "                            <div class=\"title-heading\">\n" +
    "                                <h4 class=\"title text-primary\">Create Questionnaire</h4>\n" +
    "                                <button ng-click=\"vm.addAssessment()\" class=\"btn btn-primary btn-o\" type=\"button\"><i class=\"fa fa-plus\"></i> Question</button>\n" +
    "\n" +
    "                            </div>\n" +
    "                            <div class=\"table-responsive\">\n" +
    "                                <table class=\"table table-hover\">\n" +
    "                                    <thead>\n" +
    "                                        <tr class=\"info\">\n" +
    "\n" +
    "                                            <th width=\"55\">#</th>\n" +
    "\n" +
    "                                            <th>Question</th>\n" +
    "                                            <th width=\"100\">Edit / Delete</th>\n" +
    "\n" +
    "                                        </tr>\n" +
    "\n" +
    "                                    </thead>\n" +
    "\n" +
    "                                    <tbody>\n" +
    "\n" +
    "                                        <tr ng-repeat=\"item in vm.questions\">\n" +
    "\n" +
    "                                            <td>{{vm.getQuestionIndex($index)}}</td>\n" +
    "\n" +
    "                                            <td ng-bind-html=\"item.title | nl2br\"></td>\n" +
    "                                            <td>\n" +
    "                                                <a href=\"#\" ng-click=\"vm.edit(item)\" class=\"btn btn-transparent\"><i class=\"fa fa-edit\"></i></a>\n" +
    "                                                <a href=\"#\" ng-click=\"vm.delete(item)\" class=\"btn btn-transparent btn-xs\"><i class=\"fa fa-trash-o\"></i></a>\n" +
    "\n" +
    "                                            </td>\n" +
    "\n" +
    "                                        </tr>\n" +
    "\n" +
    "                                        <tr ng-if=\"vm.questions.length == 0\">\n" +
    "\n" +
    "                                            <td colspan=\"3\">No record found!</td>\n" +
    "\n" +
    "                                        </tr>\n" +
    "\n" +
    "                                    </tbody>\n" +
    "\n" +
    "                                </table>\n" +
    "                                <div class=\"text-right\">\n" +
    "                                    <pagination rotate=\"false\" boundary-links=\"true\" max-size=\"5\" items-per-page=\"vm.questionPaginator.per_page\" total-items=\"vm.questionPaginator.total\" ng-model=\"vm.questionPaginator.current_page\" ng-change=\"vm.getQuestionList()\"></pagination>\n" +
    "\n" +
    "                                </div>\n" +
    "\n" +
    "                            </div>\n" +
    "\n" +
    "\n" +
    "                        </div>\n" +
    "                    </tab>\n" +
    "                </tabset>\n" +
    "            </div>\n" +
    "            <div class=\"form-group text-center\">\n" +
    "                <button ng-click=\"vm.gotoJob()\" class=\"btn btn-o btn-success\" type=\"button\"><i class=\"fa fa-arrow-left\"></i> Edit Job Details</button>\n" +
    "                <button class=\"btn btn-o btn-primary\" type=\"button\" ng-click=\"vm.postJob()\">Post Job <i class=\"fa fa-arrow-right\"></i></button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("jobs/templates/interview/survey.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/interview/survey.tpl.html",
    "<div ng-show=\"!isEditing() && hasSurvey()\">\n" +
    "    <table class=\"table table-striped\">\n" +
    "        <thead>\n" +
    "        <tr>\n" +
    "            <td>Title</td>\n" +
    "            <td width=\"50\">Action</td>\n" +
    "        </tr>\n" +
    "        </thead>\n" +
    "        <tbody>\n" +
    "        <tr ng-repeat=\"survey in surveys\">\n" +
    "            <td>{{::survey.title}}</td>\n" +
    "            <td align=\"center\"><i class=\"fa fa-pencil pointer\" data-ng-click=\"edit(survey)\"></i></td>\n" +
    "        </tr>\n" +
    "        </tbody>\n" +
    "    </table>\n" +
    "</div>\n" +
    "<div ng-show=\"isEditing()\">\n" +
    "    <h3>Add Questions</h3>\n" +
    "\n" +
    "    <div class=\"form-group\">\n" +
    "        <select ng-model=\"type\">\n" +
    "            <option value=\"boolean\">Yes/No</option>\n" +
    "            <option value=\"single-choice\">Single choice</option>\n" +
    "            <option value=\"multiple-choice\">Multiple choice</option>\n" +
    "            <option value=\"drop-down\">Drop Down</option>\n" +
    "        </select>\n" +
    "        <button class=\"btn btn-sm\" data-ng-click=\"add()\"><i class=\"fa fa-plus-circle\"></i> Add</button>\n" +
    "    </div>\n" +
    "    <div ng-repeat=\"question in questions\">\n" +
    "        <div ng-if=\"question.type=='boolean'\" question-type-boolean data-control=\"question\"></div>\n" +
    "        <div ng-if=\"question.type=='single-choice'\" question-type-single-choice data-control=\"question\"></div>\n" +
    "        <div ng-if=\"question.type=='multiple-choice'\" question-type-multiple-choice data-control=\"question\"></div>\n" +
    "        <div ng-if=\"question.type=='drop-down'\" question-type-dropdown data-control=\"question\"></div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "");
}]);

angular.module("jobs/templates/job/create.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/job/create.tpl.html",
    "<div class=\"container-fluid container-fullw bg-white\">\n" +
    "	<div class=\"row\">\n" +
    "		<div class=\"col-md-12\">\n" +
    "			<h1 class=\"mainTitle text-primary\">Create a Job Posting</h1>\n" +
    "			<form ng-submit=\"vm.submitForm()\" name=\"form\" novalidate class=\"form-validation\">\n" +
    "				<div class=\"form-group\">\n" +
    "					<input type=\"text\" placeholder=\"Enter Job Title\" class=\"form-control\" ng-model=\"vm.job.title\" required />\n" +
    "				</div>\n" +
    "				<div class=\"row\">\n" +
    "					<div class=\"col-md-4\">\n" +
    "						<div class=\"form-group\">\n" +
    "							<angucomplete-alt id=\"city\"\n" +
    "								placeholder=\"City\"\n" +
    "								pause=\"400\"\n" +
    "								field-required=\"true\"\n" +
    "								initial-value=\"vm.city\"\n" +
    "								remote-url=\"{{vm.filterUrls.city}}\"\n" +
    "								remote-url-data-field=\"data\"\n" +
    "								title-field=\"name\"\n" +
    "								description-field=\"\"\n" +
    "								input-class=\"form-control\"\n" +
    "								minlength=\"2\"\n" +
    "								remote-url-request-formatter=\"vm.cityRequestFormat\"\n" +
    "								selected-object=\"vm.citySelectCallback\"\n" +
    "								match-class=\"highlight\"\n" +
    "								field-required=\"true\"\n" +
    "							></angucomplete-alt>\n" +
    "							<input type=\"hidden\" required ng-model=\"vm.city.id\" />\n" +
    "						</div>\n" +
    "					</div>\n" +
    "					<div class=\"col-md-4\">\n" +
    "						<div class=\"form-group\">\n" +
    "							<input value=\"{{vm.province.name}}\" class=\"form-control\" type=\"text\" placeholder=\"Province\" readonly />\n" +
    "						</div>\n" +
    "					</div>\n" +
    "					<div class=\"col-md-4\">\n" +
    "						<div class=\"form-group\">\n" +
    "							<input type=\"text\" placeholder=\"Postal\" class=\"form-control\" ng-model=\"vm.job.postal\" ng-pattern=\"/[0-9]{5,10}/\" required />\n" +
    "						</div>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "				<div class=\"row\">\n" +
    "					<div class=\"col-md-6\">\n" +
    "						<div class=\"form-group\">\n" +
    "							<angucomplete-alt id=\"category\"\n" +
    "								placeholder=\"Category\"\n" +
    "								pause=\"400\"\n" +
    "								initial-value=\"vm.category\"\n" +
    "								remote-url=\"{{vm.filterUrls.category}}\"\n" +
    "								remote-url-data-field=\"data\"\n" +
    "								title-field=\"title\"\n" +
    "								description-field=\"\"\n" +
    "								input-class=\"form-control\"\n" +
    "								minlength=\"2\"\n" +
    "								remote-url-request-formatter=\"vm.categoryRequestFormat\"\n" +
    "								selected-object=\"vm.categorySelectCallback\"\n" +
    "								match-class=\"highlight\"\n" +
    "								field-required=\"true\"\n" +
    "							></angucomplete-alt>\n" +
    "						</div>\n" +
    "					</div>\n" +
    "					<div class=\"col-md-6\">\n" +
    "						<div class=\"form-group\">\n" +
    "							<select ng-model=\"vm.job.priority\" class=\"cs-select cs-skin-elastic\">\n" +
    "								<option ng-selected=\"vm.job.priority == 'standard'\" value=\"standard\">Standard</option>\n" +
    "								<option ng-selected=\"vm.job.priority == 'urgent'\" value=\"urgent\">Urgent</option>\n" +
    "							</select>\n" +
    "						</div>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "				<div class=\"form-group\">\n" +
    "					<textarea ckeditor=\"vm.ckeditorOptions\" ng-model=\"vm.job.description\" rows=\"5\" placeholder=\"\" class=\"form-control\"></textarea>\n" +
    "				</div>\n" +
    "				<div class=\"form-group text-center\">\n" +
    "					<button ng-if=\"!vm.job.isActive()\" ng-click=\"vm.saveDraft()\" ng-disabled=\"!form.$valid\" class=\"btn btn-o btn-success\" type=\"button\"><i class=\"ti-save\"></i> Save Draft</button>\n" +
    "					<button ng-disabled=\"!form.$valid\" class=\"btn btn-o btn-primary\" type=\"submit\"><i class=\"ti-save-alt\"></i> Questionnaire, Assessment & Video Interview</button>\n" +
    "					<button class=\"btn btn-o btn-wide btn-default\" ng-click=\"vm.cancel()\" type=\"button\">Cancel</button>\n" +
    "				</div>\n" +
    "			</form>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("jobs/templates/offer-letter/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/offer-letter/index.tpl.html",
    "<div class=\"container-fluid container-fullw bg-white\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-12\">\n" +
    "            <div class=\"clearfix\">\n" +
    "                <h1 class=\"mainTitle text-primary pull-left\">Send Offer Letters</h1>\n" +
    "\n" +
    "                <div class=\"pull-right title-btn\">\n" +
    "                    <button class=\"btn btn-primary btn-o\" type=\"button\"><i class=\"fa fa-thumbs-up\"></i> Select Template\n" +
    "                    </button>\n" +
    "                    <button class=\"btn btn-primary btn-o\" type=\"button\"><i class=\"fa fa-share-alt\"></i> Share</button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-3\">\n" +
    "                    <div class=\"panel panel-white no-padding\">\n" +
    "                        <div class=\"panel-heading\">\n" +
    "                            <h4 class=\"panel-title text-primary\">Candidates</h4>\n" +
    "                            <ct-paneltool class=\"panel-tools\" tool-refresh=\"load1\"></ct-paneltool>\n" +
    "                        </div>\n" +
    "                        <div class=\"panel-body\">\n" +
    "                            <div class=\"panel-scroll height-350\" perfect-scrollbar wheel-propagation=\"false\"\n" +
    "                                 suppress-scroll-x=\"true\">\n" +
    "                                <div class=\"table-responsive\">\n" +
    "                                    <table class=\"table table-hover\">\n" +
    "                                        <tbody>\n" +
    "                                        <tr ng-class=\"{active: vm.isActive(item)}\" ng-click=\"vm.select(item)\" ng-repeat=\"item in vm.candidates\">\n" +
    "                                            <td class=\"pointer\">\n" +
    "                                                <div class=\"checkbox clip-check check-primary\">\n" +
    "                                                    <input type=\"checkbox\" id=\"candidate_{{item.id}}\" checked=\"checked\" />\n" +
    "                                                    <label for=\"candidate_{{item.id}}\">{{item.name}}</label>\n" +
    "                                                </div>\n" +
    "                                            </td>\n" +
    "                                        </tr>\n" +
    "                                        </tbody>\n" +
    "                                    </table>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-9\">\n" +
    "                    <div class=\"panel panel-white\">\n" +
    "                        <div class=\"panel-body\">\n" +
    "                            <div class=\"panel-scroll\" perfect-scrollbar wheel-propagation=\"false\"\n" +
    "                                 suppress-scroll-x=\"true\">\n" +
    "                                <textarea ckeditor=\"vm.ckeditorOptions\" ng-model=\"body\" rows=\"5\" placeholder=\"\" class=\"form-control\"></textarea>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "			\n" +
    "			\n" +
    "			<div class=\"row\">\n" +
    "				<div class=\"col-md-12\">\n" +
    "					<div class=\"form-group text-center margin-top-30\">\n" +
    "						<button ng-click=\"vm.gotoReferenCheck()\" class=\"btn btn-o btn-success\" type=\"button\"><i\n" +
    "								class=\"fa fa-arrow-left\"></i> Reference Checks\n" +
    "						</button>\n" +
    "						<button ng-click=\"vm.gotoPlacementRecord()\" class=\"btn btn-o btn-primary\" type=\"button\">\n" +
    "							Placement\n" +
    "							Records <i class=\"fa fa-arrow-right\"></i></button>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("jobs/templates/phone-screen/add-schedule.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/phone-screen/add-schedule.tpl.html",
    "<form id=\"schedule\" class=\"form-horizontal form-validation\" name=\"form\" novalidate>\n" +
    "	<h3>Add Schedule</h3>\n" +
    "	<div class=\"ngdialog-message\">\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Available On\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<input class=\"form-control\" type=\"text\" placeholder=\"Date\" bs-datepicker ng-model=\"available_date\" data-autoclose=\"1\" data-date-type=\"string\" data-date-format=\"{{app.format_date}}\" data-container=\"#schedule\" data-min-date=\"{{minDate}}\" />\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Time\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<select name=\"available_hour\" id=\"available_hour\" ng-model=\"available_hour\" class=\"col-md-2\">\n" +
    "				  <option ng-repeat=\"option in date.hours\" value=\"{{option}}\">{{option}}</option>\n" +
    "				</select>\n" +
    "				<span class=\"col-md-1\">:</span>\n" +
    "				<select name=\"available_hour\" id=\"available_min\" ng-model=\"available_min\" class=\"col-md-2\">\n" +
    "				  <option ng-repeat=\"option in date.min\" value=\"{{option}}\">{{option}}</option>\n" +
    "				</select>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		\n" +
    "	</div>\n" +
    "	<div class=\"ngdialog-buttons text-right\">\n" +
    "	    <button type=\"button\" class=\"btn btn-primary btn-o\" ng-click=\"create()\" ng-disabled=\"!form.$valid\">Save</button>\n" +
    "	    <button type=\"button\" class=\"btn\" ng-click=\"closeThisDialog('cancel')\">Cancel</button>\n" +
    "	</div>\n" +
    "</form>");
}]);

angular.module("jobs/templates/phone-screen/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/phone-screen/index.tpl.html",
    "<div class=\"container-fluid container-fullw bg-white\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-12\">\n" +
    "            <h1 class=\"mainTitle text-primary\">Phone Screen</h1>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"row header-large-screen\">\n" +
    "        <div class=\"col-md-12\">\n" +
    "            <div class=\"col-md-3\">\n" +
    "                <p class=\"text-bold text-center\">\n" +
    "                    Candidate Quick Select\n" +
    "                </p>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-6\">\n" +
    "                <p class=\"text-bold text-center\">\n" +
    "                    Candidate Resume\n" +
    "                </p>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-3\">\n" +
    "                <div class=\"quick-note clearfix\">\n" +
    "                    <p class=\"text-bold pull-left\">Quick Notes</p>\n" +
    "                    <div class=\"pull-right\">\n" +
    "                        <button ng-click=\"vm.saveQuickNote()\" class=\"btn btn-primary btn-o\" type=\"button\">Save</button>\n" +
    "                        <button ng-click=\"vm.share()\" class=\"btn btn-primary btn-o\" type=\"button\">Share</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-12\">\n" +
    "            <div class=\"col-md-3\">\n" +
    "                <div class=\"form-group header-inline\">\n" +
    "                    <p class=\"text-bold text-center\">\n" +
    "                        Candidate Quick Select\n" +
    "                    </p>\n" +
    "                </div>\n" +
    "                <div class=\"panel panel-white no-padding\">\n" +
    "                    <div class=\"panel-heading\">\n" +
    "                        <h4 class=\"panel-title text-primary\">Candidates</h4>\n" +
    "                        <ct-paneltool class=\"panel-tools\" tool-refresh=\"load1\"></ct-paneltool>\n" +
    "                    </div>\n" +
    "                    <div class=\"panel-body\">\n" +
    "                        <div class=\"panel-scroll height-300\" perfect-scrollbar wheel-propagation=\"false\" suppress-scroll-x=\"true\">\n" +
    "                            <div class=\"table-responsive\" perfect-scrollbar=\"\" wheel-propagation=\"false\" suppress-scroll-x=\"true\">\n" +
    "                                <table class=\"table table-hover\">\n" +
    "                                    <tbody>\n" +
    "                                        <tr ng-class=\"{active: vm.isActive(item)}\" ng-click=\"vm.select(item)\" ng-repeat=\"item in vm.items\">\n" +
    "                                            <td class=\"pointer\">\n" +
    "                                                {{item.candidate.name}}\n" +
    "                                            </td>\n" +
    "                                        </tr>\n" +
    "                                    </tbody>\n" +
    "                                </table>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-6\">\n" +
    "                <div class=\"form-group header-inline\">\n" +
    "                    <p class=\"text-bold text-center\">\n" +
    "                        Candidate Resume\n" +
    "                    </p>\n" +
    "                </div>\n" +
    "                <div class=\"panel panel-white\">\n" +
    "                    <div class=\"panel-body\">\n" +
    "                        <div class=\"panel-scroll phone-candidate-resume\" perfect-scrollbar wheel-propagation=\"false\" suppress-scroll-x=\"true\">\n" +
    "                            <div ng-if=\"vm.isSelected()\" pdf-viewer data-model=\"vm.candidateSelected\" pdf-url=\"vm.candidateSelected.resume\"></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-3\">\n" +
    "                <div class=\"quick-note clearfix header-inline\">\n" +
    "                    <p class=\"text-bold pull-left\">Quick Notes</p>\n" +
    "                    <div class=\"pull-right\">\n" +
    "                        <button ng-click=\"vm.saveQuickNote()\" class=\"btn btn-primary btn-o\" type=\"button\">Save</button>\n" +
    "                        <button ng-click=\"vm.share()\" class=\"btn btn-primary btn-o\" type=\"button\">Share</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <textarea class=\"phone-quick-note\" rows=\"14\" placeholder=\"\" class=\"form-control\" ng-model=\"vm.selectedPhoneScreen.quick_note\" ng-readonly=\"!vm.selectedPhoneScreen\"></textarea>\n" +
    "                </div>\n" +
    "                <p class=\"margin-top-10\">Invite candidate to Questionnaire?</p>\n" +
    "                <div class=\"text-center\">\n" +
    "                    <button ng-disabled=\"!vm.isSelected()\" class=\"btn btn-primary btn-large btn-o\" type=\"button\" ng-click=\"vm.invite()\">Invite</button>\n" +
    "                    <button ng-if=\"!vm.showInvite()\" ng-disabled=\"true\" class=\"btn btn-success btn-large  btn-o\" type=\"button\">Invited</button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"form-group floating-nav-jobs text-center\">\n" +
    "            <div>\n" +
    "            <button ng-click=\"vm.gotoCandidates()\" class=\"btn btn-o btn-success\" type=\"button\"><i class=\"fa fa-arrow-left\"></i> Candidates</button>\n" +
    "            <button ng-click=\"vm.gotoVideo()\" class=\"btn btn-o btn-primary btn-o\" type=\"button\">Review Videos <i class=\"fa fa-arrow-right\"></i></button>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("jobs/templates/phone-screen/share.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/phone-screen/share.tpl.html",
    "<div class=\"ngdialog-message\">\n" +
    "    <table class=\"table table-striped table-user\">\n" +
    "        <thead>\n" +
    "            <tr>\n" +
    "                <th>&nbsp;</th>\n" +
    "                <th>Username</th>\n" +
    "                <th>Name</th>\n" +
    "                <th>Phone</th>\n" +
    "                <th>Email</th>\n" +
    "                <th>Access</th>\n" +
    "            </tr>\n" +
    "        </thead>\n" +
    "        <tbody>\n" +
    "            <tr ng-repeat=\"item in employees\">\n" +
    "                <td>\n" +
    "                    <div class=\"checkbox clip-check check-primary\">\n" +
    "                        <input type=\"checkbox\" id=\"checkbox_{{item.getId()}}\" ng-model=\"item.checked\" />\n" +
    "                        <label for=\"checkbox_{{item.getId()}}\"></label>\n" +
    "                    </div>\n" +
    "                </td>\n" +
    "                <td>{{item.getUserName()}}</td>\n" +
    "                <td>{{item.getFullName()}}</td>\n" +
    "                <td>{{item.phone}}</td>\n" +
    "                <td>{{item.email}}</td>\n" +
    "                <td>{{item.group}}</td>\n" +
    "            </tr>\n" +
    "            <tr ng-if=\"vm.items.length == 0\">\n" +
    "                <td colspan=\"6\">No record found!</td>\n" +
    "            </tr>\n" +
    "        </tbody>\n" +
    "    </table>\n" +
    "    <div class=\"text-right\">\n" +
    "        <pagination rotate=\"false\" boundary-links=\"true\" max-size=\"5\" items-per-page=\"paginator.per_page\" total-items=\"paginator.total\" ng-model=\"paginator.current_page\" ng-change=\"getList()\"></pagination>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"ngdialog-buttons text-right\">\n" +
    "    <button type=\"button\" class=\"btn btn-primary btn-o\" ng-click=\"share()\">Share</button>\n" +
    "    <button type=\"button\" class=\"btn btn-primary btn-o\" ng-click=\"closeThisDialog('no')\">Close</button>\n" +
    "</div>");
}]);

angular.module("jobs/templates/placement-record/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/placement-record/index.tpl.html",
    "<div class=\"container-fluid container-fullw bg-white\">\n" +
    "	<div class=\"row\">\n" +
    "		<div class=\"col-md-12\">\n" +
    "			<h1 class=\"mainTitle text-primary\">Add Placement Record</h1>\n" +
    "			<div class=\"row\">\n" +
    "				<div class=\"col-md-3\">\n" +
    "					<div class=\"form-group\">\n" +
    "						<div class=\"input-group\">\n" +
    "							<input type=\"text\" class=\"form-control\" placeholder=\"Search\">\n" +
    "							<span class=\"input-group-addon\"> <i class=\"ti-search\"></i> </span>\n" +
    "						</div>\n" +
    "					</div>\n" +
    "					<div class=\"panel panel-white\">\n" +
    "						<div class=\"panel-heading\">\n" +
    "							<h4 class=\"panel-title text-primary\">Add Placement Record</h4>\n" +
    "							<ct-paneltool class=\"panel-tools\" tool-refresh=\"load1\"></ct-paneltool>\n" +
    "						</div>\n" +
    "						<div class=\"panel-body\">\n" +
    "							<div class=\"panel-scroll height-425\" perfect-scrollbar wheel-propagation=\"false\" suppress-scroll-x=\"true\">\n" +
    "								<div class=\"table-responsive\">\n" +
    "									<table class=\"table table-hover\">\n" +
    "										<tbody>\n" +
    "										<tr ng-class=\"{active: vm.isActive(item)}\" ng-click=\"vm.select(item)\"\n" +
    "											ng-repeat=\"item in vm.candidates\">\n" +
    "											<td class=\"pointer\">\n" +
    "												{{item.name}}\n" +
    "											</td>\n" +
    "										</tr>\n" +
    "										</tbody>\n" +
    "									</table>\n" +
    "								</div>\n" +
    "							</div>\n" +
    "						</div>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "				<div class=\"col-md-9\">\n" +
    "					<div class=\"form-group text-right\">\n" +
    "						<button class=\"btn btn-primary btn-o\" type=\"button\" ng-click=\"vm.addNewRecords();\">\n" +
    "                           New Record\n" +
    "                        </button>\n" +
    "                        <button class=\"btn btn-primary btn-o\" type=\"button\">\n" +
    "                        	<i class=\"fa fa-share-alt\"></i> Share\n" +
    "                        </button>\n" +
    "                    </div>\n" +
    "					<div class=\"tabbable\">\n" +
    "						<tabset justified=\"true\" class=\"tabbable\">\n" +
    "							<tab heading=\"General Information\">\n" +
    "								<p class=\"text-bold\">Jenniffer Kiss</p>\n" +
    "								<form>\n" +
    "									<div class=\"form-group col-md-6\">\n" +
    "										<input type=\"text\" placeholder=\"Position\" class=\"form-control\">\n" +
    "									</div>\n" +
    "									<div class=\"form-group col-md-6\">\n" +
    "										<input type=\"text\" placeholder=\"Wage\" class=\"form-control\">\n" +
    "									</div>\n" +
    "									<div class=\"form-group col-md-6\">\n" +
    "										<input type=\"text\" placeholder=\"Start Date\" class=\"form-control\">\n" +
    "									</div>\n" +
    "									<div class=\"form-group col-md-6\">\n" +
    "										<input type=\"text\" placeholder=\"Vacation Time\" class=\"form-control\">\n" +
    "									</div>\n" +
    "									<div class=\"form-group col-md-6\">\n" +
    "										<input type=\"text\" placeholder=\"Phone Number\" class=\"form-control\">\n" +
    "									</div>\n" +
    "									<div class=\"form-group col-md-6\">\n" +
    "										<input type=\"text\" placeholder=\"Company Email\" class=\"form-control\">\n" +
    "									</div>\n" +
    "									<div class=\"form-group col-md-6\">\n" +
    "										<input type=\"text\" placeholder=\"Report to\" class=\"form-control\">\n" +
    "									</div>\n" +
    "									<div class=\"form-group col-md-6\">\n" +
    "										<input type=\"text\" placeholder=\"Benefits Start Date\" class=\"form-control\">\n" +
    "									</div>\n" +
    "									<div class=\"form-group col-md-12\">\n" +
    "										<textarea rows=\"5\" placeholder=\"Add comment...\" class=\"form-control\"></textarea>\n" +
    "									</div>\n" +
    "									<div class=\"form-group col-md-12\">\n" +
    "										<strong>Time with Company:  10 Years 5 Days</strong>\n" +
    "										<button class=\"btn btn-o btn-success pull-right\" type=\"button\"><i class=\"ti-save\"></i> Save</button>\n" +
    "									</div>\n" +
    "								</form>\n" +
    "							</tab>\n" +
    "							<tab heading=\"Contracts & Documents\">\n" +
    "								<form class=\"form-inline\">\n" +
    "									<button class=\"btn btn-primary pull-right btn-o\" ng-click=\"vm.uploadDocument()\">Upload Contract</button>\n" +
    "								</form>\n" +
    "								<table class=\"table table-striped table-user\">\n" +
    "									<thead>\n" +
    "										<tr>\n" +
    "											<th>&nbsp;</th>\n" +
    "											<th>Title</th>\n" +
    "											<th>Uploaded</th>\n" +
    "											<th>View</th>\n" +
    "											<th>Edit /Delete</th>\n" +
    "										</tr>\n" +
    "									</thead>\n" +
    "									<tbody>\n" +
    "										<tr>\n" +
    "											<td>\n" +
    "												<div class=\"checkbox clip-check check-primary\">\n" +
    "													<input type=\"checkbox\" id=\"checkbox_{{item.getId()}}\" ng-model=\"item.checked\" />\n" +
    "													<label for=\"checkbox_{{item.getId()}}\"></label>\n" +
    "												</div>\n" +
    "											</td>\n" +
    "											<td>Account Executive</td>\n" +
    "											<td>5 Days</td>\n" +
    "											<td><i class=\"ti-eye text-primary\"></i></td>\n" +
    "											<td>\n" +
    "												<a ui-sref=\"fox.jobs.edit({id: item.id})\" class=\"btn btn-transparent\"><i class=\"fa fa-edit\"></i></a>\n" +
    "												<a href=\"#\" ng-click=\"vm.delete(item.getId())\" class=\"btn btn-transparent btn-xs\"><i class=\"fa fa-trash-o\"></i></a>\n" +
    "											</td>\n" +
    "										</tr>\n" +
    "										<tr>\n" +
    "											<td>\n" +
    "												<div class=\"checkbox clip-check check-primary\">\n" +
    "													<input type=\"checkbox\" id=\"checkbox_{{item.getId()}}\" ng-model=\"item.checked\" />\n" +
    "													<label for=\"checkbox_{{item.getId()}}\"></label>\n" +
    "												</div>\n" +
    "											</td>\n" +
    "											<td>Account Executive</td>\n" +
    "											<td>10 Days</td>\n" +
    "											<td><i class=\"ti-eye text-primary\"></i></td>\n" +
    "											<td>\n" +
    "												<a ui-sref=\"fox.jobs.edit({id: item.id})\" class=\"btn btn-transparent\"><i class=\"fa fa-edit\"></i></a>\n" +
    "												<a href=\"#\" ng-click=\"vm.delete(item.getId())\" class=\"btn btn-transparent btn-xs\"><i class=\"fa fa-trash-o\"></i></a>\n" +
    "											</td>\n" +
    "										</tr>\n" +
    "									</tbody>\n" +
    "								</table>\n" +
    "							</tab>\n" +
    "							<tab heading=\"Benefits\">\n" +
    "								<div class=\"title-heading\">\n" +
    "									<h4 class=\"title text-primary\">Benefits</h4>\n" +
    "									<button ng-click=\"vm.addBenifits()\" class=\"btn btn-primary btn-o\" type=\"button\"><i class=\"fa fa-plus\"></i> Add</button>\n" +
    "								</div>\n" +
    "								<div class=\"table-responsive\">\n" +
    "									<table class=\"table table-hover\">\n" +
    "										<thead>\n" +
    "											<tr class=\"info\">\n" +
    "												<th width=\"55\">#</th>\n" +
    "												<th>Title</th>\n" +
    "												<th>Description</th>\n" +
    "												<th>Start Date</th>\n" +
    "												<th width=\"100\">Edit / Delete</th>\n" +
    "											</tr>\n" +
    "										</thead>\n" +
    "										<tbody>\n" +
    "											<tr ng-repeat=\"item in vm.benefits\">\n" +
    "												<td>{{vm.getBenefitIndex($index)}}</td>\n" +
    "												<td ng-bind-html=\"item.title | nl2br\"></td>\n" +
    "												<td ng-bind-html=\"item.description | nl2br\"></td>\n" +
    "												<td ng-bind-html=\"item.start_date | nl2br\"></td>\n" +
    "												<td>\n" +
    "													<a href=\"#\" ng-click=\"vm.edit(item)\" class=\"btn btn-transparent\"><i class=\"fa fa-edit\"></i></a>\n" +
    "													<a href=\"#\" ng-click=\"vm.delete(item)\" class=\"btn btn-transparent btn-xs\"><i class=\"fa fa-trash-o\"></i></a>\n" +
    "												</td>\n" +
    "											</tr>\n" +
    "											<tr ng-if=\"vm.benefits.length == 0\">\n" +
    "												<td colspan=\"3\">No record found!</td>\n" +
    "											</tr>\n" +
    "										</tbody>\n" +
    "									</table>\n" +
    "									<div class=\"text-right\">\n" +
    "										<pagination rotate=\"false\" boundary-links=\"true\" max-size=\"5\" items-per-page=\"vm.benefitPaginator.per_page\" total-items=\"vm.benefitPaginator.total\" ng-model=\"vm.benefitPaginator.current_page\" ng-change=\"vm.getBenefitList()\"></pagination>\n" +
    "									</div>\n" +
    "								</div>\n" +
    "								\n" +
    "							</tab>\n" +
    "						</tabset>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "			<div class=\"form-group floating-nav-jobs text-center\">\n" +
    "				<div>\n" +
    "				<button ng-click=\"vm.gotoOfferLetter()\" class=\"btn btn-o btn-success\" type=\"button\"><i class=\"fa fa-arrow-left\"></i> Offer Letters</button>\n" +
    "				<button class=\"btn btn-o btn-primary\" type=\"button\">Complete Hiring <i class=\"fa fa-arrow-right\"></i></button>\n" +
    "			</div>\n" +
    "				</div>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("jobs/templates/question/add.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/question/add.tpl.html",
    "<form class=\"form-horizontal form-validation\" name=\"form\" novalidate>\n" +
    "	<div class=\"ngdialog-message\">\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Question\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<textarea style=\"resize:none;\" class=\"form-control\" rows=\"5\" ng-model=\"question.title\"></textarea>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Default Answer\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "                <input type=\"text\" placeholder=\"Default Answer\" class=\"form-control\" ng-model=\"question.default_answer\" required />\n" +
    "			</div>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "	<div class=\"ngdialog-buttons text-right\">\n" +
    "	    <button type=\"button\" class=\"btn btn-primary btn-o\" ng-click=\"create()\" ng-disabled=\"!form.$valid\">Save</button>\n" +
    "	    <button type=\"button\" class=\"btn\" ng-click=\"closeThisDialog('cancel')\">Cancel</button>\n" +
    "	</div>\n" +
    "</form>");
}]);

angular.module("jobs/templates/question/assessment-question.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/question/assessment-question.tpl.html",
    "<div id=\"create-questionnaire\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-sm-12\">\n" +
    "            <div ng-show=\"showing\">\n" +
    "                <div class=\"col-xs-12 col-md-6\">\n" +
    "                    <label>Select question type</label>\n" +
    "                    <select class=\"form-control\" ng-model=\"type\" ng-options=\"type as type.name for type in QuestionType\"></select>\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label>Question Text: </label>\n" +
    "                        <input type=\"text\" ng-model=\"questionText\" class=\"form-control\" placeholder=\"Enter text of question\" />\n" +
    "                    </div>\n" +
    "                    <div ng-if=\"type.type == 'rankOrder'\" class=\"form-group\">\n" +
    "                        <label>Description: </label>\n" +
    "                        <input type=\"text\" ng-model=\"description\" class=\"form-control\" placeholder=\"Enter text of question\" />\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-xs-12 col-md-6\">\n" +
    "                    <p>Preview</p>\n" +
    "                    <div choice-multi-answer-horizontal data-control=\"ChoiceMultiAnswerHorizontal\" ng-if=\"type.type == 'ChoiceMultiAnswerHorizontal'\"></div>\n" +
    "                    <div choice-multi-answer-select-box data-control=\"ChoiceMultiAnswerSelectBox\" ng-if=\"type.type == 'ChoiceMultiAnswerSelectBox'\"></div>\n" +
    "                    <div choice-multi-answer-vertical data-control=\"choiceMultiAnswerVertical\" ng-if=\"type.type == 'choiceMultiAnswerVertical'\"></div>\n" +
    "                    <div choice-single-answer-dropdown data-control=\"choiceSingleAnswerDropdown\" ng-if=\"type.type == 'choiceSingleAnswerDropdown'\"></div>\n" +
    "                    <div choice-single-answer-horizontal data-control=\"choiceSingleAnswerHorizontal\" ng-if=\"type.type == 'choiceSingleAnswerHorizontal'\"></div>\n" +
    "                    <div choice-single-answer-vertical data-control=\"choiceSingleAnswerVertical\" ng-if=\"type.type == 'choiceSingleAnswerVertical'\"></div>\n" +
    "                    <div number-allowcation-constant-sum data-control=\"numberAllowcationConstantSum\" ng-if=\"type.type == 'numberAllowcationConstantSum'\"></div>\n" +
    "                    <div open-ended-text-comment-box data-control=\"openEndedTextCommentBox\" ng-if=\"type.type == 'openEndedTextCommentBox'\"></div>\n" +
    "                    <div open-ended-text-date-time data-control=\"openEndedTextDateTime\" ng-if=\"type.type == 'openEndedTextDateTime'\"></div>\n" +
    "                    <div open-ended-text-one-line data-control=\"openEndedTextOneLine\" ng-if=\"type.type == 'openEndedTextOneLine'\"></div>\n" +
    "                    <div rank-order data-control=\"rankOrder\" ng-if=\"type.type == 'rankOrder'\"></div>\n" +
    "                    <div file-upload data-control=\"fileUpload\" ng-if=\"type.type == 'fileUpload'\"></div>\n" +
    "                    <div datalist data-control=\"datalist\" ng-if=\"type.type == 'datalist'\"></div>\n" +
    "                    <div matrix-multi-answer-per-row data-control=\"matrixMultiAnswerPerRow\" ng-if=\"type.type == 'matrixMultiAnswerPerRow'\"></div>\n" +
    "                    <div matrix-single-answer-per-row data-control=\"matrixSingleAnswerPerRow\" ng-if=\"type.type == 'matrixSingleAnswerPerRow'\"></div>\n" +
    "                    <div matrix-spreadsheet data-control=\"matrixSpreadsheet\" ng-if=\"type.type == 'matrixSpreadsheet'\"></div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div ng-hide=\"showing\">\n" +
    "                <div ng-show=\"showOptionField\" class=\"col-xs-12 col-md-6 no-pa\">\n" +
    "                    <p>Please add fields to your Question.</p>\n" +
    "                    <div ng-if=\"!isMatrix\" ng-repeat=\"item in items\" class=\"row margin-bottom-5\">\n" +
    "                        <div class=\"col-sm-10\">\n" +
    "                            <input type=\"text\" placeholder=\"Option {{$index + 1}}\" class=\"form-control\" ng-model=\"item.title\" />\n" +
    "                        </div>\n" +
    "                        <div class=\"col-sm-2\">\n" +
    "                            <button ng-click=\"deleteField(item,type.type)\" class=\"btn btn-o btn-default\"><i class=\"fa fa-minus\"></i></button>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div ng-if=\"isMatrix\" ng-repeat=\"column in columns\">\n" +
    "                        <div class=\"col-sm-10 no-padding margin-top-10\">\n" +
    "                            <input type=\"text\" placeholder=\"Column {{$index + 1}}\" class=\"form-control\" ng-model=\"column.title\" />\n" +
    "                        </div>\n" +
    "                        <div class=\"col-sm-2  margin-top-10\">\n" +
    "                            <button style=\"display:block;\" ng-click=\"deleteField(column,type.type,true)\" class=\"btn btn-o btn-default\"><i class=\"fa fa-minus\"></i></button>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <button ng-click=\"addColumn(type.type)\" ng-if=\"isMatrix\" class=\"btn btn-primary btn-o margin-top-10\"> <i class=\"fa fa-plus\"></i> Add Column</button>\n" +
    "                    <div ng-if=\"isMatrix\" ng-repeat=\"row in rows\" ng-class=\"{'margin-bottom-10' : $index == (rows.length-1)}\">\n" +
    "                        <div class=\"col-sm-10 no-padding margin-top-10\">\n" +
    "                            <input type=\"text\" ng-blur=\"addit(row,type.type,$index)\" placeholder=\"Row {{$index + 1}}\" class=\"form-control\" ng-model=\"row.title\" />\n" +
    "                        </div>\n" +
    "                        <div class=\"col-sm-2  margin-top-10\">\n" +
    "                            <button ng-click=\"deleteField(row,type.type,false)\" class=\"btn btn-o btn-default\"><i class=\"fa fa-minus\"></i></button>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <button ng-click=\"addRow(type.type)\" ng-disabled=\"columns.length< 0\" style=\"display:block;\" ng-if=\"isMatrix\" class=\"btn margin-top-10 btn-primary btn-o margin-top-10\"> <i class=\"fa fa-plus\"></i> Add Row</button>\n" +
    "                        <button ng-click=\"addField(type.type)\"  ng-if=\"!isMatrix\" class=\"btn btn-primary btn-o margin-top-10\"> <i class=\"fa fa-plus\"></i> Add Field</button>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-xs-12 col-md-6\">\n" +
    "                        <p>Preview</p>\n" +
    "                        <div style=\"word-wrap:break-word;\" choice-multi-answer-horizontal data-control=\"ChoiceMultiAnswerHorizontalPreview\" ng-if=\"type.type == 'ChoiceMultiAnswerHorizontal'\"></div>\n" +
    "                        <div style=\"word-wrap:break-word;\" choice-multi-answer-select-box data-control=\"ChoiceMultiAnswerSelectBoxPreview\" ng-if=\"type.type == 'ChoiceMultiAnswerSelectBox'\"></div>\n" +
    "                        <div style=\"word-wrap:break-word;\" choice-multi-answer-vertical data-control=\"ChoiceMultiAnswerVerticalPreview\" ng-if=\"type.type == 'choiceMultiAnswerVertical'\"></div>\n" +
    "                        <div style=\"word-wrap:break-word;\" choice-single-answer-dropdown data-control=\"ChoiceSingleAnswerDropdownPreview\" ng-if=\"type.type == 'choiceSingleAnswerDropdown'\"></div>\n" +
    "                        <div style=\"word-wrap:break-word;\" choice-single-answer-horizontal data-control=\"ChoiceSingleAnswerHorizontalPreview\" ng-if=\"type.type == 'choiceSingleAnswerHorizontal'\"></div>\n" +
    "                        <div style=\"word-wrap:break-word;\" choice-single-answer-vertical data-control=\"ChoiceSingleAnswerVerticalPreview\" ng-if=\"type.type == 'choiceSingleAnswerVertical'\"></div>\n" +
    "                        <div style=\"word-wrap:break-word;\" number-allowcation-constant-sum data-control=\"NumberAllowcationConstantSumPreview\" ng-if=\"type.type == 'numberAllowcationConstantSum'\"></div>\n" +
    "                        <div open-ended-text-comment-box data-control=\"OpenEndedTextCommentBoxPreview\" ng-if=\"type.type == 'openEndedTextCommentBox'\"></div>\n" +
    "                        <div open-ended-text-date-time data-control=\"OpenEndedTextDateTimePreview\" ng-if=\"type.type == 'openEndedTextDateTime'\"></div>\n" +
    "                        <div open-ended-text-one-line data-control=\"OpenEndedTextOneLinePreview\" ng-if=\"type.type == 'openEndedTextOneLine'\"></div>\n" +
    "                        <div rank-order data-control=\"RankOrderPreview\" ng-if=\"type.type == 'rankOrder'\"></div>\n" +
    "                        <div file-upload data-control=\"FileUploadPreview\" ng-if=\"type.type == 'fileUpload'\"></div>\n" +
    "                        <div datalist data-control=\"datalist\" ng-if=\"type.type == 'datalist'\"></div>\n" +
    "                        <div matrix-multi-answer-per-row data-control=\"MatrixMultiAnswerPerRowPreview\" ng-if=\"type.type == 'matrixMultiAnswerPerRow'\"></div>\n" +
    "                        <div matrix-single-answer-per-row data-control=\"MatrixSingleAnswerPerRowPreview\" ng-if=\"type.type == 'matrixSingleAnswerPerRow'\"></div>\n" +
    "                        <div matrix-spreadsheet data-control=\"MatrixSpreadsheetPreview\" ng-if=\"type.type == 'matrixSpreadsheet'\"></div>\n" +
    "                    </div>\n" +
    "\n" +
    "                </div>\n" +
    "                <div class=\"col-sm-12 no-padding margin-top-15\">\n" +
    "                    <button ng-click=\"next(type.type)\" class=\"btn btn-default\">\n" +
    "                        <span ng-if=\"showing\">Next <i class=\"fa fa-arrow-right\"></i></span>\n" +
    "                        <span ng-if=\"!showing\"><i class=\"fa fa-arrow-left\"></i> Back</span>\n" +
    "                    </button>\n" +
    "                    <button ng-if=\"!showing\" class=\"btn-o btn btn-primary\" ng-click=\"post()\">\n" +
    "                        <span>Save</span>\n" +
    "                    </button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "");
}]);

angular.module("jobs/templates/reference-check/add-reference.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/reference-check/add-reference.tpl.html",
    "<form class=\"form-horizontal form-validation\" name=\"form\" novalidate>\n" +
    "	<h3>Add Reference</h3>\n" +
    "	<div class=\"ngdialog-message\">\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				name\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<input type=\"text\" class=\"form-control\" ng-model=\"reference.name\"/>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Email\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<input type=\"email\" class=\"form-control\" ng-model=\"reference.email\"/>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Phone\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<input type=\"text\" class=\"form-control\" ng-model=\"reference.phone\"/>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		\n" +
    "		\n" +
    "	</div>\n" +
    "	<div class=\"ngdialog-buttons text-right\">\n" +
    "	    <button type=\"button\" class=\"btn btn-primary btn-o\" ng-click=\"create()\" ng-disabled=\"!form.$valid\">Save</button>\n" +
    "	    <button type=\"button\" class=\"btn\" ng-click=\"closeThisDialog('cancel')\">Cancel</button>\n" +
    "	</div>\n" +
    "</form>");
}]);

angular.module("jobs/templates/reference-check/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/reference-check/index.tpl.html",
    "<div class=\"container-fluid container-fullw bg-white reference-checks\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-12\">\n" +
    "            <h1 class=\"mainTitle text-primary\">Reference Checks: Paul Z</h1>\n" +
    "\n" +
    "            <div class=\"row header-large-screen\">\n" +
    "                <div class=\"col-md-3\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <div class=\"input-group\">\n" +
    "                            <input type=\"text\" class=\"form-control\" placeholder=\"Search\">\n" +
    "                            <span class=\"input-group-addon\"> <i class=\"ti-search\"></i> </span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-6\">\n" +
    "                    <div class=\"title-heading ref-title\">\n" +
    "                        <h4 class=\"title text-primary\">&nbsp;</h4>\n" +
    "                        <button class=\"btn btn-primary btn-o\" style=\"right:162px\" type=\"button\"><i class=\"ti-pin\"></i>\n" +
    "                            Select a template\n" +
    "                        </button>\n" +
    "                        <button class=\"btn btn-primary btn-o\" type=\"button\"><i class=\"fa fa-upload\"></i> Upload a\n" +
    "                            template\n" +
    "                        </button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-3\">\n" +
    "                    <div class=\"form-group text-right ref-title\">\n" +
    "                        <button class=\"btn btn-primary btn-o\" type=\"button\" ng-click=\"vm.addReference();\">Add Reference</button>\n" +
    "                        <button class=\"btn btn-primary btn-o\" type=\"button\">Save</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-3\">\n" +
    "                    <div class=\"form-group header-inline\">\n" +
    "                        <div class=\"input-group\">\n" +
    "                            <input type=\"text\" class=\"form-control\" placeholder=\"Search\">\n" +
    "                            <span class=\"input-group-addon\"> <i class=\"ti-search\"></i> </span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"panel panel-white no-padding left-refernce-check\">\n" +
    "                        <div class=\"panel-heading\">\n" +
    "                            <h4 class=\"panel-title text-primary\">Reference Checks</h4>\n" +
    "                            <ct-paneltool class=\"panel-tools\" tool-refresh=\"load1\"></ct-paneltool>\n" +
    "                        </div>\n" +
    "                        <div class=\"panel-body\">\n" +
    "                            <div class=\"panel-scroll height-350\" perfect-scrollbar wheel-propagation=\"false\"\n" +
    "                                 suppress-scroll-x=\"true\">\n" +
    "                                <div class=\"table-responsive\">\n" +
    "                                    <table class=\"table table-hover\">\n" +
    "                                        <tbody>\n" +
    "                                        <tr ng-class=\"{active: vm.isActive(item)}\" ng-click=\"vm.select(item)\"\n" +
    "                                            ng-repeat=\"item in vm.candidates\">\n" +
    "                                            <td class=\"pointer\">\n" +
    "                                                {{item.name}}\n" +
    "                                            </td>\n" +
    "                                        </tr>\n" +
    "                                        </tbody>\n" +
    "                                    </table>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-6\">\n" +
    "                    <div class=\"title-heading ref-title header-inline\">\n" +
    "                        <h4 class=\"title text-primary\">Reference: Paul Z</h4>\n" +
    "                        <button class=\"btn btn-primary btn-o\" style=\"right:162px\" type=\"button\"><i class=\"ti-pin\"></i>\n" +
    "                            Select a template\n" +
    "                        </button>\n" +
    "                        <button class=\"btn btn-primary btn-o\" type=\"button\"><i class=\"fa fa-upload\"></i> Upload a\n" +
    "                            template\n" +
    "                        </button>\n" +
    "                    </div>\n" +
    "                    <form>\n" +
    "                        <div class=\"form-group\">\n" +
    "                            <textarea ckeditor=\"vm.ckeditorOptions\" ng-model=\"body\" rows=\"5\" placeholder=\"\"\n" +
    "                                      class=\"form-control\"></textarea>\n" +
    "                        </div>\n" +
    "                    </form>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-3\">\n" +
    "                    <div class=\"form-group text-right header-inline\">\n" +
    "                        <button class=\"btn btn-primary btn-o\" type=\"button\">Add Reference</button>\n" +
    "                        <button class=\"btn btn-primary btn-o\" type=\"button\">Save</button>\n" +
    "                    </div>\n" +
    "                    <div class=\"panel panel-white no-padding right-refernce-check\">\n" +
    "                        <div class=\"panel-heading\">\n" +
    "                            <h4 class=\"panel-title text-primary\">Jenniffers References</h4>\n" +
    "                            <ct-paneltool class=\"panel-tools\" tool-refresh=\"load1\"></ct-paneltool>\n" +
    "                        </div>\n" +
    "                        <div class=\"panel-body\">\n" +
    "                            <div class=\"panel-scroll height-350\" perfect-scrollbar wheel-propagation=\"false\"\n" +
    "                                 suppress-scroll-x=\"true\">\n" +
    "                                <div class=\"table-responsive\">\n" +
    "                                    <table class=\"table table-hover\">\n" +
    "                                        <tbody>\n" +
    "                                        <tr>\n" +
    "                                            <td>Jenniffer Kiss</td>\n" +
    "                                        </tr>\n" +
    "                                        <tr>\n" +
    "                                            <td>Amanda Todd</td>\n" +
    "                                        </tr>\n" +
    "                                        <tr>\n" +
    "                                            <td>Kevin Stedfast</td>\n" +
    "                                        </tr>\n" +
    "                                        </tbody>\n" +
    "                                    </table>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"text-center\">\n" +
    "                        <button ng-if=\"vm.showInvite()\" ng-disabled=\"!vm.isSelected()\" class=\"btn btn-primary btn-large btn-o\" type=\"button\" ng-click=\"vm.invite()\">Invite to Offer</button>\n" +
    "                        <button ng-if=\"!vm.showInvite()\" ng-disabled=\"true\" class=\"btn btn-success btn-large  btn-o\" type=\"button\">Invited</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group text-center floating-nav-jobs\">\n" +
    "                <div>\n" +
    "                <button ng-click=\"vm.gotoInterviewNote()\" class=\"btn btn-o btn-success\" type=\"button\"><i\n" +
    "                        class=\"fa fa-arrow-left\"></i> Interview Notes\n" +
    "                </button>\n" +
    "                <button ng-click=\"vm.gotoOfferLetter()\" class=\"btn btn-o btn-primary\" type=\"button\">Offer Letter <i\n" +
    "                        class=\"fa fa-arrow-right\"></i></button>\n" +
    "            </div>\n" +
    "                </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("jobs/templates/review-question-result/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/review-question-result/index.tpl.html",
    "<div class=\"container-fluid container-fullw bg-white reference-checks\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-12\">\n" +
    "            <h1 class=\"mainTitle text-primary\">Review Questionnaire and Assessment Results: {{vm.getInterviewName()}}</h1>\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-3\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <div class=\"input-group\">\n" +
    "                            <input type=\"text\" class=\"form-control\" placeholder=\"Search\">\n" +
    "                            <span class=\"input-group-addon\"><i class=\"ti-search\"></i> </span>\n" +
    "                        </div>\n" +
    "                        <button class=\"btn btn-primary btn-o\" ng-click=\"vm.parseHtml()\">Show</button>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"form-group text-center\">\n" +
    "                        <div class=\"btn-group btn-group-justified\">\n" +
    "                            <a class=\"btn btn-primary btn-o\" ng-click=\"vm.changeFilter('pass')\" ng-class=\"{active:vm.checkFilter('pass')}\" href=\"#\">\n" +
    "                                Pass\n" +
    "                            </a>\n" +
    "                            <a class=\"btn btn-primary btn-o\" ng-click=\"vm.changeFilter('fail')\" ng-class=\"{active:vm.checkFilter('fail')}\" href=\"#\">\n" +
    "                                Fail\n" +
    "                            </a>\n" +
    "                            <a class=\"btn btn-primary btn-o\" style=\"padding:0\" ng-click=\"vm.changeFilter('pending')\" ng-class=\"{active:vm.checkFilter('pending')}\" href=\"#\">\n" +
    "                                Pending\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"panel panel-white no-padding\">\n" +
    "                        <div class=\"panel-heading\">\n" +
    "                            <h4 class=\"panel-title text-primary\">Select Candidate to Review</h4>\n" +
    "                            <ct-paneltool class=\"panel-tools\" tool-refresh=\"load1\"></ct-paneltool>\n" +
    "                        </div>\n" +
    "                        <div class=\"panel-body\">\n" +
    "                            <div class=\"panel-scroll height-350\" perfect-scrollbar wheel-propagation=\"false\"\n" +
    "                                 suppress-scroll-x=\"true\">\n" +
    "                                <div class=\"table-responsive\">\n" +
    "                                    <table class=\"table table-hover\">\n" +
    "                                        <tbody>\n" +
    "                                        <tr ng-class=\"{active: vm.isActive(item)}\" ng-click=\"vm.select(item)\" ng-repeat=\"item in vm.candidates\">\n" +
    "                                            <td class=\"pointer\">\n" +
    "                                                {{item.name}}\n" +
    "                                            </td>\n" +
    "                                        </tr>\n" +
    "                                        </tbody>\n" +
    "                                    </table>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "				\n" +
    "                <div class=\"col-md-9\">\n" +
    "                    <div class=\"form-group text-right\">\n" +
    "						<h3 class=\"title text-bold pull-left\">Score: 96/100 - PASS</h3>\n" +
    "                        <button ng-click=\"vm.setButtonActive('question')\" ng-class=\"{'btn-primary btn-o': vm.checkActive('question')}\" class=\"btn btn-default \" type=\"button\"><i class=\"fa fa-tasks\"></i> Review\n" +
    "                            Questionnaire\n" +
    "                        </button>\n" +
    "                        <button ng-click=\"vm.setButtonActive('assessment')\" ng-class=\"{'btn-primary btn-o': vm.checkActive('assessment')}\" class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-tasks\"></i> Review Assessment\n" +
    "                        </button>\n" +
    "                    </div>\n" +
    "					<div class=\"panel panel-white\">\n" +
    "                        <div class=\"panel-body question-result-list\" style=\"padding:0\">\n" +
    "                            <div class=\"panel-scroll review-question-content\" perfect-scrollbar wheel-propagation=\"false\" suppress-scroll-x=\"true\">\n" +
    "                                <table ng-hide=\"vm.isButtonsActive\" class=\"table no-margin margin-bottom-10 table-striped table-user ng-scope\">\n" +
    "                                    <thead>\n" +
    "                                        <tr>\n" +
    "                                            <th>Question</th>\n" +
    "                                            <th>Answer</th>\n" +
    "                                            <th></th>\n" +
    "                                        </tr>\n" +
    "                                    </thead>\n" +
    "                                    <tbody>\n" +
    "                                        <tr>\n" +
    "                                            <td>What is the Question?</td>\n" +
    "                                            <td>This is the Answer.</td>\n" +
    "                                            <td>Right</td>\n" +
    "                                        </tr>\n" +
    "                                        <tr>\n" +
    "                                            <td>What is the Question?</td>\n" +
    "                                            <td>This is the Answer.</td>\n" +
    "                                            <td>Right</td>\n" +
    "                                        </tr>\n" +
    "                                        <tr>\n" +
    "                                            <td>What is the Question?</td>\n" +
    "                                            <td>This is the Answer.</td>\n" +
    "                                            <td>Right</td>\n" +
    "                                        </tr>\n" +
    "                                        <tr>\n" +
    "                                            <td>What is the Question?</td>\n" +
    "                                            <td>This is the Answer.</td>\n" +
    "                                            <td>Right</td>\n" +
    "                                        </tr>\n" +
    "                                        <tr>\n" +
    "                                            <td>What is the Question?</td>\n" +
    "                                            <td>This is the Answer.</td>\n" +
    "                                            <td>Right</td>\n" +
    "                                        </tr>\n" +
    "                                        <tr>\n" +
    "                                            <td>What is the Question?</td>\n" +
    "                                            <td>This is the Answer.</td>\n" +
    "                                            <td>Right</td>\n" +
    "                                        </tr>\n" +
    "                                    </tbody>\n" +
    "                                </table>\n" +
    "                                <div ng-show=\"vm.isButtonsActive\" class=\"padding-10\">\n" +
    "                                    <h2 class=\"text-bold text-center\" style=\"font-size:125px;margin-top:80px;\">9.5/10</h2>\n" +
    "                                    <h3 class=\"text-bold text-center\">Assessment Score</h3>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "			\n" +
    "					\n" +
    "						<div class=\"clearfix margin-top-20\">\n" +
    "							<h2 class=\"pull-left text-bold\">&nbsp;</h2>\n" +
    "							<div class=\"pull-right col-md-4\">\n" +
    "								<button ng-if=\"vm.showInvite()\" ng-disabled=\"!vm.isCandidateSelected()\" ng-click=\"vm.invite()\" class=\"btn btn-primary btn-o\" type=\"button\">Invite to interview</button>\n" +
    "								<button ng-if=\"!vm.showInvite()\" ng-disabled=\"true\" class=\"btn btn-success btn-large btn-o\" type=\"button\">Invited</button>\n" +
    "							</div>\n" +
    "						</div>\n" +
    "							\n" +
    "                    </div>\n" +
    "                    \n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group floating-nav-jobs text-center\">\n" +
    "                <div>\n" +
    "                <button ng-click=\"vm.gotoVideo()\" class=\"btn btn-o btn-success\" type=\"button\"><i\n" +
    "                        class=\"fa fa-arrow-left\"></i> Review Videos\n" +
    "                </button>\n" +
    "                <button ng-click=\"vm.gotoInterviewNote()\" class=\"btn btn-o btn-primary btn-o\" type=\"button\">Interview Notes <i class=\"fa fa-arrow-right\"></i></button>\n" +
    "            </div>\n" +
    "                </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("jobs/templates/review-video-interview/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("jobs/templates/review-video-interview/index.tpl.html",
    "<div class=\"container-fluid container-fullw  bg-white review-video-interview\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"inline-video col-md-12\">\n" +
    "            <h1 class=\"mainTitle text-primary\">\n" +
    "                Review Video Interview:  <h4 class=\"title text-primary\">\n" +
    "                {{vm.getInterviewName()}}\n" +
    "            </h4>\n" +
    "            </h1>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"row header-large-screen\">\n" +
    "        <div class=\"col-md-3\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <div class=\"input-group\">\n" +
    "                    <input  ng-model=\"vm.keyword\" ng-keypress=\"vm.keypress($event)\" type=\"text\" class=\"form-control\" placeholder=\"Search\" />\n" +
    "                    <span ng-click=\"vm.search()\" class=\"input-group-addon pointer\"> <i class=\"ti-search\"></i> </span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-6\">\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"col-md-3\">\n" +
    "            <div class=\"form-group text-right\">\n" +
    "                <button ng-click=\"vm.share()\" class=\"btn btn-primary btn-o\" type=\"button\">\n" +
    "                    <i class=\"fa fa-share-alt\"></i> Share\n" +
    "                </button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-12\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-3\">\n" +
    "                    <div class=\"form-group header-inline\">\n" +
    "                        <div class=\"input-group\">\n" +
    "                            <input  ng-model=\"vm.keyword\" ng-keypress=\"vm.keypress($event)\" type=\"text\" class=\"form-control\" placeholder=\"Search\" />\n" +
    "                            <span ng-click=\"vm.search()\" class=\"input-group-addon pointer\"> <i class=\"ti-search\"></i> </span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"panel panel-white no-padding candidate-video\">\n" +
    "                        <div class=\"panel-heading\">\n" +
    "                            <h4 class=\"panel-title text-primary\">Candidates</h4>\n" +
    "                            <ct-paneltool class=\"panel-tools\" tool-refresh=\"load1\"></ct-paneltool>\n" +
    "                        </div>\n" +
    "                        <div class=\"panel-body\">\n" +
    "                            <div class=\"panel-scroll video-interview-candidate-list\" perfect-scrollbar wheel-propagation=\"false\"\n" +
    "                                 suppress-scroll-x=\"true\">\n" +
    "                                <div class=\"table-responsive\">\n" +
    "                                    <table class=\"table table-hover\">\n" +
    "                                        <tbody>\n" +
    "                                            <tr ng-class=\"{active: vm.isActive(item)}\" ng-click=\"vm.select(item); vm.getInterviewVideos({{item.id}})\" ng-repeat=\"item in vm.candidates\" id=\"{{item.id}}\">\n" +
    "                                                <td class=\"pointer\">\n" +
    "                                                    {{item.name}}\n" +
    "                                                </td>\n" +
    "                                            </tr>\n" +
    "                                        </tbody>\n" +
    "                                    </table>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-6\">\n" +
    "                    <div class=\"title-heading header-inline\">\n" +
    "                        <h4 class=\"title text-primary\">\n" +
    "                            {{vm.getInterviewName()}}\n" +
    "                        </h4>\n" +
    "                    </div>\n" +
    "                    <div class=\"video-review-cover\">\n" +
    "                        <iframe class=\"embed-responsive-item\" src=\"//www.youtube.com/embed/zpOULjyy-n8?rel=0\"></iframe>\n" +
    "                    </div>\n" +
    "                    <div class=\"panel panel-white margin-top-20 no-padding video-review-notes\">\n" +
    "                        <div class=\"panel-heading\">Quick Notes</div>\n" +
    "                        <div class=\"panel-body\">\n" +
    "                            <textarea ng-model=\"vm.note\" rows=\"5\" placeholder=\"\" class=\"form-control height-100\">I would like to apply this Job.</textarea>\n" +
    "                        </div>\n" +
    "                        <div class=\"panel-footer text-right\">\n" +
    "                            <button ng-click=\"vm.saveNote()\" class=\"btn btn-primary btn-o\" type=\"button\">Save</button>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-3\">\n" +
    "                    <div class=\"form-group text-right header-inline\">\n" +
    "                        <button ng-click=\"vm.share()\" class=\"btn btn-primary btn-o\" type=\"button\">\n" +
    "                            <i class=\"fa fa-share-alt\"></i> Share\n" +
    "                        </button>\n" +
    "                    </div>\n" +
    "                    <div class=\"panel panel-white question-list\">\n" +
    "                        <div class=\"panel-body\">\n" +
    "                            <div class=\"panel-scroll video-interview-question-list\" perfect-scrollbar wheel-propagation=\"false\"\n" +
    "                                 suppress-scroll-x=\"true\">\n" +
    "                                <div class=\"table-responsive\">\n" +
    "                                    <table class=\"table table-hover\">\n" +
    "                                        <tr ng-class=\"{active: vm.isQuestionActive(item)}\" ng-click=\"vm.selectQuestion(item); vm.getclicked({{$index + 1}},{{vm.questions.length}})\" ng-repeat=\"item in vm.questions\">\n" +
    "                                            <td class=\"pointer\">\n" +
    "                                                {{item.title}}\n" +
    "                                            </td>\n" +
    "                                        </tr>\n" +
    "                                    </table>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <p class=\"margin-bottom-5\">\n" +
    "                        Invite candidate to Questionnaire?\n" +
    "                    </p>\n" +
    "                    <div class=\"text-center\">\n" +
    "                        <button ng-if=\"vm.showInvite()\" ng-disabled=\"!vm.isCandidateSelected()\" ng-click=\"vm.inviteToQuestion()\" class=\"btn btn-primary btn-o\" type=\"button\">Invite</button>\n" +
    "                        <button ng-if=\"!vm.showInvite()\" ng-disabled=\"true\" class=\"btn btn-success btn-large btn-o\" type=\"button\">Invited</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group floating-nav-jobs text-center btn-video\">\n" +
    "                <div>\n" +
    "                <button ng-click=\"vm.gotoPhoneScreen()\" class=\"btn btn-o btn-success\" type=\"button\"><i\n" +
    "                        class=\"fa fa-arrow-left\"></i> Phone Screen\n" +
    "                </button>\n" +
    "                <button ng-click=\"vm.gotoQuestionResult()\" class=\"btn btn-o btn-primary\" type=\"button\">\n" +
    "                    Questionnaire/Assessments <i class=\"fa fa-arrow-right\"></i></button>\n" +
    "            </div>\n" +
    "                </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("my-jobs/list.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("my-jobs/list.tpl.html",
    "<div class=\"container-fluid container-fullw\">\n" +
    "	<div class=\"row\">\n" +
    "		<div class=\"col-md-12\">\n" +
    "			<h1 class=\"mainTitle text-primary\">My jobs</h1>\n" +
    "			<div class=\"row\">\n" +
    "				<div class=\"col-md-3\">\n" +
    "					<div class=\"panel panel-white\">\n" +
    "						<div class=\"panel-heading border-light text-center\">\n" +
    "							<h4 class=\"panel-title\">PENDING JOBS</h4>\n" +
    "						</div>\n" +
    "						<div class=\"panel-body\">\n" +
    "							<div class=\"height-80 static-block\">\n" +
    "								<div class=\"chart-statistics normal-font-family\">\n" +
    "									<h1>5</h1>\n" +
    "								</div>\n" +
    "							</div>\n" +
    "							<div class=\"row\">\n" +
    "								<div class=\"col-md-12 text-right\">\n" +
    "									<div class=\"rate\">\n" +
    "										<i class=\"fa fa-caret-down text-red\"></i><span class=\"value\">-2</span><span class=\"percentage\">%</span>\n" +
    "									</div>\n" +
    "								</div>\n" +
    "							</div>\n" +
    "						</div>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "				<div class=\"col-md-3\">\n" +
    "					<div class=\"panel panel-white no-radius\">\n" +
    "						<div class=\"panel-heading border-light text-center\">\n" +
    "							<h4 class=\"panel-title\">DAILY VIEWS</h4>\n" +
    "						</div>\n" +
    "						<div class=\"panel-body\">\n" +
    "							<div class=\"height-100\">\n" +
    "								<canvas class=\"tc-chart\" tc-chartjs-bar chart-options=\"vm.options8\" chart-data=\"vm.data2\"></canvas>\n" +
    "							</div>\n" +
    "						</div>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "				<div class=\"col-md-3\">\n" +
    "					<div class=\"panel panel-white\">\n" +
    "						<div class=\"panel-heading border-light text-center\">\n" +
    "							<h4 class=\"panel-title\">SAVED JOBS</h4>\n" +
    "						</div>\n" +
    "						<div class=\"panel-body\">\n" +
    "							<div class=\"height-80 static-block\">\n" +
    "								<div class=\"chart-statistics normal-font-family\">\n" +
    "									<h1>25</h1>\n" +
    "								</div>\n" +
    "							</div>\n" +
    "							<div class=\"row\">\n" +
    "								<div class=\"col-md-12 text-right\">\n" +
    "									<div class=\"rate\">\n" +
    "										<i class=\"fa fa-caret-down text-red\"></i><span class=\"value\">-2</span><span class=\"percentage\">%</span>\n" +
    "									</div>\n" +
    "								</div>\n" +
    "							</div>\n" +
    "						</div>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "				<div class=\"col-md-3\">\n" +
    "					<div class=\"panel panel-full panel-white\">\n" +
    "						<div class=\"panel-body\">\n" +
    "							<button class=\"btn btn-block btn-primary btn-o btn-post-job\" ng-click=\"vm.postAJob()\">Post A Job</button>\n" +
    "						</div>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "			<div class=\"title-heading\">\n" +
    "				<h4 class=\"title text-primary\">My Jobs</h4>\n" +
    "			</div>\n" +
    "			<div class=\"clearfix md-pull\">\n" +
    "				<form class=\"form-inline pull-right\">\n" +
    "					<div class=\"form-group width-200\">\n" +
    "						<select ng-model=\"vm.status\" class=\"cs-select cs-skin-elastic\">\n" +
    "							<option value=\"\">All</option>\n" +
    "							<option ng-selected=\"vm.status == '-1'\" value=\"-1\">Deleted</option>\n" +
    "							<option ng-selected=\"vm.status == '0'\" value=\"0\">Draft</option>\n" +
    "							<option ng-selected=\"vm.status == '1'\" value=\"1\">Active</option>\n" +
    "							<option ng-selected=\"vm.status == '2'\" value=\"2\">Closed</option>\n" +
    "							<option ng-selected=\"vm.status == '3'\" value=\"3\">Filled</option>\n" +
    "							<option ng-selected=\"vm.status == '4'\" value=\"4\">Archived</option>\n" +
    "							<option ng-selected=\"vm.status == '5'\" value=\"5\">Re-Opened (Internal)</option>\n" +
    "						</select>\n" +
    "					</div>\n" +
    "					<div class=\"form-group\">\n" +
    "						<input type=\"text\" ng-model=\"vm.keyword\" class=\"form-control\" placeholder=\"Search...\" />\n" +
    "						<button ng-click=\"vm.search()\" type=\"button\" class=\"btn btn-primary btn-o\">Search</button>\n" +
    "					</div>\n" +
    "				</form>\n" +
    "			</div>\n" +
    "			<table class=\"table table-striped table-user\">\n" +
    "				<thead>\n" +
    "					<tr>\n" +
    "						<th>&nbsp;</th>\n" +
    "						<th class=\"cursor-pointer\" ng:click=\"changeSorting('title')\" ng:class=\"selectedCls('title')\">Title</th>\n" +
    "						<th class=\"cursor-pointer\" ng:click=\"changeSorting('status')\" ng:class=\"selectedCls('status')\">Status</th>\n" +
    "						<th class=\"cursor-pointer\" ng:click=\"changeSorting('priority')\" ng:class=\"selectedCls('priority')\">Job Priority</th>\n" +
    "						<th>Location</th>\n" +
    "						<th class=\"cursor-pointer\" ng:click=\"changeSorting('age')\" ng:class=\"selectedCls('age')\">Date Posted</th>\n" +
    "						<th class=\"cursor-pointer\" ng:click=\"changeSorting('candidates')\" ng:class=\"selectedCls('candidates')\">Candidates</th>\n" +
    "						<th>Edit /Delete</th>\n" +
    "					</tr>\n" +
    "				</thead>\n" +
    "				<tbody>\n" +
    "					<tr ng-repeat=\"item in vm.items|orderBy:sort.column:sort.descending\">\n" +
    "						<td>\n" +
    "							<div class=\"checkbox clip-check check-primary\">\n" +
    "								<input type=\"checkbox\" id=\"checkbox_{{item.getId()}}\" ng-model=\"item.checked\" />\n" +
    "								<label for=\"checkbox_{{item.getId()}}\"></label>\n" +
    "							</div>\n" +
    "						</td>\n" +
    "						<td>{{item.title}}</td>\n" +
    "						<td>{{item.status}}</td>\n" +
    "						<td>{{item.priority}}</td>\n" +
    "						<td>{{item.getLocation()}}</td>\n" +
    "						<td>{{item.age}}</td>\n" +
    "						<td>{{item.candidates}}</td>\n" +
    "						<td>\n" +
    "							<a ui-sref=\"fox.jobs.edit({id: item.id})\" class=\"btn btn-transparent\"><i class=\"fa fa-edit\"></i></a>\n" +
    "							<a href=\"#\" ng-click=\"vm.delete(item.getId())\" class=\"btn btn-transparent btn-xs\"><i class=\"fa fa-trash-o\"></i></a>\n" +
    "						</td>\n" +
    "					</tr>\n" +
    "					<tr ng-if=\"vm.items.length === 0\">\n" +
    "						<td colspan=\"8\">\n" +
    "							No record found!\n" +
    "						</td>\n" +
    "					</tr>\n" +
    "				</tbody>\n" +
    "			</table>\n" +
    "			<div class=\"text-right\">\n" +
    "				<pagination rotate=\"false\" boundary-links=\"true\" max-size=\"5\" items-per-page=\"vm.paginator.per_page\" total-items=\"vm.paginator.total\" ng-model=\"vm.paginator.current_page\" ng-change=\"vm.getList()\"></pagination>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("myaccount/templates/calendar.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("myaccount/templates/calendar.tpl.html",
    "<!-- start: PAGE TITLE -->\n" +
    "<section id=\"page-title\">\n" +
    "	<div class=\"row\">\n" +
    "		<div class=\"col-sm-8\">\n" +
    "			<h1 class=\"mainTitle\">Calendar</h1>\n" +
    "			<span class=\"mainDescription\">A port of the bootstrap calendar widget to AngularJS (no jQuery required).</span>\n" +
    "		</div>\n" +
    "		<div ncy-breadcrumb></div>\n" +
    "	</div>\n" +
    "</section>\n" +
    "<!-- end: PAGE TITLE -->\n" +
    "<!-- start: CALENDAR -->\n" +
    "<div class=\"container-fluid container-fullw bg-white\">\n" +
    "	<div class=\"row\">\n" +
    "		<div class=\"col-md-12\">\n" +
    "			<!-- /// controller:  'CalendarCtrl' -  localtion: assets/js/controllers/calendarCtrl.js /// -->\n" +
    "			<div ng-controller=\"CalendarController\" id=\"demo\">\n" +
    "				<div class=\"row\">\n" +
    "					<div class=\"col-md-12\">\n" +
    "						<div class=\"margin-bottom-30\">\n" +
    "							<button class=\"btn btn-primary btn-o btn-wide\" ng-click=\"addEvent()\">\n" +
    "								<i class=\"ti-plus\"></i>{{hamza}} Add new event\n" +
    "							</button>\n" +
    "						</div>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "				<div class=\"row\">\n" +
    "					<div class=\"col-md-12\">\n" +
    "						<h2 class=\"text-center margin-bottom-20 margin-top-20\">{{ calendarControl.getTitle() }} </h2>\n" +
    "					</div>\n" +
    "					<div class=\"col-xs-8\">\n" +
    "						<div class=\"btn-group\">\n" +
    "							<button class=\"btn btn-primary\" ng-click=\"calendarControl.prev()\">\n" +
    "								<i class=\"ti-angle-left\"></i>\n" +
    "							</button>\n" +
    "							<button class=\"btn btn-primary\" ng-click=\"calendarControl.next()\">\n" +
    "								<i class=\"ti-angle-right\"></i>\n" +
    "							</button>\n" +
    "						</div>\n" +
    "						<button class=\"btn btn-primary btn-o\" ng-click=\"setCalendarToToday()\">\n" +
    "							Today\n" +
    "						</button>\n" +
    "					</div>\n" +
    "					<div class=\"col-xs-4 text-right\">\n" +
    "						<div class=\"visible-md visible-lg hidden-sm hidden-xs\">\n" +
    "							<div class=\"btn-group\">\n" +
    "								<label class=\"btn btn-primary\" ng-model=\"calendarView\" btn-radio=\"'year'\">\n" +
    "									Year\n" +
    "								</label>\n" +
    "								<label class=\"btn btn-primary\" ng-model=\"calendarView\" btn-radio=\"'month'\">\n" +
    "									Month\n" +
    "								</label>\n" +
    "								<label class=\"btn btn-primary\" ng-model=\"calendarView\" btn-radio=\"'week'\">\n" +
    "									Week\n" +
    "								</label>\n" +
    "								<label class=\"btn btn-primary\" ng-model=\"calendarView\" btn-radio=\"'day'\">\n" +
    "									Day\n" +
    "								</label>\n" +
    "							</div>\n" +
    "						</div>\n" +
    "						<div class=\"visible-xs visible-sm hidden-md hidden-lg\">\n" +
    "							<div class=\"btn-group\" dropdown>\n" +
    "								<button type=\"button\" class=\"btn btn-primary dropdown-toggle\" dropdown-toggle>\n" +
    "									<i class=\"fa fa-cog\"></i>&nbsp;<span class=\"caret\"></span>\n" +
    "								</button>\n" +
    "								<ul class=\"dropdown-menu pull-right dropdown-light\" role=\"menu\">\n" +
    "									<li>\n" +
    "										<a ng-model=\"calendarView\" btn-radio=\"'year'\" href=\"\">\n" +
    "											Year\n" +
    "										</a>\n" +
    "									</li>\n" +
    "									<li>\n" +
    "										<a ng-model=\"calendarView\" btn-radio=\"'month'\" href=\"\">\n" +
    "											Month\n" +
    "										</a>\n" +
    "									</li>\n" +
    "									<li>\n" +
    "										<a ng-model=\"calendarView\" btn-radio=\"'week'\" href=\"\">\n" +
    "											Week\n" +
    "										</a>\n" +
    "									</li>\n" +
    "									<li>\n" +
    "										<a ng-model=\"calendarView\" btn-radio=\"'day'\" href=\"\">\n" +
    "											Day\n" +
    "										</a>\n" +
    "									</li>\n" +
    "								</ul>\n" +
    "							</div>\n" +
    "						</div>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "				<br>\n" +
    "				<mwl-calendar\n" +
    "				calendar-events=\"events\"\n" +
    "				calendar-view=\"calendarView\"\n" +
    "				calendar-current-day=\"calendarDay\"\n" +
    "				calendar-control=\"calendarControl\"\n" +
    "				calendar-event-click=\"eventClicked($event)\"\n" +
    "				calendar-edit-event-html=\"'<div class=\\'btn btn-primary btn-sm pull-right\\'><i class=\\'ti-pencil\\'></i></div>'\"\n" +
    "				calendar-delete-event-html=\"'<div class=\\'btn btn-danger btn-sm margin-right-10 pull-right\\'><i class=\\'ti-close\\'></i></div>'\"\n" +
    "				calendar-edit-event-click=\"eventEdited($event)\"\n" +
    "				calendar-delete-event-click=\"eventDeleted($event)\"\n" +
    "				calendar-auto-open=\"false\"\n" +
    "				></mwl-calendar>\n" +
    "				<br>\n" +
    "				<br>\n" +
    "				<br>\n" +
    "				<!-- start: EDIT EVENT TEMPLATE -->\n" +
    "				<script type=\"text/ng-template\" id=\"calendarEvent.html\">\n" +
    "					<div class=\"modal-body\">\n" +
    "					<div class=\"form-group\">\n" +
    "					<label>\n" +
    "					event.title\n" +
    "					</label>\n" +
    "					<input type=\"datetime\" placeholder=\"Enter title\" class=\"form-control underline text-large\" ng-model=\"event.title\" >\n" +
    "					</div>\n" +
    "					<div class=\"form-group\">\n" +
    "					<label>\n" +
    "					Start\n" +
    "					</label>\n" +
    "					<span class=\"input-icon\">\n" +
    "					<input type=\"text\" class=\"form-control underline\" ng-click=\"startOpen = !startOpen\" datepicker-popup=\"fullDate\" ng-model=\"event.starts_at\" is-open=\"startOpen\" ng-init=\"startOpen = false\" max-date=\"event.ends_at\" close-text=\"Close\" />\n" +
    "					<i class=\"ti-calendar\"></i> </span>\n" +
    "					<timepicker ng-model=\"event.starts_at\" show-meridian=\"true\" ng-show=\"!event.allDay\"></timepicker>\n" +
    "					</div>\n" +
    "					<div class=\"form-group\">\n" +
    "					<label>\n" +
    "					End\n" +
    "					</label>\n" +
    "					<span class=\"input-icon\">\n" +
    "					<input type=\"text\" class=\"form-control underline\" ng-click=\"endOpen = !endOpen\" datepicker-popup=\"fullDate\" ng-model=\"event.ends_at\" is-open=\"endOpen\" ng-init=\"endOpen = false\" min-date=\"event.starts_at\" close-text=\"Close\" />\n" +
    "					<i class=\"ti-calendar\"></i> </span>\n" +
    "					<timepicker ng-model=\"event.ends_at\" show-meridian=\"true\" ng-show=\"!event.allDay\"></timepicker>\n" +
    "					</div>\n" +
    "					<div class=\"form-group\">\n" +
    "					<label>\n" +
    "					Category\n" +
    "					</label>\n" +
    "					<div class=\"row\">\n" +
    "					<div class=\"col-xs-6\">\n" +
    "					<div class=\"radio clip-radio radio-primary\">\n" +
    "					<input type=\"radio\" id=\"job\" name=\"optionsCategory\" value=\"job\" ng-model=\"event.type\">\n" +
    "					<label for=\"job\">\n" +
    "					<span class=\"fa fa-circle text-primary\"></span> Job\n" +
    "					</label>\n" +
    "					</div>\n" +
    "					<div class=\"radio clip-radio radio-primary\">\n" +
    "					<input type=\"radio\" id=\"home\" name=\"optionsCategory\" value=\"home\" ng-model=\"event.type\">\n" +
    "					<label for=\"home\">\n" +
    "					<span class=\"fa fa-circle text-purple\"></span> Home\n" +
    "					</label>\n" +
    "					</div>\n" +
    "					<div class=\"radio clip-radio radio-primary\">\n" +
    "					<input type=\"radio\" id=\"off-site-work\" name=\"optionsCategory\" value=\"off-site-work\" ng-model=\"event.type\">\n" +
    "					<label for=\"off-site-work\">\n" +
    "					<span class=\"fa fa-circle text-green\"></span> Off site\n" +
    "					</label>\n" +
    "					</div>\n" +
    "					</div>\n" +
    "					<div class=\"col-xs-6\">\n" +
    "					<div class=\"radio clip-radio radio-primary\">\n" +
    "					<input type=\"radio\" id=\"cancelled\" name=\"optionsCategory\" value=\"cancelled\" ng-model=\"event.type\">\n" +
    "					<label for=\"cancelled\">\n" +
    "					<span class=\"fa fa-circle text-yellow\"></span> Cancelled\n" +
    "					</label>\n" +
    "					</div>\n" +
    "					<div class=\"radio clip-radio radio-primary\">\n" +
    "					<input type=\"radio\" id=\"generic\" name=\"optionsCategory\" value=\"generic\" ng-model=\"event.type\">\n" +
    "					<label for=\"generic\">\n" +
    "					<span class=\"fa fa-circle text-info\"></span> Generic\n" +
    "					</label>\n" +
    "					</div>\n" +
    "					<div class=\"radio clip-radio radio-primary\">\n" +
    "					<input type=\"radio\" id=\"to-do\" name=\"optionsCategory\" value=\"to-do\" ng-model=\"event.type\">\n" +
    "					<label for=\"to-do\">\n" +
    "					<span class=\"fa fa-circle text-orange\"></span> ToDo\n" +
    "					</label>\n" +
    "					</div>\n" +
    "					</div>\n" +
    "					</div>\n" +
    "					</div>\n" +
    "					</div>\n" +
    "					</div>\n" +
    "					<div class=\"modal-footer\">\n" +
    "					<button class=\"btn btn-danger btn-o\" ng-click=\"deleteEvent(event)\">\n" +
    "					Delete\n" +
    "					</button>\n" +
    "					<button class=\"btn btn-primary btn-o\" ng-click=\"cancel()\">\n" +
    "					Ok\n" +
    "					</button>\n" +
    "					</div>\n" +
    "				</script>\n" +
    "				<!-- end: EDIT EVENT TEMPLATE -->\n" +
    "			</div>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>\n" +
    "<!-- end: CALENDAR -->\n" +
    "");
}]);

angular.module("myaccount/templates/messages.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("myaccount/templates/messages.tpl.html",
    "<div class=\"inbox\" id=\"inbox\" toggleable active-class=\"message-open\">\n" +
    "    <!-- start: EMAIL OPTIONS -->\n" +
    "    <div class=\"col email-options\">\n" +
    "        <div class=\"padding-15\">\n" +
    "            <button class=\"btn btn-primary btn-block margin-bottom-30 btn-o\">\n" +
    "                Compose {{$scope.isLargeDevice}}\n" +
    "            </button>\n" +
    "            <p class=\"email-options-title no-margin\">\n" +
    "                BROWSE\n" +
    "            </p>\n" +
    "            <ul class=\"main-options padding-15\">\n" +
    "                <li>\n" +
    "                    <a href ng-click=\"filters = {sent: false}\">\n" +
    "                        <span class=\"title\"><i class=\"ti-import\"></i> Inbox</span>\n" +
    "                        <span class=\"badge pull-right\" ng-if=\"messages.length\">{{(messages |filter: filters = {sent: false}).length}}</span>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ng-click=\"filters = {sent: true}\">\n" +
    "                        <span class=\"title\"><i class=\"ti-upload\"></i> Sent</span>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "			</ul>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <!-- end: EMAIL OPTIONS -->\n" +
    "    <!-- start: EMAIL LIST -->\n" +
    "    <div class=\"col email-list\" ng-hide=\"$state.current.name.indexOf('inbox')>= 0 && isMobileDevice\">\n" +
    "        <div class=\"wrap-list\">\n" +
    "            <div class=\"wrap-options\" id=\"wrap-options\" toggleable active-class=\"search-open\">\n" +
    "                <div class=\"messages-options padding-15\">\n" +
    "                    <div class=\"btn-group\" dropdown>\n" +
    "                        <button type=\"button\" class=\"btn btn-primary btn-wide\">\n" +
    "                            Compose\n" +
    "                        </button>\n" +
    "                        <button type=\"button\" class=\"btn btn-primary dropdown-toggle\" dropdown-toggle>\n" +
    "                            <span class=\"caret\"></span>\n" +
    "                        </button>\n" +
    "                        <ul class=\"dropdown-menu dropdown-light\" role=\"menu\">\n" +
    "                            <li>\n" +
    "                                <a href ng-click=\"filters = {sent: false}\">\n" +
    "                                    <span class=\"title\"><i class=\"ti-import\"></i> Inbox</span>\n" +
    "                                    <span class=\"badge\" ng-if=\"messages.length\">{{(messages |filter: filters = {sent: false}).length}}</span>\n" +
    "                                </a>\n" +
    "                            </li>\n" +
    "                            <li>\n" +
    "                                <a href ng-click=\"filters = {sent: true}\">\n" +
    "                                    <span class=\"title\"><i class=\"ti-upload\"></i> Sent</span>\n" +
    "                                </a>\n" +
    "                            </li>\n" +
    "                            <li>\n" +
    "                                <a href ng-click=\"filters = {spam: true}\">\n" +
    "                                    <span class=\"title\"><i class=\"ti-na\"></i> Spam</span>\n" +
    "                                    <span class=\"badge\" ng-if=\"(messages |filter: {spam: true}).length\">{{(messages |filter: filters = {spam: true}).length}}</span>\n" +
    "                                </a>\n" +
    "                            </li>\n" +
    "                            <li>\n" +
    "                                <a href ng-click=\"filters = {starred: true}\">\n" +
    "                                    <span class=\"title\"><i class=\"ti-star\"></i> Starred</span>\n" +
    "                                    <span class=\"badge\" ng-if=\"(messages |filter: {starred: true}).length\">{{(messages |filter: filters = {starred: true}).length}}</span>\n" +
    "                                </a>\n" +
    "                            </li>\n" +
    "                        </ul>\n" +
    "                    </div>\n" +
    "                    <button class=\"btn btn-transparent pull-right open-message-search\" ct-toggle=\"on\" target=\"wrap-options\">\n" +
    "                        <i class=\"ti-search\"></i>\n" +
    "                    </button>\n" +
    "                    <button class=\"btn btn-transparent pull-right close-message-search\" ct-toggle=\"off\" target=\"wrap-options\" ng-click=\"inbox.search = null\">\n" +
    "                        <i class=\"ti-close\"></i>\n" +
    "                    </button>\n" +
    "                </div>\n" +
    "                <div class=\"messages-search\">\n" +
    "                    <form>\n" +
    "                        <input type=\"text\" class=\"form-control underline\" placeholder=\"Search messages...\" ng-model=\"inbox.search.$\">\n" +
    "                    </form>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <ul class=\"messages-list\" perfect-scrollbar wheel-propagation=\"true\" suppress-scroll-x=\"true\">\n" +
    "                <li ng-if=\"!messages.length\">\n" +
    "                    <div class=\"\">\n" +
    "                        No messages! Try sending one to a friend.\n" +
    "                    </div>\n" +
    "                </li>\n" +
    "                <li class=\"messages-item\" ng-class=\"{starred: message.starred}\" ng-repeat=\"message in messages | filter:filters | filter:inbox.search\">\n" +
    "                    <a message-item=\"{{message.id}}\" ui-sref=\"fox.myaccount.messages.view({ id:message.id })\" href>\n" +
    "                        <span title=\"{{ message.starred && 'Remove star' || 'Mark as starred' }}\" class=\"messages-item-star\" ng-click=\"message.starred = !message.starred\"><i class=\"fa fa-star\"></i></span>\n" +
    "                        <img alt=\"{{ message.from }}\" ng-src=\"{{ message.avatar && message.avatar || noAvatarImg }}\" class=\"messages-item-avatar bordered border-primary\">\n" +
    "                        <span class=\"messages-item-from\">{{ message.from }}</span>\n" +
    "                        <div class=\"messages-item-time\">\n" +
    "                            <span class=\"text\">{{ message.date | date: \"MM/dd/yyyy 'at' h:mma\" }}</span>\n" +
    "                        </div>\n" +
    "                        <span class=\"messages-item-subject\"> <em class=\"spam\" ng-if=\"message.spam\">[SPAM] </em> {{ message.subject }}</span>\n" +
    "                        <span class=\"messages-item-content\">{{ message.content | htmlToPlaintext | words:15 :true }}</span>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <!-- end: EMAIL LIST -->\n" +
    "    <!-- start: EMAIL READER -->\n" +
    "    <div class=\"email-reader\" perfect-scrollbar wheel-propagation=\"true\" suppress-scroll-x=\"true\" ng-show=\"$state.current.name.indexOf('inbox')>= 0 || isLargeDevice || isSmallDevice\">\n" +
    "        <div ui-view>\n" +
    "            <div class=\"no-messages\">\n" +
    "                <h2>No email has been selected</h2>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <!-- end: EMAIL READER -->\n" +
    "</div>\n" +
    "");
}]);

angular.module("myaccount/templates/view-message.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("myaccount/templates/view-message.tpl.html",
    "<div class=\"message-actions\">\n" +
    "    <ul class=\"actions no-margin no-padding block\">\n" +
    "        <li class=\"email-list-toggle\">\n" +
    "            <a ui-sref=\"app.pages.messages\"><i class=\"fa fa-angle-left\"></i> All Inboxes </a>\n" +
    "        </li>\n" +
    "        <li class=\"actions-dropdown\">\n" +
    "            <span class=\"dropdown\" dropdown on-toggle=\"toggled(open)\">\n" +
    "				<a href class=\"dropdown-toggle\" dropdown-toggle class=\"btn btn-transparent\">\n" +
    "					<i class=\"caret\"></i>\n" +
    "				</a>\n" +
    "				<ul class=\"dropdown-menu dropdown-light\">\n" +
    "					<li>\n" +
    "						<a href=\"#\">\n" +
    "							Reply\n" +
    "						</a>\n" +
    "					</li>\n" +
    "					<li>\n" +
    "						<a href=\"#\">\n" +
    "							Reply all\n" +
    "						</a>\n" +
    "					</li>\n" +
    "					<li>\n" +
    "						<a href=\"#\">\n" +
    "							Forward\n" +
    "						</a>\n" +
    "					</li>\n" +
    "					<li>\n" +
    "						<a href=\"#\">\n" +
    "							Delete\n" +
    "						</a>\n" +
    "					</li>\n" +
    "				</ul> </span>\n" +
    "        </li>\n" +
    "        <li class=\"no-padding\">\n" +
    "            <a class=\"text-info\" href=\"#\">\n" +
    "				Reply\n" +
    "			</a>\n" +
    "        </li>\n" +
    "        <li class=\"no-padding\">\n" +
    "            <a href=\"#\">\n" +
    "				Reply all\n" +
    "			</a>\n" +
    "        </li>\n" +
    "        <li class=\"no-padding\">\n" +
    "            <a href=\"#\">\n" +
    "				Forward\n" +
    "			</a>\n" +
    "        </li>\n" +
    "        <li class=\"no-padding\">\n" +
    "            <a href=\"#\">\n" +
    "				Delete\n" +
    "			</a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "</div>\n" +
    "<div class=\"message-header\">\n" +
    "    <img alt=\"{{ message.from }}\" ng-src=\"{{ message.avatar && message.avatar || noAvatarImg }}\" class=\"message-item-avatar\">\n" +
    "    <div class=\"message-time\">\n" +
    "        {{message.date | date: \"MM/dd/yyyy 'at' h:mma\" }}\n" +
    "    </div>\n" +
    "    <div class=\"message-from\" ng-if=\"message.email\">\n" +
    "        {{message.from}} &lt;{{message.email}}&gt;\n" +
    "    </div>\n" +
    "    <div class=\"message-to\">\n" +
    "        To: Peter Clark\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"message-subject\">\n" +
    "    <span class=\"spam\" ng-if=\"message.spam\">[SPAM] </span> {{message.subject}}\n" +
    "</div>\n" +
    "<div class=\"message-content\" ng-bind-html=\"message.content\"></div>\n" +
    "");
}]);

angular.module("placement-record/add-documents.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("placement-record/add-documents.tpl.html",
    "<form class=\"form-horizontal form-validation\" name=\"form\" novalidate>\n" +
    "	<h3>Add Documents</h3>\n" +
    "	<div class=\"ngdialog-message\">\n" +
    "		<div file-upload data-control=\"fileUpload\"></div>\n" +
    "	</div>\n" +
    "	<div class=\"ngdialog-buttons text-right\">\n" +
    "	    <button type=\"button\" class=\"btn btn-primary btn-o\" ng-click=\"create()\" ng-disabled=\"!form.$valid\">Save</button>\n" +
    "	    <button type=\"button\" class=\"btn\" ng-click=\"closeThisDialog('cancel')\">Cancel</button>\n" +
    "	</div>\n" +
    "	\n" +
    "</form>");
}]);

angular.module("placement-record/add.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("placement-record/add.tpl.html",
    "<form class=\"form-horizontal form-validation\" name=\"form\" novalidate>\n" +
    "	<h3>Add Benefits</h3>\n" +
    "	<div class=\"ngdialog-message\">\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Title\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<input type=\"text\" class=\"form-control\" ng-model=\"benefit.title\"/>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Description\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<textarea style=\"resize:none;\" class=\"form-control\" rows=\"4\" ng-model=\"benefit.description\"></textarea>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Start Date\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<input type=\"date\" class=\"form-control\" ng-model=\"benefit.start_date\"/>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "	<div class=\"ngdialog-buttons text-right\">\n" +
    "	    <button type=\"button\" class=\"btn btn-primary btn-o\" ng-click=\"create()\" ng-disabled=\"!form.$valid\">Save</button>\n" +
    "	    <button type=\"button\" class=\"btn\" ng-click=\"closeThisDialog('cancel')\">Cancel</button>\n" +
    "	</div>\n" +
    "</form>");
}]);

angular.module("placement-record/placement-record.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("placement-record/placement-record.tpl.html",
    "<div class=\"container-fluid container-fullw bg-white\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-12\">\n" +
    "            <h1 class=\"mainTitle text-primary\">Placement Record</h1>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-3\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <div class=\"input-group\">\n" +
    "                            <input type=\"text\" class=\"form-control\" placeholder=\"Search\">\n" +
    "                            <span class=\"input-group-addon\"> <i class=\"ti-search\"></i> </span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"panel panel-white\">\n" +
    "                        <div class=\"panel-heading\">\n" +
    "                            <h4 class=\"panel-title text-primary\">Placement Record</h4>\n" +
    "                            <ct-paneltool class=\"panel-tools\" tool-refresh=\"load1\"></ct-paneltool>\n" +
    "                        </div>\n" +
    "                        <div class=\"panel-body\">\n" +
    "                            <div class=\"panel-scroll height-425\" perfect-scrollbar wheel-propagation=\"false\" suppress-scroll-x=\"true\">\n" +
    "                                <div class=\"table-responsive\">\n" +
    "                                    <table class=\"table table-hover\">\n" +
    "                                        <tbody>\n" +
    "                                            <tr ng-class=\"{active: vm.isActive(item)}\" ng-click=\"vm.select(item)\" ng-repeat=\"item in vm.candidates\">\n" +
    "                                                <td class=\"pointer\">\n" +
    "                                                    {{item.name}}\n" +
    "                                                </td>\n" +
    "                                            </tr>\n" +
    "                                        </tbody>\n" +
    "                                    </table>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-9\">\n" +
    "                    <div class=\"form-group text-right\">\n" +
    "                        <button class=\"btn btn-primary btn-o\" type=\"button\" ng-click=\"vm.addNewRecords();\">\n" +
    "                           New Record\n" +
    "                        </button>\n" +
    "						<button class=\"btn btn-primary btn-o\" type=\"button\">\n" +
    "                            <i class=\"fa fa-share-alt\"></i> Share\n" +
    "                        </button>					\n" +
    "                    </div>\n" +
    "                    <div class=\"tabbable\">\n" +
    "                        <tabset justified=\"true\" class=\"tabbable\">\n" +
    "                            <tab heading=\"General Information\">\n" +
    "                                <p class=\"text-bold\">Jenniffer Kiss</p>\n" +
    "                                <form>\n" +
    "                                    <div class=\"form-group col-md-6\">\n" +
    "                                        <input type=\"text\" placeholder=\"Position\" class=\"form-control\">\n" +
    "                                    </div>\n" +
    "                                    <div class=\"form-group col-md-6\">\n" +
    "                                        <input type=\"text\" placeholder=\"Wage\" class=\"form-control\">\n" +
    "                                    </div>\n" +
    "                                    <div class=\"form-group col-md-6\">\n" +
    "                                        <input type=\"text\" placeholder=\"Start Date\" class=\"form-control\">\n" +
    "                                    </div>\n" +
    "                                    <div class=\"form-group col-md-6\">\n" +
    "                                        <input type=\"text\" placeholder=\"Vacation Time\" class=\"form-control\">\n" +
    "                                    </div>\n" +
    "                                    <div class=\"form-group col-md-6\">\n" +
    "                                        <input type=\"text\" placeholder=\"Phone Number\" class=\"form-control\">\n" +
    "                                    </div>\n" +
    "                                    <div class=\"form-group col-md-6\">\n" +
    "                                        <input type=\"text\" placeholder=\"Company Email\" class=\"form-control\">\n" +
    "                                    </div>\n" +
    "                                    <div class=\"form-group col-md-6\">\n" +
    "                                        <input type=\"text\" placeholder=\"Report to\" class=\"form-control\">\n" +
    "                                    </div>\n" +
    "                                    <div class=\"form-group col-md-6\">\n" +
    "                                        <input type=\"text\" placeholder=\"Benefits Start Date\" class=\"form-control\">\n" +
    "                                    </div>\n" +
    "                                    <div class=\"form-group col-md-12\">\n" +
    "                                        <textarea rows=\"5\" placeholder=\"Add comment...\" class=\"form-control\"></textarea>\n" +
    "                                    </div>\n" +
    "                                    <div class=\"form-group col-md-12\">\n" +
    "                                        <strong>Time with Company:  10 Years 5 Days</strong>\n" +
    "                                        <button class=\"btn btn-o btn-success pull-right\" type=\"button\"><i class=\"ti-save\"></i> Save</button>\n" +
    "                                    </div>\n" +
    "                                </form>\n" +
    "                            </tab>\n" +
    "                            <tab heading=\"Contracts & Documents\">\n" +
    "                                <form class=\"form-inline\">\n" +
    "                                    <button class=\"btn btn-primary pull-right btn-o\" ng-click=\"vm.uploadDocument()\">Upload Contract</button>\n" +
    "                                </form>\n" +
    "                                <table class=\"table table-striped table-user\">\n" +
    "                                    <thead>\n" +
    "                                        <tr>\n" +
    "                                            <th>&nbsp;</th>\n" +
    "                                            <th>Title</th>\n" +
    "                                            <th>Uploaded</th>\n" +
    "                                            <th>View</th>\n" +
    "                                            <th>Edit /Delete</th>\n" +
    "                                        </tr>\n" +
    "                                    </thead>\n" +
    "                                    <tbody>\n" +
    "                                        <tr>\n" +
    "                                            <td>\n" +
    "                                                <div class=\"checkbox clip-check check-primary\">\n" +
    "                                                    <input type=\"checkbox\" id=\"checkbox_{{item.getId()}}\" ng-model=\"item.checked\" />\n" +
    "                                                    <label for=\"checkbox_{{item.getId()}}\"></label>\n" +
    "                                                </div>\n" +
    "                                            </td>\n" +
    "                                            <td>Account Executive</td>\n" +
    "                                            <td>5 Days</td>\n" +
    "                                            <td><i class=\"ti-eye text-primary\"></i></td>\n" +
    "                                            <td>\n" +
    "                                                <a ui-sref=\"fox.jobs.edit({id: item.id})\" class=\"btn btn-transparent\"><i class=\"fa fa-edit\"></i></a>\n" +
    "                                                <a href=\"#\" ng-click=\"vm.delete(item.getId())\" class=\"btn btn-transparent btn-xs\"><i class=\"fa fa-trash-o\"></i></a>\n" +
    "                                            </td>\n" +
    "                                        </tr>\n" +
    "                                        <tr>\n" +
    "                                            <td>\n" +
    "                                                <div class=\"checkbox clip-check check-primary\">\n" +
    "                                                    <input type=\"checkbox\" id=\"checkbox_{{item.getId()}}\" ng-model=\"item.checked\" />\n" +
    "                                                    <label for=\"checkbox_{{item.getId()}}\"></label>\n" +
    "                                                </div>\n" +
    "                                            </td>\n" +
    "                                            <td>Account Executive</td>\n" +
    "                                            <td>10 Days</td>\n" +
    "                                            <td><i class=\"ti-eye text-primary\"></i></td>\n" +
    "                                            <td>\n" +
    "                                                <a ui-sref=\"fox.jobs.edit({id: item.id})\" class=\"btn btn-transparent\"><i class=\"fa fa-edit\"></i></a>\n" +
    "                                                <a href=\"#\" ng-click=\"vm.delete(item.getId())\" class=\"btn btn-transparent btn-xs\"><i class=\"fa fa-trash-o\"></i></a>\n" +
    "                                            </td>\n" +
    "                                        </tr>\n" +
    "                                    </tbody>\n" +
    "                                </table>\n" +
    "                            </tab>\n" +
    "                            <tab heading=\"Benefits\">\n" +
    "                                <div class=\"title-heading\">\n" +
    "									<h4 class=\"title text-primary\">Benefits</h4>\n" +
    "									<button ng-click=\"vm.addBenifits()\" class=\"btn btn-primary btn-o\" type=\"button\"><i class=\"fa fa-plus\"></i> Add</button>\n" +
    "								</div>\n" +
    "								<div class=\"table-responsive\">\n" +
    "									<table class=\"table table-hover\">\n" +
    "										<thead>\n" +
    "											<tr class=\"info\">\n" +
    "												<th width=\"55\">#</th>\n" +
    "												<th>Title</th>\n" +
    "												<th>Description</th>\n" +
    "												<th>Start Date</th>\n" +
    "												<th width=\"100\">Edit / Delete</th>\n" +
    "											</tr>\n" +
    "										</thead>\n" +
    "										<tbody>\n" +
    "											<tr ng-repeat=\"item in vm.benefits\">\n" +
    "												<td>{{vm.getBenefitIndex($index)}}</td>\n" +
    "												<td ng-bind-html=\"item.title | nl2br\"></td>\n" +
    "												<td ng-bind-html=\"item.description | nl2br\"></td>\n" +
    "												<td ng-bind-html=\"item.start_date | nl2br\"></td>\n" +
    "												<td>\n" +
    "													<a href=\"#\" ng-click=\"vm.edit(item)\" class=\"btn btn-transparent\"><i class=\"fa fa-edit\"></i></a>\n" +
    "													<a href=\"#\" ng-click=\"vm.delete(item)\" class=\"btn btn-transparent btn-xs\"><i class=\"fa fa-trash-o\"></i></a>\n" +
    "												</td>\n" +
    "											</tr>\n" +
    "											<tr ng-if=\"vm.benefits.length == 0\">\n" +
    "												<td colspan=\"3\">No record found!</td>\n" +
    "											</tr>\n" +
    "										</tbody>\n" +
    "									</table>\n" +
    "									<div class=\"text-right\">\n" +
    "										<pagination rotate=\"false\" boundary-links=\"true\" max-size=\"5\" items-per-page=\"vm.benefitPaginator.per_page\" total-items=\"vm.benefitPaginator.total\" ng-model=\"vm.benefitPaginator.current_page\" ng-change=\"vm.getBenefitList()\"></pagination>\n" +
    "									</div>\n" +
    "								</div>\n" +
    "								\n" +
    "								\n" +
    "                            </tab>\n" +
    "                        </tabset>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaire/types/ChoiceMultiAnswerHorizontal/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/ChoiceMultiAnswerHorizontal/view.tpl.html",
    "<div class=\"questionnare ChoiceMultiAnswerHorizontal\">\n" +
    "    <div class=\"panel panel-white\">\n" +
    "        <div class=\"panel-heading\">\n" +
    "        	<strong ng-bind=\"control.title\"></strong>\n" +
    "        </div>\n" +
    "        <div class=\"panel-body\">\n" +
    "            <table class=\"col-xs-12\">\n" +
    "                <tr>\n" +
    "                    <td ng-repeat=\"item in control.items\">\n" +
    "                        <input type=\"checkbox\" id=\"answer-{{control.name}}-{{$index}}\" ng-model=\"item.value\" ng-change=\"select(item)\">\n" +
    "                        <label for=\"answer-{{control.name}}-{{$index}}\" ng-bind=\"item.title\"></label>\n" +
    "                    </td>\n" +
    "                </tr>\n" +
    "            </table>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaire/types/ChoiceMultiAnswerSelectBox/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/ChoiceMultiAnswerSelectBox/view.tpl.html",
    "<div class=\"questionnare ChoiceMultiAnswerSelectBox\">\n" +
    "    <div class=\"panel panel-white\">\n" +
    "        <div class=\"panel-heading\">\n" +
    "            <strong ng-bind=\"control.title\"></strong>\n" +
    "        </div>\n" +
    "        <div class=\"panel-body\">\n" +
    "            <div ng-repeat=\"item in control.items\" ng-click=\"select(item)\">\n" +
    "                <p ng-class=\"item.value ? 'selected-answer' : ''\" ng-bind=\"item.title\"></p>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaire/types/boolean/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/boolean/view.tpl.html",
    "<div>\n" +
    "    <h3>{{control.title}}</h3>\n" +
    "    <label class=\"checkbox-inline\" ng-repeat=\"item in control.items\">\n" +
    "        <input name=\"{{control.name}}\" type=\"radio\" ng-model=\"item.value\" ng-value=\"{{item.value}}\" /> {{item.title}}\n" +
    "    </label>\n" +
    "</div>");
}]);

angular.module("questionnaire/types/choiceMultiAnswerVertical/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/choiceMultiAnswerVertical/view.tpl.html",
    "<div class=\"questionnare choiceMultiAnswerVertical\">\n" +
    "    <div class=\"panel panel-white\">\n" +
    "        <div class=\"panel-heading\">\n" +
    "            <strong ng-bind=\"control.title\"></strong>\n" +
    "        </div>\n" +
    "        <div class=\"panel-body\">\n" +
    "            <div ng-repeat=\"item in control.items\">\n" +
    "                <input type=\"checkbox\" id=\"answer-{{control.name}}-{{$index}}\" ng-model=\"item.value\" ng-change=\"select(item)\">\n" +
    "                <label for=\"answer-{{control.name}}-{{$index}}\" ng-bind=\"item.title\"></label>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaire/types/choiceSingleAnswerDropdown/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/choiceSingleAnswerDropdown/view.tpl.html",
    "<div class=\"questionnare choiceSingleAnswerDropdown\">\n" +
    "    <div class=\"panel panel-white\">\n" +
    "        <div class=\"panel-heading\">\n" +
    "            <strong ng-bind=\"control.title\"></strong>\n" +
    "        </div>\n" +
    "        <div class=\"panel-body\">\n" +
    "            <select class=\"cs-select cs-skin-elastic\" ng-change=\"select(item)\" ng-model=\"item\" ng-options=\"item as item.title for item in control.items\"></select>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaire/types/choiceSingleAnswerHorizontal/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/choiceSingleAnswerHorizontal/view.tpl.html",
    "<div class=\"questionnare choiceSingleAnswerHorizontal\">\n" +
    "    <div class=\"panel panel-white\">\n" +
    "        <div class=\"panel-heading\">\n" +
    "        	<strong ng-bind=\"control.title\"></strong>\n" +
    "        </div>\n" +
    "        <div class=\"panel-body\">\n" +
    "            <table class=\"col-xs-12\">\n" +
    "                <tr>\n" +
    "                    <td ng-repeat=\"item in control.items\">\n" +
    "                        <input type=\"radio\" name=\"control.name\" id=\"answer-{{control.name}}-{{$index}}\" ng-model=\"item.value\" ng-change=\"select(item)\">\n" +
    "                        <label for=\"answer-{{control.name}}-{{$index}}\" ng-bind=\"item.title\"></label>\n" +
    "                    </td>\n" +
    "                </tr>\n" +
    "            </table>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaire/types/choiceSingleAnswerVertical/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/choiceSingleAnswerVertical/view.tpl.html",
    "<div class=\"questionnare choiceSingleAnswerVertical\">\n" +
    "    <div class=\"panel panel-white\">\n" +
    "        <div class=\"panel-heading\">\n" +
    "            <strong ng-bind=\"control.title\"></strong>\n" +
    "        </div>\n" +
    "        <div class=\"panel-body\">\n" +
    "            <div ng-repeat=\"item in control.items\">\n" +
    "                <input type=\"radio\" name=\"control.name\" id=\"answer-{{control.name}}-{{$index}}\" ng-model=\"item.value\" ng-change=\"select(item)\">\n" +
    "                <label for=\"answer-{{control.name}}-{{$index}}\" ng-bind=\"item.title\"></label>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaire/types/datalist/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/datalist/view.tpl.html",
    "<div class=\"questionnare dataList\">\n" +
    "    <div class=\"panel panel-white\">\n" +
    "        <div class=\"panel-heading\">\n" +
    "            <strong ng-bind=\"control.title\"></strong>\n" +
    "        </div>\n" +
    "        <div class=\"panel-body\">\n" +
    "            <table class=\"col-xs-12\">\n" +
    "                <tr ng-repeat=\"item in control.getValue()\">\n" +
    "                    <td ng-bind=\"item.title\"></td>\n" +
    "                    <td ng-if=\"item.type == 'choiceSingleAnswerDropdown'\">\n" +
    "                        <div class=\"form-group\">\n" +
    "                        	<select class=\"cs-select cs-skin-elastic\" ng-change=\"select(ItemData)\" ng-model=\"ItemData\" ng-options=\"ItemData as ItemData.title for ItemData in item.items\"></select>\n" +
    "                        </div>\n" +
    "                    </td>\n" +
    "                    <td ng-if=\"item.type == 'openEndedTextOneLine'\">\n" +
    "                        <div class=\"form-group\">\n" +
    "                        	<input type=\"text\" class=\"form-control\" ng-model=\"item.value\">\n" +
    "                        </div>\n" +
    "                    </td>\n" +
    "                </tr>\n" +
    "            </table>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaire/types/drop-down/edit.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/drop-down/edit.tpl.html",
    "<div>\n" +
    "    <div class=\"form-group\">\n" +
    "        <i class=\"fa fa-save\" data-ng-click=\"save()\"></i> <input ng-model=\"control.title\"/>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"form-group\" ng-repeat=\"item in control.items\">\n" +
    "        <i class=\"fa fa-plus\" data-ng-click=\"add()\"></i> <i class=\"fa fa-trash\" data-ng-click=\"remove(item)\"></i>\n" +
    "        <input type=\"text\" ng-model=\"item.title\"/>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("questionnaire/types/drop-down/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/drop-down/index.tpl.html",
    "<div>\n" +
    "    <div ng-if=\"!editing\" ng-include=\"'questionnaire/types/drop-down/view.tpl.html'\"></div>\n" +
    "    <div ng-if=\"editing\" ng-include=\"'questionnaire/types/drop-down/edit.tpl.html'\"></div>\n" +
    "</div>");
}]);

angular.module("questionnaire/types/drop-down/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/drop-down/view.tpl.html",
    "<div class=\"form-group\">\n" +
    "    <h3>{{control.title}} <i class=\"fa fa-pencil\" data-ng-click=\"toggle()\"></i></h3>\n" +
    "    <select class=\"cs-select cs-skin-elastic\"\n" +
    "            ng-options=\"item.value as item.title for item in control.getItems()\"\n" +
    "            ng-model=\"control.value\">\n" +
    "    </select>\n" +
    "</div>");
}]);

angular.module("questionnaire/types/fileUpload/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/fileUpload/view.tpl.html",
    "<div class=\"questionnare rankOrder\">\n" +
    "    <div class=\"panel panel-white\">\n" +
    "        <div class=\"panel-heading\">\n" +
    "            <strong ng-bind=\"control.title\"></strong>\n" +
    "        </div>\n" +
    "        <div class=\"panel-body\">\n" +
    "            <div class=\"form-group\">\n" +
    "            	<input type=\"file\">\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaire/types/matrixMultiAnswerPerRow/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/matrixMultiAnswerPerRow/view.tpl.html",
    "<div class=\"questionnare matrixMultiAnswerPerRow\">\n" +
    "    <div class=\"panel panel-white\">\n" +
    "        <div class=\"panel-heading\">\n" +
    "            <strong ng-bind=\"control.title\"></strong>\n" +
    "        </div>\n" +
    "        <div class=\"panel-body\">\n" +
    "            <table class=\"col-xs-12\">\n" +
    "                <tr>\n" +
    "                    <td></td>\n" +
    "                    <td ng-repeat=\"item in control.items\" ng-bind=\"item.title\" class=\"text-center\"></td>\n" +
    "                </tr>\n" +
    "                <tr ng-repeat=\"value in control.values\">\n" +
    "                    <td ng-bind=\"value.title\"></td>\n" +
    "                    <td ng-repeat=\"it in value.items\" class=\"text-center\">\n" +
    "                        <input type=\"checkbox\" ng-model=\"it.value\">\n" +
    "                    </td>\n" +
    "                </tr>\n" +
    "            </table>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaire/types/matrixSingleAnswerPerRow/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/matrixSingleAnswerPerRow/view.tpl.html",
    "<div class=\"questionnare matrixSingleAnswerPerRow\">\n" +
    "    <div class=\"panel panel-white\">\n" +
    "        <div class=\"panel-heading\">\n" +
    "            <strong ng-bind=\"control.title\"></strong>\n" +
    "        </div>\n" +
    "        <div class=\"panel-body\">\n" +
    "            <table class=\"col-xs-12\">\n" +
    "                <tr>\n" +
    "                    <td></td>\n" +
    "                    <td ng-repeat=\"item in control.items\" ng-bind=\"item.title\" class=\"text-center\"></td>\n" +
    "                </tr>\n" +
    "                <tr ng-repeat=\"(key, value) in control.values\">\n" +
    "                    <td ng-bind=\"value.title\"></td>\n" +
    "                    <td ng-repeat=\"it in value.items\" class=\"text-center\">\n" +
    "                        <input type=\"radio\" name=\"{{control.name}}-{{key}}\" ng-model=\"it.value\">\n" +
    "                    </td>\n" +
    "                </tr>\n" +
    "            </table>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaire/types/matrixSpreadsheet/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/matrixSpreadsheet/view.tpl.html",
    "<div class=\"questionnare matrixSpreadsheet\">\n" +
    "    <div class=\"panel panel-white\">\n" +
    "        <div class=\"panel-heading\">\n" +
    "            <strong ng-bind=\"control.title\"></strong>\n" +
    "        </div>\n" +
    "        <div class=\"panel-body\">\n" +
    "            <table class=\"col-xs-12\">\n" +
    "                <tr>\n" +
    "                    <td></td>\n" +
    "                    <td ng-repeat=\"item in control.items\" ng-bind=\"item.title\" class=\"text-center\"></td>\n" +
    "                </tr>\n" +
    "                <tr ng-repeat=\"(key, value) in control.values\">\n" +
    "                    <td ng-bind=\"value.title\"></td>\n" +
    "                    <td ng-repeat=\"it in value.items\" class=\"text-center\">\n" +
    "                        <div class=\"col-xs-12\">\n" +
    "                        	<div class=\"form-group\">\n" +
    "                        	    <input type=\"text\" class=\"form-control\" name=\"{{control.name}}-{{key}}\" ng-model=\"it.value\">\n" +
    "                        	</div>\n" +
    "                        </div>\n" +
    "                    </td>\n" +
    "                </tr>\n" +
    "            </table>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaire/types/multiple-choice/edit.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/multiple-choice/edit.tpl.html",
    "<div>\n" +
    "    <div class=\"form-group\">\n" +
    "        <i class=\"fa fa-save\" data-ng-click=\"save()\"></i> <input ng-model=\"control.title\"/>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"form-group\" ng-repeat=\"item in control.items\">\n" +
    "        <i class=\"fa fa-plus\" data-ng-click=\"add()\"></i> <i class=\"fa fa-trash\" data-ng-click=\"remove(item)\"></i>\n" +
    "        <input type=\"text\" ng-model=\"item.title\"/>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("questionnaire/types/multiple-choice/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/multiple-choice/index.tpl.html",
    "<div>\n" +
    "    <div ng-if=\"!editing\" ng-include=\"'questionnaire/types/multiple-choice/view.tpl.html'\"></div>\n" +
    "    <div ng-if=\"editing\" ng-include=\"'questionnaire/types/multiple-choice/edit.tpl.html'\"></div>\n" +
    "</div>");
}]);

angular.module("questionnaire/types/multiple-choice/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/multiple-choice/view.tpl.html",
    "<div>\n" +
    "    <h3>{{control.title}} <i class=\"fa fa-pencil\" data-ng-click=\"toggle()\"></i></h3>\n" +
    "\n" +
    "    <div class=\"checkbox\" ng-repeat=\"item in control.items\">\n" +
    "        <label>\n" +
    "            <input name=\"{{control.name}}\" type=\"checkbox\" ng-model=\"item.value\" ng-true-value=\"{{item.value}}\"/>\n" +
    "            {{item.title}}\n" +
    "        </label>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("questionnaire/types/numberAllowcationConstantSum/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/numberAllowcationConstantSum/view.tpl.html",
    "<div class=\"questionnare numberAllowcationConstantSum\">\n" +
    "    <div class=\"panel panel-white\">\n" +
    "        <div class=\"panel-heading\">\n" +
    "            <strong ng-bind=\"control.title\"></strong>\n" +
    "        </div>\n" +
    "        <div class=\"panel-body\">\n" +
    "            <table class=\"col-xs-12\">\n" +
    "                <tr ng-repeat=\"item in control.items\">\n" +
    "                    <td ng-bind=\"item.title\"></td>\n" +
    "                    <td>\n" +
    "                        <div class=\"form-group\">\n" +
    "                            <input type=\"number\" class=\"form-control\" ng-model=\"item.value\" ng-change=\"change(item)\">\n" +
    "                        </div>\n" +
    "                    </td>\n" +
    "                </tr>\n" +
    "                <tr>\n" +
    "                    <td>\n" +
    "                        <strong>Total</strong>\n" +
    "                    </td>\n" +
    "                    <td ng-bind=\"control.total\"></td>\n" +
    "                </tr>\n" +
    "            </table>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaire/types/openEndedTextCommentBox/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/openEndedTextCommentBox/view.tpl.html",
    "<div class=\"questionnare numberAllowcationConstantSum\">\n" +
    "    <div class=\"panel panel-white\">\n" +
    "        <div class=\"panel-heading\">\n" +
    "            <strong ng-bind=\"control.title\"></strong>\n" +
    "        </div>\n" +
    "        <div class=\"panel-body\">\n" +
    "            <textarea class=\"form-control\" ng-model=\"control.value\"></textarea>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaire/types/openEndedTextDateTime/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/openEndedTextDateTime/view.tpl.html",
    "<div class=\"questionnare openEndedTextDateTime\">\n" +
    "    <div class=\"panel panel-white\">\n" +
    "        <div class=\"panel-heading\">\n" +
    "            <strong ng-bind=\"control.title\"></strong>\n" +
    "        </div>\n" +
    "        <div class=\"panel-body\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label for=\"form-field-mask-1\">\n" +
    "                    Date <small class=\"text-success\">DD/MM/YYYY</small>\n" +
    "                </label>\n" +
    "                <div class=\"input-group\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"masked\" placeholder=\"DD/MM/YYYY\" ui-mask=\"99/99/9999\" ng-model=\"control.value\">\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaire/types/openEndedTextOneLine/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/openEndedTextOneLine/view.tpl.html",
    "<div class=\"questionnare openEndedTextOneLine\">\n" +
    "    <div class=\"panel panel-white\">\n" +
    "        <div class=\"panel-heading\">\n" +
    "            <strong ng-bind=\"control.title\"></strong>\n" +
    "        </div>\n" +
    "        <div class=\"panel-body\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <input type=\"text\" class=\"form-control\" ng-model=\"control.value\">\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaire/types/rankOrder/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/rankOrder/view.tpl.html",
    "<div class=\"questionnare rankOrder\">\n" +
    "    <div class=\"panel panel-white\">\n" +
    "        <div class=\"panel-heading\">\n" +
    "            <strong ng-bind=\"control.title\"></strong>\n" +
    "        </div>\n" +
    "        <div class=\"panel-body\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <p ng-bind=\"control.description\"></p>\n" +
    "            </div>\n" +
    "            <table class=\"col-xs-12\">\n" +
    "                <tr ng-repeat=\"item in control.items\">\n" +
    "                    <td ng-bind=\"item.title\"></td>\n" +
    "                    <td>\n" +
    "                        <div class=\"form-group\">\n" +
    "                            <input type=\"number\" class=\"form-control\" ng-model=\"item.value\" ng-change=\"change(item)\">\n" +
    "                        </div>\n" +
    "                    </td>\n" +
    "                </tr>\n" +
    "            </table>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaire/types/single-choice/edit.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/single-choice/edit.tpl.html",
    "<div>\n" +
    "    <div class=\"form-group\">\n" +
    "        <i class=\"fa fa-save\" data-ng-click=\"save()\"></i> <input ng-model=\"control.title\"/>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"form-group\" ng-repeat=\"item in control.items\">\n" +
    "        <i class=\"fa fa-plus\" data-ng-click=\"add()\"></i> <i class=\"fa fa-trash\" data-ng-click=\"remove(item)\"></i>\n" +
    "        <input type=\"text\" ng-model=\"item.title\"/>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("questionnaire/types/single-choice/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/single-choice/index.tpl.html",
    "<div>\n" +
    "    <div ng-if=\"!editing\" ng-include=\"'questionnaire/types/single-choice/view.tpl.html'\"></div>\n" +
    "    <div ng-if=\"editing\" ng-include=\"'questionnaire/types/single-choice/edit.tpl.html'\"></div>\n" +
    "</div>");
}]);

angular.module("questionnaire/types/single-choice/view.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaire/types/single-choice/view.tpl.html",
    "<div>\n" +
    "    <h3>{{control.title}} <i class=\"fa fa-pencil\" data-ng-click=\"toggle()\"></i></h3>\n" +
    "    <div class=\"radio\" ng-repeat=\"item in control.items\">\n" +
    "        <label>\n" +
    "            <input name=\"{{control.name}}\" type=\"radio\" ng-model=\"item.value\" ng-value=\"{{item.value}}\"/> {{item.title}}\n" +
    "        </label>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("settings/add-user.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("settings/add-user.tpl.html",
    "<form class=\"form-horizontal form-validation\" name=\"form\" novalidate>\n" +
    "	<div class=\"ngdialog-message\">\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Email\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<input type=\"email\" required=\"\" ng-model=\"model.email\" class=\"form-control\" placeholder=\"Email\" />\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				First Name\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<input type=\"text\" required=\"\" ng-model=\"model.first_name\" class=\"form-control\" placeholder=\"First Name\" />\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Last Name\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<input type=\"text\" required=\"\" ng-model=\"model.last_name\" class=\"form-control\" placeholder=\"Last Name\" />\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Phone\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<input type=\"text\" required=\"\" ng-model=\"model.phone\" class=\"form-control\" placeholder=\"Phone\" />\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Position\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<select class=\"form-control\" ng-model=\"model.role\" ng-options=\"role as role for role in roles\"></select>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Password\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<input type=\"password\" required=\"\" ng-model=\"model.password\" class=\"form-control\" placeholder=\"Password\" />\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Repeat Password\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<input type=\"password\" required=\"\" ng-model=\"model.password_confirmation\" class=\"form-control\" placeholder=\"Repeat Password\" />\n" +
    "			</div>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "	<div class=\"ngdialog-buttons\">\n" +
    "	    <button type=\"button\" class=\"ngdialog-button ngdialog-button-secondary\" ng-click=\"closeThisDialog('cancel')\">Cancel</button>\n" +
    "	    <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"confirm(model)\" ng-disabled=\"!form.$valid\">Add</button>\n" +
    "	</div>\n" +
    "</form>");
}]);

angular.module("settings/company.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("settings/company.tpl.html",
    "<div class=\"container-fluid container-fullw\">\n" +
    "	<div class=\"row\">\n" +
    "		<div class=\"col-md-12\">\n" +
    "			<h1 class=\"mainTitle text-primary\">Company</h1>\n" +
    "			<div class=\"row\">\n" +
    "				<div class=\"col-md-7\">\n" +
    "					<div class=\"title-heading\">\n" +
    "						<h4 class=\"title text-primary\">Company Information</h4>\n" +
    "					</div>\n" +
    "					<form>\n" +
    "						<div class=\"form-group\">\n" +
    "							<input type=\"text\" placeholder=\"Company Name\" ng-model=\"vm.company.name\" class=\"form-control\">\n" +
    "						</div>\n" +
    "						<div class=\"form-group\">\n" +
    "							<input type=\"text\" placeholder=\"Primary Contact\" ng-model=\"vm.company.primary_contact\" class=\"form-control\">\n" +
    "						</div>\n" +
    "						<div class=\"form-group\">\n" +
    "							<input type=\"text\" placeholder=\"Phone Number\" ng-model=\"vm.company.phone\" class=\"form-control\">\n" +
    "						</div>\n" +
    "						<div class=\"form-group\">\n" +
    "							<input type=\"text\" placeholder=\"Email\" ng-model=\"vm.company.email\" class=\"form-control\">\n" +
    "						</div>\n" +
    "						<div class=\"form-group\">\n" +
    "							<textarea rows=\"5\" placeholder=\"Add comment...\" class=\"form-control\"></textarea>\n" +
    "						</div>\n" +
    "					</form>\n" +
    "				</div>\n" +
    "				<div class=\"col-md-5\">\n" +
    "					<div class=\"title-heading\">\n" +
    "						<h4 class=\"title text-primary\">Employees</h4>\n" +
    "						<button ng-click=\"vm.addUser()\" class=\"btn btn-primary btn-o\" type=\"button\"><i class=\"fa fa-user-plus\"></i> User</button>\n" +
    "					</div>\n" +
    "					<table class=\"table table-striped\">\n" +
    "						<thead>\n" +
    "							<tr>\n" +
    "								<th>#</th>\n" +
    "								<th>Name</th>\n" +
    "								<th>Position</th>\n" +
    "							</tr>\n" +
    "						</thead>\n" +
    "						<tbody>\n" +
    "							<tr ng-repeat=\"employee in vm.employees\">\n" +
    "								<td ng-bind=\"$index + 1\"></td>\n" +
    "								<td><span ng-bind=\"employee.first_name\"></span> <span ng-bind=\"employee.last_name\"></span></td>\n" +
    "								<td ng-bind=\"employee.role_name\"></td>\n" +
    "							</tr>\n" +
    "						</tbody>\n" +
    "					</table>\n" +
    "					<div class=\"text-right\">\n" +
    "						<pagination rotate=\"false\" boundary-links=\"true\" max-size=\"5\" items-per-page=\"vm.paginator.per_page\" total-items=\"vm.paginator.total\" ng-model=\"vm.paginator.current_page\" ng-change=\"vm.getList()\"></pagination>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "			<div class=\"form-group text-center margin-top-30\">\n" +
    "				<button class=\"btn btn-o btn-primary\" type=\"button\" ng-click=\"vm.save(vm.company)\"><i class=\"fa fa-save\"></i> Save</button>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("settings/invoices.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("settings/invoices.tpl.html",
    "<!-- start: PAGE TITLE -->\n" +
    "<section id=\"page-title\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-sm-8\">\n" +
    "            <h1 class=\"mainTitle\" translate=\"sidebar.nav.pages.INVOICE\">{{ mainTitle }}</h1>\n" +
    "            <span class=\"mainDescription\">Beautifully simple invoicing and payments. Give clients attractive invoices, estimates, and receipts.</span>\n" +
    "        </div>\n" +
    "        <div ncy-breadcrumb></div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "<!-- end: PAGE TITLE -->\n" +
    "<!-- start: INVOICE -->\n" +
    "<div class=\"container-fluid container-fullw bg-white\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-12\">\n" +
    "            <div class=\"invoice\">\n" +
    "                <div class=\"row invoice-logo\">\n" +
    "                    <div class=\"col-sm-6\">\n" +
    "                        <img alt=\"\" src=\"http://cdn.project-disco.org/wp-content/uploads/2013/10/DEMO-logo.png\">\n" +
    "                    </div>\n" +
    "                    <div class=\"col-sm-6\">\n" +
    "                        <p class=\"text-dark\">\n" +
    "                            #1233219 / 01 Jan 2014 <small class=\"text-light\"> Lorem ipsum dolor sit amet </small>\n" +
    "                        </p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <hr>\n" +
    "                <div class=\"row\">\n" +
    "                    <div class=\"col-sm-4\">\n" +
    "                        <h4>Client:</h4>\n" +
    "                        <div class=\"well\">\n" +
    "                            <address>\n" +
    "                                <strong class=\"text-dark\">Customer Company, Inc.</strong>\n" +
    "                                <br> 1 Infinite Loop\n" +
    "                                <br> Cupertino, CA 95014\n" +
    "                                <br>\n" +
    "                                <abbr title=\"Phone\">P:</abbr> (123) 456-7890\n" +
    "                            </address>\n" +
    "                            <address>\n" +
    "                                <strong class=\"text-dark\">E-mail:</strong>\n" +
    "                                <a href=\"mailto:#\">\n" +
    "                                    info@customer.com\n" +
    "                                </a>\n" +
    "                            </address>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-sm-4\">\n" +
    "                        <h4>We appreciate your business.</h4>\n" +
    "                        <div class=\"padding-bottom-30 padding-top-10 text-dark\">\n" +
    "                            Thanks for being a customer. A detailed summary of your invoice is below.\n" +
    "                            <br> If you have questions, we're happy to help.\n" +
    "                            <br> Email support@cliptheme.com or contact us through other support channels.\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-sm-4 pull-right\">\n" +
    "                        <h4>Payment Details:</h4>\n" +
    "                        <ul class=\"list-unstyled invoice-details padding-bottom-30 padding-top-10 text-dark\">\n" +
    "                            <li>\n" +
    "                                <strong>V.A.T Reg #:</strong> 233243444\n" +
    "                            </li>\n" +
    "                            <li>\n" +
    "                                <strong>Account Name:</strong> Company Ltd\n" +
    "                            </li>\n" +
    "                            <li>\n" +
    "                                <strong>SWIFT code:</strong> 1233F4343ABCDEW\n" +
    "                            </li>\n" +
    "                            <li>\n" +
    "                                <strong>DATE:</strong> 01/01/2014\n" +
    "                            </li>\n" +
    "                            <li>\n" +
    "                                <strong>DUE:</strong> 11/02/2014\n" +
    "                            </li>\n" +
    "                        </ul>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"row\">\n" +
    "                    <div class=\"col-sm-12\">\n" +
    "                        <table class=\"table table-striped\">\n" +
    "                            <thead>\n" +
    "                                <tr>\n" +
    "                                    <th> # </th>\n" +
    "                                    <th> Title </th>\n" +
    "                                    <th class=\"hidden-480\"> Date </th>\n" +
    "                                    <th class=\"hidden-480\">\n" +
    "                                        <div class=\"dropdown\">\n" +
    "                                            <span type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">Status <span class=\"caret\"></span></span>\n" +
    "                                            <ul class=\"dropdown-menu\" aria-labelledby=\"dropdownMenu1\">\n" +
    "                                                <li ng-repeat=\"st in validInvoiceStatus\" ng-click=\"changeStatusFilter(st)\">\n" +
    "                                                    <a ng-bind=\"st\"></a>\n" +
    "                                                </li>\n" +
    "                                            </ul>\n" +
    "                                        </div>\n" +
    "                                    </th>\n" +
    "                                    <th class=\"hidden-480\"> Amount </th>\n" +
    "                                </tr>\n" +
    "                            </thead>\n" +
    "                            <tbody>\n" +
    "                                <tr ng-repeat=\"invoice in invoices\">\n" +
    "                                    <td ng-bind=\"$index + 1\"></td>\n" +
    "                                    <td ng-bind=\"invoice.name\"></td>\n" +
    "                                    <td class=\"hidden-480\" ng-bind=\"invoice.getDateTime()\"></td>\n" +
    "                                    <td class=\"hidden-480\" ng-bind=\"invoice.status\"></td>\n" +
    "                                    <td><span>$</span><span ng-bind=\"invoice.amount\"></span></td>\n" +
    "                                </tr>\n" +
    "                            </tbody>\n" +
    "                        </table>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"row\" ng-show=\"showpagination\">\n" +
    "                    <ul class=\"pagination pull-right\">\n" +
    "                        <li>\n" +
    "                            <a href=\"#\" aria-label=\"Previous\">\n" +
    "                                <span aria-hidden=\"true\">&laquo;</span>\n" +
    "                            </a>\n" +
    "                        </li>\n" +
    "                        <li ng-repeat=\"p in pages\" ng-class=\"page == p ? 'active' : ''\" ng-click=\"setPage(p)\"><a ng-bind=\"p\"></a></li>\n" +
    "                        <li>\n" +
    "                            <a href=\"#\" aria-label=\"Next\">\n" +
    "                                <span aria-hidden=\"true\">&raquo;</span>\n" +
    "                            </a>\n" +
    "                        </li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "                <div class=\"row\">\n" +
    "                    <div class=\"col-sm-12 invoice-block\">\n" +
    "                        <ul class=\"list-unstyled amounts text-small\">\n" +
    "                            <li>\n" +
    "                                <strong>Sub-Total:</strong> <span ng-bind=\"invoices | SubTotalFilter:status\"></span>\n" +
    "                            </li>\n" +
    "                            <li>\n" +
    "                                <strong>Discount:</strong> <span ng-bind=\"discount\"></span><span>%</span>\n" +
    "                            </li>\n" +
    "                            <li>\n" +
    "                                <strong>VAT:</strong> <span ng-bind=\"vat\"></span><span>%</span>\n" +
    "                            </li>\n" +
    "                            <li class=\"text-extra-large text-dark margin-top-15\">\n" +
    "                                <strong>Total:</strong>\n" +
    "                                <span$</span><span ng-bind=\"invoices | TotalFilter:status:discount:vat\"></span>\n" +
    "                            </li>\n" +
    "                        </ul>\n" +
    "                        <br>\n" +
    "                        <a onclick=\"javascript:window.print();\" class=\"btn btn-lg btn-primary hidden-print\">\n" +
    "                            Print <i class=\"fa fa-print\"></i>\n" +
    "                        </a>\n" +
    "                        <a class=\"btn btn-lg btn-primary btn-o hidden-print\">\n" +
    "                            Submit Your Invoice <i class=\"fa fa-check\"></i>\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<!-- end: INVOICE -->\n" +
    "");
}]);

angular.module("users/add/add.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("users/add/add.tpl.html",
    "<form class=\"form-horizontal form-validation\" name=\"form\" novalidate>\n" +
    "	<div class=\"ngdialog-message\">\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Email\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<input type=\"email\" required=\"\" ng-model=\"model.email\" class=\"form-control\" placeholder=\"Email\" />\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				First Name\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<input type=\"text\" required=\"\" ng-model=\"model.first_name\" class=\"form-control\" placeholder=\"First Name\" />\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Last Name\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<input type=\"text\" required=\"\" ng-model=\"model.last_name\" class=\"form-control\" placeholder=\"Last Name\" />\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Phone\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<input type=\"text\" required=\"\" ng-model=\"model.phone\" class=\"form-control\" placeholder=\"Phone\" />\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Position\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<select class=\"form-control\" ng-model=\"model.role\" ng-options=\"role as role for role in roles\"></select>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Password\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<input type=\"password\" required=\"\" ng-model=\"model.password\" class=\"form-control\" placeholder=\"Password\" />\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class=\"form-group\">\n" +
    "			<label class=\"control-label col-md-3\">\n" +
    "				Repeat Password\n" +
    "			</label>\n" +
    "			<div class=\"col-md-9\">\n" +
    "				<input type=\"password\" required=\"\" ng-model=\"model.password_confirmation\" class=\"form-control\" placeholder=\"Repeat Password\" />\n" +
    "			</div>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "	<div class=\"ngdialog-buttons\">\n" +
    "	    <button type=\"button\" class=\"ngdialog-button ngdialog-button-secondary\" ng-click=\"closeThisDialog('cancel')\">Cancel</button>\n" +
    "	    <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"confirm(model)\" ng-disabled=\"!form.$valid\">Add</button>\n" +
    "	</div>\n" +
    "</form>");
}]);

angular.module("users/list.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("users/list.tpl.html",
    "<div class=\"container-fluid container-fullw\">\n" +
    "	<div class=\"row\">\n" +
    "		<div class=\"col-md-12\">\n" +
    "			<h1 class=\"mainTitle text-primary\">USER MANAGEMENT</h1>\n" +
    "			<form class=\"form-inline pull-left\">\n" +
    "				<div class=\"form-group\">\n" +
    "					<div class=\"checkbox clip-check check-primary\">\n" +
    "						<input ng-model=\"vm.allRows\" type=\"checkbox\" id=\"checkbox-all\" ng-change=\"vm.toggleRows()\" />\n" +
    "						<label for=\"checkbox-all\">All rows</label>\n" +
    "					</div>\n" +
    "				</div>\n" +
    "				<div class=\"form-group\">\n" +
    "					<select class=\"cs-select cs-skin-elastic\">\n" +
    "						<option value=\"Action\" selected>Action</option>\n" +
    "						<option value=\"Action 1\">Action 1</option>\n" +
    "						<option value=\"Action 2\">Action 2</option>\n" +
    "						<option value=\"Action 3\">Action 3</option>\n" +
    "						<option value=\"Action 4\">Action 4</option>\n" +
    "					</select>\n" +
    "				</div>\n" +
    "				<div class=\"form-group\">\n" +
    "					<button class=\"btn btn-primary btn-o\">Apply</button>\n" +
    "				</div>\n" +
    "			</form>\n" +
    "			<form class=\"form-inline pull-right\">\n" +
    "				<div class=\"form-group\">\n" +
    "					<button ngif=\"loggedInUser.role_name == 'admin' || loggedInUser.role_name == 'company'\" ng-click=\"vm.addUser()\" class=\"btn btn-primary btn-o ng-click-active\" type=\"button\"><i class=\"fa fa-user-plus\"></i> User</button>\n" +
    "					<input ng-keypress=\"vm.keypress($event)\" type=\"text\" ng-model=\"vm.keyword\" class=\"form-control\" placeholder=\"Search...\" />\n" +
    "					<button ng-click=\"vm.getList()\" type=\"button\" class=\"btn btn-primary btn-o\">Search</button>\n" +
    "				</div>\n" +
    "			</form>\n" +
    "			<table class=\"table table-striped table-user\">\n" +
    "				<thead>\n" +
    "					<tr>\n" +
    "						<th>&nbsp;</th>\n" +
    "						<th>Name</th>\n" +
    "						<th>Phone</th>\n" +
    "						<th>Email</th>\n" +
    "						<th>Access</th>\n" +
    "						<th>Action</th>\n" +
    "					</tr>\n" +
    "				</thead>\n" +
    "				<tbody>\n" +
    "					<tr ng-repeat=\"item in vm.items\">\n" +
    "						<td>\n" +
    "							<div class=\"checkbox clip-check check-primary\">\n" +
    "								<input type=\"checkbox\" id=\"checkbox_{{item.getId()}}\" ng-model=\"item.checked\" />\n" +
    "								<label for=\"checkbox_{{item.getId()}}\"></label>\n" +
    "							</div>\n" +
    "						</td>\n" +
    "						<td>{{item.getFullName()}}</td>\n" +
    "						<td>{{item.phone}}</td>\n" +
    "						<td>{{item.email}}</td>\n" +
    "						<td>{{item.role_name}}</td>\n" +
    "						<td>\n" +
    "							<a href=\"#\" class=\"btn btn-transparent\"><i class=\"fa fa-edit\"></i></a>\n" +
    "							<a href=\"#\" class=\"btn btn-transparent btn-xs\"><i class=\"fa fa-trash-o\"></i></a>\n" +
    "						</td>\n" +
    "					</tr>\n" +
    "					<tr ng-if=\"vm.items.length == 0\">\n" +
    "						<td colspan=\"6\">No record found!</td>\n" +
    "					</tr>\n" +
    "				</tbody>\n" +
    "			</table>\n" +
    "			<div class=\"text-right\">\n" +
    "				<pagination rotate=\"false\" boundary-links=\"true\" max-size=\"5\" items-per-page=\"vm.paginator.per_page\" total-items=\"vm.paginator.total\" ng-model=\"vm.paginator.current_page\" ng-change=\"vm.getList()\"></pagination>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>");
}]);
