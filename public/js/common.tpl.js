angular.module('fox.common.templates', ['common/alert-dialog.tpl.html', 'common/completed-job.tpl.html', 'common/confirm-dialog.tpl.html', 'common/index.tpl.html', 'common/main-content.tpl.html', 'common/nav.tpl.html', 'common/off-sidebar.tpl.html', 'common/pdf-viewer.tpl.html', 'common/pdf.tpl.html', 'common/posting-job.tpl.html', 'common/sidebar.tpl.html', 'common/top-navbar.tpl.html']);

angular.module("common/alert-dialog.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("common/alert-dialog.tpl.html",
    "<div class=\"ngdialog-message\">\n" +
    "    <h3 ng-if=\"title\">{{title}}</h3>\n" +
    "    <div class=\"text-center\" ng-bind-html=\"message\"></div>\n" +
    "</div>\n" +
    "<div class=\"ngdialog-buttons text-right\">\n" +
    "    <button type=\"button\" class=\"btn btn-primary btn-o\" ng-click=\"closeThisDialog('no')\">Close</button>\n" +
    "</div>");
}]);

angular.module("common/completed-job.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("common/completed-job.tpl.html",
    "<div class=\"ngdialog-message\">\n" +
    "    <h3 ng-if=\"title\">{{title}}</h3>\n" +
    "    <div class=\"text-center\">\n" +
    "		POSTING COMPLETED\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"ngdialog-buttons text-right\">\n" +
    "    <button type=\"button\" class=\"btn btn-primary btn-o\" ng-click=\"closeThisDialog('no')\">Close</button>\n" +
    "</div>");
}]);

angular.module("common/confirm-dialog.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("common/confirm-dialog.tpl.html",
    "<div class=\"ngdialog-message\">\n" +
    "    <h3>{{title}}</h3>\n" +
    "    <div class=\"text-center\" ng-bind-html=\"message\"></div>\n" +
    "</div>\n" +
    "<div class=\"ngdialog-buttons text-right\">\n" +
    "    <button type=\"button\" class=\"btn btn-primary btn-o\" ng-click=\"confirm('yes')\">Yes</button>\n" +
    "    <button type=\"button\" class=\"btn\" ng-click=\"closeThisDialog('no')\">No</button>\n" +
    "</div>");
}]);

angular.module("common/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("common/index.tpl.html",
    "<!-- sidebar -->\n" +
    "<div class=\"sidebar app-aside\" id=\"sidebar\" toggleable parent-active-class=\"app-slide-off\" >\n" +
    "    <div perfect-scrollbar wheel-propagation=\"false\" suppress-scroll-x=\"true\" class=\"sidebar-container\">\n" +
    "        <div data-ng-include=\"'common/sidebar.tpl.html'\"></div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<!-- / sidebar -->\n" +
    "<div class=\"app-content\">\n" +
    "    <header data-ng-include=\"'common/top-navbar.tpl.html'\" class=\"navbar navbar-default navbar-static-top\"></header>\n" +
    "    <!-- main content -->\n" +
    "    <div data-ng-include=\"'common/main-content.tpl.html'\" class=\"main-content\" ></div>\n" +
    "    <!-- / main content -->\n" +
    "</div>\n" +
    "\n" +
    "<div data-ng-include=\" 'common/off-sidebar.tpl.html' \" id=\"off-sidebar\" class=\"sidebar\" toggleable parent-active-class=\"app-offsidebar-open\"></div>");
}]);

angular.module("common/main-content.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("common/main-content.tpl.html",
    "<!-- start: MAIN CONTENT -->\n" +
    "<div ui-view class=\"wrap-content container fade-in-up\" id=\"container\"></div>\n" +
    "<!-- end: MAIN CONTENT -->");
}]);

angular.module("common/nav.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("common/nav.tpl.html",
    "<!-- start: SEARCH FORM -->\n" +
    "<div class=\"search-form\">\n" +
    "    <a class=\"s-open\" href=\"#\">\n" +
    "        <i class=\"ti-search\"></i>\n" +
    "    </a>\n" +
    "    <form class=\"navbar-form\" role=\"search\">\n" +
    "        <a class=\"s-remove\" href=\"#\" ng-toggle-class=\"search-open\" target=\".navbar-form\">\n" +
    "            <i class=\"ti-close\"></i>\n" +
    "        </a>\n" +
    "        <div class=\"form-group\">\n" +
    "            <input type=\"text\" class=\"form-control\" placeholder=\"Search...\">\n" +
    "            <button class=\"btn search-button\" type=\"button\">\n" +
    "                <i class=\"ti-search\"></i>\n" +
    "            </button>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "</div>\n" +
    "<!-- end: SEARCH FORM -->\n" +
    "<!-- start: MAIN NAVIGATION MENU -->\n" +
    "<ul class=\"main-navigation-menu\">\n" +
    "    <li ng-class=\"{'active':$state.includes('fox.dashboard')}\">\n" +
    "        <a ui-sref=\"fox.dashboard\">\n" +
    "            <div class=\"item-content\">\n" +
    "                <div class=\"item-media\">\n" +
    "                    <i class=\"ti-dashboard\"></i>\n" +
    "                </div>\n" +
    "                <div class=\"item-inner\">\n" +
    "                    <span class=\"title\">Dashboard</span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </a>\n" +
    "    </li>\n" +
    "    <li ng-class=\"{'active':$state.includes('fox.admindashboard')}\">\n" +
    "        <a ui-sref=\"fox.admindashboard\">\n" +
    "            <div class=\"item-content\">\n" +
    "                <div class=\"item-media\">\n" +
    "                    <i class=\"ti-dashboard\"></i>\n" +
    "                </div>\n" +
    "                <div class=\"item-inner\">\n" +
    "                    <span class=\"title\">Admin Dashboard</span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </a>\n" +
    "    </li>\n" +
    "    <li ng-class=\"{'active':$state.includes('fox.candidates')}\">\n" +
    "        <a ui-sref=\"fox.candidates\">\n" +
    "            <div class=\"item-content\">\n" +
    "                <div class=\"item-media\">\n" +
    "                    <i class=\"ti-user\"></i>\n" +
    "                </div>\n" +
    "                <div class=\"item-inner\">\n" +
    "                    <span class=\"title\">Candidates</span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </a>\n" +
    "    </li>\n" +
    "    <!--<li ng-class=\"{'active':$state.includes('fox.resumes')}\">-->\n" +
    "    <!--<a ui-sref=\"fox.resumes\">-->\n" +
    "    <!--<div class=\"item-content\">-->\n" +
    "    <!--<div class=\"item-media\">-->\n" +
    "    <!--<i class=\"ti-credit-card\"></i>-->\n" +
    "    <!--</div>-->\n" +
    "    <!--<div class=\"item-inner\">-->\n" +
    "    <!--<span class=\"title\">Resumes</span>-->\n" +
    "    <!--</div>-->\n" +
    "    <!--</div>-->\n" +
    "    <!--</a>-->\n" +
    "    <!--</li>-->\n" +
    "    <li ng-class=\"{'active':$state.includes('fox.my-jobs')}\">\n" +
    "        <a ng-click=\"GoToMyJobsPage()\">\n" +
    "            <div class=\"item-content\">\n" +
    "                <div class=\"item-media\">\n" +
    "                    <i class=\"ti-view-list-alt\"></i>\n" +
    "                </div>\n" +
    "                <div class=\"item-inner\">\n" +
    "                    <span class=\"title\">My Jobs</span>\n" +
    "                    <i class=\"icon-arrow\"></i>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </a>\n" +
    "        <ul class=\"sub-menu\">\n" +
    "            <li ng-class=\"MyJobsListStatus == 0 ? 'active' : ''\">\n" +
    "                <a ui-sref=\"fox.my-jobs.list({status: '0'})\">\n" +
    "                    <span class=\"title\">Draft ({{jobsCount['status-0']}})</span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "            <li ng-class=\"MyJobsListStatus == 1 ? 'active' : ''\">\n" +
    "                <a ui-sref=\"fox.my-jobs.list({status: '1'})\">\n" +
    "                    <span class=\"title\">Active ({{jobsCount['status-1']}})</span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "            <li ui-sref-active=\"active\">\n" +
    "                <a ui-sref=\"fox.jobs-create.create\">\n" +
    "                    <span class=\"title\">Post Job</span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </li>\n" +
    "    <li ng-class=\"{'active':$state.includes('fox.accounting')}\">\n" +
    "        <a href=\"#\">\n" +
    "            <div class=\"item-content\">\n" +
    "                <div class=\"item-media\">\n" +
    "                    <i class=\"ti-book\"></i>\n" +
    "                </div>\n" +
    "                <div class=\"item-inner\">\n" +
    "                    <span class=\"title\">Accounting</span>\n" +
    "                    <i class=\"icon-arrow\"></i>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </a>\n" +
    "        <ul class=\"sub-menu\">\n" +
    "            <li ui-sref-active=\"active\">\n" +
    "                <a ui-sref=\"fox.accounting.accounting\">\n" +
    "                    <span class=\"title\">Accounting</span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "            <li ui-sref-active=\"active\">\n" +
    "                <a ui-sref=\"fox.accounting.revenue\">\n" +
    "                    <span class=\"title\">Revenue</span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "            <li ui-sref-active=\"active\">\n" +
    "                <a ui-sref=\"fox.settings.invoices\">\n" +
    "                    <span class=\"title\">Invoices</span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "            <li ui-sref-active=\"active\">\n" +
    "                <a ui-sref=\"fox.accounting.payment-details\">\n" +
    "                    <span class=\"title\">Payment Details</span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "            <li ui-sref-active=\"active\">\n" +
    "                <a ui-sref=\"fox.accounting.create-invoices\">\n" +
    "                    <span class=\"title\">Create Invoices</span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "            <li ui-sref-active=\"active\">\n" +
    "                <a ui-sref=\"fox.accounting.subscription\">\n" +
    "                    <span class=\"title\">Subscription</span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </li>\n" +
    "    <li ng-class=\"{'active open':$state.includes('fox.myaccount')}\">\n" +
    "        <a href=\"javascript:void(0)\">\n" +
    "            <div class=\"item-content\">\n" +
    "                <div class=\"item-media\">\n" +
    "                    <i class=\"fa fa-folder-open-o\"></i>\n" +
    "                </div>\n" +
    "                <div class=\"item-inner\">\n" +
    "                    <span class=\"title\">My Account</span><i class=\"icon-arrow\"></i>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </a>\n" +
    "        <ul class=\"sub-menu\">\n" +
    "            <li ui-sref-active=\"active\">\n" +
    "                <a ui-sref=\"fox.myaccount.messages\">\n" +
    "                    <span class=\"title\">Messages</span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "            <li ui-sref-active=\"active\">\n" +
    "                <a ui-sref=\"fox.myaccount.interviews\">\n" +
    "                    <span class=\"title\">Interviews</span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "\n" +
    "    </li>\n" +
    "    <li ng-class=\"{'active open':$state.includes('fox.settings')}\">\n" +
    "        <a href=\"javascript:void(0)\">\n" +
    "            <div class=\"item-content\">\n" +
    "                <div class=\"item-media\">\n" +
    "                    <i class=\"fa fa-folder-open-o\"></i>\n" +
    "                </div>\n" +
    "                <div class=\"item-inner\">\n" +
    "                    <span class=\"title\">Settings</span><i class=\"icon-arrow\"></i>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </a>\n" +
    "        <ul class=\"sub-menu\">\n" +
    "            <li ui-sref-active=\"active\">\n" +
    "                <a ui-sref=\"fox.settings.users\">\n" +
    "                    <span class=\"title\">Users</span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "            <li ui-sref-active=\"active\">\n" +
    "                <a ui-sref=\"fox.settings.company\">\n" +
    "                    <span class=\"title\">Company</span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </li>\n" +
    "    <li ng-class=\"{'active open':$state.includes('fox.placement-record')}\">\n" +
    "        <a ui-sref=\"fox.placement-record\">\n" +
    "            <div class=\"item-content\">\n" +
    "                <div class=\"item-media\">\n" +
    "                    <i class=\"ti-notepad\"></i>\n" +
    "                </div>\n" +
    "                <div class=\"item-inner\">\n" +
    "                    <span class=\"title\">Placement Record</span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </a>\n" +
    "    </li>\n" +
    "</ul>\n" +
    "<!-- end: MAIN NAVIGATION MENU\n" +
    "");
}]);

angular.module("common/off-sidebar.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("common/off-sidebar.tpl.html",
    "<!-- start: OFF RIGHT SIDEBAR TABSET -->\n" +
    "<tabset class=\"sidebar-wrapper\" justified=\"true\">\n" +
    "	<!-- start: TAB USER -->\n" +
    "	<tab>\n" +
    "		<tab-heading>\n" +
    "			<i class=\"ti-comments\"></i>\n" +
    "		</tab-heading>\n" +
    "		<!-- /// controller:  'ChatCtrl' -  localtion: assets/js/controllers/chatCtrl.js /// -->\n" +
    "		<div id=\"users\" toggleable active-class=\"chat-open\" ng-controller=\"ChatCtrl\">\n" +
    "			<div class=\"users-list\">\n" +
    "				<div class=\"sidebar-content\" perfect-scrollbar wheel-propagation=\"false\" suppress-scroll-x=\"true\">\n" +
    "					<h5 class=\"sidebar-title\">On-line</h5>\n" +
    "					<ul class=\"media-list\">\n" +
    "						<li class=\"media\">\n" +
    "							<a ct-toggle=\"on\" target=\"users\" href ng-click=\"setOtherId(50223457)\">\n" +
    "								<i class=\"fa fa-circle status-online\"></i>\n" +
    "								<img alt=\"...\" src=\"img/avatar-1.jpg\" class=\"media-object\">\n" +
    "								<div class=\"media-body\">\n" +
    "									<h4 class=\"media-heading\">Nicole Bell</h4>\n" +
    "									<span> Content Designer </span>\n" +
    "								</div>\n" +
    "							</a>\n" +
    "						</li>\n" +
    "						<li class=\"media\">\n" +
    "							<a ct-toggle=\"on\" target=\"users\" href ng-click=\"setOtherId(50223458)\">\n" +
    "								<div class=\"user-label\">\n" +
    "									<span class=\"label label-success\">3</span>\n" +
    "								</div>\n" +
    "								<i class=\"fa fa-circle status-online\"></i>\n" +
    "								<img alt=\"...\" src=\"img/avatar-1.jpg\" class=\"media-object\">\n" +
    "								<div class=\"media-body\">\n" +
    "									<h4 class=\"media-heading\">Steven Thompson</h4>\n" +
    "									<span> Visual Designer </span>\n" +
    "								</div>\n" +
    "							</a>\n" +
    "						</li>\n" +
    "						<li class=\"media\">\n" +
    "							<a ct-toggle=\"on\" target=\"users\" href ng-click=\"setOtherId(50223459)\">\n" +
    "								<i class=\"fa fa-circle status-online\"></i>\n" +
    "								<img alt=\"...\" src=\"img/avatar-1.jpg\" class=\"media-object\">\n" +
    "								<div class=\"media-body\">\n" +
    "									<h4 class=\"media-heading\">Ella Patterson</h4>\n" +
    "									<span> Web Editor </span>\n" +
    "								</div>\n" +
    "							</a>\n" +
    "						</li>\n" +
    "						<li class=\"media\">\n" +
    "							<a ct-toggle=\"on\" target=\"users\" href ng-click=\"setOtherId(50223460)\">\n" +
    "								<i class=\"fa fa-circle status-online\"></i>\n" +
    "								<img alt=\"...\" src=\"img/avatar-1.jpg\" class=\"media-object\">\n" +
    "								<div class=\"media-body\">\n" +
    "									<h4 class=\"media-heading\">Kenneth Ross</h4>\n" +
    "									<span> Senior Designer </span>\n" +
    "								</div>\n" +
    "							</a>\n" +
    "						</li>\n" +
    "					</ul>\n" +
    "					<h5 class=\"sidebar-title\">Off-line</h5>\n" +
    "					<ul class=\"media-list\">\n" +
    "						<li class=\"media\">\n" +
    "							<a ct-toggle=\"on\" target=\"users\" href ng-click=\"setOtherId(50223457)\">\n" +
    "								<img alt=\"...\" src=\"img/avatar-1.jpg\" class=\"media-object\">\n" +
    "								<div class=\"media-body\">\n" +
    "									<h4 class=\"media-heading\">Nicole Bell</h4>\n" +
    "									<span> Content Designer </span>\n" +
    "								</div>\n" +
    "							</a>\n" +
    "						</li>\n" +
    "						<li class=\"media\">\n" +
    "							<a ct-toggle=\"on\" target=\"users\" href ng-click=\"setOtherId(50223458)\">\n" +
    "								<div class=\"user-label\">\n" +
    "									<span class=\"label label-success\">3</span>\n" +
    "								</div>\n" +
    "								<img alt=\"...\" src=\"img/avatar-1.jpg\" class=\"media-object\">\n" +
    "								<div class=\"media-body\">\n" +
    "									<h4 class=\"media-heading\">Steven Thompson</h4>\n" +
    "									<span> Visual Designer </span>\n" +
    "								</div>\n" +
    "							</a>\n" +
    "						</li>\n" +
    "						<li class=\"media\">\n" +
    "							<a ct-toggle=\"on\" target=\"users\" href ng-click=\"setOtherId(50223459)\">\n" +
    "								<img alt=\"...\" src=\"img/avatar-1.jpg\" class=\"media-object\">\n" +
    "								<div class=\"media-body\">\n" +
    "									<h4 class=\"media-heading\">Ella Patterson</h4>\n" +
    "									<span> Web Editor </span>\n" +
    "								</div>\n" +
    "							</a>\n" +
    "						</li>\n" +
    "						<li class=\"media\">\n" +
    "							<a ct-toggle=\"on\" target=\"users\" href ng-click=\"setOtherId(50223460)\">\n" +
    "								<img alt=\"...\" src=\"img/avatar-1.jpg\" class=\"media-object\">\n" +
    "								<div class=\"media-body\">\n" +
    "									<h4 class=\"media-heading\">Kenneth Ross</h4>\n" +
    "									<span> Senior Designer </span>\n" +
    "								</div>\n" +
    "							</a>\n" +
    "						</li>\n" +
    "						<li class=\"media\">\n" +
    "							<a ct-toggle=\"on\" target=\"users\" href ng-click=\"setOtherId(50223459)\">\n" +
    "								<img alt=\"...\" src=\"img/avatar-1.jpg\" class=\"media-object\">\n" +
    "								<div class=\"media-body\">\n" +
    "									<h4 class=\"media-heading\">Ella Patterson</h4>\n" +
    "									<span> Web Editor </span>\n" +
    "								</div>\n" +
    "							</a>\n" +
    "						</li>\n" +
    "						<li class=\"media\">\n" +
    "							<a ct-toggle=\"on\" target=\"users\" href ng-click=\"setOtherId(50223460)\">\n" +
    "								<img alt=\"...\" src=\"img/avatar-1.jpg\" class=\"media-object\">\n" +
    "								<div class=\"media-body\">\n" +
    "									<h4 class=\"media-heading\">Kenneth Ross</h4>\n" +
    "									<span> Senior Designer </span>\n" +
    "								</div>\n" +
    "							</a>\n" +
    "						</li>\n" +
    "					</ul>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "			<div class=\"user-chat\">				\n" +
    "					<div class=\"chat-content \">\n" +
    "						<a class=\"sidebar-back pull-left\" href=\"#\" ct-toggle=\"off\" target=\"users\"><i class=\"ti-angle-left\"></i> <span translate=\"offsidebar.chat.BACK\">Back</span></a>\n" +
    "						<div class=\"sidebar-content\" id=\"off-chat\" perfect-scrollbar wheel-propagation=\"false\" suppress-scroll-x=\"true\">\n" +
    "							\n" +
    "							<clip-chat messages=\"chat\" id-self=\"selfIdUser\" id-other=\"otherIdUser\"></clip-chat>\n" +
    "						</div>\n" +
    "					</div>\n" +
    "					<chat-submit submit-function=\"sendMessage\" ng-model=\"chatMessage\" scroll-element=\"#off-chat\"></chat-submit>\n" +
    "				\n" +
    "			</div>\n" +
    "		</div>\n" +
    "	</tab>\n" +
    "	<!-- end: TAB USER -->\n" +
    "</tabset>\n" +
    "<!-- end: OFF RIGHT SIDEBAR TABSET -->\n" +
    "");
}]);

angular.module("common/pdf-viewer.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("common/pdf-viewer.tpl.html",
    "<nav ng-class=\"getNavStyle(scroll)\">\n" +
    "  <button ng-click=\"goPrevious()\"><</span></button>\n" +
    "  <button ng-click=\"goNext()\">></span></button>\n" +
    "\n" +
    "  <button ng-click=\"zoomIn()\">+</span></button>\n" +
    "  <button ng-click=\"zoomOut()\">-</span></button>\n" +
    "\n" +
    "  <button ng-click=\"rotate()\">90</span></button>\n" +
    "\n" +
    "  <!-- <span>Page: </span>\n" +
    "  <input type=\"text\" min=1 ng-model=\"pageNum\">\n" +
    "  <span> / {{pageCount}}</span> -->\n" +
    "</nav>\n" +
    "\n" +
    "<hr>\n" +
    "\n" +
    "{{loading}}\n" +
    "<canvas id=\"pdf\" class=\"rotate0\"></canvas>");
}]);

angular.module("common/pdf.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("common/pdf.tpl.html",
    "<div>\n" +
    "	<ng-pdf ng-if=\"isAvailable()\" template-url=\"common/pdf-viewer.tpl.html\" canvasid=\"pdf\" scale=\"page-fit\" page=13></ng-pdf>\n" +
    "	<div ng-if=\"!isAvailable()\">\n" +
    "		PDF file is not available!\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("common/posting-job.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("common/posting-job.tpl.html",
    "<div class=\"ngdialog-message\">\n" +
    "    <h3 ng-if=\"title\">{{title}}</h3>\n" +
    "    <div class=\"text-center\">\n" +
    "    	<div>\n" +
    "    		POSTING TO HUNDREDS OF JOB BOARDS\n" +
    "    	</div>\n" +
    "    	<div class=\"windows8 margin-top-10\">\n" +
    "			<div class=\"wBall\" id=\"wBall_1\">\n" +
    "				<div class=\"wInnerBall\"></div>\n" +
    "			</div>\n" +
    "			<div class=\"wBall\" id=\"wBall_2\">\n" +
    "				<div class=\"wInnerBall\"></div>\n" +
    "			</div>\n" +
    "			<div class=\"wBall\" id=\"wBall_3\">\n" +
    "				<div class=\"wInnerBall\"></div>\n" +
    "			</div>\n" +
    "			<div class=\"wBall\" id=\"wBall_4\">\n" +
    "				<div class=\"wInnerBall\"></div>\n" +
    "			</div>\n" +
    "			<div class=\"wBall\" id=\"wBall_5\">\n" +
    "				<div class=\"wInnerBall\"></div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<!-- <div class=\"ngdialog-buttons text-right\">\n" +
    "    <button type=\"button\" class=\"btn btn-primary btn-o\" ng-click=\"closeThisDialog('no')\">Close</button>\n" +
    "</div> -->");
}]);

angular.module("common/sidebar.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("common/sidebar.tpl.html",
    "<!-- start: SIDEBAR -->\n" +
    "<nav data-ng-include=\"'common/nav.tpl.html'\"></nav>\n" +
    "<!-- end: SIDEBAR -->");
}]);

angular.module("common/top-navbar.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("common/top-navbar.tpl.html",
    "<!-- start: NAVBAR HEADER -->\n" +
    "<div class=\"navbar-header\">\n" +
    "    <a href=\"#\" class=\"sidebar-mobile-toggler pull-left hidden-md hidden-lg\" ng-click=\"toggle('sidebar')\" class=\"btn btn-navbar sidebar-toggle\">\n" +
    "        <i class=\"ti-align-justify\"></i>\n" +
    "    </a>\n" +
    "    <a class=\"navbar-brand\">\n" +
    "        <img src=\"img/logo-home.png\" height=\"60\" alt=\"Career Fox\" />\n" +
    "    </a>\n" +
    "    <a href=\"#\" class=\"sidebar-toggler pull-right visible-md visible-lg\" ng-click=\"app.layout.isSidebarClosed = !app.layout.isSidebarClosed\">\n" +
    "        <i class=\"ti-align-justify\"></i>\n" +
    "    </a>\n" +
    "    <a class=\"pull-right menu-toggler visible-xs-block\" id=\"menu-toggler\" ng-click=\"navbarCollapsed = !navbarCollapsed\">\n" +
    "        <span class=\"sr-only\">Toggle navigation</span>\n" +
    "        <i class=\"ti-view-grid\"></i>\n" +
    "    </a>\n" +
    "</div>\n" +
    "<!-- end: NAVBAR HEADER -->\n" +
    "<!-- start: NAVBAR COLLAPSE -->\n" +
    "<div class=\"navbar-collapse collapse\" collapse=\"navbarCollapsed\" ng-init=\"navbarCollapsed = true\" off-click=\"navbarCollapsed = true\" off-click-if='!navbarCollapsed' off-click-filter=\"#menu-toggler\">\n" +
    "    <ul class=\"nav navbar-right\" ct-fullheight=\"window\" data-ct-fullheight-exclusion=\"header\" data-ct-fullheight-if=\"isSmallDevice\">\n" +
    "        <!-- start: LANGUAGE SWITCHER -->\n" +
    "        <!-- <li class=\"dropdown\" dropdown on-toggle=\"toggled(open)\">\n" +
    "            <a href class=\"dropdown-toggle\" dropdown-toggle>\n" +
    "                <i class=\"ti-world\"></i> {{language.selected}}\n" +
    "            </a>\n" +
    "            <ul role=\"menu\" class=\"dropdown-menu dropdown-light fadeInUpShort\">\n" +
    "                <li ng-repeat=\"(localeId, langName) in language.available\">\n" +
    "                    <a ng-click=\"language.set(localeId, $event)\" href=\"#\" class=\"menu-toggler\">\n" +
    "                        {{langName}}\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </li> -->\n" +
    "        <!-- start: LANGUAGE SWITCHER -->\n" +
    "        <li class=\"dropdown\" dropdown on-toggle=\"toggled(open)\">\n" +
    "            <a href class=\"dropdown-toggle\" dropdown-toggle>\n" +
    "                <span class=\"dot-badge partition-red\"></span> <i class=\"ti-comment\"></i> <span>MESSAGES</span>\n" +
    "            </a>\n" +
    "            <ul class=\"dropdown-menu dropdown-light dropdown-messages dropdown-large\">\n" +
    "                <li>\n" +
    "                    <span class=\"dropdown-header\"> Unread messages</span>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <div class=\"drop-down-wrapper ps-container\">\n" +
    "                        <ul>\n" +
    "                            <li class=\"unread\">\n" +
    "                                <a href=\"javascript:;\" class=\"unread\">\n" +
    "                                    <div class=\"clearfix\">\n" +
    "                                        <div class=\"thread-image\">\n" +
    "                                            <img src=\"./img/avatar-2.jpg\" alt=\"\">\n" +
    "                                        </div>\n" +
    "                                        <div class=\"thread-content\">\n" +
    "                                            <span class=\"author\">Nicole Bell</span>\n" +
    "                                            <span class=\"preview\">Duis mollis, est non commodo luctus, nisi erat porttitor ligula...</span>\n" +
    "                                            <span class=\"time\"> Just Now</span>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                </a>\n" +
    "                            </li>\n" +
    "                            <li>\n" +
    "                                <a href=\"javascript:;\" class=\"unread\">\n" +
    "                                    <div class=\"clearfix\">\n" +
    "                                        <div class=\"thread-image\">\n" +
    "                                            <img src=\"./img/avatar-3.jpg\" alt=\"\">\n" +
    "                                        </div>\n" +
    "                                        <div class=\"thread-content\">\n" +
    "                                            <span class=\"author\">Steven Thompson</span>\n" +
    "                                            <span class=\"preview\">Duis mollis, est non commodo luctus, nisi erat porttitor ligula...</span>\n" +
    "                                            <span class=\"time\">8 hrs</span>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                </a>\n" +
    "                            </li>\n" +
    "                            <li>\n" +
    "                                <a href=\"javascript:;\">\n" +
    "                                    <div class=\"clearfix\">\n" +
    "                                        <div class=\"thread-image\">\n" +
    "                                            <img src=\"./img/avatar-5.jpg\" alt=\"\">\n" +
    "                                        </div>\n" +
    "                                        <div class=\"thread-content\">\n" +
    "                                            <span class=\"author\">Kenneth Ross</span>\n" +
    "                                            <span class=\"preview\">Duis mollis, est non commodo luctus, nisi erat porttitor ligula...</span>\n" +
    "                                            <span class=\"time\">14 hrs</span>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                </a>\n" +
    "                            </li>\n" +
    "                        </ul>\n" +
    "                    </div>\n" +
    "                </li>\n" +
    "                <li class=\"view-all\">\n" +
    "                    <a href=\"#\">\n" +
    "                        See All\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </li>\n" +
    "        <!-- start: USER OPTIONS DROPDOWN -->\n" +
    "        <li class=\"dropdown current-user\" dropdown on-toggle=\"toggled(open)\">\n" +
    "            <a href class=\"dropdown-toggle\" dropdown-toggle>\n" +
    "                <img src=\"img/avatar-1.jpg\" alt=\"Peter\"> <span class=\"username\">Peter <i class=\"ti-angle-down\"></i></i></span>\n" +
    "            </a>\n" +
    "            <ul class=\"dropdown-menu dropdown-dark\">\n" +
    "                <li>\n" +
    "                    <a ui-sref=\"app.pages.user\">\n" +
    "                        My Profile\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href=\"#\">\n" +
    "                        My Calendar\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href=\"#\">\n" +
    "                        My Messages (3)\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href=\"#\">\n" +
    "                        Lock Screen\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href=\"#\">\n" +
    "                        Log Out\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </li>\n" +
    "        <!-- end: USER OPTIONS DROPDOWN -->\n" +
    "    </ul>\n" +
    "    <!-- start: MENU TOGGLER FOR MOBILE DEVICES -->\n" +
    "    <div class=\"close-handle visible-xs-block menu-toggler\" ng-click=\"navbarCollapsed = true\">\n" +
    "        <div class=\"arrow-left\"></div>\n" +
    "        <div class=\"arrow-right\"></div>\n" +
    "    </div>\n" +
    "    <!-- end: MENU TOGGLER FOR MOBILE DEVICES -->\n" +
    "\n" +
    "</div>\n" +
    "<a class=\"dropdown-off-sidebar\" style=\"height:65px;background-color:#FDF5E8 !important;\" ng-click=\"toggle('off-sidebar')\">\n" +
    "    &nbsp;\n" +
    "</a>\n" +
    "<!-- end: NAVBAR COLLAPSE\n" +
    "");
}]);
