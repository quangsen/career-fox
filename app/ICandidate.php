<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/29/2015
 * Time: 10:25 PM
 */

namespace Fox;


interface ICandidate
{
    public function getId();

    public function getFirstName();

    public function getLastName();

    public function getEmail();
}