<?php
/**
 * @author Sławomir Żytko <slawomir.zytko@gmail.com>
 * @homepage https://github.com/szytko
 */

namespace Fox\Api\Rivs\Video;

/**
 * Class Crawler
 * @package App\Api\Rivs\Video
 */
class Crawler 
{

	const USER_AGENT =  'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36';

	const LOGIN_FORM_URL = 'https://www.rivs.com/login';

	const LOGIN_ACTION_URL = 'https://www.rivs.com/ajax/gLogin/';

	private $curlHandle;

	private $username;

	private $password;

	private $cookieFilePath;

	public function __construct($options)
	{
		$this->username = $options['username'];
		$this->password = $options['password'];
		$this->cookieFilePath = $options['cookieFilePath'];

		$this->initCurl();
	}

	protected function initCurl()
	{
		$this->curlHandle = curl_init();
		curl_setopt($this->curlHandle, CURLOPT_VERBOSE, false);
		curl_setopt($this->curlHandle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($this->curlHandle, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($this->curlHandle, CURLOPT_COOKIEJAR, $this->cookieFilePath);
		curl_setopt($this->curlHandle, CURLOPT_USERAGENT, self::USER_AGENT);
		curl_setopt($this->curlHandle, CURLOPT_FOLLOWLOCATION, false);
		curl_setopt($this->curlHandle, CURLOPT_RETURNTRANSFER, true);
	}

	protected function authenticate($url)
	{
		curl_setopt($this->curlHandle, CURLOPT_URL, self::LOGIN_ACTION_URL);
		curl_setopt($this->curlHandle, CURLOPT_POST, true);
		curl_setopt($this->curlHandle, CURLOPT_REFERER, self::LOGIN_FORM_URL);
		curl_setopt($this->curlHandle, CURLOPT_HTTPHEADER, [
			'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8',
			'Origin' => 'https://www.rivs.com',
			'X-Requested-With' =>  'XMLHttpRequest'
		]);
		$data = [
			'sLoginLoc' => 'page',
			'sPageReferral' => $url,
			'sEmail' => $this->username,
			'sPassword' => $this->password,
			'sType' => 'recruiter'
		];
		curl_setopt($this->curlHandle, CURLOPT_POSTFIELDS, http_build_query($data));
		//Execute the login request.
		$responseJSON = curl_exec($this->curlHandle);
		if(curl_errno($this->curlHandle)){
		    throw new \Exception(curl_error($this->curlHandle));
		}
		$response = json_decode($responseJSON);

		curl_setopt($this->curlHandle, CURLOPT_POST, false);
		curl_setopt($this->curlHandle, CURLOPT_URL, $response->sRedirect);

		return curl_exec($this->curlHandle);
	}

	public function getContent($url)
	{
		$this->authenticate($url);
		
		curl_setopt($this->curlHandle, CURLOPT_URL, $url);

		return curl_exec($this->curlHandle);
	}

	public function createRequisitionj($url, $data) 
	{
		$result = $this->authenticate($url);

		$postUrl = 'https://v3.rivs.com/ajax/recruiter/requisition/setup/';
		
		curl_setopt($this->curlHandle, CURLOPT_HTTPHEADER, [
			'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8',
			'Host' => 'v3.rivs.com',
			'Origin' => 'https://v3.rivs.com',
			'X-Requested-With' =>  'XMLHttpRequest'
		]);
		curl_setopt($this->curlHandle, CURLOPT_URL, $postUrl);
		curl_setopt($this->curlHandle, CURLOPT_POST, true);
		curl_setopt($this->curlHandle, CURLOPT_POSTFIELDS, $data);

		return curl_exec($this->curlHandle);
	}
}