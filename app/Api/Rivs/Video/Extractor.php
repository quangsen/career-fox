<?php
/**
 * @author Sławomir Żytko <slawomir.zytko@gmail.com>
 * @homepage https://github.com/szytko
 */

namespace App\Api\Rivs\Video;

/**
 * Class Extractor
 * @package App\Api\Rivs\Video
 */
class Extractor {

	private $content;

	/**
	 * @param $content
	 */
	public function __construct($content)
	{
		$this->content = $content;
	}

	/**
	 * @return array
	 */
	public function extract() 
	{
		preg_match_all('/\"file\"\:(.*)\}\]/i', $this->content, $videos);
		preg_match_all('/image\:(.*)\,/i', $this->content, $images);

		$data = [];

		foreach ($videos[1] as $i => $video) {
			$data[]= [
				'video' => str_replace("\"", '', $video),
				'image' => str_replace("'", '', $images[1][$i])
			];
		}

		return $data;
	}
}