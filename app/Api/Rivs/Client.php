<?php
/**
 * @author Sławomir Żytko <slawomir.zytko@gmail.com>
 * @homepage https://github.com/szytko
 */

namespace Fox\Api\Rivs;
use App\Api\Rivs\Resource\Exception\ResourceNotFoundException;

/**
 * Class Client
 *
 *
 * Usage
 *
 * ```
 * $client = new \App\Api\Rivs\Client(['uri' => '', 'token' => '']);
 * $client->requisitionCandidates->get();
 * $client->requisitionCandidates->create(['iRequisition' => '..', ....]);
 *
 * @package App\Api\Rivs
 */
class Client
{
    /**
     * @var array
     */
    private $config;

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param $url
     * @param $method
     * @return \Httpful\Request
     */
    public function request($url, $method)
    {
        /** @var \Httpful\Request $request */
        $request = \Httpful\Request::init($method)->uri(
            sprintf(
                $this->config['uri'] . '/' . ltrim($url, '/')
            )
        );
        $request->addHeader('Authorization', 'OAuth ' . $this->config['token']);
        $request->expectsType('json');

        return $request;
    }

    /**
     * @param $name
     * @return object
     * @throws ResourceNotFoundException
     */
    public function __get($name)
    {
        $resourceClassName = sprintf(__NAMESPACE__ . '\\Resource\\%s', ucfirst($name));
        try {
            $reflectionClass = new \ReflectionClass($resourceClassName);
            $resource = $reflectionClass->newInstance($this);
            return $resource;
        } catch (\ReflectionException $e) {
            throw new ResourceNotFoundException($name);
        }
    }
}