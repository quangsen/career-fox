<?php
/**
 * @author Sławomir Żytko <slawomir.zytko@gmail.com>
 * @homepage https://github.com/szytko
 */

namespace Fox\Api\Rivs\Resource;

use App\Api\Rivs\AbstractResource;
use Httpful\Http;

/**
 * Class RequisitionCandidates
 * @see https://v3.rivs.com/api/requisitionCandidates/
 * @method get($id = null)
 * @method create(array $data)
 * @method update($id, array $data)
 * @package App\Api\Rivs\Resource
 */
class RequisitionCandidates extends AbstractResource
{

    /**
     * @return string
     */
    public function getUri()
    {
        return '/requisitionCandidates';
    }

    /**
     * @param $id
     * @return \Httpful\Response
     */
    public function shareLinks($id)
    {
        $uri = $this->getUri() . '/' . $id . '/shareLinks';
        $request = $this->client->request($uri, Http::GET);

        return $request->send();
    }

    /**
     * @param $id
     * @throws \Exception
     */
    public function delete($id)
    {
        throw new \Exception('Not allowed');
    }
}