<?php
/**
 * @author Sławomir Żytko <slawomir.zytko@gmail.com>
 * @homepage https://github.com/szytko
 */

namespace App\Api\Rivs\Resource;

use App\Api\Rivs\AbstractResource;
use Httpful\Http;

/**
 * Class Requisitions
 * @see https://v3.rivs.com/api/companies/
 * @method get($id = null)
 * @method create(array $data)
 * @method update($id, array $data)
 * @package App\Api\Rivs\Resource
 */
class Companies extends AbstractResource
{
    /**
     * @return string
     */
    public function getUri()
    {
        return '/companies';
    }

    /**
     * @param $id
     * @return \Httpful\Response
     */
    public function users($id)
    {
        $uri = $this->getUri() . '/' . $id . '/users';
        $request = $this->client->request($uri, Http::GET);

        return $request->send();
    }
}