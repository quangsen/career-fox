<?php
namespace Fox\Api\MaxHire\Resource;

use Fox\Api\MaxHire\AbstractResource;
use Fox\Api\MaxHire\DataSet\Job\JobDataSet;
use Fox\IJob;

/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/29/2015
 * Time: 1:21 PM
 */
class Job extends AbstractResource
{
    public function getEmptyForAdd()
    {
        $result = $this->client->Job_GetEmptyForAdd();
        return $result->Job_GetEmptyForAddResult;
    }

    public function get($id)
    {
        $params = [
            'intReference' => (int)$id
        ];
        $body = $this->client->Job_Get($params);
        return $body->Job_GetResult;
    }

    public function add(IJob $job)
    {
        try {
            $dataSet = new JobDataSet($job);
            $dataSet->transform();
            $xml = $dataSet->toString();
            $params = [
                'dsJob' => new \SoapVar($xml, 147)
            ];

            $result = $this->client->Job_Add($params);
            $xml = $result->Job_AddResult;
            return AddResult::fromXml($xml);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function update()
    {
    }

    public function delete()
    {
    }
}