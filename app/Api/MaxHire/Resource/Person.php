<?php
namespace Fox\Api\MaxHire\Resource;

use Fox\Api\MaxHire\AbstractResource;
use Fox\Api\MaxHire\DataSet\ITemplate;
use Fox\Api\MaxHire\DataSet\Person\PersonDataSet;
use Fox\ICandidate;


/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/29/2015
 * Time: 1:21 PM
 */
class Person extends AbstractResource implements ITemplate
{
    private $dsPerson = __DIR__ . '/ds-person.xml';

    public function getPath()
    {
        return $this->dsPerson;
    }


    public function getEmptyForAdd()
    {
        $result = $this->client->Person_GetEmptyForAdd();
        return $result->Person_GetEmptyForAddResult;
    }

    public function get($id)
    {
        $params = [
            'intPersonId' => (int)$id
        ];
        $body = $this->client->Person_Get($params);
        return $body->Person_GetResult;
    }

    public function add(ICandidate $candidate)
    {
        try {
            $dataSet = new PersonDataSet($candidate);
            $dataSet->transform();
            $xml = $dataSet->toString();
            $params = [
                'dsPerson' => new \SoapVar($xml, 147),
                'blnCheckForDuplicates' => true
            ];

            $result = $this->client->Person_Add($params);
            $xml = $result->Person_AddResult;
            return AddResult::fromXml($xml);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function update()
    {

    }

    public function delete()
    {

    }
}