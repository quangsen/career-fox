<?php

namespace Fox\Api\MaxHire\DataSet\Job;

/**
 * Created by PhpStorm.
 * User: Charles Oneka
 * Date: 11/1/2015
 * Time: 4:42 PM
 */

use Fox\Api\MaxHire\DataSet\ITransformer;
use Fox\IJob;

class JobDataSetTransformer implements ITransformer
{

    protected $document;

    /**
     * PersonDataSetTransformer constructor.
     */
    public function __construct(IJob $document)
    {
        $this->document = $document;
    }

    public function transform(\DOMDocument $doc)
    {
        $xpath = new \DOMXPath($doc);

        $nodes = $xpath->query("//NewDataSet/Table/doctitle");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->document->getTitle();
        }
        $nodes = $xpath->query("//NewDataSet/Table/dateenter");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->document->getDate();
        }
        $nodes = $xpath->query("//NewDataSet/Table/enterby");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->document->getEnterBy();
        }
        $nodes = $xpath->query("//NewDataSet/Table/FileExt");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->document->getFileExt();
        }

    }

}