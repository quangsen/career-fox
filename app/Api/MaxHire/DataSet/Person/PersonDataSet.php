<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/31/2015
 * Time: 9:18 PM
 */

namespace Fox\Api\MaxHire\DataSet\Person;

use Fox\Api\MaxHire\DataSet\DataSet as AbstractDataSet;
use Fox\ICandidate;

class PersonDataSet extends AbstractDataSet
{
    protected $template = __DIR__ . '/ds-person.xml';
    protected $candidate;

    /**
     * PersonDataSet constructor.
     */
    public function __construct(ICandidate $candidate)
    {
        $this->candidate = $candidate;
        $transformer = new PersonDataSetTransformer($this->candidate);
        parent::__construct($this->template, $transformer);
    }

    public function toString()
    {
        $xml = $this->doc->saveHTML();
        $xml = trim(str_replace(['<root>', '</root>'], ['<ns1:dsPerson>', '</ns1:dsPerson>'], $xml));

        return $xml;
    }
}