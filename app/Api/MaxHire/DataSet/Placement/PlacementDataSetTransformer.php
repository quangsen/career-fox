<?php

namespace Fox\Api\MaxHire\DataSet\Job;

/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/31/2015
 * Time: 4:42 PM
 */

use Fox\Api\MaxHire\DataSet\ITransformer;
use Fox\IPlacement;

class PlacementDataSetTransformer implements ITransformer
{

    protected $placement;

    /**
     * PersonDataSetTransformer constructor.
     */
    public function __construct(IJob $placement)
    {
        $this->job = $placement;
    }

    public function transform(\DOMDocument $doc)
    {
        $xpath = new \DOMXPath($doc);

        $nodes = $xpath->query("//NewDataSet/Table/contacts_id");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->job->getContactsID();
        }
        $nodes = $xpath->query("//NewDataSet/Table/Placedivisions");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->job->getPlaceDivisions();
        }
        $nodes = $xpath->query("//NewDataSet/Table/dateenter");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->job->getDateEnter();
        }

         $nodes = $xpath->query("//NewDataSet/Table/enterby");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->job->getEnterBy();
        }

         $nodes = $xpath->query("//NewDataSet/Table/modifiedby");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->job->getModifiedBy();
        }
        $nodes = $xpath->query("//NewDataSet/Table/CompCity");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->job->getCity();
        }
        $nodes = $xpath->query("//NewDataSet/Table/CompPhone");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->job->getPhone();
        }
        $nodes = $xpath->query("//NewDataSet/Table/CompState");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->job->getState();
        }
        $nodes = $xpath->query("//NewDataSet/Table/compAddr");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->job->getAddress();
        }
        $nodes = $xpath->query("//NewDataSet/Table/CompState");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->job->getZip();
        }
        $nodes = $xpath->query("//NewDataSet/Table/PlaceCandFirst");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->job->getFirstName();
        }
        $nodes = $xpath->query("//NewDataSet/Table/PlaceCandLast");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->job->getFirstName();
        }
        $nodes = $xpath->query("//NewDataSet/Table/PlaceJobTitle");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->job->getJobTitle();
        }
         $nodes = $xpath->query("//NewDataSet/Table/PlaceDateMade");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->job->getPlacementDate();
        }

    }

}