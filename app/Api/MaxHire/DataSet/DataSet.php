<?php

namespace Fox\Api\MaxHire\DataSet;
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/30/2015
 * Time: 10:39 AM
 */
abstract class DataSet
{
    /**
     * @var \DOMDocument
     */
    protected $doc;


    /**
     * @var
     */
    protected $templatePath;

    protected $transformer;

    /**
     * @param $templatePath
     * @param ITransformer $transformer
     */
    public function __construct($templatePath, ITransformer $transformer)
    {
        $this->templatePath = $templatePath;
        $this->transformer = $transformer;

        $this->doc = new \DOMDocument();
        $xml = file_get_contents($templatePath);
        $this->doc->loadXML($xml);
    }


    abstract public function toString();

    public function __toString()
    {
        return $this->toString();
    }


    public function transform()
    {
        $this->transformer->transform($this->doc);
    }

    public function getXmlDoc()
    {
        return $this->doc;
    }
}