<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/29/2015
 * Time: 1:05 PM
 */

namespace Fox\Api\MaxHire;

use Zend\Soap\Client as SoapClient;
use Config;

class Client
{
    protected $wsdl = 'https://www.maxhire.net/MaxHireAPI/Services.asmx?WSDL';
    protected $ns = 'http://www.maxhire.net/';
    protected $options = array();

    protected $soap = null;

    public function __construct($options = null)
    {
        $databaseName = Config::get('maxhire.DatabaseName');
        $securityKey = Config::get('maxhire.SecurityKey');
        $username = Config::get('maxhire.MHUserName');

        $this->soap = $client = new SoapClient($this->wsdl, array('soap_version' => SOAP_1_1));
        $auth = new Auth();
        $auth->DatabaseName = new \SoapVar($databaseName, XSD_STRING, NULL, $this->ns, NULL, $this->ns);
        $auth->SecurityKey = new \SoapVar($securityKey, XSD_STRING, NULL, $this->ns, NULL, $this->ns);
        $auth->MHUserName = new \SoapVar($username, XSD_STRING, NULL, $this->ns, NULL, $this->ns);
        $header = new \SoapHeader($this->ns, 'AuthHeader', $auth);
        $this->soap->addSoapInputHeader($header, true);
    }

    public function __call($name, $arguments)
    {
        try {
            return $this->soap->call($name, $arguments);
        } catch (\Exception $ex) {
            throw $ex;
        } finally {
            // Debug
            // file_put_contents('soap-request.xml', $this->soap->getLastRequest());
        }
    }
}