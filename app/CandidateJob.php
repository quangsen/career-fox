<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;


class CandidateJob extends Model
{
    protected $table = 'candidate_jobs';
    
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['company_id', 'candidate_id', 'job_id'];
    
    protected $guarded = ['id'];

    
    public function company() {
        return $this->belongsTo('Fox\Company');
    }
    
    public function job() {
        return $this->belongsTo('Fox\Job');
    }
    
    public function candidate() {
        return $this->belongsTo('Fox\User');
    }
}