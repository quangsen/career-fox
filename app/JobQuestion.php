<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

class JobQuestion extends Model
{
    protected $table = 'questions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'default_answer', 'job_id', 'rivs_question_id', 'user_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
    
    public function job() {
        return $this->belongsTo('Fox\Job');
    }
}
