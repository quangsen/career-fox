<?php

namespace Fox\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use Psr\Log\LoggerInterface;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class Handler extends ExceptionHandler
{
    protected $response;
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
    ];

    public function __construct(LoggerInterface $log, Response $response)
    {
        parent::__construct($log);
        $this->response = $response;
    }

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
//        if ($e instanceof ModelNotFoundException) {
//            $e = new NotFoundHttpException($e->getMessage(), $e);
//        }

        if ($e instanceof TokenExpiredException) {
            return $this->response->errorUnauthorized('Token has expired');
        } else if ($e instanceof TokenInvalidException) {
            return $this->response->errorUnauthorized('Token is invalid');
        } else if ($e instanceof \Efficiently\AuthorityController\Exceptions\AccessDenied) {
            $msg = $e->getMessage();
            return $this->response->errorForbidden($msg);
        } else if ($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {
            return $this->response->errorNotFound($e->getMessage());
        } else if ($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            return $this->response->errorNotFound('Route not found');
        } else if ($e instanceof \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException) {
            return $this->response->errorMethodNotAllowed('Method not allowed');
        } else if ($e instanceof \Illuminate\Database\QueryException) {
            return $this->response->errorGone($e->getMessage());
        }

        return parent::render($request, $e);
    }

}
