<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;

use Fox\City;

class CityTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'province'
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    public function transform(City $entity)
    {
        return [
            'id' => (int) $entity->id,
            'name' => $entity->name,
            'province_id' => (int)$entity->province_id,
        ];
    }

    public function includeProvince(City $entity){
        $model = $entity->province;
        if(!$model){
            return null;
        }

        return $this->item($model, new ProvinceTransformer());
    }
}