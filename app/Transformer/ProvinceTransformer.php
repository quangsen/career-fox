<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;

use Fox\Province;

class ProvinceTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'country'
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    public function transform(Province $entity)
    {
        return [
            'id' => (int) $entity->id,
            'name' => $entity->name,
            'country_id' => (int)$entity->country_id,
        ];
    }

    public function includeCountry(Province $entity){
        $model = $entity->country;
        if(!$model){
            return null;
        }

        return $this->item($model, new CountryTransformer());
    }
}