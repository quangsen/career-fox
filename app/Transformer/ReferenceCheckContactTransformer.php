<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;

use Fox\ReferenceCheckContact;

class ReferenceCheckContactTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    public function transform(ReferenceCheckContact $entity)
    {
        return [
            'id' => (int) $entity->id,
            'name' => $entity->name,
            'phone' => $entity->phone
        ];
    }
}