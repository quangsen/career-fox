<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;

use Fox\PhoneScreenInvitationResult;

class PhoneScreenInvitationResultTransformer extends AbstractTransformer
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];

    public $is_invited = false;

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    public $defaultIncludes = [
    ];

    public function transform(PhoneScreenInvitationResult $obj)
    {
        return [
            'invited_candidates' => $obj->invited_candidates,
            'message' => $obj->message,
            'candidate_ids' => $obj->candidate_ids,
        ];
    }
}