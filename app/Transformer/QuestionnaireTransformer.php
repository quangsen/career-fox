<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;

use Fox\Questionnaire;

class QuestionnaireTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'job'
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    public function transform(Questionnaire $entity)
    {
        return [
            'id' => (int) $entity->id,
            //'question' => $entity->question,
            'job_id' => $entity->job_id,
            'candidate_id' => $entity->candidate_id,
            'video_interview_id' => (int) $entity->video_interview_id
        ];
    }
    
    public function includeJob(Questionnaire $entity)
    {
        $model = $entity->job;
        
        if ( ! $model) {
            return null;
        }
        
        return $this->item($model, new JobTransformer());
    }
}