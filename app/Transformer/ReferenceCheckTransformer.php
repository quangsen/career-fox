<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;

use Fox\ReferenceCheck;

class ReferenceCheckTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //'job',
        'contact'
    ];

    public function transform(ReferenceCheck $entity)
    {
        return [
            'id' => (int) $entity->id,
            'candidate_id' => (int) $entity->candidate_id,
            'job_id' => (int) $entity->job_id,
            'quick_note' => $entity->quick_note
        ];
    }
    
    public function includeContact(ReferenceCheck $entity)
    {
        $model = $entity->contacts;
        
        if ( ! $model) {
            return null;
        }
        return $this->collection($model, new ReferenceCheckContactTransformer());
    }
    
    public function includeCandidate(ReferenceCheck $entity)
    {
        $model = $entity->candidate;
        if ( ! $model) {
            return null;
        }
        return $this->item($model, new CandidateTransformer());
    }
}