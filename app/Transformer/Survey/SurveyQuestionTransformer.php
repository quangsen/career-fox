<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer\Survey;

use Fox\SurveyQuestion;
use Fox\Transformer\AbstractTransformer;
use Fox\Transformer\JobTransformer;

class SurveyQuestionTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'options'
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    public function transform(SurveyQuestion $entity)
    {
        return [
            'id' => (int)$entity->id,
            'title' => $entity->title,
            'survey_id' => (int)$entity->survey_id,
            'created_at' => $this->formatDate($entity->created_at),
            'updated_at' => $this->formatDate($entity->updated_at)
        ];
    }

    public function includeOptions(SurveyQuestion $entity)
    {
        $model = $entity->questionOptions;
        if (!$model) {
            return null;
        }
        return $this->collection($model, new JobTransformer());
    }
}