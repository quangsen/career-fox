<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer\Survey;

use Fox\SurveyQuestionOption;
use Fox\Transformer\AbstractTransformer;

class SurveyQuestionOptionTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    public function transform(SurveyQuestionOption $entity)
    {
        return [
            'id' => (int)$entity->id,
            'title' => $entity->title,
            'value' => $entity->value,
            'survey_question_id' => (int)$entity->survey_question_id,
            'created_at' => $this->formatDate($entity->created_at),
            'updated_at' => $this->formatDate($entity->updated_at)
        ];
    }
}