<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;

use Fox\VideoInterview;

class VideoInterviewTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'job',
        'candidate'
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    public $defaultIncludes = [
    ];

    public function transform(VideoInterview $entity)
    {
        return [
            'id' => (int) $entity->id,
            'quick_note' => $entity->quick_note,
            'job_id' => $entity->job_id,
            'candidate_id' => $entity->candidate_id,
            'phone_screen_id' => (int) $entity->phone_screen_id,
            'invited_questionnaire' => (int) $entity->is_invited ? 'yes' : 'no'
        ];
    }
    
    public function includeJob(VideoInterview $entity)
    {
        $model = $entity->job;
        
        if ( ! $model) {
            return null;
        }
        
        return $this->item($model, new JobTransformer());
    }
    
    public function includeCandidate(VideoInterview $entity)
    {
        $model = $entity->candidate;
        
        if ( ! $model) {
            return null;
        }
        
        return $this->item($model, new CandidateTransformer());
    }
}