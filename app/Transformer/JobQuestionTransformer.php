<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;

use Fox\JobQuestion;

class JobQuestionTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    public function transform(JobQuestion $entity)
    {
        return [
            'id' => (int) $entity->id,
            'title' => $entity->title,
            'default_answer' => $entity->title
        ];
    }
}