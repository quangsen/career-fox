<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;

use Fox\Interview;

class InterviewTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'job',
        'candidate'
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //'job',
        //'candidate'
    ];

    public function transform(Interview $entity)
    {
        return [
            'id' => (int) $entity->id,
            'candidate_id' => (int) $entity->candidate_id,
            'job_id' => (int) $entity->job_id,
            'quick_note' => $entity->quick_note
        ];
    }
    
    public function includeJob(Interview $entity)
    {
        $model = $entity->job;
        if ( ! $model) {
            return null;
        }
        return $this->item($model, new JobTransformer());
    }
    
    public function includeCandidate(Interview $entity)
    {
        $model = $entity->candidate;
        if ( ! $model) {
            return null;
        }
        return $this->item($model, new CandidateTransformer());
    }
}