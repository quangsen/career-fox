<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;

use Fox\Job;

class JobTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'category',
        'user',
        'company',
        'city',
        'country',
        'province'
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    public function transform(Job $entity)
    {
        $status = $entity->isActive() ? 'Active' : ($entity->isDraft() ? 'Draft' : 'Inactive');

        $time_ago = time() - strtotime($entity->created_at);
        $days_ago = ceil($time_ago / (24 * 3600));

        $candidates = $entity->candidates;
        $candidate_num = is_numeric($entity->candidates) ? (int)$candidates : count($entity->candidates);

        $priority = $entity->priority == 10 ? 'urgent' : 'standard';
        
        return [
            'id' => (int)$entity->id,
            'title' => $entity->title,
            'city_id' => (int)$entity->city_id,
            'province_id' => (int)$entity->province_id,
            'country_id' => (int)$entity->country_id,
            'city_name' => $entity->city_name,
            'province_name' => $entity->province_name,
            'country_name' => $entity->country_name,
            'postal' => $entity->postal,
            'description' => $entity->description,
            'category_id' => (int)$entity->category_id,
            'company_id' => (int)$entity->company_id,
            'user_id' => (int)$entity->user_id,
            'status' => $status,
            'priority' => $priority,
            'candidates' => $candidate_num,
            'age' => $days_ago
        ];
    }

    public function includeUser(Job $entity)
    {
        $model = $entity->user;
        if (!$model) {
            return null;
        }
        return $this->item($model, new UserTransformer());
    }

    public function includeCategory(Job $entity)
    {
        $model = $entity->category;
        if (!$model) {
            return null;
        }
        return $this->item($model, new CategoryTransformer());
    }

    public function includeCompany(Job $entity)
    {
        $model = $entity->company;
        if (!$model) {
            return null;
        }
        return $this->item($model, new CompanyTransformer());
    }

    public function includeCity(Job $entity)
    {
        $model = $entity->city;
        if (!$model) {
            return null;
        }
        return $this->item($model, new CityTransformer());
    }

    public function includeProvince(Job $entity)
    {
        $model = $entity->province;
        if (!$model) {
            return null;
        }
        return $this->item($model, new ProvinceTransformer());
    }

    public function includeCountry(Job $entity)
    {
        $model = $entity->country;
        if (!$model) {
            return null;
        }
        return $this->item($model, new CountryTransformer());
    }
}