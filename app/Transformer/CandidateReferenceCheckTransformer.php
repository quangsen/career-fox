<?php
/**
 * Created by NetBean.
 * User: Vinh
 */

namespace Fox\Transformer;

use Fox\Candidate;

class CandidateReferenceCheckTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'job',
        'company',
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //'address',
        'user'
    ];
    

    public function transform(Candidate $entity)
    {
        //$storage_url = url('storage');

        $data = [
            'id' => (int)$entity->id,
            'name' => $entity->user->first_name . ' ' . $entity->user->last_name,
            'email' => $entity->email,
            'job_title' => $entity->job_title,
            //'views' => (int)$entity->views,
            //'photo' => $entity->photo ? $storage_url . '/candidate_photos/' . $entity->photo : '',
            //'resume' => $entity->resume ? $storage_url . '/resumes/' . $entity->resume : '',
            //'cover_letter' => $entity->cover_letter ? $storage_url . '/cover_letters/' . $entity->cover_letter : '',
            'date' => $this->formatDate($entity->created_at),
            'reference_check_id' => $entity->reference_check_id,
            'invited_offer_letter' => $entity->is_invited > 0 ? true : false
        ];
        
        return $data;
    }

    public function includeUser(Candidate $entity)
    {
        $model = $entity->user;
        if (!$model) {
            return null;
        }

        return $this->item($model, new UserTransformer());
    }

    public function includeCompany(Candidate $entity)
    {
        $model = $entity->company;
        if (!$model) {
            return null;
        }
        return $this->item($model, new CompanyTransformer());
    }

    public function includeJob(Candidate $entity)
    {
        $model = $entity->job;
        if (!$model) {
            return null;
        }
        return $this->item($model, new JobTransformer());
    }

    public function includeAddress(Candidate $entity)
    {
        $model = $entity->address;
        if (!$model) {
            return null;
        }
        return $this->item($model, new AddressTransformer());
    }
}