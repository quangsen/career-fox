<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;


class AbstractTransformer extends \League\Fractal\TransformerAbstract
{

    protected function formatDate($date)
    {
        if (!($date instanceof \DateTime)) {
            return $date;
        }
        $format = config('format.date');
        return $date->format($format);
    }
}