<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;

use Fox\Candidate;

class CandidateTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'job',
        'company',
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'address',
        'user'
    ];

    public $has_job = false;
    public $is_invited_to_phone_screen = false;
    public $is_invited_to_video_interview = false;

    public function transform(Candidate $entity)
    {
        $storage_url = url('storage');

        $data = [
            'id' => (int)$entity->id,
            'name' => $entity->name,
            'email' => $entity->email,
            'job_title' => $entity->job_title,
            'views' => (int)$entity->views,
            'photo' => $entity->photo ? $storage_url . '/candidate_photos/' . $entity->photo : '',
            'resume' => $entity->resume ? $storage_url . '/resumes/' . $entity->resume : '',
            'cover_letter' => $entity->cover_letter ? $storage_url . '/cover_letters/' . $entity->cover_letter : '',
            'date' => $this->formatDate($entity->created_at)
        ];

        if ($this->has_job) {
            $data['is_invited'] = $entity->is_invited ? true : false;
        }

        if ($this->is_invited_to_phone_screen) {
            $data['phone_screen_id'] = $entity->phone_screen_id;
            $data['invited_video_interview'] = (int)$entity->invited_video_interview ? true : false;
        }

        if ($this->is_invited_to_video_interview) {
            $data['video_interview_id'] = $entity->video_interview_id;
            $data['invited_questionnaire'] = (int)$entity->invited_questionnaire ? true : false;
        }

        return $data;
    }

    public function includeUser(Candidate $entity)
    {
        $model = $entity->user;
        if (!$model) {
            return null;
        }

        return $this->item($model, new UserTransformer());
    }

    public function includeCompany(Candidate $entity)
    {
        $model = $entity->company;
        if (!$model) {
            return null;
        }
        return $this->item($model, new CompanyTransformer());
    }

    public function includeJob(Candidate $entity)
    {
        $model = $entity->job;
        if (!$model) {
            return null;
        }
        return $this->item($model, new JobTransformer());
    }

    public function includeAddress(Candidate $entity)
    {
        $model = $entity->address;
        if (!$model) {
            return null;
        }
        return $this->item($model, new AddressTransformer());
    }
}