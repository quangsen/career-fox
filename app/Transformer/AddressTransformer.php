<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;

use Fox\Address;

class AddressTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'user', 'company'
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    public function transform(Address $entity)
    {
        return [
            'id' => (int) $entity->id,
            'address' => $entity->address,
            'city' => $entity->city,
            'province' => $entity->province,
            'country' => $entity->country,
            'phone' => $entity->phone,
        ];
    }
    
    public function includeUser(Address $entity)
    {
        $user = $entity->user;
        if ( ! $user) {
            return null;
        }
        return $this->item($user, new AddressTransformer());
    }
    
    public function includeCompany(Address $entity)
    {
        $model = $entity->company;
        if ( ! $model) {
            return null;
        }
        return $this->item($model, new CompanyTransformer());
    }
}