<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;

use Fox\Invoice;

class InvoiceTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    public function transform(Invoice $entity)
    {
        return [
            'id' => (int) $entity->id,
            'name' => $entity->name,
            'amount' => $entity->amount,
            'status' => $entity->status,
            'datetime' => $entity['created_at']->toDateTimeString(),
//            'date' => date('Y-m-d', strtotime($entity['created_at']->toDateTimeString())),
        ];
    }
}