<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 8/30/2015
 * Time: 8:37 AM
 */

namespace Fox\Repositories\PhoneScreen;

use Fox\Repositories\Query\TraitSearchFields;

class PhoneScreenSearchFields
{
    use TraitSearchFields;

    public $keyword;
    public $job_id;
}