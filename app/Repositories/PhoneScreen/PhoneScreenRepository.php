<?php
namespace Fox\Repositories\PhoneScreen;

use Fox\Repositories\AbstractRepository;
use Fox\Repositories\Query\QueryOption;
use Illuminate\Pagination\Paginator;
use Validator;

use DB;
use Fox\PhoneScreen;
use Fox\VideoInterview;
use Fox\User;
use Fox\Candidate;

use Mail;

class PhoneScreenRepository extends AbstractRepository
{
    protected $rules = [
        'candidate_id' => 'required|integer',
        'job_id' => 'required|integer',
        'quick_note' => 'required|min:3'
    ];


    protected $errors;

    protected $sortFields = [
        //'title',
    ];

    protected $model;

    public function __construct(PhoneScreen $model)
    {
        $this->model = $model;
    }


    public function errors()
    {
        return $this->errors;
    }

    public function all(array $search = array(), QueryOption $searchOptions = null)
    {
        $searchFields = new PhoneScreenSearchFields($search);
        Paginator::currentPageResolver(function () use ($searchOptions) {
            return $searchOptions->getPage();
        });

        $query = PhoneScreen::query();
        if ($searchFields->job_id) {
            $query->where('job_id', '=', (int)$searchFields->job_id);
        }

        /*if ($searchFields->keyword) {
            $keyword = '%' . $searchFields->keyword . '%';
            $query->where(function ($q) use ($keyword) {
                $q->orWhere('title', 'LIKE', $keyword);
            });
        }*/

        $sortBy = $searchOptions->getSortBy();
        if ($sortBy && in_array($sortBy, $this->sortFields)) {
            $query = $query->orderBy($sortBy, $searchOptions->getSortOrder());
        } else {
            $query = $query->orderBy('created_at', 'desc');
        }
        $query = $query->paginate($searchOptions->getLimit());
        return $query;
    }

    public function find($id)
    {
        return $this->getById($id);
    }

    public function findByIds(array $ids)
    {
        return PhoneScreen::whereIn('id', $ids);
    }

    private function prepare(array $data)
    {
        return $data;
    }

    public function create(array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }

            $item = $this->prepare($data);
            $model = $this->model->newInstance($item);
            $model->save();
            if (!$model->id) {
                return false;
            }
            return $model;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function update($entity, array $data)
    {
        try {
            $validator = Validator::make($data, ['quick_note' => 'required|min:2']);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }

            $item = $this->prepare($data, false);
            if (!$entity->update($item)) {
                return false;
            }
            return $entity;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function delete($id)
    {
        return PhoneScreen::destroy($id);
    }

    public function invite($id, $model)
    {
        try {
            global $user;

            if ($model->is_invited) {
                return false;
            }

            $video_interview = VideoInterview::firstOrCreate([
                'phone_screen_id' => $id,
                'candidate_id' => $model->candidate_id,
                'job_id' => $model->job_id
            ]);
            $video_interview->user_id = $user->id;
            $video_interview->save();

            $model->is_invited = 1;
            $model->save();

            return $video_interview;
            // send email to candidate
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
    
    public function share($id, $model, $data)
    {
        try {
            global $user;

            $validator = Validator::make($data, ['employee_ids' => 'array']);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }            
            
            $candidate_id = $model->candidate_id;
            $candidate = Candidate::find($candidate_id)->user;

            if ( isset($data['employee_ids']) && $data['employee_ids'] ) {
                $employee_ids = $data['employee_ids'];
                array_filter($employee_ids, function($value) {
                    return (int) $value;
                });

                $employee_ids = array_unique($employee_ids);
                
                // get all employees of the current company that is also in $employee_ids
                $employees = User::where('company_id', $user->company_id)->whereIn('id', $employee_ids)->get();
                
                if ( ! count($employees) ) {
                    $this->errors = 'There are no any employees specified in the employee_ids';
                    return false;
                }
            } else {
                // get all employees of the current company
                $employees = User::where('company_id', $user->company_id)->where('id', '<>', $user->id)->get();
                
                if ( ! count($employees) ) {
                    $this->errors = 'There are no any employees in the company';
                    return false;
                }
            }
            
            foreach ($employees AS $employee) {
                $candidate->employee_fname = $employee->first_name;
                // send email
                Mail::send('emails.video_invitations', ['data' => $candidate], function ($m) use ($employee) {
                    $m->to($employee->email, $employee->name)->subject('A new candidate is invited to video interview!');
                    //$m->to('sunaroad@gmail.com', $employee->name)->subject('A new candidate is invited to video interview!');
                });
            }

            $model->is_shared = 1;
            $model->save();

            return true;

        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function getCandidatesInPhoneScreen($job_id)
    {
        $candidates = Candidate::join('phone_screens', 'candidates.id', '=', 'phone_screens.candidate_id')
            ->where('phone_screens.job_id', $job_id)
            ->select('candidates.*', DB::raw('phone_screens.id AS phone_screen_id, phone_screens.is_invited AS invited_video_interview '))
            ->paginate(15);

        return $candidates;
    }

    public function invitedVideoInterview($job_id, $candidate_id)
    {
        $count = VideoInterview::where('job_id', $job_id)->where('candidate_id', $candidate_id)->count();
        return $count > 0 ? true : false;
    }
}