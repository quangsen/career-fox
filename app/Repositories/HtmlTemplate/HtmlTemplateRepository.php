<?php
namespace Fox\Repositories\HtmlTemplate;

use Fox\Repositories\AbstractRepository;
use Fox\HtmlTemplate;

use Fox\Repositories\Query\QueryOption;
use Illuminate\Pagination\Paginator;
use Validator;
use DB;

use Input;

class HtmlTemplateRepository extends AbstractRepository
{
    protected $rules = [
        'title' => 'required|min:3',
        'content' => 'required'
    ];


    protected $errors;

    protected $sortFields = [
        'title',
    ];

    protected $model;

    public function __construct(HtmlTemplate $model)
    {
        $this->model = $model;
        
        $this->template_path = public_path() . '/storage/templates/';
    }


    public function errors()
    {
        return $this->errors;
    }

    public function all(array $search = array(), QueryOption $searchOptions = null)
    {
        $searchFields = new HtmlTemplateSearchFields($search);
        Paginator::currentPageResolver(function () use ($searchOptions) {
            return $searchOptions->getPage();
        });

        $query = HtmlTemplate::query();
        if ($searchFields->keyword) {
            $keyword = '%' . $searchFields->keyword . '%';
            $query->where(function ($q) use ($keyword) {
                $q->orWhere('title', 'LIKE', $keyword);
            });
        }
        $sortBy = $searchOptions->getSortBy();
        if ($sortBy && in_array($sortBy, $this->sortFields)) {
            $query = $query->orderBy($sortBy, $searchOptions->getSortOrder());
        } else {
            $query = $query->orderBy('created_at', 'desc');
        }
        $query = $query->paginate($searchOptions->getLimit());
        return $query;
    }

    public function find($id)
    {
        return $this->getById($id);
    }

    public function findByIds(array $ids)
    {
        return HtmlTemplate::whereIn('id', $ids);
    }

    private function prepare(array $data)
    {
        return $data;
    }

    public function create(array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            $item = $this->prepare($data);
            $model = $this->model->newInstance($item);
            $model->save();
            if (!$model->id) {
                return false;
            }
            return $model;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function update($entity, array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            $item = $this->prepare($data, false);
            if (!$entity->update($item)) {
                return false;
            }
            return $entity;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function delete($id)
    {
        return HtmlTemplate::destroy($id);
    }
    
    public function upload($data)
    {
        try {
            global $user;
            
            $rules = [
                'title' => 'required',
                'template' => 'required|mimes:html,htm,doc,docx|max:2000'
            ];
        
            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            $data = [
                'title' => $data['title'],
                'user_id' => $user->id
            ];
            
            // upload file
            $template = Input::file('template');
            $data['attachment_name'] = $this->upload_file($template, $this->template_path);
            
            return $this->model->create($data);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
    
    protected function upload_file($file, $path) {
        if ($file) {
            $file_name = $file->getClientOriginalName();

            $i = 1;
            $file_path = $path . $file_name;
            while ( file_exists($file_path) ) {
                $file_name = $i . $file->getClientOriginalName();
                $file_path = $path . $file_name;
                $i++;
            }

            $file->move($path, $file_name);

            return $file_name;
        }
        
        return '';
    }
}