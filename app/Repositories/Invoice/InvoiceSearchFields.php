<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 8/30/2015
 * Time: 8:37 AM
 */

namespace Fox\Repositories\Invoice;

use Fox\Repositories\Query\TraitSearchFields;

class InvoiceSearchFields
{
    use TraitSearchFields;

    public $keyword;
}