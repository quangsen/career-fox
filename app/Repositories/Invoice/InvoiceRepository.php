<?php
namespace Fox\Repositories\Invoice;

use Fox\Repositories\AbstractRepository;
use Fox\Invoice;
use Fox\Repositories\Query\QueryOption;
use Illuminate\Pagination\Paginator;
use Validator;
use DB;

class InvoiceRepository extends AbstractRepository
{
    protected $rules = [
        'name' => 'required|min:3',
        'amount' => 'required',
        'status' => 'required|in:pending,sent,paid,cancelled',        
    ];


    protected $errors;

    protected $sortFields = [
        'name',
        'amount',
        'status',
    ];

    protected $model;

    public function __construct(Invoice $model)
    {
        $this->model = $model;
    }


    public function errors()
    {
        return $this->errors;
    }

    public function all(array $search = array(), QueryOption $searchOptions = null)
    {
        $searchFields = new InvoiceSearchFields($search);
        Paginator::currentPageResolver(function () use ($searchOptions) {
            return $searchOptions->getPage();
        });

        $query = Invoice::query();
        if ($searchFields->keyword) {
            $keyword = '%' . $searchFields->keyword . '%';
            $query->where(function ($q) use ($keyword) {
                $q->orWhere('name', 'LIKE', $keyword)
                    ->orWhere('amount', 'LIKE', $keyword)
                    ->orWhere('status', 'LIKE', $keyword)                    
                    ;
            });
        }
        $sortBy = $searchOptions->getSortBy();
        if ($sortBy && in_array($sortBy, $this->sortFields)) {
            $query = $query->orderBy($sortBy, $searchOptions->getSortOrder());
        } else {
            $query = $query->orderBy('created_at');
        }
        $query = $query->paginate($searchOptions->getLimit());
        return $query;
    }

    public function find($id)
    {
        return $this->getById($id);
    }

    public function findByIds(array $ids)
    {
        return Invoice::whereIn('id', $ids);
    }

    private function prepare(array $data)
    {
        return $data;
    }

    public function create(array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            $item = $this->prepare($data);
            $model = $this->model->newInstance($item);
            $model->save();
            if (!$model->id) {
                return false;
            }
            return $model;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function update($entity, array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            $item = $this->prepare($data, false);
            if (!$entity->update($item)) {
                return false;
            }
            return $entity;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function delete($id)
    {
        return Invoice::destroy($id);
    }
	
	public function monthWiseRevenue($get)
    {
        return $this->model->monthWiseRevenue($get);
    }
	
	public function monthWiseJob($get)
    {
        return $this->model->monthWiseJob($get);
    }
}