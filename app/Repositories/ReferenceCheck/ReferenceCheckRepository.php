<?php
namespace Fox\Repositories\ReferenceCheck;

use Fox\Repositories\AbstractRepository;
use Fox\Repositories\Query\QueryOption;
use Illuminate\Pagination\Paginator;
use Validator;

use Fox\ReferenceCheck;
use Fox\Candidate;
use Fox\OfferLetter;
use DB;


class ReferenceCheckRepository extends AbstractRepository
{
    protected $rules = [
        'quick_note' => 'required|min:3'
    ];


    protected $errors;

    protected $sortFields = [
        'question',
    ];

    protected $model;

    public function __construct(ReferenceCheck $model)
    {
        $this->model = $model;
    }


    public function errors()
    {
        return $this->errors;
    }

    public function all(array $search = array(), QueryOption $searchOptions = null)
    {
        $searchFields = new ReferenceCheckSearchFields($search);
        Paginator::currentPageResolver(function () use ($searchOptions) {
            return $searchOptions->getPage();
        });

        $query = ReferenceCheck::query();
        
        /*if ($searchFields->keyword) {
            $keyword = '%' . $searchFields->keyword . '%';
            $query->where(function ($q) use ($keyword) {
                $q->orWhere('question', 'LIKE', $keyword);
            });
        }*/
        
        $sortBy = $searchOptions->getSortBy();
        if ($sortBy && in_array($sortBy, $this->sortFields)) {
            $query = $query->orderBy($sortBy, $searchOptions->getSortOrder());
        } else {
            $query = $query->orderBy('created_at', 'desc');
        }
        $query = $query->paginate($searchOptions->getLimit());
        return $query;
    }

    public function find($id)
    {
        return $this->getById($id);
    }

    public function findByIds(array $ids)
    {
        return ReferenceCheck::whereIn('id', $ids);
    }

    private function prepare(array $data)
    {
        return $data;
    }


    public function update($entity, array $data)
    {
        try {
            $validator = Validator::make($data, ['quick_note' => 'required|min:2']);
            
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            $item = $this->prepare($data, false);
            if (!$entity->update($item)) {
                return false;
            }
            return $entity;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function delete($id)
    {
        return ReferenceCheck::destroy($id);
    }
    
    public function getCandidatesInReferenceCheck($job_id)
    {
        $candidates = Candidate::join('reference_checks', 'candidates.id', '=', 'reference_checks.candidate_id')
                ->where('reference_checks.job_id', $job_id)
                ->select('candidates.*', DB::raw('reference_checks.id AS reference_check_id, reference_checks.is_invited'))
                ->paginate(15);
        
        return $candidates;
    }
    
    /**
     * Invite a candidate to offer letter
     */
    public function invite($id, $model)
    {
        try {
            global $user;
            
            if ($model->is_invited) {
                return false;
            }

            $offer_letter = OfferLetter::firstOrCreate([
                'candidate_id' => $model->candidate_id,
                'job_id' => $model->job_id
            ]);
            $offer_letter->user_id = $user->id;
            $offer_letter->save();
            
            $model->is_invited = 1;
            $model->save();

            return $offer_letter;
            // send email to candidate
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
}