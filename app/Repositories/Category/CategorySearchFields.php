<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 8/30/2015
 * Time: 8:37 AM
 */

namespace Fox\Repositories\Category;

use Fox\Repositories\Query\TraitSearchFields;

class CategorySearchFields
{
    use TraitSearchFields;

    public $keyword;
}