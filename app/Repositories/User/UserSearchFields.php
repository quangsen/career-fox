<?php

namespace Fox\Repositories\User;

use Fox\Repositories\Query\TraitSearchFields;

class UserSearchFields
{
    use TraitSearchFields;

    public $keyword;
}