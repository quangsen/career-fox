<?php
namespace Fox\Repositories\Job;

use Fox\Events\JobWasCreated;
use Fox\Events\JobWasUpdated;
use Fox\Repositories\AbstractRepository;
use Fox\Job;
use Fox\Company;
use Fox\Category;

use Fox\Repositories\Query\QueryOption;
use Illuminate\Pagination\Paginator;
use Validator;
use DB;
use Event;

class JobRepository extends AbstractRepository
{
    protected $rules = [
        'title' => 'required|min:3',
        'category_id' => 'required|integer|exists:categories,id',
        'city_id' => 'required|integer|exists:cities,id',
        'postal' => 'required|min:5',
        'description' => 'required',
    ];


    protected $errors;

    protected $sortFields = [
        'title',
        'status',
        'age',
        'location',
    ];

    protected $model;

    public function __construct(Job $model)
    {
        $this->model = $model;
    }


    public function errors()
    {
        return $this->errors;
    }

    public function all(array $search = array(), QueryOption $searchOptions = null)
    {
        global $user;

        $searchFields = new JobSearchFields($search);
        Paginator::currentPageResolver(function () use ($searchOptions) {
            return $searchOptions->getPage();
        });

        $query = Job::query();
        $query->leftJoin('candidate_jobs', 'jobs.id', '=', 'candidate_jobs.job_id')
            ->join('provinces', 'jobs.province_id', '=', 'provinces.id')
            ->join('cities', 'jobs.city_id', '=', 'cities.id')
            ->select(DB::raw('jobs.*, provinces.name AS province, cities.name AS city, COUNT(candidate_jobs.id) as candidates'))
            ->groupBy('jobs.id');

        if ($user->hasRole('company') || $user->hasRole('recruiter') || $user->hasRole('employee') ) {
            $query->company($user->company_id);
        }

        if ($searchFields->keyword) {
            $keyword = '%' . $searchFields->keyword . '%';
            $query->where(function ($q) use ($keyword) {
                $q->orWhere('jobs.title', 'LIKE', $keyword);
            });
        }
        if ($searchFields->status) {
            $status = 0;
            switch ($searchFields->status) {
                case 'draft':
                    $status = 0;
                    break;

                case 'active':
                    $status = 1;
                    break;

                case 'inactive':
                    $status = -1;
                    break;
            }
            $query->where('jobs.status', '=', $status);
        }
        $sortBy = $searchOptions->getSortBy();
        if ($sortBy && in_array($sortBy, $this->sortFields)) {
            if ($sortBy == 'location') {
                $query->orderBy('jobs.city_name', $searchOptions->getSortOrder());
                $query->orderBy('jobs.province_name', $searchOptions->getSortOrder());
                $query->orderBy('jobs.country_name', $searchOptions->getSortOrder());
            } else if ($sortBy == 'age') {
                $query->orderBy('jobs.created_at', $searchOptions->getSortOrder());
            } else if ($sortBy == 'status') {
                $query->orderBy('jobs.status', $searchOptions->getSortOrder());
            } else {
                $query->orderBy('jobs.' . $sortBy, $searchOptions->getSortOrder());
            }
        } else {
            $query->orderBy('jobs.created_at', 'desc');
        }

        $result = $query->paginate($searchOptions->getLimit());


        return $result;
    }

    public function find($id)
    {
        return $this->getById($id);
    }

    public function findByIds(array $ids)
    {
        return Job::whereIn('id', $ids);
    }

    private function prepare(array $data)
    {
        return $data;
    }

    public function create(array $data)
    {
        try {
            $this->rules['company_id'] = 'required|integer|exists:companies,id';
            
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }

            $city_id = (int)$data['city_id'];
            $sql = 'SELECT ct.*, pv.name AS province, pv.country_id, co.name AS country'
                . ' FROM cities AS ct'
                . ' INNER JOIN provinces AS pv'
                . ' ON pv.id = ct.province_id'
                . ' INNER JOIN countries AS co'
                . ' ON co.id = pv.country_id'
                . ' WHERE ct.id = ' . $city_id
                . ' LIMIT 1';

            $city = DB::select(DB::raw($sql));

            if (!$city[0]) {
                return false;
            }

            $data['province_id'] = $city[0]->province_id;
            $data['country_id'] = $city[0]->country_id;
            $data['city_name'] = $city[0]->name;
            $data['province_name'] = $city[0]->province;
            $data['country_name'] = $city[0]->country;

            $item = $this->prepare($data);
            $model = $this->model->newInstance($item);
            $model->save();
            if (!$model->id) {
                return false;
            }

            /**
             * Fire event when a new candidate was created
             */
            Event::fire(new JobWasCreated($model));
            return $model;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function update($entity, array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }

            if (!Category::find($data['category_id'])) {
                $this->errors = 'Category does not exists';
                return false;
            }

            $item = $this->prepare($data, false);
            if (!$entity->update($item)) {
                return false;
            }
            /**
             * Fire event when a new candidate was created
             */
            Event::fire(new JobWasUpdated($entity));

            return $entity;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function delete($id)
    {
        $entity = $this->find($id);
        if (!$entity) {
            throw new \Exception('Job not found');
        }

        $result = Job::destroy($id);
        if ($result) {
            /**
             * Fire event when a new candidate was created
             */
            Event::fire(new JobWasCreated($entity));
        }
        return $result;
    }
    
    public function findByJobId($job_id)
    {
        return DB::table('questions')->where('job_id', $job_id)->get();
    }

}