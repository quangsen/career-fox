<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 8/30/2015
 * Time: 8:37 AM
 */

namespace Fox\Repositories\Job;

use Fox\Repositories\Query\TraitSearchFields;

class JobSearchFields
{
    use TraitSearchFields;

    public $keyword;
    public $status;
}