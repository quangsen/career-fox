<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 8/30/2015
 * Time: 8:37 AM
 */

namespace Fox\Repositories\OfferLetter;

use Fox\Repositories\Query\TraitSearchFields;

class OfferLetterSearchFields
{
    use TraitSearchFields;

    public $keyword;
}