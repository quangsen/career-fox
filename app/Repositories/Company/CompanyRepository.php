<?php
namespace Fox\Repositories\Company;

use Fox\Repositories\AbstractRepository;
use Fox\Company;
use Fox\Repositories\Query\QueryOption;
use Illuminate\Pagination\Paginator;
use Validator;

use DB;
use Fox\User;
use Fox\Job;
use Hash;

class CompanyRepository extends AbstractRepository
{
    protected $rules = [
        'name' => 'required|min:3',
        'first_name' => 'required',
        'last_name' => 'required',
        'phone' => 'required|min:3',
        'description' => 'required|min:3',
        'email' => 'required|email|unique:users,email',
        'password' => 'required',
        'primary_contact' => 'required'
    ];


    protected $errors;

    protected $sortFields = [
        'name',
        'email'
    ];

    protected $model;

    public function __construct(Company $model)
    {
        $this->model = $model;
    }


    public function errors()
    {
        return $this->errors;
    }

    public function all(array $search = array(), QueryOption $searchOptions = null)
    {
        $searchFields = new CompanySearchFields($search);
        Paginator::currentPageResolver(function () use ($searchOptions) {
            return $searchOptions->getPage();
        });

        $query = Company::query();
        if ($searchFields->keyword) {
            $keyword = '%' . $searchFields->keyword . '%';
            $query->where(function ($q) use ($keyword) {
                $q->orWhere('name', 'LIKE', $keyword)
                    ->orWhere('email', 'LIKE', $keyword);
            });
        }
        $sortBy = $searchOptions->getSortBy();
        if ($sortBy && in_array($sortBy, $this->sortFields)) {
            $query = $query->orderBy($sortBy, $searchOptions->getSortOrder());
        } else {
            $query = $query->orderBy('created_at', 'desc');
        }
        $query = $query->paginate($searchOptions->getLimit());
        return $query;
    }

    public function find($id)
    {
        return $this->getById($id);
    }

    public function findByIds(array $ids)
    {
        return Company::whereIn('id', $ids);
    }

    private function prepare(array $data)
    {
        return $data;
    }

    public function create(array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            $item = $this->prepare($data);
            
            $model = $this->model->createCompany($item);
            
            return $model;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function update($entity, array $data)
    {
        try {
            $user_id = $entity->user->id;
            
            $this->rules['email'] = 'required|email|unique:users,email,' . $user_id;
            unset($this->rules['password']);
            
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            $item = $this->prepare($data, false);
            
            if (!$entity->update($item)) {
                return false;
            }
            
            $user = User::find($user_id);
            $user->first_name = $data['first_name'];
            $user->last_name = $data['last_name'];
            $user->name = $data['name'];
            $user->phone = $data['phone'];
            $user->email = $data['email'];
            $user->save();
            
            return $entity;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function delete($entity, $id)
    {
        // delete users
        User::where('company_id', $id)->delete();
        // delete jobs
        Job::where('company_id', $id)->delete();
        // delete job related items (candidate, phonescreen, interview, ...)
        return Company::destroy($id);
    }
}