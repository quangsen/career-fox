<?php
namespace Fox\Repositories\Survey;

use Fox\Repositories\AbstractRepository;
use Fox\Survey;

use Fox\Repositories\Query\QueryOption;
use Illuminate\Pagination\Paginator;
use Validator;
use DB;

class SurveyRepository extends AbstractRepository
{
    protected $rules = [
        'title' => 'required|min:3',
        'job_id' => 'integer|required|exists:jobs,id',
    ];


    protected $errors;

    protected $sortFields = [
        'title',
    ];

    protected $model;

    public function __construct(Survey $model)
    {
        $this->model = $model;
    }


    public function errors()
    {
        return $this->errors;
    }

    public function all(array $search = array(), QueryOption $searchOptions = null)
    {
        $searchFields = new SurveySearchFields($search);
        Paginator::currentPageResolver(function () use ($searchOptions) {
            return $searchOptions->getPage();
        });

        $query = Survey::query();
        if ($searchFields->keyword) {
            $keyword = '%' . $searchFields->keyword . '%';
            $query->where(function ($q) use ($keyword) {
                $q->orWhere('title', 'LIKE', $keyword);
            });
        }
        if ($searchFields->job_id) {
            $query->where('job_id', '=', (int)$searchFields->job_id);
        }
        if ($searchFields->company_id) {
            $query->where('company_id', '=', (int)$searchFields->company_id);
        }

        $sortBy = $searchOptions->getSortBy();
        if ($sortBy && in_array($sortBy, $this->sortFields)) {
            $query = $query->orderBy($sortBy, $searchOptions->getSortOrder());
        } else {
            $query = $query->orderBy('created_at', 'desc');
        }
        $query = $query->paginate($searchOptions->getLimit());
        return $query;
    }

    public function find($id)
    {
        return $this->getById($id);
    }

    public function findForJob($jobId, $id)
    {
        return $this->model->query()
            ->where('id', '=', (int)$id)
            ->where('job_id', '=', (int)$jobId)
            ->firstOrFail();
    }

    public function findByIds(array $ids)
    {
        return Survey::whereIn('id', $ids);
    }

    private function prepare(array $data)
    {
        return $data;
    }

    public function create(array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }

            $item = $this->prepare($data);
            $model = $this->model->newInstance($item);
            $model->save();
            if (!$model->id) {
                return false;
            }
            return $model;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function update($entity, array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }

            $item = $this->prepare($data, false);
            if (!$entity->update($item)) {
                return false;
            }
            return $entity;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function delete($id)
    {
        return Survey::destroy($id);
    }
}