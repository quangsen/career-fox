<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 8/30/2015
 * Time: 8:37 AM
 */

namespace Fox\Repositories\Survey;

use Fox\Repositories\Query\TraitSearchFields;

class SurveyQuestionSearchFields
{
    use TraitSearchFields;

    public $keyword;
    public $survey_id;
}