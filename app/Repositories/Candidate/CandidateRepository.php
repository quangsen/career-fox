<?php
namespace Fox\Repositories\Candidate;

use Fox\Repositories\AbstractRepository;

use Fox\Candidate;

use Fox\Repositories\Query\QueryOption;
use Illuminate\Pagination\Paginator;
use Validator;
use DB;
use Input;

use Fox\User;

class CandidateRepository extends AbstractRepository
{
    protected $rules = [
        'first_name' => 'required|min:3',
        'last_name' => 'required|min:3',
        'resume' => 'required|mimes:pdf,doc,docx',
        'cover_letter' => 'mimes:pdf,doc,docx',
        'job_title' => 'required',
        'email' => 'required|email|unique:users,email',
        'photo' => 'mimes:jpg,gif,png,jpeg',
        //'username' => 'required|min:3|unique:users,username',
        'password' => 'required|min:6',
        'address' => 'required|min:10',
        //'city' => 'required|min:2',
        'city_id' => 'required|integer|exists:cities,id',
        //'province' => 'required|min:2',
        'phone' => 'required|min:8'
    ];


    protected $errors;

    protected $sortFields = [
        'name',
        'email'
    ];

    protected $model;

    public function __construct(Candidate $model)
    {
        $this->model = $model;
        
        $this->photo_path = public_path() . '/storage/candidate_photos/';
        $this->cover_letter_path = public_path() . '/storage/cover_letters/';
        $this->resume_path = public_path() . '/storage/resumes/';
    }


    public function errors()
    {
        return $this->errors;
    }

    public function all(array $search = array(), QueryOption $searchOptions = null, $job_id = null)
    {
        $searchFields = new CandidateSearchFields($search);
        Paginator::currentPageResolver(function () use ($searchOptions) {
            return $searchOptions->getPage();
        });

        $query = Candidate::query();
        if ($job_id > 0) {
            $query->join('candidate_jobs', 'candidates.user_id', '=', 'candidate_jobs.user_id')
                    ->leftJoin('phone_screens', 'candidates.id', '=', 'phone_screens.candidate_id')
                    ->where('candidate_jobs.job_id', $job_id)
                    ->select('candidates.*', DB::raw('COUNT(phone_screens.id) AS is_invited'))
                    ->groupBy('candidates.id')
                ;
        }
        if ($searchFields->keyword) {
            $keyword = '%' . $searchFields->keyword . '%';
            $query->where(function ($q) use ($keyword) {
                $q->orWhere('name', 'LIKE', $keyword)
                    ->orWhere('email', 'LIKE', $keyword);
            });
        }
        
        if (isset($searchFields->days)) {
            $days = (int) $searchFields->days < 180 ? (int) $searchFields->days : 180;
            $date = new \DateTime();
            $date->sub(new \DateInterval("P{$days}D"));
            $date_limit = $date->format('Y-m-d');
            
            $query->where('created_at', '>=', $date_limit);
        }
        
        $sortBy = $searchOptions->getSortBy();
        if ($sortBy && in_array($sortBy, $this->sortFields)) {
            $query = $query->orderBy($sortBy, $searchOptions->getSortOrder());
        } else {
            $query = $query->orderBy('created_at', 'desc');
        }
        $query = $query->paginate($searchOptions->getLimit());
        return $query;
    }

    public function find($id)
    {
        return $this->getById($id);
    }

    public function findByIds(array $ids)
    {
        return Candidate::whereIn('id', $ids);
    }

    private function prepare(array $data)
    {
        return $data;
    }
    
    protected function upload_file($file, $path) {
        if ($file) {
            $file_name = $file->getClientOriginalName();

            $i = 1;
            $file_path = $path . $file_name;
            while ( file_exists($file_path) ) {
                $file_name = $i . $file->getClientOriginalName();
                $file_path = $path . $file_name;
                $i++;
            }

            $file->move($path, $file_name);

            return $file_name;
        }
        
        return '';
    }

    public function create(array $data)
    {
        $this->rules['resume'] = 'required|mimes:pdf,doc,docx';
        
        try {
            $validator = Validator::make($data, $this->rules);
            
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            // upload photo image
            $photo = Input::file('photo');
            $data['photo'] = $this->upload_file($photo, $this->photo_path);
            
            // upload resume
            $resume = Input::file('resume');
            $data['resume'] = $this->upload_file($resume, $this->resume_path);
            
            // upload resume
            $cover_letter = Input::file('cover_letter');
            $data['cover_letter'] = $this->upload_file($cover_letter, $this->cover_letter_path);
            
            
            //$item = $this->prepare($data);
            return $this->model->createCandidate($data);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function update($entity, array $data)
    {
        global $user;
        
        try {
            
            $this->rules['resume'] = 'required|mimes:pdf,doc,docx';
            $this->rules['email'] = 'required|email|unique:users,email,' . $user->id;
            //unset($this->rules['username']);
            unset($this->rules['city_id']);
            
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            $item = $this->prepare($data, false);
            
            // upload photo image
            $photo = Input::file('photo');
            $item['photo'] = $this->upload_file($photo, $this->photo_path);
            
            // upload resume
            $resume = Input::file('resume');
            $item['resume'] = $this->upload_file($resume, $this->resume_path);
            
            // upload resume
            $cover_letter = Input::file('cover_letter');
            $item['cover_letter'] = $this->upload_file($cover_letter, $this->cover_letter_path);
            
            // update users table first
            $user = User::find($user->id);
            $user->phone = $data['phone'];
            $user->first_name = $data['first_name'];
            $user->last_name  = $data['last_name'];
            $user->phone = $data['phone'];
            $user->email = $data['email'];
            $user->save();
            
            // update candidates table
            if (!$entity->update($item)) {
                return false;
            }
            return $entity;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function delete($id)
    {
        return Candidate::destroy($id);
    }
    
    public function getCandidateByJob($job_id)
    {
        return Candidate::join('candidate_jobs', 'candidates.user_id', '=', 'candidate_jobs.user_id')
                ->whereNotExists(function($query) {
                    $query->select(DB::raw(1))
                          ->from('phone_screens')
                          ->whereRaw('candidate_id = candidates.id AND job_id = candidate_jobs.job_id');
                })
                ->where('candidate_jobs.job_id', '=', $job_id)
                ->select('candidates.*')
            ->paginate(15);
    }
}