<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

class ReferenceCheckContact extends Model
{
    protected $table = 'reference_contacts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'phone', 'reference_check_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
    
    public function job() {
        return $this->belongsTo('Fox\Job');
    }
}
