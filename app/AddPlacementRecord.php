<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

class AddPlacementRecord extends Model
{
    protected $table = 'add_placement_record';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
}
