<?php

namespace Fox\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Fox\Events\CandidateWasCreated' => [
            'Fox\Listeners\MaxHireAddPerson'
        ],

        'Fox\Events\JobWasCreated' => [
            'Fox\Listeners\MaxHireAddJob'
        ],

        'Fox\Events\JobWasUpdated' => [
            'Fox\Listeners\MaxHireUpdateJob'
        ],

        'Fox\Events\JobWasDeleted' => [
            'Fox\Listeners\MaxHireDeleteJob'
        ],

        'Fox\Events\UserHasAppliedJob' => [
            'Fox\Listeners\UserHasAppliedJobHandler'
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
