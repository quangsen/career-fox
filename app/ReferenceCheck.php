<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

class ReferenceCheck extends Model
{
    protected $table = 'reference_checks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'job_id', 'candidate_id', 'quick_note'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
    
    public function contacts()
    {
        return $this->hasMany('Fox\ReferenceCheckContact');
    }
}
