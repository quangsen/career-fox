<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'demo'], function () {
    Route::get('/', 'Frontend\JobController@index');

    Route::get('jobs/{job_id}/confirm', 'Frontend\JobController@confirm');

    Route::get('jobs/{id}', ['as' => 'demo.job.id', 'uses' => 'Frontend\JobController@show'])->where('id', '[0-9]+');
    Route::post('jobs/{id}', ['as' => 'demo.job.id', 'uses' => 'Frontend\JobController@applyJob'])->where('id', '[0-9]+');
    Route::post('jobs/{id}/confirm', 'Frontend\JobController@registerCandidate')->where('id', '[0-9]+');
    Route::get('thankyou', ['as' => 'thankyou', 'uses' => 'Frontend\JobController@thankYou']);

    Route::post('login', 'Frontend\JobController@login');
    Route::get('logout', 'Frontend\JobController@logOut');

    //Route::get('getProvinces/{id}', 'Frontend\JobController@getProvincesByCountry');
    Route::get('getProvinces/{id}', 'Frontend\JobController@getProvincesByCountry');
    Route::get('getCities/{id}', 'Frontend\JobController@getCitiesByProvince');

    Route::get('jobfeed', 'Frontend\JobController@getJobFeed');
});

/**
 * This View is used for Single Page Application
 */
Route::get('/', function () {
    return view('spa');
});

Route::post('/api/v1/login', 'AuthController@login');


Route::group(['prefix' => 'api/v1', 'middleware' => ['jwt.auth']], function () {
    Route::get('/me', 'AuthController@me');
    Route::get('cities', 'CityController@index');
    Route::get('provinces', 'ProvinceController@index');

    Route::resource('companies', 'CompanyController', ['except' => ['create', 'edit']]);
	Route::get('companies', 'CompanyController@index');
    Route::resource('categories', 'CategoryController', ['except' => ['create', 'edit', 'show']]);

    Route::resource('jobs', 'JobController', ['except' => ['create', 'edit']]);
    Route::post('post_job', 'JobController@activeJob');

    Route::resource('jobs.questions', 'JobQuestionController', ['except' => ['create', 'edit']]);

    Route::post('invoices', 'InvoiceController@store');
    Route::put('invoices/{invoice_id}', 'InvoiceController@update');
    Route::get('invoices/{invoice_id}', 'InvoiceController@show');
    Route::delete('invoices/{invoice_id}', 'InvoiceController@destroy');
    Route::get('revenue', 'InvoiceController@revenue');
	Route::get('invoices', 'InvoiceController@index');


    Route::resource('candidates', 'CandidateController', ['except' => ['create', 'edit']]);
    Route::post('apply_job', 'CandidateController@applyJob');

    Route::get('employees', 'UserController@getEmployees');
    //Route::get('user-detail', 'UserController@getUserDetail');
    Route::resource('users', 'UserController', ['except' => ['create', 'edit']]);

    /*========== REVIEW CANDIDATE SECTION ==========*/
    Route::get('jobs/{job_id}/candidates', 'CandidateController@index');
    Route::get('jobs/{job_id}/review-candidates', 'CandidateController@getCandidateByJob');

    // invite candidate to phone screen, @params: candidate_ids
    Route::post('jobs/{job_id}/phone-invitations', 'PhoneScreenController@inviteToPhoneScreen')->where('job_id', '[0-9]+');

    /*========== PHONE SCREEN SECTION ==========*/
    Route::get('jobs/{job_id}/phone-invitations', 'PhoneScreenController@getCandidatesInPhoneScreen');

    // Route::get('jobs/{job_id}/candidates/{candidate_id}', 'PhoneScreenController@getCandidateDetailsInPhoneScreen')->where(['job_id' => '[0-9]+', 'candidate_id' => '[0-9]+']);
    // save phonescreen note for candidates, @params: notes
    Route::post('jobs/{job_id}/phone-invitations/candidate/{candidate_id}/notes', 'PhoneScreenController@addNoteCandidate')->where(['job_id' => '[0-9]+', 'candidate_id' => '[0-9]+']);

    Route::post('phone-invitations/{phone_id}/share', 'PhoneScreenController@share'); // share 1 candidate from phonescreen
    Route::post('phone-invitations/{phone_id}/video-invitation', 'PhoneScreenController@invite'); // invite candidate to video interview

    Route::put('phone-invitations/{phone_id}', 'PhoneScreenController@update'); // update note on phonescreen
    Route::get('jobs/{job_id}/phone-invitations/{phone_id}', 'PhoneScreenController@show'); // get phonescreen detail

    /*========== VIDEO INTERVIEW SECTION ==========*/
    Route::get('jobs/{job_id}/video-invitations', 'VideoInterviewController@getCandidatesInVideoInterview');
    Route::post('video-invitations/{video_interview_id}/questionnaire-invitation', 'VideoInterviewController@invite');

    Route::get('video-invitations/{video_interview_id}', 'VideoInterviewController@show');
    Route::put('video-invitations/{video_interview_id}', 'VideoInterviewController@update');

    Route::resource('jobs.video-interviews', 'VideoInterviewController', ['except' => ['create', 'edit']]);

    /*==========  REVIEW QUESTIONNAIRE AND RESULTS SECTION ==========*/

    Route::get('jobs/{job_id}/questionnaire-invitations', 'QuestionnaireController@getCandidatesInQuestionnaire'); // get candidates list in questionnaire result screen
    Route::post('questionnaire-invitations/{questionnaire_id}/interview-invitation', 'QuestionnaireController@invite'); // inviate a candidate to next step
    Route::get('questionnaire-invitations/{questionnaire_id}', 'QuestionnaireController@show'); // get candidate detail
	

    /*========== INTERVIEW NOTE SECTION ==========*/
    Route::get('jobs/{job_id}/interview-invitations', 'InterviewController@getCandidatesInInterview');

    Route::resource('interview-invitations', 'InterviewController', ['except' => ['create', 'edit', 'destroy', 'index']]);
    Route::resource('interview-invitations.notes', 'InterviewNoteController', ['except' => ['create', 'edit']]);


    /*========== REFERENCE CHECKS SECTION ==========*/
    Route::post('templates/upload', 'HtmlTemplateController@upload');
    Route::resource('templates', 'HtmlTemplateController', ['except' => ['create', 'edit']]);
    Route::get('jobs/{job_id}/reference-checks', 'ReferenceCheckController@getCandidatesInReferenceCheck'); // get candidate list in reference checks

    Route::get('reference-checks/{reference_id}', 'ReferenceCheckController@show'); // get candidate detail
    Route::put('reference-checks/{reference_id}', 'ReferenceCheckController@update'); // add quick_note
    Route::post('reference-checks/{reference_id}/offer-invitation', 'ReferenceCheckController@invite'); // invite a candidate to next step
    Route::resource('reference-checks.contacts', 'ReferenceCheckContactController', ['except' => ['create', 'edit']]);

    /*========== OFFER LETTERS SECTION ==========*/
    Route::get('jobs/{job_id}/offer-invitations', 'OfferLetterController@getCandidatesInOfferLetter'); // get candidate list in offer letters
    Route::post('jobs/{job_id}/send-offers', 'OfferLetterController@sendOffers'); // send offers to selected candidates
    //Route::resource('offer_letters', 'OfferLetterController', ['except' => ['create', 'edit']]);
	//Route::get('jobs/monthly-job', 'OfferLetterController@monthlyJob');
	Route::get('monthly-job', 'InvoiceController@monthlyJob');


    /*========== PLACEMENT RECORD SECTION ==========*/
    //Route::resource('add_placement_record', 'AddPlacementRecordController', ['except' => ['create', 'edit']]);

    // Survey
    Route::resource('jobs.surveys', 'SurveyController', ['except' => ['create', 'edit']]);
    Route::resource('surveys.questions', 'SurveyQuestionController', ['except' => ['create', 'edit']]);

    // get list of users that the authenticated users can chat with
    Route::get('messages/users', 'MessageUserController@index');

    // get list of chats with the user
    Route::get('messages/users/{id}', 'MessageUserController@show');

    // send message to particular user
    Route::post('messages/users/{id}', 'MessageUserController@store');

    // get inbox messages
    Route::get('messages/inbox', 'MessageController@inbox');
     
    // get sent messages
    Route::get('messages/sent', 'MessageController@sent');
     
    // get replies to particular message
    Route::get('messages/{id}/replies', 'MessageController@replies');
    
    // send reply to particular message
    Route::post('messages/{id}', 'MessageController@store');
    Route::resource('messages', 'MessageController', ['except' => ['edit', 'update']]);
});


/*========== RivsAPI ROUTES ==========*/
Route::group(['prefix' => 'api/rivs', 'middleware' => ['']], function () {
    Route::resource('candidates', 'Rivs\\CandidatesController', ['except' => ['create', 'edit', 'destroy']]);
    Route::resource('requisitions', 'Rivs\\RequisitionsController', ['except' => ['create', 'edit', 'destroy']]);
    Route::resource('workflows', 'Rivs\\WorkflowsController', ['except' => ['create', 'edit', 'destroy']]);
});

/*========== ASCENTII ==========*/
Route::get('ascentii', 'AscentiiController@getSearchPage');
Route::get('ascentii/candidates', 'AscentiiController@getCandidateScores');
Route::post('ascentii/send', 'AscentiiController@sendRequest');

Route::group(['prefix' => 'maxhire'], function () {
    Route::get('/', function () {
        $person = new Fox\Api\MaxHire\Resource\Person();
        $candidate = \Fox\Candidate::find(4);
        $result = $person->add($candidate);
        var_dump($result);
    });
});