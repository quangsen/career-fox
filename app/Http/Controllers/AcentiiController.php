<?php

namespace Fox\Http\Controllers;

use Illuminate\Http\Request;

use Fox\Http\Requests;
use Fox\Http\Controllers\Controller;
use Input;

class AscentiiController extends Controller
{
    protected $client;
    protected $username, $password,$url, $default_url, $home_url;

    public function __construct() {
    	$this->username = \Config::get('ascentii.login.username');
    	$this->password = \Config::get('ascentii.login.password');
    	$this->url = \Config::get('ascentii.login.url');
    	$this->default_url = 'http://www.ascentiiassessments.com/RF/Scripts/default.asp?';
    	$this->home_url = 'http://www.ascentiiassessments.com/RF/Scripts/AdminHome.asp?NLEmail=careerfox@gmail.com&NLCID=2047';
    }

    /**
     * Login to Acentii, post to Generate New Assesment and get the resulting page.
     * @return [type] [description]
     */
    public function getSearchPage() {
    	$data = "txtLoginID=".$this->username."&txtPassword=".$this->password."&cboDataSource=CentACS";
    	$post_data = "hidScreenName=COMPANYURL&hidScreenCompanyID=136&hidScreenRefresh=N&hidScreenAction=WhoCares&hidScreenCandidateID=4505246717531652256C3F727A2C653A4C184719092B0B473D122B0469401A75462054405F3C472466017E164F214C2B7A0C6F10572C&cboStatus=0&cboRole=0&cboRoleFitIndex=All&txtPersonLast=&cboOrderBy=1";
    	$this->login($this->url, $data);
    	$this->post_data($this->default_url, $post_data);
    	$crawler = $this->grab_page($this->default_url);

    	return $crawler;
    }

    /**
     * Sending request to page, needs job category and subcategory.
     * @param  [type] $job_family [description]
     * @param  [type] $job_id     [description]
     * @return [type]             [description]
     */
    public function sendRequest() {
    	/**
    	 * These variables need to be submitted when posting to send request. They are taken from select options in the previous function - getSearchPage.
    	 * @var [type]
    	 */
    	$job_family = Input::get('cboTraitFit');
    	$job_id = Input::get('cboJob');
    	$hidden_screen = Input::get('hidScreenName');
    	$hidden_compid = Input::get('hidScreenCompanyID');
    	$hidden_action = Input::get('hidScreenAction');
    	$hidden_candid = Input::get('hidScreenCandidateID');
    	$hidden_scrpos = Input::get('hidScreenPositionDescID');
    	$hidden_emailid = Input::get('hidScreenEmailID');
    	
    	$login_data = "txtLoginID=".$this->username."&txtPassword=".$this->password."&cboDataSource=CentACS";

    	$post_data = "hidScreenName=".$hidden_screen."&hidScreenCompanyID=".$hidden_compid."&hidScreenAction=".$hidden_action."&hidScreenCandidateID=".$hidden_candid."&hidScreenPositionDescID=".$hidden_scrpos."&hidScreenEmailID=".$hidden_emailid."&cboTraitFit=".$job_family."&cboJob=".$job_id;
    	/** login */
    	$this->login($this->url, $login_data);
    	/** send email */
    	$this->post_data($this->default_url, $post_data);
    	$crawler = $this->grab_page($this->default_url);

    	return $crawler;
    }

    public function getCandidateScores() {
    	$data = "txtLoginID=".$this->username."&txtPassword=".$this->password."&cboDataSource=CentACS";
    	$this->login($this->url, $data);
    	$crawler = $this->grab_page($this->home_url);
    	
    	return $crawler;
    }

    protected function login($url,$data){
	    $fp = fopen("cookie.txt", "w");
	    fclose($fp);
	    $login = curl_init();
	    curl_setopt($login, CURLOPT_COOKIEJAR, "./temp/scookie.txt");
	    curl_setopt($login, CURLOPT_COOKIEFILE, "./temp/cookie.txt");
	    curl_setopt($login, CURLOPT_TIMEOUT, 40000);
	    curl_setopt($login, CURLOPT_RETURNTRANSFER, TRUE);
	    curl_setopt($login, CURLOPT_URL, $url);
	    curl_setopt($login, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
	    curl_setopt($login, CURLOPT_FOLLOWLOCATION, TRUE);
	    curl_setopt($login, CURLOPT_POST, TRUE);
	    curl_setopt($login, CURLOPT_POSTFIELDS, $data);
	    ob_start();
	    return curl_exec ($login);
	    ob_end_clean();
	    curl_close ($login);
	    unset($login);    
	}                  
	 
	protected function grab_page($site){
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	    curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 40);
	    curl_setopt($ch, CURLOPT_COOKIEFILE, "./temp/scookie.txt");
	    curl_setopt($ch, CURLOPT_URL, $site);
	    ob_start();
	    return curl_exec ($ch);
	    ob_end_clean();
	    curl_close ($ch);
	}
	 
	protected function post_data($site,$data){
	    $datapost = curl_init();
	        $headers = array("Expect:");
	    curl_setopt($datapost, CURLOPT_URL, $site);
	        curl_setopt($datapost, CURLOPT_TIMEOUT, 40000);
	    curl_setopt($datapost, CURLOPT_HEADER, TRUE);
	        curl_setopt($datapost, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($datapost, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
	    curl_setopt($datapost, CURLOPT_POST, TRUE);
	    curl_setopt($datapost, CURLOPT_POSTFIELDS, $data);
	        curl_setopt($datapost, CURLOPT_COOKIEFILE, "cookie.txt");
	    ob_start();
	    return curl_exec ($datapost);
	    ob_end_clean();
	    curl_close ($datapost);
	    unset($datapost);    
	}
}
