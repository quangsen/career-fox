<?php

namespace Fox\Http\Controllers;

use Fox\Transformer\VideoInterviewTransformer;
use Fox\Transformer\CandidateTransformer;
use Fox\Transformer\QuestionnaireTransformer;
use Fox\Transformer\QuestionVideoTransformer;
use Fox\Api\Rivs\Video\Crawler;
use Fox\Api\Rivs\Video\Extractor;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Fox\Repositories\VideoInterview\VideoInterviewRepository;
use Input;
use Config;
use DB;

class VideoInterviewController extends ApiController
{

    protected $allowedFields = [
        'search' => ['keyword']
    ];

    protected $repository;

    public function __construct(Response $response, VideoInterviewRepository $repository)
    {
        $this->loadAndAuthorizeResource();
        
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        global $user;
        
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorNotFound('Could not found the given Object');
        }
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        $video_interview_transformer = new VideoInterviewTransformer();
        $video_interview_transformer->defaultIncludes[] = 'candidate';
        
        return $this->response->withItem($entity, $video_interview_transformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        global $user;
        
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorWrongArgs('video_interview_id could not be found');
        }
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        $post = Input::all();
        if ($entity = $this->repository->update($entity, $post)) {
            return $this->response->withItem($entity, new VideoInterviewTransformer());
        }
        $errors = $this->repository->errors();
        $msg = 'Input data was wrong';
        if ($errors) {
            $msg = $msg . ' [' . $errors->first() . ']';
        }
        return $this->response->errorWrongArgs($msg);
    }

    
    /**
     * get list of candidates that are invited to video interview
     */
    public function getCandidatesInVideoInterview($job_id) {
        global $user;
       // if(isset($_POST['candidate_id'])){
		   $post = Input::all();
		   if(!empty($post['candidate_id']) && !empty($post['getReqCadId']))
		   {
				$candidate_jobs = DB::table('candidate_jobs')
                    ->where('user_id', $post['candidate_id'])
                    ->Where('job_id', $job_id)
                    ->get();
					
					if(!empty($candidate_jobs))
					{
						$rivsConfig = Config::get('rivs.dashboard');
						$tmp_fname = $_SERVER['DOCUMENT_ROOT'].'cookie.txt';
						$options = array('username'=>$rivsConfig['account']['username'],'password'=>$rivsConfig['account']['password'],'cookieFilePath'=>$tmp_fname);
						$Crawler = new Crawler($options);
						$content = $Crawler->getContent('https://v3.rivs.com/requisitionCandidate/'.$candidate_jobs->req_candidate_id);
						$Extractor = new Extractor($content);
						//echo "<pre>";
						//print_r($Extractor->extract());
						//exit;
						$obj = $Extractor->extract();
						return json_encode(array('data' => $obj));
						//return $this->response->withItem($obj, new QuestionVideoTransformer());
					}else{
						return json_encode(array('data' => "No Video found"));
					}
		   }
		//}
		
		
			
			
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        $candidates = $this->repository->getCandidatesInVideoInterview($job_id);
        $candidate_transformer = new CandidateTransformer();
        $candidate_transformer->is_invited_to_video_interview = true;
        return $this->response->withPaginator($candidates, $candidate_transformer);
    }
    
    /**
     * Invite a candidate to questionnaire
     */
    public function invite($video_interview_id)
    {
        global $user;

        $entity = $this->repository->find($video_interview_id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Video Interview Id could not be found');
        }
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }

        $questionnaire = $this->repository->invite($video_interview_id, $entity);

        if ($questionnaire) {
            return $this->response->withItem($questionnaire, new QuestionnaireTransformer());
        } else {
            return $this->response->errorUnwillingToProcess('This candidate is already invited to questionnaire. He can not be invited again');
        }
    }
}
