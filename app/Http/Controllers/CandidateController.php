<?php

namespace Fox\Http\Controllers;

use Fox\Transformer\CandidateTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Fox\Repositories\Candidate\CandidateRepository;
use Fox\Repositories\Job\JobRepository;
use Input;

use Fox\Candidate;
use Fox\Job;
use Fox\User;
use DB;

use Validator;

class CandidateController extends ApiController
{
    protected $allowedFields = [
        'search' => ['keyword', 'days']
    ];

    protected $repository;

    public function __construct(Response $response, CandidateRepository $repository, JobRepository $job_repository)
    {
        $this->loadAndAuthorizeResource();
        
        parent::__construct($response);
        $this->repository = $repository;
        $this->job_repository = $job_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($job_id = null)
    {
        global $user;
        
        if ($job_id > 0) {
            $entity = $this->job_repository->find($job_id);
            if (!$entity) {
                return $this->response->errorNotFound('Job not found');
            }
            
            // check if the company has permission on this job
            if (! $this->companyHasJob($user->company_id, $job_id) ) {
                return $this->response->errorUnauthorized('You are not the owner of this job');
            }
        }
        
        $searchOptions = $this->createQueryOptionsFromRequest();
        $searchFields = $this->searchFields;
        $items = $this->repository->all($searchFields, $searchOptions, $job_id);

        if ($job_id) {
            $candidate_transformer = new CandidateTransformer();
            $candidate_transformer->has_job = true;
            return $this->response->withPaginator($items, $candidate_transformer);
        } else {
            return $this->response->withPaginator($items, new CandidateTransformer());
        }
    }
    
    public function getCandidateByJob($job_id)
    {
        global $user;
        
        // check if the company has permission on this job
        if (! $this->companyHasJob($user->company_id, $job_id) ) {
            return $this->response->errorUnauthorized('You are not the owner of this job');
        }
        
        $items = $this->repository->getCandidateByJob($job_id);
                
        return $this->response->withPaginator($items, new CandidateTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorNotFound('Candidate not found');
        }
        
        // update views number of candidate
        $candidate = Candidate::find($id);
        $candidate->views++;
        $candidate->save();
        
        $entity = $this->repository->find($id);
        return $this->response->withItem($entity, new CandidateTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            $post = Input::all();
            //$post['user_id'] = $this->getCurrentUser()->id;
            
            if ($model = $this->repository->create($post)) {
                return $this->response->withItem($model, new CandidateTransformer());
            }
            $errors = $this->repository->errors();
            $msg = 'Input data was wrong';
            if ($errors) {
                $msg = $msg . ' [' . $errors->first() . ']';
            }
            return $this->response->errorWrongArgs($msg);
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        global $user;

        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Object Id could not be found');
        }
        
        if ($user->role != 1 || $user->candidate->id != $id) {
            return $this->response->errorWrongArgs('Permission denied');
        }
        
        $post = Input::all();
        if ($entity = $this->repository->update($entity, $post)) {
            return $this->response->withItem($entity, new CandidateTransformer());
        }
        $errors = $this->repository->errors();
        $msg = 'Input data was wrong';
        if ($errors) {
            $msg = $msg . ' [' . $errors->first() . ']';
        }
        return $this->response->errorWrongArgs($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorWrongArgs('Job not found');
        }
        $this->repository->delete($id);
        return $this->response->withArray(['id' => $id]);
    }
    
    public function applyJob() {
        global $user;
        
        /*if (!$this->isCandidate()) {
            return $this->response->errorWrongArgs('Only candiates can apply for jobs');
        }*/
        
        $input = Input::all();
        
        $validator = Validator::make($input, ['job_id' => 'required|integer|exists:jobs,id']);
            
        if ($validator->fails()) {
            $msg = 'Input data was wrong';
            $msg = $msg . ' [' . $validator->errors()->first() . ']';
            return $this->response->errorWrongArgs($msg);
        }
        
        $job_id = (int) $input['job_id'];
        $job = Job::find($job_id);
        
        // check if candidate has applied
        $candidate_job = Candidate::join('candidate_jobs', 'candidate_jobs.user_id', '=', 'candidates.user_id')
                        ->where('candidates.user_id', $user->id)
                        ->where('candidate_jobs.job_id', $job_id)
                        ->get();
        
        

        if (count($candidate_job) > 0) {
            $msg = 'You already applied this job';
            return $this->response->withArray(['job_id' => $job->id, 'message' => $msg]);
        }
        
        // set pivot table
        User::find($user->id)->jobs()->attach($job->id, ['company_id' => $job->company_id]);
        
        $msg = 'You applied this job successfully';
        return $this->response->withArray(['job_id' => $job->id, 'message' => $msg]);
    }
}
