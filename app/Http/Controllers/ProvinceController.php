<?php

namespace Fox\Http\Controllers;

use Fox\Transformer\ProvinceTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Fox\Repositories\Province\ProvinceRepository;
use Input;


class ProvinceController extends ApiController
{

    protected $allowedFields = [
        'search' => ['keyword']
    ];

    protected $repository;

    public function __construct(Response $response, ProvinceRepository $repository)
    {
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $searchOptions = $this->createQueryOptionsFromRequest();
        $searchFields = $this->searchFields;
        $items = $this->repository->all($searchFields, $searchOptions);
        return $this->response->withPaginator($items, new ProvinceTransformer());
    }
    
}
