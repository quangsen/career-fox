<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/14/2015
 * Time: 11:09 PM
 */

namespace Fox\Http\Controllers;

use Fox\Exceptions\NotAllowedSearchFieldsException;
use EllipseSynergie\ApiResponse\Contracts\Response;

use Request;
use Fox\Repositories\Query\QueryOption;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\JWTException;

use Fox\Job;

class ApiController extends Controller
{

    /**
     * Sets which fields may be searched against, and which fields are allowed to be returned in
     * partial responses.  This will be overridden in child Controllers that support searching
     * and partial responses.
     * @var array
     */
    protected $allowedFields = array(
        'search' => array()
    );

    /**
     * If query string contains 'q' parameter.
     * This indicates the request is searching an entity
     * @var boolean
     */
    protected $isSearch = false;

    /**
     * Set when there is a 'limit' query parameter
     * @var integer
     */
    protected $limit = null;

    /**
     * Set when there is an 'offset' query parameter
     * @var integer
     */
    protected $offset = null;

    /**
     * Array of fields requested to be searched against
     * @var array
     */
    protected $searchFields = array();

    /**
     * @param Response $response
     */
    public function __construct(Response $response)
    {
        $this->response = $response;
        $this->getCurrentUser2();
        $this->parseRequest();
        //$this->loadAndAuthorizeResource();
    }

    /**
     * Parses out the search parameters from a request.
     * Unparsed, they will look like this:
     *    (name:Benjamin Framklin,location:Philadelphia)
     * Parsed:
     *     array('name'=>'Benjamin Franklin', 'location'=>'Philadelphia')
     * @param  string $unparsed Unparsed search string
     * @return array            An array of fieldname=>value search parameters
     */
    protected function parseSearchParameters($unparsed)
    {
        // Strip parens that come with the request string
        $unparsed = trim($unparsed, '()');

        // Now we have an array of "key:value" strings.
        $splitFields = preg_split("/\s*,\s*/", $unparsed, -1, PREG_SPLIT_NO_EMPTY);
        $mapped = array();

        // Split the strings at their colon, set left to key, and right to value.
        foreach ($splitFields as $field) {
            $splitField = explode(':', $field);
            $mapped[$splitField[0]] = $splitField[1];
        }
        return $mapped;
    }

    /**
     * Main method for parsing a query string.
     * Finds search paramters, partial response fields, limits, and offsets.
     * Sets Controller fields for these variables.
     *
     * @return bool              Always true if no exception is thrown
     *
     * @throws HTTPException
     */
    protected function parseRequest()
    {
        $searchParams = Request::input('q', null);

        // If there's a 'q' parameter, parse the fields, then determine that all the fields in the search
        // are allowed to be searched from $allowedFields['search']
        if ($searchParams) {
            $this->isSearch = true;
            $this->searchFields = $this->parseSearchParameters($searchParams);

            // This handly snippet determines if searchFields is a strict subset of allowedFields['search']
            if (array_diff(array_keys($this->searchFields), $this->allowedFields['search'])) {
                throw new NotAllowedSearchFieldsException();
            }
        }

        return true;
    }


    /**
     * Check if the request has a search query
     *
     * @return bool
     */
    public function isSearch()
    {
        return Request::has('q');
    }

    /**
     * Get the search query
     *
     * @throws \Exception
     * @return string
     */
    public function getSearchQuery()
    {
        if (!$this->isSearch()) {
            $this->response->errorMethodNotAllowed('Cannot get search query for non search request');
        }

        return Request::input('q');
    }

    public function hasPage()
    {
        return Request::has('page');
    }

    public function getPage()
    {
        return (int)Request::input('page');
    }

    public function hasPageSize()
    {
        return Request::has('pageSize');
    }

    public function getPageSize()
    {
        return (int)Request::input('pageSize');
    }

    public function hasLimit()
    {
        return Request::has('limit');
    }

    public function getLimit()
    {
        return (int)Request::input('limit');
    }

    public function hasOffset()
    {
        return Request::has('offset');
    }

    public function getOffset()
    {
        return (int)Request::input('offset');
    }

    public function getSortOrder()
    {
        $possible = [
            'ASC',
            'DESC'
        ];

        $sortOrder = strtoupper(Request::input('sortOrder'));

        if ($sortOrder && !in_array($sortOrder, $possible)) {
            throw new \Exception("Invalid sort order " . $sortOrder);
        }

        return $sortOrder;
    }

    public function hasSortOrder()
    {
        return Request::has('sortOrder');
    }

    public function getSortBy()
    {
        return Request::input('sortBy');
    }

    public function hasSortBy()
    {
        return Request::has('sortBy');
    }

    /**
     * Createas a QueryOptions object from the request
     * @return QueryOption
     */
    protected function createQueryOptionsFromRequest()
    {

        $options = QueryOption::create();

        // Get only certain IDs
        if (Request::has('ids')) {
            // Filter input
            $ids = Request::input('ids');
            if (is_array($ids) && count($ids)) {
                // Get where
                $options->filterByIds($ids);
            }
        }

        // Limit the query
        if ($this->hasLimit()) {
            $options->setLimit($this->getLimit());
        }

        // Offset the query
        if ($this->hasOffset()) {
            $options->offset = $this->getOffset();
        }

        // Sort the query
        if ($this->hasSortBy()) {
            $options->sortBy = $this->getSortBy();
            $options->sortOrder = $this->getSortOrder();
        }

        // Page
        if ($this->hasPage()) {
            $options->page = $this->getPage();
        }
        if ($this->hasPageSize()) {
            $options->pageSize = $this->getPageSize();
        }
        return $options;
    }

    protected function getCurrentUser2()
    {        
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        $GLOBALS['user'] = $user;

        return $user;
    }
    
    protected function companyHasJob($company_id, $job_id)
    {
        $job_count = Job::where('company_id', $company_id)->where('id', $job_id)->count();
        return $job_count > 0 ? true : false;
    }
    
    protected function isCompanyGroup()
    {
        global $user;
        
        return ($user->hasRole('company') || $user->hasRole('recruiter') || $user->hasRole('employee'));
    }
    
    static public function isCompany()
    {
        global $user;
        
        return $user->role == 3 ? true : false;
    }
    
    static public function isAdmin()
    {
        global $user;
        
        return $user->role == 10 ? true : false;
    }
}