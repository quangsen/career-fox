<?php

namespace Fox\Http\Controllers;

use Fox\Jobs\SendPhoneScreenInvitationMail;
use Fox\PhoneScreenInvitationResult;
use Fox\Transformer\PhoneScreenInvitationResultTransformer;
use Fox\Transformer\PhoneScreenTransformer;
use Fox\Transformer\VideoInterviewTransformer;

use EllipseSynergie\ApiResponse\Contracts\Response;
use Fox\Repositories\PhoneScreen\PhoneScreenRepository;

use Input;
use Validator;
use Mail;

use Fox\Candidate;
use Fox\PhoneScreen;
use DB;

class PhoneScreenController extends ApiController
{

    protected $allowedFields = [
        'search' => ['keyword', 'job_id']
    ];

    protected $repository;

    public function __construct(Response $response, PhoneScreenRepository $repository)
    {
        $this->loadAndAuthorizeResource();
        
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * Display the detail candidate in phonescreen list.
     *
     * @param  int $id
     * @return Response
     */
    public function show($jobId, $id)
    {
        global $user;

        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorNotFound('Could not found the given Object');
        }

        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }

        return $this->response->withItem($entity, new PhoneScreenTransformer());
    }

    /**
     * Add a note for candidate in phonescreen
     *
     * @param  int $id , text $quick_note
     * @return Response
     */
    public function update($id)
    {
        global $user;

        $entity = PhoneScreen::find($id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Object Id could not be found');
        }

        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }

        $post = Input::all();
        if ($entity = $this->repository->update($entity, $post)) {
            return $this->response->withItem($entity, new PhoneScreenTransformer());
        }

        $errors = $this->repository->errors();
        $msg = 'Input data was wrong';

        if ($errors) {
            $msg = $msg . ' [' . $errors->first() . ']';
        }

        return $this->response->errorWrongArgs($msg);
    }


    /**
     * add note for each candidate in phone screen.
     *
     * @param  int $candidate_id , text quick_note
     * @return Response
     */
    public function addNoteCandidate($job_id, $candidate_id)
    {
        $quick_note = Input::get('note');
        $validator = Validator::make([
            'candidate_id' => $candidate_id,
            'job_id' => $job_id,
            'quick_note' => $quick_note,
        ], [
            'job_id' => 'required|integer|exists:jobs,id',
            'candidate_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            $msg = 'Input data was wrong';
            $msg = $msg . ' [' . $validator->errors()->first() . ']';
            return $this->response->errorWrongArgs($msg);
        }

        if (PhoneScreen::where('candidate_id', $candidate_id)->count() <= 0) {
            $msg = 'candidate was not invited';
            return $this->response->errorWrongArgs($msg);
        } else {
            $invitedCandidate = PhoneScreen::where('candidate_id', $candidate_id)->first();
            $invitedCandidate->quick_note = $quick_note;
            $invitedCandidate->save();
            return $this->response->withArray(['candidate_id' => $candidate_id, 'message' => 'Add note for candidate successfully']);
        }
    }

    /**
     * invite multiple candidates to phone screen
     *
     * @param  array $candidate_ids , int $job_id
     * @return Response
     */
    public function inviteToPhoneScreen($job_id)
    {
        global $user;

        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }

        $candidate_ids = Input::get('candidate_ids');
        $validator = Validator::make(['job_id' => $job_id, 'candidate_ids' => $candidate_ids], [
            'job_id' => 'required|integer|exists:jobs,id',
            'candidate_ids' => 'required|array'
        ]);

        if ($validator->fails()) {
            $msg = 'Input data was wrong';
            $msg = $msg . ' [' . $validator->errors()->first() . ']';
            return $this->response->errorWrongArgs($msg);
        }

        DB::beginTransaction();

        foreach ($candidate_ids AS $candidate_id) {
            $candidate_id = (int)$candidate_id;

            if (!$candidate_id || Candidate::isCandidateInJob($job_id, $candidate_id) <= 0) {
                DB::rollback();
                return $this->response->errorWrongArgs('Input data was wrong [Candidate list is invalid]');
            }

            $item_count = PhoneScreen::where('candidate_id', $candidate_id)
                ->where('job_id', $job_id)
                ->get();

            // check if candidate is invited or not, if not we invite
            if (count($item_count) > 0) {
                DB::rollback();
                return $this->response->errorWrongArgs('Input data was wrong [Candidate list is invalid, a few candidates are already be invited]');
            } else {
                PhoneScreen::create(['job_id' => $job_id, 'candidate_id' => $candidate_id, 'user_id' => $user->id]);
            }
        }

        $obj = new PhoneScreenInvitationResult();
        $obj->invited_candidates = count($candidate_ids);
        $obj->candidate_ids = $candidate_ids;
        $obj->message = 'candidates are invited successfully';
        DB::commit();

        // Dispatch Jobs
        $this->dispatch(new SendPhoneScreenInvitationMail($candidate_ids));

        return $this->response->withItem($obj, new PhoneScreenInvitationResultTransformer());
    }

    /**
     * Invite a candidate to video interview
     *
     * @param int $phone_screen_id
     */
    public function invite($phone_screen_id)
    {
        global $user;

        $entity = $this->repository->find($phone_screen_id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Phone Screen Id could not be found');
        }

        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }

        $video_interview = $this->repository->invite($phone_screen_id, $entity);

        if ($video_interview) {
            return $this->response->withItem($video_interview, new VideoInterviewTransformer);
        } else {
            return $this->response->errorUnwillingToProcess('This candidate is already invited to video interview. He can not be invited again');
        }
    }

    /**
     * share a candidate to all employees or only selected candidates in the company
     *
     * @params: (int) $phone_screen_id, (array) $employee_ids
     */
    public function share($phone_screen_id)
    {
        global $user;

        $entity = $this->repository->find($phone_screen_id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Phone Screen Id could not be found');
        }

        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }

        $post = Input::all();
        $is_shared = $this->repository->share($phone_screen_id, $entity, $post);

        $errors = $this->repository->errors();

        if ($errors) {
            if (is_object($errors)) {
                $msg = 'Input data was wrong';
                $msg = $msg . ' [' . $errors->first() . ']';
            } else {
                $msg = $errors;
            }

            return $this->response->errorWrongArgs($msg);
        }

        if ($is_shared) {
            $entity->message = 'The candidate is shared successfully';
        } else {
            $entity->message = 'The candidate is already shared, so it can not be shared again';
        }

        return $this->response->withItem($entity, new PhoneScreenTransformer());
    }

    /**
     * get list of candidates that are invited to phone screen
     *
     * @param: int $job_id
     */
    public function getCandidatesInPhoneScreen($job_id)
    {
        global $user;

        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        $searchOptions = $this->createQueryOptionsFromRequest();
        $items = $this->repository->all(['job_id' => $job_id], $searchOptions);
        return $this->response->withPaginator($items, new PhoneScreenTransformer());
    }

}
