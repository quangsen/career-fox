<?php

namespace Fox\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

//use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\JWTException;

use Efficiently\AuthorityController\ControllerAdditions as AuthorityControllerAdditions;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;
    use AuthorityControllerAdditions;

    public function __construct()
    {
        //$this->loadAndAuthorizeResource();
        //$this->middleware('jwt.auth');
        //$this->getCurrentUser();
    }
}
