<?php namespace Fox\Http\Controllers;

use Fox\Http\Requests;
use Fox\Transformer\UserTransformer;
use Input;
use JWTAuth;

class AuthController extends ApiController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function login()
    {
        $credentials = Input::only('email', 'password');
        if (!$token = JWTAuth::attempt($credentials)) {
            return $this->response->errorUnauthorized('Not authorized');
        }
        return $this->response->withArray(compact('token'));
    }

    /**
     * Display current logged in user
     * @return mixed
     */
    public function me()
    {
        $user = JWTAuth::parseToken()->toUser();
        return $this->response->withItem($user, new UserTransformer());
    }
}
