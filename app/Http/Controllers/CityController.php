<?php

namespace Fox\Http\Controllers;

use Fox\Transformer\CityTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Fox\Repositories\City\CityRepository;
use Input;


class CityController extends ApiController
{

    protected $allowedFields = [
        'search' => ['keyword']
    ];

    protected $repository;

    public function __construct(Response $response, CityRepository $repository)
    {
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $searchOptions = $this->createQueryOptionsFromRequest();
        $searchFields = $this->searchFields;
        $items = $this->repository->all($searchFields, $searchOptions, ['province']);
        return $this->response->withPaginator($items, new CityTransformer());
    }

}
