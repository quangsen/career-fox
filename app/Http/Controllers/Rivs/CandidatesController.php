<?php

namespace Fox\Http\Controllers\Rivs;

use Illuminate\Http\Request;

use Fox\Http\Requests;
/*use Fox\Http\Controllers\Controller;*/
use Fox\Http\Controllers\ApiController;

use GuzzleHttp\Client;

class CandidatesController extends ApiController
{
    protected $api_key;
    protected $api_uri;
    protected $client;

    /**
     *
     * @param Client $client
     */
    public function __construct(Client $client) {
        $this->api_token = \Config::get('api.token');
        $this->api_uri = \Config::get('api.uri');
        $this->client = $client;
    }

    /**
     * Get list of candidates from RIVS.
     *
     * @return Response
     */
    public function index()
    {
        $res = $this->client->request('GET', $this->api_uri."/candidates");
        $response = $res->getBody();
        dd($response);
        return \Response::json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
