<?php

namespace Fox\Http\Controllers;

use Fox\Exceptions\JobNotFoundException;
use Fox\Exceptions\SurveyNotFoundException;
use Fox\Repositories\Job\JobRepository;
use Fox\Repositories\Survey\SurveyQuestionRepository;
use Fox\Repositories\Survey\SurveyRepository;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Fox\Transformer\Survey\SurveyQuestionTransformer;
use Fox\Transformer\Survey\SurveyTransformer;
use Input;


class SurveyQuestionController extends ApiController
{

    protected $allowedFields = [
        'search' => ['keyword', 'survey_id']
    ];

    protected $repository;
    protected $surveyRepository;

    public function __construct(Response $response,
                                SurveyQuestionRepository $repository,
                                SurveyRepository $surveyRepository
    )
    {
        parent::__construct($response);
        $this->repository = $repository;
        $this->surveyRepository = $surveyRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($surveyId)
    {
        $searchOptions = $this->createQueryOptionsFromRequest();
        $searchFields = $this->searchFields;
        $searchFields['survey_id'] = (int)$surveyId;

        $items = $this->repository->all($searchFields, $searchOptions);
        return $this->response->withPaginator($items, new SurveyQuestionTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($surveyId, $id)
    {
        $entity = $this->repository->findForSurvey($surveyId, $id);
        if (!$entity) {
            return $this->response->errorNotFound('Question could not found');
        }
        return $this->response->withItem($entity, new SurveyQuestionTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($surveyId)
    {
        try {
            $survey = $this->surveyRepository->find($surveyId);
            if (!$survey) {
                throw new SurveyNotFoundException();
            }

            $post = [];
            $post['title'] = Input::get('title');
            $post['survey_id'] = $surveyId;

            if ($model = $this->repository->create($post)) {
                return $this->response->withItem($model, new SurveyQuestionTransformer());
            }
            $errors = $this->repository->errors();
            $msg = 'Input data was wrong';
            if ($errors) {
                $msg = $msg . ' [' . $errors->first() . ']';
            }
            return $this->response->errorWrongArgs($msg);
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($surveyId, $id)
    {
        $entity = $this->repository->findForSurvey($surveyId, $id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Question could not be found');
        }

        $post = [];
        $post['title'] = Input::get('title');
        if ($entity = $this->repository->update($entity, $post)) {
            return $this->response->withItem($entity, new SurveyQuestionTransformer());
        }
        $errors = $this->repository->errors();
        $msg = 'Input data was wrong';
        if ($errors) {
            $msg = $msg . ' [' . $errors->first() . ']';
        }
        return $this->response->errorWrongArgs($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($surveyId, $id)
    {
        $entity = $this->repository->findForSurvey($surveyId, $id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Question could not be found');
        }
        $this->repository->delete($id);
        return $this->response->withArray(['id' => $id]);
    }
}
