<?php

namespace Fox\Http\Controllers;

use Fox\Transformer\CandidateQuestionnaireTransformer;
use Fox\Transformer\QuestionnaireTransformer;
use Fox\Transformer\InterviewTransformer;

use EllipseSynergie\ApiResponse\Contracts\Response;
use Fox\Repositories\Questionnaire\QuestionnaireRepository;
use Input;


class QuestionnaireController extends ApiController
{

    protected $allowedFields = [
        'search' => ['keyword']
    ];

    protected $repository;

    public function __construct(Response $response, QuestionnaireRepository $repository)
    {
        $this->loadAndAuthorizeResource();
        
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        global $user;
        
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorNotFound('Could not found the given Object');
        }
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        return $this->response->withItem($entity, new QuestionnaireTransformer());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Object Id could not be found');
        }
        $post = Input::all();
        if ($entity = $this->repository->update($entity, $post)) {
            return $this->response->withItem($entity, new QuestionnaireTransformer());
        }
        $errors = $this->repository->errors();
        $msg = 'Input data was wrong';
        if ($errors) {
            $msg = $msg . ' [' . $errors->first() . ']';
        }
        return $this->response->errorWrongArgs($msg);
    }

    
    /**
     * get list of candidates that are invited to interview note
     */
    public function getCandidatesInQuestionnaire($job_id) {
        global $user;
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        $candidates = $this->repository->getCandidatesInQuestionnaire($job_id);
        $candidate_transformer = new CandidateQuestionnaireTransformer();
        return $this->response->withPaginator($candidates, $candidate_transformer);
    }
    
    /**
     * Invite a candidate to questionnaire
     */
    public function invite($questionnaire_id)
    {
        global $user;

        $entity = $this->repository->find($questionnaire_id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Questionnaire Id could not be found');
        }
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }

        $interview = $this->repository->invite($questionnaire_id, $entity);

        if ($interview) {
            return $this->response->withItem($interview, new InterviewTransformer());
        } else {
            return $this->response->errorUnwillingToProcess('This candidate is already invited to interview. He can not be invited again');
        }
    }
}
