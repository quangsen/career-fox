<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

class PhoneScreenInvitationResult
{
    public $invited_candidates = 0;
    public $candidate_ids = [];
}
