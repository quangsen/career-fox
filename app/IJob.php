<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/29/2015
 * Time: 10:25 PM
 */

namespace Fox;


interface IJob
{
    public function getId();

    public function getTitle();

    public function getDescription();

    public function getCity();
}