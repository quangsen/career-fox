<?php

namespace Fox\Listeners;

use Fox\Events\UserHasAppliedJob;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserHasAppliedJobHandler implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserHasAppliedJob $event
     * @return void
     */
    public function handle(UserHasAppliedJob $event)
    {
        //
    }
}
