<?php

namespace Fox\Listeners;

use Fox\Events\JobWasUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Fox\Api\MaxHire\Resource\Job;

class MaxHireUpdateJob implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  JobWasUpdated $event
     * @return void
     */
    public function handle(JobWasUpdated $event)
    {
        $resource = new Job();
        $resource->update($event->job);
    }
}
