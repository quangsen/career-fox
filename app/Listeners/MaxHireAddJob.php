<?php

namespace Fox\Listeners;

use Fox\Events\JobWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Fox\Api\MaxHire\Resource\Job;

class MaxHireAddJob implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  JobWasCreated $event
     * @return void
     */
    public function handle(JobWasCreated $event)
    {
        $job = $event->job;
        $resource = new Job();
        $result = $resource->add($job);
        if ($result->Success) {
            // @todo: Tracking ID of MaxHire for update later
            $job->maxhire_job_id = $result->Id;
            $job->save();
        }
    }
}
