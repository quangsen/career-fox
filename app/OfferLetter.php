<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

class OfferLetter extends Model
{
    protected $table = 'offer_letters';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'candidate_id', 'offer_letter', 'job_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
}
