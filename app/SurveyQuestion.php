<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestion extends Model
{
    protected $table = 'survey_questions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'survey_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

    public function questionOptions()
    {
        return $this->hasMany(SurveyQuestionOption::class);
    }
}
