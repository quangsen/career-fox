<?php

namespace Fox;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'first_name', 'last_name', 'email', 'password', 'role_id', 'company_id', 'phone'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    public function role()
    {
        return $this->belongsTo('Fox\Authority\Role');
    }

    /*public function permissions()
    {
        return $this->hasMany('Fox\Authority\Permission');
    }*/

    public function hasRole($key)
    {
        $role = $this->role;
        return $role ? $role->name == $key : false;
    }
    
    public function candidate()
    {
        return $this->hasOne('Fox\Candidate');
    }
    
    public function jobs()
    {
        return $this->belongsToMany('Fox\Job', 'candidate_jobs', 'user_id', 'job_id');
    }
    
    public function company()
    {
        return $this->belongsTo('Fox\Company');
    }
}
