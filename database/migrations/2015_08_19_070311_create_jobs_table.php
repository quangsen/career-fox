<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('city_name', 50);
            $table->string('province_name', 50);
            $table->string('country_name', 50);
            $table->string('postal', 10);
            $table->text('description');
            
            $table->tinyInteger('priority')->default(5);
            //$table->tinyInteger('pending_jobs')->default(0);
            //$table->tinyInteger('save_jobs')->default(0);
            $table->integer('views')->unsigned()->default(0);
            $table->tinyInteger('status')->default(0);
            $table->float('candidate_total_value')->unsign()->default(0);
            
            // set foreign key to companies table
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            
            // set foreign key to cities table
            $table->integer('city_id')->unsigned();
            $table->foreign('city_id')->references('id')->on('cities');
            
            // set foreign key to provinces table
            $table->integer('province_id')->unsigned();
            $table->foreign('province_id')->references('id')->on('provinces');
            
            // set foreign key to provinces table
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');
            
            // set foreign key to companies table
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');
            
            // set foreign key to users table
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jobs');
    }
}
