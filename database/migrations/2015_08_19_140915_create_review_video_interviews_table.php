<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewVideoInterviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_video_interviews', function (Blueprint $table) {
            $table->increments('id');

            $table->string('video');
            $table->string('candidate');            
			$table->string('questions_displayed');
			$table->string('invite');
			$table->string('list_of_questions');

            $table->integer('job_id')->unsigned();
            $table->foreign('job_id')->references('id')->on('jobs');
            
            // set foreign key to users table
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('review_video_interviews');
    }
}
