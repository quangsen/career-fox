<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddPlacementRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_placement_record', function (Blueprint $table) {
            $table->increments('id');
            $table->string('candidate', 50);
            $table->string('position', 50);
            $table->integer('wage')->unsigned();
            $table->dateTime('start_date');
            $table->dateTime('vacation_time');
            $table->string('phone_number', 15);
            $table->string('company_email')->unique();
            $table->integer('report_to')->unsigned();
            $table->dateTime('benefit_date');
            $table->text('comment');
            
            // set foreign key to jobs table
            $table->integer('job_id')->unsigned();
            $table->foreign('job_id')->references('id')->on('jobs');
            
            // set foreign key to users table
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('add_placement_record');
    }
}
