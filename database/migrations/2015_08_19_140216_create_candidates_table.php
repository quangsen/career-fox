<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 100)->unique();
            $table->string('name', 50);
            $table->string('photo');
            $table->string('job_title', 100);
            $table->integer('views')->unsigned()->default(0);
            
            $table->string('resume', 255);
            $table->string('cover_letter', 255);
            
            // set foreign key to groups table
            //$table->integer('group_id')->unsigned();
            //$table->foreign('user_id')->references('id')->on('users');
            
            // set foreign key to companies table
            //$table->integer('company_id')->unsigned();
            //$table->foreign('company_id')->references('id')->on('companies');
            
            // set foreign key to companies table
            //$table->integer('job_id')->unsigned();
            //$table->foreign('job_id')->references('id')->on('jobs');
            
            // set foreign key to users table
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('candidates');
    }
}
