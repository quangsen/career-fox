<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotes extends Migration
{
    public function up()
    {
        Schema::create('interview_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('filename', 100);

            $table->integer('interview_id')->unsigned();
            $table->foreign('interview_id')->references('id')->on('interviews');
            
            // set foreign key to users table
            $table->integer('user_id')->unsigned();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('interview_notes');
    }
}
