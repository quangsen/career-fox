<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoInterviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_interviews', function (Blueprint $table) {
            $table->increments('id');
            $table->string('question');

            $table->text('quick_note');
            
            $table->integer('job_id')->unsigned();
            $table->foreign('job_id')->references('id')->on('jobs');
            
            $table->integer('candidate_id')->unsigned();
            $table->foreign('candidate_id')->references('id')->on('candidates');
            
            $table->integer('phone_screen_id')->unsigned();
            $table->foreign('phone_screen_id')->references('id')->on('phone_screens');
            
            $table->tinyInteger('is_invited')->unsigned()->default(0);
            
            // set foreign key to users table
            $table->integer('user_id')->unsigned();
            //$table->foreign('user_id')->references('id')->on('users');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('video_interviews');
    }
}
