<?php

use Illuminate\Database\Seeder;
use Fox\Candidate;

class CandidateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
        //DB::table('candidates')->delete();
        $faker = Faker\Factory::create();
        
        $cities = DB::table('cities')->lists('id');
        
        /*$jobs_arr1 = DB::table('jobs')->select('id', 'company_id')->where('company_id', 1)->take(10)->get();
        $jobs_arr2 = DB::table('jobs')->select('id', 'company_id')->where('company_id', 2)->take(10)->get();
        $jobs_arr = array_merge($jobs_arr1, $jobs_arr1);*/
        
        $now = time();
        $time_arr = [];
        for ($i = 0; $i < 100; $i++) {
            $time_arr[] = $now - $i * 24 * 3600;
        }
        
        $i = 0;
        while ($i < 500) {
            $city_id = $cities[array_rand($cities)];
            
            //$job = $jobs_arr[array_rand($jobs_arr)];
            $candidate = new Candidate;
            $candidate_name = 'candidate' . ($i + 1);
            $date_timestamp = $time_arr[array_rand($time_arr)];
            $date = date('Y-m-d H:i:s', $date_timestamp);
            
            $item = [
                'name' => $candidate_name,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                //'company_id' => $job->company_id,
                'resume' => 'resume.pdf',
                'cover_letter' => 'cover_letter.pdf',
                'email' => $candidate_name . '@gmail.com',
                'photo' => '',
                'username' =>  $candidate_name,
                'password' => $candidate_name,
                'address' => $faker->streetAddress,
                'city_id' => $city_id,
                'phone' => $faker->phoneNumber,
                'created_at' => $date
            ];
            
            
            /*
            $user_count = DB::table('users')
                    ->where('username', $item['username'])
                    ->orWhere('email', $item['email'])
                    ->count();
            if ($user_count) {
                continue;
            }
             */
            
            $candidate->createCandidate($item);
            $i++;
        }
    }
}
