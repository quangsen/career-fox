<?php

use Illuminate\Database\Seeder;
use Fox\Job;

class JobTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
    
        DB::table('jobs')->delete();
        $provinces = DB::select(DB::raw('select pv.id AS province_id, pv.name AS province, co.id AS country_id, co.name AS country FROM provinces AS pv INNER JOIN countries AS co ON co.id = pv.country_id'));
        
        $city_list = [];
        foreach ($provinces AS $province) {
            $cities = DB::table('cities')->select('id', 'name')->where('province_id', $province->province_id)->take(10)->get();
            
            foreach ($cities AS $k => $city) {
                $city->country_id = $province->country_id;
                $city->country = $province->country;
                $city->province_id = $province->province_id;
                $city->province = $province->province;
                $cities[$k] = $city;
            }

            $city_list = array_merge($city_list, $cities);
        }
        
        $categories = DB::table('categories')->lists('id');
        
        $faker = Faker\Factory::create();
        
        $status = [-1, 0, 1];
        
        $now = time();
        $time_arr = [];
        for ($i = 0; $i < 100; $i++) {
            $time_arr[] = $now - $i * 24 * 3600;
        }
        
        $priority_arr = [5, 10];
        for ($i = 0; $i < 1000; $i++) {
            $city = $city_list[array_rand($city_list)];

            if ($i%10 == 0) {
                $company_id = rand(1, 2);
            } else {
                $company_id = rand(1, 100);
            }
            $item['title'] = $faker->sentence(5);
            $item['city_id'] = $city->id;
            $item['province_id'] = $city->province_id;
            $item['country_id'] = $city->country_id;
            $item['city_name'] = $city->name;
            $item['province_name'] = $city->province;
            $item['country_name'] = $city->country;
            
            $item['postal'] = $faker->postcode;
            $item['description'] = $faker->text(500);
            $item['priority'] = $priority_arr[array_rand($priority_arr)];
            
            $item['status'] = $status[array_rand($status)];
            
            $item['user_id'] = rand(3, 4);
            $item['company_id'] = $company_id;
            $item['category_id'] = $categories[array_rand($categories)];
            $item['created_at'] = $time_arr[array_rand($time_arr)];
            
            Job::create($item);
        }
    }
}
