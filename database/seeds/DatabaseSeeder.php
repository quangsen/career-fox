<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        DB::table('cities')->delete();
        DB::table('provinces')->delete();
        DB::table('countries')->delete();
        DB::unprepared(file_get_contents(dirname(__FILE__). '/../../countries_provinces_cities_db.sql'));
        echo 'Done importing countries, provinces and cities' . "\n";

        $this->call(UserTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(CompanyTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(JobTableSeeder::class);
        $this->call(CandidateTableSeeder::class);
        $this->call(CandidateJobsTableSeeder::class);
        
        Model::reguard();
    }
}
